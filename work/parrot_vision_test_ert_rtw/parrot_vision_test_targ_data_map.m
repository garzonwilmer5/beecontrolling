  function targMap = targDataMap(),

  ;%***********************
  ;% Create Parameter Map *
  ;%***********************
      
    nTotData      = 0; %add to this count as we go
    nTotSects     = 15;
    sectIdxOffset = 0;
    
    ;%
    ;% Define dummy sections & preallocate arrays
    ;%
    dumSection.nData = -1;  
    dumSection.data  = [];
    
    dumData.logicalSrcIdx = -1;
    dumData.dtTransOffset = -1;
    
    ;%
    ;% Init/prealloc paramMap
    ;%
    paramMap.nSections           = nTotSects;
    paramMap.sectIdxOffset       = sectIdxOffset;
      paramMap.sections(nTotSects) = dumSection; %prealloc
    paramMap.nTotData            = -1;
    
    ;%
    ;% Auto data (nkdmff52xg)
    ;%
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% nkdmff52xg.Sensors
	  section.data(1).logicalSrcIdx = 0;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(1) = section;
      clear section
      
      section.nData     = 13;
      section.data(13)  = dumData; %prealloc
      
	  ;% nkdmff52xg.Constant_Value
	  section.data(1).logicalSrcIdx = 1;
	  section.data(1).dtTransOffset = 0;
	
	  ;% nkdmff52xg.Constant_Value_hdawg2ibui
	  section.data(2).logicalSrcIdx = 2;
	  section.data(2).dtTransOffset = 1;
	
	  ;% nkdmff52xg.Ruido_Value
	  section.data(3).logicalSrcIdx = 3;
	  section.data(3).dtTransOffset = 2;
	
	  ;% nkdmff52xg.Umbral07AR7_Value
	  section.data(4).logicalSrcIdx = 4;
	  section.data(4).dtTransOffset = 3;
	
	  ;% nkdmff52xg.Toleranciasuperior_Value
	  section.data(5).logicalSrcIdx = 5;
	  section.data(5).dtTransOffset = 4;
	
	  ;% nkdmff52xg.ToleranciaInferior_Value
	  section.data(6).logicalSrcIdx = 6;
	  section.data(6).dtTransOffset = 5;
	
	  ;% nkdmff52xg.Switch_Threshold
	  section.data(7).logicalSrcIdx = 7;
	  section.data(7).dtTransOffset = 6;
	
	  ;% nkdmff52xg.Gain_Gain
	  section.data(8).logicalSrcIdx = 8;
	  section.data(8).dtTransOffset = 7;
	
	  ;% nkdmff52xg.Switch_Threshold_eo0r5ngyeb
	  section.data(9).logicalSrcIdx = 9;
	  section.data(9).dtTransOffset = 167;
	
	  ;% nkdmff52xg.Gain_Gain_afd0b0wnq4
	  section.data(10).logicalSrcIdx = 10;
	  section.data(10).dtTransOffset = 168;
	
	  ;% nkdmff52xg.Gain_Gain_nnmdp233bg
	  section.data(11).logicalSrcIdx = 11;
	  section.data(11).dtTransOffset = 328;
	
	  ;% nkdmff52xg.Gain_Gain_k0vrcsbyfg
	  section.data(12).logicalSrcIdx = 12;
	  section.data(12).dtTransOffset = 448;
	
	  ;% nkdmff52xg.RateTransition_InitialCondition
	  section.data(13).logicalSrcIdx = 13;
	  section.data(13).dtTransOffset = 608;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(2) = section;
      clear section
      
      section.nData     = 6;
      section.data(6)  = dumData; %prealloc
      
	  ;% nkdmff52xg.pgfzy3n1r1z.outlierBelowFloor_const
	  section.data(1).logicalSrcIdx = 14;
	  section.data(1).dtTransOffset = 0;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.outlierJump_const
	  section.data(2).logicalSrcIdx = 15;
	  section.data(2).dtTransOffset = 1;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.currentEstimateVeryOffFromPress
	  section.data(3).logicalSrcIdx = 16;
	  section.data(3).dtTransOffset = 2;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.currentStateVeryOffsonarflt_con
	  section.data(4).logicalSrcIdx = 17;
	  section.data(4).dtTransOffset = 3;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.Checkerrorcondition_const
	  section.data(5).logicalSrcIdx = 18;
	  section.data(5).dtTransOffset = 4;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.u0continuousOFerrors_const
	  section.data(6).logicalSrcIdx = 19;
	  section.data(6).dtTransOffset = 5;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(3) = section;
      clear section
      
      section.nData     = 23;
      section.data(23)  = dumData; %prealloc
      
	  ;% nkdmff52xg.pgfzy3n1r1z.DiscreteDerivative_ICPrevScaled
	  section.data(1).logicalSrcIdx = 20;
	  section.data(1).dtTransOffset = 0;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.CompareToConstant_const
	  section.data(2).logicalSrcIdx = 21;
	  section.data(2).dtTransOffset = 1;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.CompareToConstant1_const
	  section.data(3).logicalSrcIdx = 22;
	  section.data(3).dtTransOffset = 2;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.maxp_const
	  section.data(4).logicalSrcIdx = 23;
	  section.data(4).dtTransOffset = 3;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.maxq_const
	  section.data(5).logicalSrcIdx = 24;
	  section.data(5).dtTransOffset = 4;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.maxw1_const
	  section.data(6).logicalSrcIdx = 25;
	  section.data(6).dtTransOffset = 5;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.maxw2_const
	  section.data(7).logicalSrcIdx = 26;
	  section.data(7).dtTransOffset = 6;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.maxdw1_const
	  section.data(8).logicalSrcIdx = 27;
	  section.data(8).dtTransOffset = 7;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.maxdw2_const
	  section.data(9).logicalSrcIdx = 28;
	  section.data(9).dtTransOffset = 8;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.maxp2_const
	  section.data(10).logicalSrcIdx = 29;
	  section.data(10).dtTransOffset = 9;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.maxq2_const
	  section.data(11).logicalSrcIdx = 30;
	  section.data(11).dtTransOffset = 10;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.maxw3_const
	  section.data(12).logicalSrcIdx = 31;
	  section.data(12).dtTransOffset = 11;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.maxw4_const
	  section.data(13).logicalSrcIdx = 32;
	  section.data(13).dtTransOffset = 12;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.minHeightforOF_const
	  section.data(14).logicalSrcIdx = 33;
	  section.data(14).dtTransOffset = 13;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.CompareToConstant_co_jv54xfq4hk
	  section.data(15).logicalSrcIdx = 34;
	  section.data(15).dtTransOffset = 14;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.CompareToConstant1_c_owbmifo2tv
	  section.data(16).logicalSrcIdx = 35;
	  section.data(16).dtTransOffset = 15;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.CompareToConstant2_const
	  section.data(17).logicalSrcIdx = 36;
	  section.data(17).dtTransOffset = 16;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.CompareToConstant3_const
	  section.data(18).logicalSrcIdx = 37;
	  section.data(18).dtTransOffset = 17;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.CompareToConstant4_const
	  section.data(19).logicalSrcIdx = 38;
	  section.data(19).dtTransOffset = 18;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.CompareToConstant5_const
	  section.data(20).logicalSrcIdx = 39;
	  section.data(20).dtTransOffset = 19;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.DeactivateAccelerationIfOFisnot
	  section.data(21).logicalSrcIdx = 40;
	  section.data(21).dtTransOffset = 20;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.donotuseaccifopticalflowneverav
	  section.data(22).logicalSrcIdx = 41;
	  section.data(22).dtTransOffset = 21;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.donotuseaccifoptical_pwrcnhfofi
	  section.data(23).logicalSrcIdx = 42;
	  section.data(23).dtTransOffset = 22;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(4) = section;
      clear section
      
      section.nData     = 3;
      section.data(3)  = dumData; %prealloc
      
	  ;% nkdmff52xg.pgfzy3n1r1z.WrapToZero_Threshold
	  section.data(1).logicalSrcIdx = 43;
	  section.data(1).dtTransOffset = 0;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.WrapToZero_Threshold_mtnfdf4o2z
	  section.data(2).logicalSrcIdx = 44;
	  section.data(2).dtTransOffset = 1;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.CompareToConstant_co_bel12wt5ko
	  section.data(3).logicalSrcIdx = 45;
	  section.data(3).dtTransOffset = 2;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(5) = section;
      clear section
      
      section.nData     = 42;
      section.data(42)  = dumData; %prealloc
      
	  ;% nkdmff52xg.pgfzy3n1r1z._Value
	  section.data(1).logicalSrcIdx = 46;
	  section.data(1).dtTransOffset = 0;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.Constant6_Value
	  section.data(2).logicalSrcIdx = 47;
	  section.data(2).dtTransOffset = 1;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.Constant5_Value
	  section.data(3).logicalSrcIdx = 48;
	  section.data(3).dtTransOffset = 2;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.Constant2_Value
	  section.data(4).logicalSrcIdx = 49;
	  section.data(4).dtTransOffset = 3;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.Constant_Value
	  section.data(5).logicalSrcIdx = 50;
	  section.data(5).dtTransOffset = 4;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.Switch_Threshold
	  section.data(6).logicalSrcIdx = 51;
	  section.data(6).dtTransOffset = 5;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.Constant3_Value
	  section.data(7).logicalSrcIdx = 52;
	  section.data(7).dtTransOffset = 6;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.Lykyhatkk1_Y0
	  section.data(8).logicalSrcIdx = 53;
	  section.data(8).dtTransOffset = 7;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.deltax_Y0
	  section.data(9).logicalSrcIdx = 54;
	  section.data(9).dtTransOffset = 8;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.Constant5_Value_cx4haj0vmu
	  section.data(10).logicalSrcIdx = 55;
	  section.data(10).dtTransOffset = 9;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.Constant_Value_oyxuoz12zb
	  section.data(11).logicalSrcIdx = 56;
	  section.data(11).dtTransOffset = 10;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.KalmanGainM_Value
	  section.data(12).logicalSrcIdx = 57;
	  section.data(12).dtTransOffset = 11;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.C_Value
	  section.data(13).logicalSrcIdx = 58;
	  section.data(13).dtTransOffset = 13;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.Delay2_InitialCondition
	  section.data(14).logicalSrcIdx = 59;
	  section.data(14).dtTransOffset = 15;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.X0_Value
	  section.data(15).logicalSrcIdx = 60;
	  section.data(15).dtTransOffset = 16;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.SaturationSonar_LowerSat
	  section.data(16).logicalSrcIdx = 61;
	  section.data(16).dtTransOffset = 18;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.soonarFilter_IIR_NumCoef
	  section.data(17).logicalSrcIdx = 62;
	  section.data(17).dtTransOffset = 19;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.soonarFilter_IIR_DenCoef
	  section.data(18).logicalSrcIdx = 63;
	  section.data(18).dtTransOffset = 23;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.soonarFilter_IIR_InitialStates
	  section.data(19).logicalSrcIdx = 64;
	  section.data(19).dtTransOffset = 27;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.KalmanGainM_Value_nonntarswh
	  section.data(20).logicalSrcIdx = 65;
	  section.data(20).dtTransOffset = 28;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.Wait3Seconds_Value
	  section.data(21).logicalSrcIdx = 66;
	  section.data(21).dtTransOffset = 32;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.DelayOneStep_InitialCondition
	  section.data(22).logicalSrcIdx = 67;
	  section.data(22).dtTransOffset = 33;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.u5meters_Value
	  section.data(23).logicalSrcIdx = 68;
	  section.data(23).dtTransOffset = 34;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.PitchExploracion_Value
	  section.data(24).logicalSrcIdx = 69;
	  section.data(24).dtTransOffset = 35;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.PitchAleteo_Value
	  section.data(25).logicalSrcIdx = 70;
	  section.data(25).dtTransOffset = 36;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.Constant4_Value
	  section.data(26).logicalSrcIdx = 71;
	  section.data(26).dtTransOffset = 37;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.Delay_InitialCondition
	  section.data(27).logicalSrcIdx = 72;
	  section.data(27).dtTransOffset = 38;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.Gain_Gain
	  section.data(28).logicalSrcIdx = 73;
	  section.data(28).dtTransOffset = 39;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.Gain1_Gain
	  section.data(29).logicalSrcIdx = 74;
	  section.data(29).dtTransOffset = 40;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.Switch2_Threshold
	  section.data(30).logicalSrcIdx = 75;
	  section.data(30).dtTransOffset = 41;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.TiempodeFrenado_Value
	  section.data(31).logicalSrcIdx = 76;
	  section.data(31).dtTransOffset = 42;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.TiempodeCalibracion_Value
	  section.data(32).logicalSrcIdx = 77;
	  section.data(32).dtTransOffset = 43;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.Constant1_Value
	  section.data(33).logicalSrcIdx = 78;
	  section.data(33).dtTransOffset = 44;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.Switch1_Threshold
	  section.data(34).logicalSrcIdx = 79;
	  section.data(34).dtTransOffset = 45;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.A_Value
	  section.data(35).logicalSrcIdx = 80;
	  section.data(35).dtTransOffset = 46;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.B_Value
	  section.data(36).logicalSrcIdx = 81;
	  section.data(36).dtTransOffset = 50;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.KalmanGainL_Value
	  section.data(37).logicalSrcIdx = 82;
	  section.data(37).dtTransOffset = 52;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.D_Value
	  section.data(38).logicalSrcIdx = 83;
	  section.data(38).dtTransOffset = 54;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.gravity_Value
	  section.data(39).logicalSrcIdx = 84;
	  section.data(39).dtTransOffset = 55;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.gravity_Value_hcwwgfk3pi
	  section.data(40).logicalSrcIdx = 85;
	  section.data(40).dtTransOffset = 58;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.gainaccinput_Gain
	  section.data(41).logicalSrcIdx = 86;
	  section.data(41).dtTransOffset = 61;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.KalmanGainL_Value_ej2qhm05wt
	  section.data(42).logicalSrcIdx = 87;
	  section.data(42).dtTransOffset = 62;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(6) = section;
      clear section
      
      section.nData     = 47;
      section.data(47)  = dumData; %prealloc
      
	  ;% nkdmff52xg.pgfzy3n1r1z._Value_edondrnz5n
	  section.data(1).logicalSrcIdx = 88;
	  section.data(1).dtTransOffset = 0;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.Out1_Y0
	  section.data(2).logicalSrcIdx = 89;
	  section.data(2).dtTransOffset = 1;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.Gain_Gain_foebj1ktmb
	  section.data(3).logicalSrcIdx = 90;
	  section.data(3).dtTransOffset = 2;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.Gain_Gain_hf1mawwijk
	  section.data(4).logicalSrcIdx = 91;
	  section.data(4).dtTransOffset = 3;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.Gain1_Gain_eos0skjx3x
	  section.data(5).logicalSrcIdx = 92;
	  section.data(5).dtTransOffset = 4;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.Gain2_Gain
	  section.data(6).logicalSrcIdx = 93;
	  section.data(6).dtTransOffset = 5;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.Gain3_Gain
	  section.data(7).logicalSrcIdx = 94;
	  section.data(7).dtTransOffset = 6;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.Gain4_Gain
	  section.data(8).logicalSrcIdx = 95;
	  section.data(8).dtTransOffset = 7;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.Gain_Gain_jxqemkiive
	  section.data(9).logicalSrcIdx = 96;
	  section.data(9).dtTransOffset = 8;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.opticalFlowErrorCorrect_Gain
	  section.data(10).logicalSrcIdx = 97;
	  section.data(10).dtTransOffset = 9;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.Lykyhatkk1_Y0_hduw5vett2
	  section.data(11).logicalSrcIdx = 98;
	  section.data(11).dtTransOffset = 10;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.deltax_Y0_pdt1bicqlf
	  section.data(12).logicalSrcIdx = 99;
	  section.data(12).dtTransOffset = 11;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.SimplyIntegrateVelocity_gainval
	  section.data(13).logicalSrcIdx = 100;
	  section.data(13).dtTransOffset = 12;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.SimplyIntegrateVelocity_IC
	  section.data(14).logicalSrcIdx = 101;
	  section.data(14).dtTransOffset = 13;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.invertzaxisGain_Gain
	  section.data(15).logicalSrcIdx = 102;
	  section.data(15).dtTransOffset = 14;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.prsToAltGain_Gain
	  section.data(16).logicalSrcIdx = 103;
	  section.data(16).dtTransOffset = 15;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.pressureFilter_IIR_NumCoef
	  section.data(17).logicalSrcIdx = 104;
	  section.data(17).dtTransOffset = 16;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.pressureFilter_IIR_DenCoef
	  section.data(18).logicalSrcIdx = 105;
	  section.data(18).dtTransOffset = 20;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.pressureFilter_IIR_InitialState
	  section.data(19).logicalSrcIdx = 106;
	  section.data(19).dtTransOffset = 24;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.Memory_InitialCondition
	  section.data(20).logicalSrcIdx = 107;
	  section.data(20).dtTransOffset = 25;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.Constant_Value_n3ipl20d2q
	  section.data(21).logicalSrcIdx = 108;
	  section.data(21).dtTransOffset = 28;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.Gain_Gain_du3h0hghjr
	  section.data(22).logicalSrcIdx = 109;
	  section.data(22).dtTransOffset = 29;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.Assumingthatcalibwasdonelevel_B
	  section.data(23).logicalSrcIdx = 110;
	  section.data(23).dtTransOffset = 30;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.inverseIMU_gain_Gain
	  section.data(24).logicalSrcIdx = 111;
	  section.data(24).dtTransOffset = 36;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.IIR_IMUgyro_r_NumCoef
	  section.data(25).logicalSrcIdx = 112;
	  section.data(25).dtTransOffset = 42;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.IIR_IMUgyro_r_DenCoef
	  section.data(26).logicalSrcIdx = 113;
	  section.data(26).dtTransOffset = 48;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.IIR_IMUgyro_r_InitialStates
	  section.data(27).logicalSrcIdx = 114;
	  section.data(27).dtTransOffset = 54;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.Gain_Gain_areo4q4eg1
	  section.data(28).logicalSrcIdx = 115;
	  section.data(28).dtTransOffset = 55;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.FIR_IMUaccel_InitialStates
	  section.data(29).logicalSrcIdx = 116;
	  section.data(29).dtTransOffset = 56;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.FIR_IMUaccel_Coefficients
	  section.data(30).logicalSrcIdx = 117;
	  section.data(30).dtTransOffset = 57;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.Constant_Value_nxpvzjeigy
	  section.data(31).logicalSrcIdx = 118;
	  section.data(31).dtTransOffset = 63;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.Merge_InitialOutput
	  section.data(32).logicalSrcIdx = 119;
	  section.data(32).dtTransOffset = 64;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.C_Value_m4atldjp5j
	  section.data(33).logicalSrcIdx = 120;
	  section.data(33).dtTransOffset = 65;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.X0_Value_aqycbtybjo
	  section.data(34).logicalSrcIdx = 121;
	  section.data(34).dtTransOffset = 69;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.IIRgyroz_NumCoef
	  section.data(35).logicalSrcIdx = 122;
	  section.data(35).dtTransOffset = 71;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.IIRgyroz_DenCoef
	  section.data(36).logicalSrcIdx = 123;
	  section.data(36).dtTransOffset = 77;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.IIRgyroz_InitialStates
	  section.data(37).logicalSrcIdx = 124;
	  section.data(37).dtTransOffset = 83;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.TSamp_WtEt
	  section.data(38).logicalSrcIdx = 125;
	  section.data(38).dtTransOffset = 84;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.Delay_InitialConditi_gs3s4wjgbq
	  section.data(39).logicalSrcIdx = 126;
	  section.data(39).dtTransOffset = 85;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.Delay1_InitialCondition
	  section.data(40).logicalSrcIdx = 127;
	  section.data(40).dtTransOffset = 86;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.Gain_Gain_g2zser4ghz
	  section.data(41).logicalSrcIdx = 128;
	  section.data(41).dtTransOffset = 87;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.Gain_Gain_hivrheh0pv
	  section.data(42).logicalSrcIdx = 129;
	  section.data(42).dtTransOffset = 88;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.Gain1_Gain_lf2r1uis51
	  section.data(43).logicalSrcIdx = 130;
	  section.data(43).dtTransOffset = 89;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.AlturadeVuelo_Value
	  section.data(44).logicalSrcIdx = 131;
	  section.data(44).dtTransOffset = 90;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.A_Value_oyp5eortrt
	  section.data(45).logicalSrcIdx = 132;
	  section.data(45).dtTransOffset = 91;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.B_Value_amai3l3o1s
	  section.data(46).logicalSrcIdx = 133;
	  section.data(46).dtTransOffset = 95;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.D_Value_n5jithwxnk
	  section.data(47).logicalSrcIdx = 134;
	  section.data(47).dtTransOffset = 99;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(7) = section;
      clear section
      
      section.nData     = 13;
      section.data(13)  = dumData; %prealloc
      
	  ;% nkdmff52xg.pgfzy3n1r1z.Constant_Value_jqncxricp5
	  section.data(1).logicalSrcIdx = 135;
	  section.data(1).dtTransOffset = 0;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.Constant_Value_gfbbx4gdzu
	  section.data(2).logicalSrcIdx = 136;
	  section.data(2).dtTransOffset = 1;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.Delay2_DelayLength
	  section.data(3).logicalSrcIdx = 137;
	  section.data(3).dtTransOffset = 2;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.MemoryX_DelayLength
	  section.data(4).logicalSrcIdx = 138;
	  section.data(4).dtTransOffset = 3;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.Output_InitialCondition
	  section.data(5).logicalSrcIdx = 139;
	  section.data(5).dtTransOffset = 4;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.MemoryX_DelayLength_bqtld0ygjy
	  section.data(6).logicalSrcIdx = 140;
	  section.data(6).dtTransOffset = 5;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.Delay_DelayLength
	  section.data(7).logicalSrcIdx = 141;
	  section.data(7).dtTransOffset = 6;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.Delay1_DelayLength
	  section.data(8).logicalSrcIdx = 142;
	  section.data(8).dtTransOffset = 7;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.Output_InitialCondit_kp42slhnsu
	  section.data(9).logicalSrcIdx = 143;
	  section.data(9).dtTransOffset = 8;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.DelayOneStep_DelayLength
	  section.data(10).logicalSrcIdx = 144;
	  section.data(10).dtTransOffset = 9;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.FixPtConstant_Value
	  section.data(11).logicalSrcIdx = 145;
	  section.data(11).dtTransOffset = 10;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.Delay_DelayLength_gorcxbtfib
	  section.data(12).logicalSrcIdx = 146;
	  section.data(12).dtTransOffset = 11;
	
	  ;% nkdmff52xg.pgfzy3n1r1z.FixPtConstant_Value_gtodp00wtx
	  section.data(13).logicalSrcIdx = 147;
	  section.data(13).dtTransOffset = 12;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(8) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% nkdmff52xg.pgfzy3n1r1z.controlModePosVsOrient_Value
	  section.data(1).logicalSrcIdx = 148;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(9) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% nkdmff52xg.pgfzy3n1r1z.Merge_InitialOutput_nbmeijphuh
	  section.data(1).logicalSrcIdx = 149;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(10) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% nkdmff52xg.pgfzy3n1r1z.li14jlnqdt.Constant_Value
	  section.data(1).logicalSrcIdx = 150;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(11) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% nkdmff52xg.pgfzy3n1r1z.g3fmi2n5mt.Constant_Value
	  section.data(1).logicalSrcIdx = 151;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(12) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% nkdmff52xg.pgfzy3n1r1z.mn3pxsxeas.Constant_Value
	  section.data(1).logicalSrcIdx = 152;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(13) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% nkdmff52xg.pgfzy3n1r1z.ltq03ey2sd.Constant_Value
	  section.data(1).logicalSrcIdx = 153;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(14) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% nkdmff52xg.pgfzy3n1r1z.dzqztyjg5fv.Constant_Value
	  section.data(1).logicalSrcIdx = 154;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(15) = section;
      clear section
      
    
      ;%
      ;% Non-auto Data (parameter)
      ;%
    

    ;%
    ;% Add final counts to struct.
    ;%
    paramMap.nTotData = nTotData;
    


  ;%**************************
  ;% Create Block Output Map *
  ;%**************************
      
    nTotData      = 0; %add to this count as we go
    nTotSects     = 9;
    sectIdxOffset = 0;
    
    ;%
    ;% Define dummy sections & preallocate arrays
    ;%
    dumSection.nData = -1;  
    dumSection.data  = [];
    
    dumData.logicalSrcIdx = -1;
    dumData.dtTransOffset = -1;
    
    ;%
    ;% Init/prealloc sigMap
    ;%
    sigMap.nSections           = nTotSects;
    sigMap.sectIdxOffset       = sectIdxOffset;
      sigMap.sections(nTotSects) = dumSection; %prealloc
    sigMap.nTotData            = -1;
    
    ;%
    ;% Auto data (cqikrl4hwwb)
    ;%
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% cqikrl4hwwb.k3arj51fv3
	  section.data(1).logicalSrcIdx = 0;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      sigMap.sections(1) = section;
      clear section
      
      section.nData     = 6;
      section.data(6)  = dumData; %prealloc
      
	  ;% cqikrl4hwwb.jr3sxy225h
	  section.data(1).logicalSrcIdx = 1;
	  section.data(1).dtTransOffset = 0;
	
	  ;% cqikrl4hwwb.gc4d31ewth
	  section.data(2).logicalSrcIdx = 2;
	  section.data(2).dtTransOffset = 9760;
	
	  ;% cqikrl4hwwb.imrx2dzkee
	  section.data(3).logicalSrcIdx = 3;
	  section.data(3).dtTransOffset = 14560;
	
	  ;% cqikrl4hwwb.nsa45urllk
	  section.data(4).logicalSrcIdx = 4;
	  section.data(4).dtTransOffset = 19520;
	
	  ;% cqikrl4hwwb.lrys2sb5xl
	  section.data(5).logicalSrcIdx = 5;
	  section.data(5).dtTransOffset = 29120;
	
	  ;% cqikrl4hwwb.gkvxtsua0k
	  section.data(6).logicalSrcIdx = 6;
	  section.data(6).dtTransOffset = 34080;
	
      nTotData = nTotData + section.nData;
      sigMap.sections(2) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% cqikrl4hwwb.pgfzy3n1r1z.h44yrsqqar
	  section.data(1).logicalSrcIdx = 8;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      sigMap.sections(3) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% cqikrl4hwwb.pgfzy3n1r1z.cbw2dxubli
	  section.data(1).logicalSrcIdx = 22;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      sigMap.sections(4) = section;
      clear section
      
      section.nData     = 8;
      section.data(8)  = dumData; %prealloc
      
	  ;% cqikrl4hwwb.pgfzy3n1r1z.himr34cerp
	  section.data(1).logicalSrcIdx = 9;
	  section.data(1).dtTransOffset = 0;
	
	  ;% cqikrl4hwwb.pgfzy3n1r1z.bywgvaz0b1
	  section.data(2).logicalSrcIdx = 10;
	  section.data(2).dtTransOffset = 1;
	
	  ;% cqikrl4hwwb.pgfzy3n1r1z.femejkk35d
	  section.data(3).logicalSrcIdx = 11;
	  section.data(3).dtTransOffset = 2;
	
	  ;% cqikrl4hwwb.pgfzy3n1r1z.bmw5t5gawz
	  section.data(4).logicalSrcIdx = 12;
	  section.data(4).dtTransOffset = 3;
	
	  ;% cqikrl4hwwb.pgfzy3n1r1z.b0ksqu2fit
	  section.data(5).logicalSrcIdx = 13;
	  section.data(5).dtTransOffset = 4;
	
	  ;% cqikrl4hwwb.pgfzy3n1r1z.mw4tdk345o
	  section.data(6).logicalSrcIdx = 14;
	  section.data(6).dtTransOffset = 5;
	
	  ;% cqikrl4hwwb.pgfzy3n1r1z.f5cgs2c3fw
	  section.data(7).logicalSrcIdx = 15;
	  section.data(7).dtTransOffset = 6;
	
	  ;% cqikrl4hwwb.pgfzy3n1r1z.kytxh3jgay
	  section.data(8).logicalSrcIdx = 16;
	  section.data(8).dtTransOffset = 8;
	
      nTotData = nTotData + section.nData;
      sigMap.sections(5) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% cqikrl4hwwb.pgfzy3n1r1z.ioyddqyofd
	  section.data(1).logicalSrcIdx = 17;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      sigMap.sections(6) = section;
      clear section
      
      section.nData     = 3;
      section.data(3)  = dumData; %prealloc
      
	  ;% cqikrl4hwwb.pgfzy3n1r1z.b0nvtmkzik
	  section.data(1).logicalSrcIdx = 18;
	  section.data(1).dtTransOffset = 0;
	
	  ;% cqikrl4hwwb.pgfzy3n1r1z.dmukd4qor5
	  section.data(2).logicalSrcIdx = 23;
	  section.data(2).dtTransOffset = 2;
	
	  ;% cqikrl4hwwb.pgfzy3n1r1z.htx2jakwbr
	  section.data(3).logicalSrcIdx = 29;
	  section.data(3).dtTransOffset = 4;
	
      nTotData = nTotData + section.nData;
      sigMap.sections(7) = section;
      clear section
      
      section.nData     = 11;
      section.data(11)  = dumData; %prealloc
      
	  ;% cqikrl4hwwb.pgfzy3n1r1z.gsyf5hrkpy
	  section.data(1).logicalSrcIdx = 19;
	  section.data(1).dtTransOffset = 0;
	
	  ;% cqikrl4hwwb.pgfzy3n1r1z.hacjapqgot
	  section.data(2).logicalSrcIdx = 20;
	  section.data(2).dtTransOffset = 1;
	
	  ;% cqikrl4hwwb.pgfzy3n1r1z.lianmxcwfh
	  section.data(3).logicalSrcIdx = 21;
	  section.data(3).dtTransOffset = 7;
	
	  ;% cqikrl4hwwb.pgfzy3n1r1z.eto5bcsxo5
	  section.data(4).logicalSrcIdx = 24;
	  section.data(4).dtTransOffset = 9;
	
	  ;% cqikrl4hwwb.pgfzy3n1r1z.mhhuoqwfzb
	  section.data(5).logicalSrcIdx = 25;
	  section.data(5).dtTransOffset = 10;
	
	  ;% cqikrl4hwwb.pgfzy3n1r1z.a1h2lqfym3
	  section.data(6).logicalSrcIdx = 26;
	  section.data(6).dtTransOffset = 13;
	
	  ;% cqikrl4hwwb.pgfzy3n1r1z.kybmhsx0as
	  section.data(7).logicalSrcIdx = 27;
	  section.data(7).dtTransOffset = 25;
	
	  ;% cqikrl4hwwb.pgfzy3n1r1z.od3chivsgi
	  section.data(8).logicalSrcIdx = 28;
	  section.data(8).dtTransOffset = 26;
	
	  ;% cqikrl4hwwb.pgfzy3n1r1z.avrdr30r12
	  section.data(9).logicalSrcIdx = 32;
	  section.data(9).dtTransOffset = 28;
	
	  ;% cqikrl4hwwb.pgfzy3n1r1z.dqa4jyqiav
	  section.data(10).logicalSrcIdx = 35;
	  section.data(10).dtTransOffset = 29;
	
	  ;% cqikrl4hwwb.pgfzy3n1r1z.hx0spal444
	  section.data(11).logicalSrcIdx = 38;
	  section.data(11).dtTransOffset = 30;
	
      nTotData = nTotData + section.nData;
      sigMap.sections(8) = section;
      clear section
      
      section.nData     = 2;
      section.data(2)  = dumData; %prealloc
      
	  ;% cqikrl4hwwb.pgfzy3n1r1z.ljfl43jffy
	  section.data(1).logicalSrcIdx = 39;
	  section.data(1).dtTransOffset = 0;
	
	  ;% cqikrl4hwwb.pgfzy3n1r1z.acds554hqi
	  section.data(2).logicalSrcIdx = 40;
	  section.data(2).dtTransOffset = 1;
	
      nTotData = nTotData + section.nData;
      sigMap.sections(9) = section;
      clear section
      
    
      ;%
      ;% Non-auto Data (signal)
      ;%
    

    ;%
    ;% Add final counts to struct.
    ;%
    sigMap.nTotData = nTotData;
    


  ;%*******************
  ;% Create DWork Map *
  ;%*******************
      
    nTotData      = 0; %add to this count as we go
    nTotSects     = 21;
    sectIdxOffset = 9;
    
    ;%
    ;% Define dummy sections & preallocate arrays
    ;%
    dumSection.nData = -1;  
    dumSection.data  = [];
    
    dumData.logicalSrcIdx = -1;
    dumData.dtTransOffset = -1;
    
    ;%
    ;% Init/prealloc dworkMap
    ;%
    dworkMap.nSections           = nTotSects;
    dworkMap.sectIdxOffset       = sectIdxOffset;
      dworkMap.sections(nTotSects) = dumSection; %prealloc
    dworkMap.nTotData            = -1;
    
    ;%
    ;% Auto data (n3a3mjwxhcs)
    ;%
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% n3a3mjwxhcs.kbuut0w4f3
	  section.data(1).logicalSrcIdx = 0;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(1) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% n3a3mjwxhcs.mrp2rfcwfn
	  section.data(1).logicalSrcIdx = 1;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(2) = section;
      clear section
      
      section.nData     = 4;
      section.data(4)  = dumData; %prealloc
      
	  ;% n3a3mjwxhcs.pk1he242bd
	  section.data(1).logicalSrcIdx = 2;
	  section.data(1).dtTransOffset = 0;
	
	  ;% n3a3mjwxhcs.mgc1b0vu1n
	  section.data(2).logicalSrcIdx = 3;
	  section.data(2).dtTransOffset = 1;
	
	  ;% n3a3mjwxhcs.dwpgti0ecy
	  section.data(3).logicalSrcIdx = 4;
	  section.data(3).dtTransOffset = 2;
	
	  ;% n3a3mjwxhcs.eg3upoiick
	  section.data(4).logicalSrcIdx = 5;
	  section.data(4).dtTransOffset = 4;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(3) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% n3a3mjwxhcs.m1dryfa4sc
	  section.data(1).logicalSrcIdx = 6;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(4) = section;
      clear section
      
      section.nData     = 2;
      section.data(2)  = dumData; %prealloc
      
	  ;% n3a3mjwxhcs.eaoep1cmbl
	  section.data(1).logicalSrcIdx = 7;
	  section.data(1).dtTransOffset = 0;
	
	  ;% n3a3mjwxhcs.euak0lee3a
	  section.data(2).logicalSrcIdx = 8;
	  section.data(2).dtTransOffset = 1;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(5) = section;
      clear section
      
      section.nData     = 10;
      section.data(10)  = dumData; %prealloc
      
	  ;% n3a3mjwxhcs.pgfzy3n1r1z.lton45oxvr
	  section.data(1).logicalSrcIdx = 9;
	  section.data(1).dtTransOffset = 0;
	
	  ;% n3a3mjwxhcs.pgfzy3n1r1z.l05ce0kqdk
	  section.data(2).logicalSrcIdx = 10;
	  section.data(2).dtTransOffset = 1;
	
	  ;% n3a3mjwxhcs.pgfzy3n1r1z.iqnur1koqi
	  section.data(3).logicalSrcIdx = 11;
	  section.data(3).dtTransOffset = 3;
	
	  ;% n3a3mjwxhcs.pgfzy3n1r1z.era33ggz0e
	  section.data(4).logicalSrcIdx = 12;
	  section.data(4).dtTransOffset = 6;
	
	  ;% n3a3mjwxhcs.pgfzy3n1r1z.nlfnj21c4j
	  section.data(5).logicalSrcIdx = 13;
	  section.data(5).dtTransOffset = 7;
	
	  ;% n3a3mjwxhcs.pgfzy3n1r1z.aop3eo33fr
	  section.data(6).logicalSrcIdx = 15;
	  section.data(6).dtTransOffset = 8;
	
	  ;% n3a3mjwxhcs.pgfzy3n1r1z.nmzynhnnkb
	  section.data(7).logicalSrcIdx = 16;
	  section.data(7).dtTransOffset = 9;
	
	  ;% n3a3mjwxhcs.pgfzy3n1r1z.b0j043ogv4
	  section.data(8).logicalSrcIdx = 17;
	  section.data(8).dtTransOffset = 10;
	
	  ;% n3a3mjwxhcs.pgfzy3n1r1z.pjskqbort1
	  section.data(9).logicalSrcIdx = 18;
	  section.data(9).dtTransOffset = 11;
	
	  ;% n3a3mjwxhcs.pgfzy3n1r1z.df03u1ko1t
	  section.data(10).logicalSrcIdx = 19;
	  section.data(10).dtTransOffset = 12;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(6) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% n3a3mjwxhcs.pgfzy3n1r1z.meebibvrcu.LoggedData
	  section.data(1).logicalSrcIdx = 20;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(7) = section;
      clear section
      
      section.nData     = 8;
      section.data(8)  = dumData; %prealloc
      
	  ;% n3a3mjwxhcs.pgfzy3n1r1z.kr1gyxbxa5
	  section.data(1).logicalSrcIdx = 21;
	  section.data(1).dtTransOffset = 0;
	
	  ;% n3a3mjwxhcs.pgfzy3n1r1z.cn4z5zryrs
	  section.data(2).logicalSrcIdx = 22;
	  section.data(2).dtTransOffset = 2;
	
	  ;% n3a3mjwxhcs.pgfzy3n1r1z.ewwnewr3ha
	  section.data(3).logicalSrcIdx = 23;
	  section.data(3).dtTransOffset = 5;
	
	  ;% n3a3mjwxhcs.pgfzy3n1r1z.bwjagq1x0r
	  section.data(4).logicalSrcIdx = 24;
	  section.data(4).dtTransOffset = 10;
	
	  ;% n3a3mjwxhcs.pgfzy3n1r1z.m2mx2xdzyr
	  section.data(5).logicalSrcIdx = 25;
	  section.data(5).dtTransOffset = 25;
	
	  ;% n3a3mjwxhcs.pgfzy3n1r1z.omrkw1fskh
	  section.data(6).logicalSrcIdx = 26;
	  section.data(6).dtTransOffset = 27;
	
	  ;% n3a3mjwxhcs.pgfzy3n1r1z.i5hgs5v3og
	  section.data(7).logicalSrcIdx = 27;
	  section.data(7).dtTransOffset = 37;
	
	  ;% n3a3mjwxhcs.pgfzy3n1r1z.jlyxvomjow
	  section.data(8).logicalSrcIdx = 29;
	  section.data(8).dtTransOffset = 39;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(8) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% n3a3mjwxhcs.pgfzy3n1r1z.hfcdq5uv5o
	  section.data(1).logicalSrcIdx = 30;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(9) = section;
      clear section
      
      section.nData     = 2;
      section.data(2)  = dumData; %prealloc
      
	  ;% n3a3mjwxhcs.pgfzy3n1r1z.bsxefawgkg
	  section.data(1).logicalSrcIdx = 31;
	  section.data(1).dtTransOffset = 0;
	
	  ;% n3a3mjwxhcs.pgfzy3n1r1z.ow2ezx3vpw
	  section.data(2).logicalSrcIdx = 32;
	  section.data(2).dtTransOffset = 1;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(10) = section;
      clear section
      
      section.nData     = 13;
      section.data(13)  = dumData; %prealloc
      
	  ;% n3a3mjwxhcs.pgfzy3n1r1z.cqr2olhgww
	  section.data(1).logicalSrcIdx = 34;
	  section.data(1).dtTransOffset = 0;
	
	  ;% n3a3mjwxhcs.pgfzy3n1r1z.ovemiov1ms
	  section.data(2).logicalSrcIdx = 36;
	  section.data(2).dtTransOffset = 3;
	
	  ;% n3a3mjwxhcs.pgfzy3n1r1z.pwkfvmrzt5
	  section.data(3).logicalSrcIdx = 37;
	  section.data(3).dtTransOffset = 5;
	
	  ;% n3a3mjwxhcs.pgfzy3n1r1z.k3lo3zhb5t
	  section.data(4).logicalSrcIdx = 38;
	  section.data(4).dtTransOffset = 6;
	
	  ;% n3a3mjwxhcs.pgfzy3n1r1z.j23e0qkcqj
	  section.data(5).logicalSrcIdx = 39;
	  section.data(5).dtTransOffset = 7;
	
	  ;% n3a3mjwxhcs.pgfzy3n1r1z.brgnnkfuz0
	  section.data(6).logicalSrcIdx = 40;
	  section.data(6).dtTransOffset = 8;
	
	  ;% n3a3mjwxhcs.pgfzy3n1r1z.n21qvbqic1
	  section.data(7).logicalSrcIdx = 41;
	  section.data(7).dtTransOffset = 9;
	
	  ;% n3a3mjwxhcs.pgfzy3n1r1z.gkmrzj0rpy
	  section.data(8).logicalSrcIdx = 42;
	  section.data(8).dtTransOffset = 10;
	
	  ;% n3a3mjwxhcs.pgfzy3n1r1z.memyors3cc
	  section.data(9).logicalSrcIdx = 43;
	  section.data(9).dtTransOffset = 11;
	
	  ;% n3a3mjwxhcs.pgfzy3n1r1z.ljbtow31hy
	  section.data(10).logicalSrcIdx = 44;
	  section.data(10).dtTransOffset = 12;
	
	  ;% n3a3mjwxhcs.pgfzy3n1r1z.e3nymhwyaf
	  section.data(11).logicalSrcIdx = 45;
	  section.data(11).dtTransOffset = 13;
	
	  ;% n3a3mjwxhcs.pgfzy3n1r1z.opewhk55al
	  section.data(12).logicalSrcIdx = 46;
	  section.data(12).dtTransOffset = 14;
	
	  ;% n3a3mjwxhcs.pgfzy3n1r1z.djdahgix3c
	  section.data(13).logicalSrcIdx = 47;
	  section.data(13).dtTransOffset = 15;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(11) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% n3a3mjwxhcs.pgfzy3n1r1z.hreuitr1h2
	  section.data(1).logicalSrcIdx = 48;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(12) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% n3a3mjwxhcs.pgfzy3n1r1z.bhsqx5zj1k
	  section.data(1).logicalSrcIdx = 49;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(13) = section;
      clear section
      
      section.nData     = 8;
      section.data(8)  = dumData; %prealloc
      
	  ;% n3a3mjwxhcs.pgfzy3n1r1z.naqcmwe1wg
	  section.data(1).logicalSrcIdx = 50;
	  section.data(1).dtTransOffset = 0;
	
	  ;% n3a3mjwxhcs.pgfzy3n1r1z.fgbacfgvph
	  section.data(2).logicalSrcIdx = 51;
	  section.data(2).dtTransOffset = 1;
	
	  ;% n3a3mjwxhcs.pgfzy3n1r1z.anisxdooef
	  section.data(3).logicalSrcIdx = 52;
	  section.data(3).dtTransOffset = 2;
	
	  ;% n3a3mjwxhcs.pgfzy3n1r1z.jib43orxrc
	  section.data(4).logicalSrcIdx = 53;
	  section.data(4).dtTransOffset = 3;
	
	  ;% n3a3mjwxhcs.pgfzy3n1r1z.dbfxjtthiy
	  section.data(5).logicalSrcIdx = 54;
	  section.data(5).dtTransOffset = 4;
	
	  ;% n3a3mjwxhcs.pgfzy3n1r1z.p0bj3fzi1h
	  section.data(6).logicalSrcIdx = 55;
	  section.data(6).dtTransOffset = 5;
	
	  ;% n3a3mjwxhcs.pgfzy3n1r1z.dvfvama0sm
	  section.data(7).logicalSrcIdx = 56;
	  section.data(7).dtTransOffset = 6;
	
	  ;% n3a3mjwxhcs.pgfzy3n1r1z.cmsturmcm5
	  section.data(8).logicalSrcIdx = 57;
	  section.data(8).dtTransOffset = 7;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(14) = section;
      clear section
      
      section.nData     = 4;
      section.data(4)  = dumData; %prealloc
      
	  ;% n3a3mjwxhcs.pgfzy3n1r1z.l4oqgqvf4c
	  section.data(1).logicalSrcIdx = 58;
	  section.data(1).dtTransOffset = 0;
	
	  ;% n3a3mjwxhcs.pgfzy3n1r1z.ivyvbje3yu
	  section.data(2).logicalSrcIdx = 59;
	  section.data(2).dtTransOffset = 1;
	
	  ;% n3a3mjwxhcs.pgfzy3n1r1z.kwwimbj2xr
	  section.data(3).logicalSrcIdx = 60;
	  section.data(3).dtTransOffset = 2;
	
	  ;% n3a3mjwxhcs.pgfzy3n1r1z.c4vrxgccda
	  section.data(4).logicalSrcIdx = 61;
	  section.data(4).dtTransOffset = 3;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(15) = section;
      clear section
      
      section.nData     = 4;
      section.data(4)  = dumData; %prealloc
      
	  ;% n3a3mjwxhcs.pgfzy3n1r1z.numdfksrrc
	  section.data(1).logicalSrcIdx = 62;
	  section.data(1).dtTransOffset = 0;
	
	  ;% n3a3mjwxhcs.pgfzy3n1r1z.gae21avcon
	  section.data(2).logicalSrcIdx = 63;
	  section.data(2).dtTransOffset = 1;
	
	  ;% n3a3mjwxhcs.pgfzy3n1r1z.eltk03s5h3
	  section.data(3).logicalSrcIdx = 64;
	  section.data(3).dtTransOffset = 2;
	
	  ;% n3a3mjwxhcs.pgfzy3n1r1z.oq51mgu332
	  section.data(4).logicalSrcIdx = 65;
	  section.data(4).dtTransOffset = 3;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(16) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% n3a3mjwxhcs.pgfzy3n1r1z.li14jlnqdt.gwjbi3m4yl
	  section.data(1).logicalSrcIdx = 66;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(17) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% n3a3mjwxhcs.pgfzy3n1r1z.g3fmi2n5mt.gwjbi3m4yl
	  section.data(1).logicalSrcIdx = 67;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(18) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% n3a3mjwxhcs.pgfzy3n1r1z.mn3pxsxeas.gwjbi3m4yl
	  section.data(1).logicalSrcIdx = 68;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(19) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% n3a3mjwxhcs.pgfzy3n1r1z.ltq03ey2sd.gwjbi3m4yl
	  section.data(1).logicalSrcIdx = 69;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(20) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% n3a3mjwxhcs.pgfzy3n1r1z.dzqztyjg5fv.gwjbi3m4yl
	  section.data(1).logicalSrcIdx = 70;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(21) = section;
      clear section
      
    
      ;%
      ;% Non-auto Data (dwork)
      ;%
    

    ;%
    ;% Add final counts to struct.
    ;%
    dworkMap.nTotData = nTotData;
    


  ;%
  ;% Add individual maps to base struct.
  ;%

  targMap.paramMap  = paramMap;    
  targMap.signalMap = sigMap;
  targMap.dworkMap  = dworkMap;
  
  ;%
  ;% Add checksums to base struct.
  ;%


  targMap.checksum0 = 83534095;
  targMap.checksum1 = 111819645;
  targMap.checksum2 = 341440094;
  targMap.checksum3 = 3492858158;

