  function targMap = targDataMap(),

  ;%***********************
  ;% Create Parameter Map *
  ;%***********************
      
    nTotData      = 0; %add to this count as we go
    nTotSects     = 15;
    sectIdxOffset = 0;
    
    ;%
    ;% Define dummy sections & preallocate arrays
    ;%
    dumSection.nData = -1;  
    dumSection.data  = [];
    
    dumData.logicalSrcIdx = -1;
    dumData.dtTransOffset = -1;
    
    ;%
    ;% Init/prealloc paramMap
    ;%
    paramMap.nSections           = nTotSects;
    paramMap.sectIdxOffset       = sectIdxOffset;
      paramMap.sections(nTotSects) = dumSection; %prealloc
    paramMap.nTotData            = -1;
    
    ;%
    ;% Auto data (g0szqdi43g)
    ;%
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% g0szqdi43g.Sensors
	  section.data(1).logicalSrcIdx = 0;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(1) = section;
      clear section
      
      section.nData     = 13;
      section.data(13)  = dumData; %prealloc
      
	  ;% g0szqdi43g.Constant_Value
	  section.data(1).logicalSrcIdx = 1;
	  section.data(1).dtTransOffset = 0;
	
	  ;% g0szqdi43g.Constant_Value_fpxjlztgew
	  section.data(2).logicalSrcIdx = 2;
	  section.data(2).dtTransOffset = 1;
	
	  ;% g0szqdi43g.Ruido_Value
	  section.data(3).logicalSrcIdx = 3;
	  section.data(3).dtTransOffset = 2;
	
	  ;% g0szqdi43g.Umbral07AR7_Value
	  section.data(4).logicalSrcIdx = 4;
	  section.data(4).dtTransOffset = 3;
	
	  ;% g0szqdi43g.Toleranciasuperior_Value
	  section.data(5).logicalSrcIdx = 5;
	  section.data(5).dtTransOffset = 4;
	
	  ;% g0szqdi43g.ToleranciaInferior_Value
	  section.data(6).logicalSrcIdx = 6;
	  section.data(6).dtTransOffset = 5;
	
	  ;% g0szqdi43g.Switch_Threshold
	  section.data(7).logicalSrcIdx = 7;
	  section.data(7).dtTransOffset = 6;
	
	  ;% g0szqdi43g.Gain_Gain
	  section.data(8).logicalSrcIdx = 8;
	  section.data(8).dtTransOffset = 7;
	
	  ;% g0szqdi43g.Switch_Threshold_mxp2mrzert
	  section.data(9).logicalSrcIdx = 9;
	  section.data(9).dtTransOffset = 167;
	
	  ;% g0szqdi43g.Gain_Gain_i4wlhlbj0o
	  section.data(10).logicalSrcIdx = 10;
	  section.data(10).dtTransOffset = 168;
	
	  ;% g0szqdi43g.Gain_Gain_cieuh1klpl
	  section.data(11).logicalSrcIdx = 11;
	  section.data(11).dtTransOffset = 328;
	
	  ;% g0szqdi43g.Gain_Gain_gooewqhr41
	  section.data(12).logicalSrcIdx = 12;
	  section.data(12).dtTransOffset = 448;
	
	  ;% g0szqdi43g.RateTransition_InitialCondition
	  section.data(13).logicalSrcIdx = 13;
	  section.data(13).dtTransOffset = 608;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(2) = section;
      clear section
      
      section.nData     = 6;
      section.data(6)  = dumData; %prealloc
      
	  ;% g0szqdi43g.bunacxzsnx4.outlierBelowFloor_const
	  section.data(1).logicalSrcIdx = 14;
	  section.data(1).dtTransOffset = 0;
	
	  ;% g0szqdi43g.bunacxzsnx4.outlierJump_const
	  section.data(2).logicalSrcIdx = 15;
	  section.data(2).dtTransOffset = 1;
	
	  ;% g0szqdi43g.bunacxzsnx4.currentEstimateVeryOffFromPress
	  section.data(3).logicalSrcIdx = 16;
	  section.data(3).dtTransOffset = 2;
	
	  ;% g0szqdi43g.bunacxzsnx4.currentStateVeryOffsonarflt_con
	  section.data(4).logicalSrcIdx = 17;
	  section.data(4).dtTransOffset = 3;
	
	  ;% g0szqdi43g.bunacxzsnx4.Checkerrorcondition_const
	  section.data(5).logicalSrcIdx = 18;
	  section.data(5).dtTransOffset = 4;
	
	  ;% g0szqdi43g.bunacxzsnx4.u0continuousOFerrors_const
	  section.data(6).logicalSrcIdx = 19;
	  section.data(6).dtTransOffset = 5;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(3) = section;
      clear section
      
      section.nData     = 24;
      section.data(24)  = dumData; %prealloc
      
	  ;% g0szqdi43g.bunacxzsnx4.DiscreteDerivative_ICPrevScaled
	  section.data(1).logicalSrcIdx = 20;
	  section.data(1).dtTransOffset = 0;
	
	  ;% g0szqdi43g.bunacxzsnx4.DiscreteDerivative_I_mgyebwvuk3
	  section.data(2).logicalSrcIdx = 21;
	  section.data(2).dtTransOffset = 1;
	
	  ;% g0szqdi43g.bunacxzsnx4.CompareToConstant_const
	  section.data(3).logicalSrcIdx = 22;
	  section.data(3).dtTransOffset = 2;
	
	  ;% g0szqdi43g.bunacxzsnx4.CompareToConstant1_const
	  section.data(4).logicalSrcIdx = 23;
	  section.data(4).dtTransOffset = 3;
	
	  ;% g0szqdi43g.bunacxzsnx4.maxp_const
	  section.data(5).logicalSrcIdx = 24;
	  section.data(5).dtTransOffset = 4;
	
	  ;% g0szqdi43g.bunacxzsnx4.maxq_const
	  section.data(6).logicalSrcIdx = 25;
	  section.data(6).dtTransOffset = 5;
	
	  ;% g0szqdi43g.bunacxzsnx4.maxw1_const
	  section.data(7).logicalSrcIdx = 26;
	  section.data(7).dtTransOffset = 6;
	
	  ;% g0szqdi43g.bunacxzsnx4.maxw2_const
	  section.data(8).logicalSrcIdx = 27;
	  section.data(8).dtTransOffset = 7;
	
	  ;% g0szqdi43g.bunacxzsnx4.maxdw1_const
	  section.data(9).logicalSrcIdx = 28;
	  section.data(9).dtTransOffset = 8;
	
	  ;% g0szqdi43g.bunacxzsnx4.maxdw2_const
	  section.data(10).logicalSrcIdx = 29;
	  section.data(10).dtTransOffset = 9;
	
	  ;% g0szqdi43g.bunacxzsnx4.maxp2_const
	  section.data(11).logicalSrcIdx = 30;
	  section.data(11).dtTransOffset = 10;
	
	  ;% g0szqdi43g.bunacxzsnx4.maxq2_const
	  section.data(12).logicalSrcIdx = 31;
	  section.data(12).dtTransOffset = 11;
	
	  ;% g0szqdi43g.bunacxzsnx4.maxw3_const
	  section.data(13).logicalSrcIdx = 32;
	  section.data(13).dtTransOffset = 12;
	
	  ;% g0szqdi43g.bunacxzsnx4.maxw4_const
	  section.data(14).logicalSrcIdx = 33;
	  section.data(14).dtTransOffset = 13;
	
	  ;% g0szqdi43g.bunacxzsnx4.minHeightforOF_const
	  section.data(15).logicalSrcIdx = 34;
	  section.data(15).dtTransOffset = 14;
	
	  ;% g0szqdi43g.bunacxzsnx4.CompareToConstant_co_cg5nq0gp5c
	  section.data(16).logicalSrcIdx = 35;
	  section.data(16).dtTransOffset = 15;
	
	  ;% g0szqdi43g.bunacxzsnx4.CompareToConstant1_c_f1zaoeyxul
	  section.data(17).logicalSrcIdx = 36;
	  section.data(17).dtTransOffset = 16;
	
	  ;% g0szqdi43g.bunacxzsnx4.CompareToConstant2_const
	  section.data(18).logicalSrcIdx = 37;
	  section.data(18).dtTransOffset = 17;
	
	  ;% g0szqdi43g.bunacxzsnx4.CompareToConstant3_const
	  section.data(19).logicalSrcIdx = 38;
	  section.data(19).dtTransOffset = 18;
	
	  ;% g0szqdi43g.bunacxzsnx4.CompareToConstant4_const
	  section.data(20).logicalSrcIdx = 39;
	  section.data(20).dtTransOffset = 19;
	
	  ;% g0szqdi43g.bunacxzsnx4.CompareToConstant5_const
	  section.data(21).logicalSrcIdx = 40;
	  section.data(21).dtTransOffset = 20;
	
	  ;% g0szqdi43g.bunacxzsnx4.DeactivateAccelerationIfOFisnot
	  section.data(22).logicalSrcIdx = 41;
	  section.data(22).dtTransOffset = 21;
	
	  ;% g0szqdi43g.bunacxzsnx4.donotuseaccifopticalflowneverav
	  section.data(23).logicalSrcIdx = 42;
	  section.data(23).dtTransOffset = 22;
	
	  ;% g0szqdi43g.bunacxzsnx4.donotuseaccifoptical_enkomcbtma
	  section.data(24).logicalSrcIdx = 43;
	  section.data(24).dtTransOffset = 23;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(4) = section;
      clear section
      
      section.nData     = 5;
      section.data(5)  = dumData; %prealloc
      
	  ;% g0szqdi43g.bunacxzsnx4.WrapToZero_Threshold
	  section.data(1).logicalSrcIdx = 44;
	  section.data(1).dtTransOffset = 0;
	
	  ;% g0szqdi43g.bunacxzsnx4.WrapToZero_Threshold_lt3aeunade
	  section.data(2).logicalSrcIdx = 45;
	  section.data(2).dtTransOffset = 1;
	
	  ;% g0szqdi43g.bunacxzsnx4.WrapToZero_Threshold_bsvf1b4vt4
	  section.data(3).logicalSrcIdx = 46;
	  section.data(3).dtTransOffset = 2;
	
	  ;% g0szqdi43g.bunacxzsnx4.CompareToConstant_co_iurzr1ydze
	  section.data(4).logicalSrcIdx = 47;
	  section.data(4).dtTransOffset = 3;
	
	  ;% g0szqdi43g.bunacxzsnx4.CompareToConstant_co_msvj024jaz
	  section.data(5).logicalSrcIdx = 48;
	  section.data(5).dtTransOffset = 4;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(5) = section;
      clear section
      
      section.nData     = 42;
      section.data(42)  = dumData; %prealloc
      
	  ;% g0szqdi43g.bunacxzsnx4._Value
	  section.data(1).logicalSrcIdx = 49;
	  section.data(1).dtTransOffset = 0;
	
	  ;% g0szqdi43g.bunacxzsnx4.Constant1_Value
	  section.data(2).logicalSrcIdx = 50;
	  section.data(2).dtTransOffset = 1;
	
	  ;% g0szqdi43g.bunacxzsnx4.Gain_Gain
	  section.data(3).logicalSrcIdx = 51;
	  section.data(3).dtTransOffset = 2;
	
	  ;% g0szqdi43g.bunacxzsnx4.Constant6_Value
	  section.data(4).logicalSrcIdx = 52;
	  section.data(4).dtTransOffset = 3;
	
	  ;% g0szqdi43g.bunacxzsnx4.Constant5_Value
	  section.data(5).logicalSrcIdx = 53;
	  section.data(5).dtTransOffset = 4;
	
	  ;% g0szqdi43g.bunacxzsnx4.Constant2_Value
	  section.data(6).logicalSrcIdx = 54;
	  section.data(6).dtTransOffset = 5;
	
	  ;% g0szqdi43g.bunacxzsnx4.Constant_Value
	  section.data(7).logicalSrcIdx = 55;
	  section.data(7).dtTransOffset = 6;
	
	  ;% g0szqdi43g.bunacxzsnx4.Switch_Threshold
	  section.data(8).logicalSrcIdx = 56;
	  section.data(8).dtTransOffset = 7;
	
	  ;% g0szqdi43g.bunacxzsnx4.Constant3_Value
	  section.data(9).logicalSrcIdx = 57;
	  section.data(9).dtTransOffset = 8;
	
	  ;% g0szqdi43g.bunacxzsnx4.Lykyhatkk1_Y0
	  section.data(10).logicalSrcIdx = 58;
	  section.data(10).dtTransOffset = 9;
	
	  ;% g0szqdi43g.bunacxzsnx4.deltax_Y0
	  section.data(11).logicalSrcIdx = 59;
	  section.data(11).dtTransOffset = 10;
	
	  ;% g0szqdi43g.bunacxzsnx4.KalmanGainM_Value
	  section.data(12).logicalSrcIdx = 60;
	  section.data(12).dtTransOffset = 11;
	
	  ;% g0szqdi43g.bunacxzsnx4.C_Value
	  section.data(13).logicalSrcIdx = 61;
	  section.data(13).dtTransOffset = 13;
	
	  ;% g0szqdi43g.bunacxzsnx4.Delay2_InitialCondition
	  section.data(14).logicalSrcIdx = 62;
	  section.data(14).dtTransOffset = 15;
	
	  ;% g0szqdi43g.bunacxzsnx4.X0_Value
	  section.data(15).logicalSrcIdx = 63;
	  section.data(15).dtTransOffset = 16;
	
	  ;% g0szqdi43g.bunacxzsnx4.SaturationSonar_LowerSat
	  section.data(16).logicalSrcIdx = 64;
	  section.data(16).dtTransOffset = 18;
	
	  ;% g0szqdi43g.bunacxzsnx4.soonarFilter_IIR_NumCoef
	  section.data(17).logicalSrcIdx = 65;
	  section.data(17).dtTransOffset = 19;
	
	  ;% g0szqdi43g.bunacxzsnx4.soonarFilter_IIR_DenCoef
	  section.data(18).logicalSrcIdx = 66;
	  section.data(18).dtTransOffset = 23;
	
	  ;% g0szqdi43g.bunacxzsnx4.soonarFilter_IIR_InitialStates
	  section.data(19).logicalSrcIdx = 67;
	  section.data(19).dtTransOffset = 27;
	
	  ;% g0szqdi43g.bunacxzsnx4.KalmanGainM_Value_a25ehjtkcp
	  section.data(20).logicalSrcIdx = 68;
	  section.data(20).dtTransOffset = 28;
	
	  ;% g0szqdi43g.bunacxzsnx4.Constant4_Value
	  section.data(21).logicalSrcIdx = 69;
	  section.data(21).dtTransOffset = 32;
	
	  ;% g0szqdi43g.bunacxzsnx4.Delay_InitialCondition
	  section.data(22).logicalSrcIdx = 70;
	  section.data(22).dtTransOffset = 33;
	
	  ;% g0szqdi43g.bunacxzsnx4.Gain_Gain_aeylx1ate0
	  section.data(23).logicalSrcIdx = 71;
	  section.data(23).dtTransOffset = 34;
	
	  ;% g0szqdi43g.bunacxzsnx4.Gain1_Gain
	  section.data(24).logicalSrcIdx = 72;
	  section.data(24).dtTransOffset = 35;
	
	  ;% g0szqdi43g.bunacxzsnx4.Switch2_Threshold
	  section.data(25).logicalSrcIdx = 73;
	  section.data(25).dtTransOffset = 36;
	
	  ;% g0szqdi43g.bunacxzsnx4.PitchAleteo_Value
	  section.data(26).logicalSrcIdx = 74;
	  section.data(26).dtTransOffset = 37;
	
	  ;% g0szqdi43g.bunacxzsnx4.PitchExploracion_Value
	  section.data(27).logicalSrcIdx = 75;
	  section.data(27).dtTransOffset = 38;
	
	  ;% g0szqdi43g.bunacxzsnx4.TiempodeFrenado_Value
	  section.data(28).logicalSrcIdx = 76;
	  section.data(28).dtTransOffset = 39;
	
	  ;% g0szqdi43g.bunacxzsnx4.TiempodeCalibracion_Value
	  section.data(29).logicalSrcIdx = 77;
	  section.data(29).dtTransOffset = 40;
	
	  ;% g0szqdi43g.bunacxzsnx4.Wait3Seconds_Value
	  section.data(30).logicalSrcIdx = 78;
	  section.data(30).dtTransOffset = 41;
	
	  ;% g0szqdi43g.bunacxzsnx4.DelayOneStep_InitialCondition
	  section.data(31).logicalSrcIdx = 79;
	  section.data(31).dtTransOffset = 42;
	
	  ;% g0szqdi43g.bunacxzsnx4.u5meters_Value
	  section.data(32).logicalSrcIdx = 80;
	  section.data(32).dtTransOffset = 43;
	
	  ;% g0szqdi43g.bunacxzsnx4.Constant1_Value_nywtsdf2ft
	  section.data(33).logicalSrcIdx = 81;
	  section.data(33).dtTransOffset = 44;
	
	  ;% g0szqdi43g.bunacxzsnx4.Switch1_Threshold
	  section.data(34).logicalSrcIdx = 82;
	  section.data(34).dtTransOffset = 45;
	
	  ;% g0szqdi43g.bunacxzsnx4.A_Value
	  section.data(35).logicalSrcIdx = 83;
	  section.data(35).dtTransOffset = 46;
	
	  ;% g0szqdi43g.bunacxzsnx4.B_Value
	  section.data(36).logicalSrcIdx = 84;
	  section.data(36).dtTransOffset = 50;
	
	  ;% g0szqdi43g.bunacxzsnx4.KalmanGainL_Value
	  section.data(37).logicalSrcIdx = 85;
	  section.data(37).dtTransOffset = 52;
	
	  ;% g0szqdi43g.bunacxzsnx4.D_Value
	  section.data(38).logicalSrcIdx = 86;
	  section.data(38).dtTransOffset = 54;
	
	  ;% g0szqdi43g.bunacxzsnx4.gravity_Value
	  section.data(39).logicalSrcIdx = 87;
	  section.data(39).dtTransOffset = 55;
	
	  ;% g0szqdi43g.bunacxzsnx4.gravity_Value_e0zojwbx5c
	  section.data(40).logicalSrcIdx = 88;
	  section.data(40).dtTransOffset = 58;
	
	  ;% g0szqdi43g.bunacxzsnx4.gainaccinput_Gain
	  section.data(41).logicalSrcIdx = 89;
	  section.data(41).dtTransOffset = 61;
	
	  ;% g0szqdi43g.bunacxzsnx4.KalmanGainL_Value_pdr0hu10ea
	  section.data(42).logicalSrcIdx = 90;
	  section.data(42).dtTransOffset = 62;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(6) = section;
      clear section
      
      section.nData     = 95;
      section.data(95)  = dumData; %prealloc
      
	  ;% g0szqdi43g.bunacxzsnx4.D_xy_Gain
	  section.data(1).logicalSrcIdx = 91;
	  section.data(1).dtTransOffset = 0;
	
	  ;% g0szqdi43g.bunacxzsnx4.Gain_Gain_kcq05epejt
	  section.data(2).logicalSrcIdx = 92;
	  section.data(2).dtTransOffset = 2;
	
	  ;% g0szqdi43g.bunacxzsnx4.Saturation_UpperSat
	  section.data(3).logicalSrcIdx = 93;
	  section.data(3).dtTransOffset = 3;
	
	  ;% g0szqdi43g.bunacxzsnx4.Saturation_LowerSat
	  section.data(4).logicalSrcIdx = 94;
	  section.data(4).dtTransOffset = 4;
	
	  ;% g0szqdi43g.bunacxzsnx4.P_xy_Gain
	  section.data(5).logicalSrcIdx = 95;
	  section.data(5).dtTransOffset = 5;
	
	  ;% g0szqdi43g.bunacxzsnx4.D_z1_Gain
	  section.data(6).logicalSrcIdx = 96;
	  section.data(6).dtTransOffset = 7;
	
	  ;% g0szqdi43g.bunacxzsnx4.P_z1_Gain
	  section.data(7).logicalSrcIdx = 97;
	  section.data(7).dtTransOffset = 8;
	
	  ;% g0szqdi43g.bunacxzsnx4.takeoff_gain1_Gain
	  section.data(8).logicalSrcIdx = 98;
	  section.data(8).dtTransOffset = 9;
	
	  ;% g0szqdi43g.bunacxzsnx4._Value_jdqaukrlkk
	  section.data(9).logicalSrcIdx = 99;
	  section.data(9).dtTransOffset = 10;
	
	  ;% g0szqdi43g.bunacxzsnx4.Switch1_Threshold_lehcgisfz3
	  section.data(10).logicalSrcIdx = 100;
	  section.data(10).dtTransOffset = 11;
	
	  ;% g0szqdi43g.bunacxzsnx4.kp_Gain
	  section.data(11).logicalSrcIdx = 101;
	  section.data(11).dtTransOffset = 12;
	
	  ;% g0szqdi43g.bunacxzsnx4.degToRad_Gain
	  section.data(12).logicalSrcIdx = 102;
	  section.data(12).dtTransOffset = 13;
	
	  ;% g0szqdi43g.bunacxzsnx4.Switch1_Threshold_pjzrynbacf
	  section.data(13).logicalSrcIdx = 103;
	  section.data(13).dtTransOffset = 14;
	
	  ;% g0szqdi43g.bunacxzsnx4.Switch_Threshold_imhbg1j1p2
	  section.data(14).logicalSrcIdx = 104;
	  section.data(14).dtTransOffset = 15;
	
	  ;% g0szqdi43g.bunacxzsnx4.Out1_Y0
	  section.data(15).logicalSrcIdx = 105;
	  section.data(15).dtTransOffset = 16;
	
	  ;% g0szqdi43g.bunacxzsnx4.Gain_Gain_mkz3hadjae
	  section.data(16).logicalSrcIdx = 106;
	  section.data(16).dtTransOffset = 17;
	
	  ;% g0szqdi43g.bunacxzsnx4.Saturation_UpperSat_cfqml1e3hc
	  section.data(17).logicalSrcIdx = 107;
	  section.data(17).dtTransOffset = 18;
	
	  ;% g0szqdi43g.bunacxzsnx4.Saturation_LowerSat_bwgayzpds3
	  section.data(18).logicalSrcIdx = 108;
	  section.data(18).dtTransOffset = 19;
	
	  ;% g0szqdi43g.bunacxzsnx4.P_xy_Gain_dvwoecc0t2
	  section.data(19).logicalSrcIdx = 109;
	  section.data(19).dtTransOffset = 20;
	
	  ;% g0szqdi43g.bunacxzsnx4.Gain_Gain_bqak2blc14
	  section.data(20).logicalSrcIdx = 110;
	  section.data(20).dtTransOffset = 22;
	
	  ;% g0szqdi43g.bunacxzsnx4.Gain1_Gain_fzvq0za4is
	  section.data(21).logicalSrcIdx = 111;
	  section.data(21).dtTransOffset = 23;
	
	  ;% g0szqdi43g.bunacxzsnx4.Gain2_Gain
	  section.data(22).logicalSrcIdx = 112;
	  section.data(22).dtTransOffset = 24;
	
	  ;% g0szqdi43g.bunacxzsnx4.Gain3_Gain
	  section.data(23).logicalSrcIdx = 113;
	  section.data(23).dtTransOffset = 25;
	
	  ;% g0szqdi43g.bunacxzsnx4.Gain4_Gain
	  section.data(24).logicalSrcIdx = 114;
	  section.data(24).dtTransOffset = 26;
	
	  ;% g0szqdi43g.bunacxzsnx4.Gain_Gain_ahdovdrtbi
	  section.data(25).logicalSrcIdx = 115;
	  section.data(25).dtTransOffset = 27;
	
	  ;% g0szqdi43g.bunacxzsnx4.opticalFlowErrorCorrect_Gain
	  section.data(26).logicalSrcIdx = 116;
	  section.data(26).dtTransOffset = 28;
	
	  ;% g0szqdi43g.bunacxzsnx4.Lykyhatkk1_Y0_lo3hotsx3z
	  section.data(27).logicalSrcIdx = 117;
	  section.data(27).dtTransOffset = 29;
	
	  ;% g0szqdi43g.bunacxzsnx4.deltax_Y0_pdhsig5w2d
	  section.data(28).logicalSrcIdx = 118;
	  section.data(28).dtTransOffset = 30;
	
	  ;% g0szqdi43g.bunacxzsnx4.SimplyIntegrateVelocity_gainval
	  section.data(29).logicalSrcIdx = 119;
	  section.data(29).dtTransOffset = 31;
	
	  ;% g0szqdi43g.bunacxzsnx4.SimplyIntegrateVelocity_IC
	  section.data(30).logicalSrcIdx = 120;
	  section.data(30).dtTransOffset = 32;
	
	  ;% g0szqdi43g.bunacxzsnx4.invertzaxisGain_Gain
	  section.data(31).logicalSrcIdx = 121;
	  section.data(31).dtTransOffset = 33;
	
	  ;% g0szqdi43g.bunacxzsnx4.prsToAltGain_Gain
	  section.data(32).logicalSrcIdx = 122;
	  section.data(32).dtTransOffset = 34;
	
	  ;% g0szqdi43g.bunacxzsnx4.pressureFilter_IIR_NumCoef
	  section.data(33).logicalSrcIdx = 123;
	  section.data(33).dtTransOffset = 35;
	
	  ;% g0szqdi43g.bunacxzsnx4.pressureFilter_IIR_DenCoef
	  section.data(34).logicalSrcIdx = 124;
	  section.data(34).dtTransOffset = 39;
	
	  ;% g0szqdi43g.bunacxzsnx4.pressureFilter_IIR_InitialState
	  section.data(35).logicalSrcIdx = 125;
	  section.data(35).dtTransOffset = 43;
	
	  ;% g0szqdi43g.bunacxzsnx4.Memory_InitialCondition
	  section.data(36).logicalSrcIdx = 126;
	  section.data(36).dtTransOffset = 44;
	
	  ;% g0szqdi43g.bunacxzsnx4.Constant_Value_f15o3zn5co
	  section.data(37).logicalSrcIdx = 127;
	  section.data(37).dtTransOffset = 47;
	
	  ;% g0szqdi43g.bunacxzsnx4.Gain_Gain_ccvzzc4yky
	  section.data(38).logicalSrcIdx = 128;
	  section.data(38).dtTransOffset = 48;
	
	  ;% g0szqdi43g.bunacxzsnx4.Assumingthatcalibwasdonelevel_B
	  section.data(39).logicalSrcIdx = 129;
	  section.data(39).dtTransOffset = 49;
	
	  ;% g0szqdi43g.bunacxzsnx4.inverseIMU_gain_Gain
	  section.data(40).logicalSrcIdx = 130;
	  section.data(40).dtTransOffset = 55;
	
	  ;% g0szqdi43g.bunacxzsnx4.IIR_IMUgyro_r_NumCoef
	  section.data(41).logicalSrcIdx = 131;
	  section.data(41).dtTransOffset = 61;
	
	  ;% g0szqdi43g.bunacxzsnx4.IIR_IMUgyro_r_DenCoef
	  section.data(42).logicalSrcIdx = 132;
	  section.data(42).dtTransOffset = 67;
	
	  ;% g0szqdi43g.bunacxzsnx4.IIR_IMUgyro_r_InitialStates
	  section.data(43).logicalSrcIdx = 133;
	  section.data(43).dtTransOffset = 73;
	
	  ;% g0szqdi43g.bunacxzsnx4.Gain_Gain_oyqgfyjvb4
	  section.data(44).logicalSrcIdx = 134;
	  section.data(44).dtTransOffset = 74;
	
	  ;% g0szqdi43g.bunacxzsnx4.FIR_IMUaccel_InitialStates
	  section.data(45).logicalSrcIdx = 135;
	  section.data(45).dtTransOffset = 75;
	
	  ;% g0szqdi43g.bunacxzsnx4.FIR_IMUaccel_Coefficients
	  section.data(46).logicalSrcIdx = 136;
	  section.data(46).dtTransOffset = 76;
	
	  ;% g0szqdi43g.bunacxzsnx4.Constant_Value_okooziqiuy
	  section.data(47).logicalSrcIdx = 137;
	  section.data(47).dtTransOffset = 82;
	
	  ;% g0szqdi43g.bunacxzsnx4.Merge_InitialOutput
	  section.data(48).logicalSrcIdx = 138;
	  section.data(48).dtTransOffset = 83;
	
	  ;% g0szqdi43g.bunacxzsnx4.C_Value_d1t33aijy4
	  section.data(49).logicalSrcIdx = 139;
	  section.data(49).dtTransOffset = 84;
	
	  ;% g0szqdi43g.bunacxzsnx4.X0_Value_l0suz4vone
	  section.data(50).logicalSrcIdx = 140;
	  section.data(50).dtTransOffset = 88;
	
	  ;% g0szqdi43g.bunacxzsnx4.IIRgyroz_NumCoef
	  section.data(51).logicalSrcIdx = 141;
	  section.data(51).dtTransOffset = 90;
	
	  ;% g0szqdi43g.bunacxzsnx4.IIRgyroz_DenCoef
	  section.data(52).logicalSrcIdx = 142;
	  section.data(52).dtTransOffset = 96;
	
	  ;% g0szqdi43g.bunacxzsnx4.IIRgyroz_InitialStates
	  section.data(53).logicalSrcIdx = 143;
	  section.data(53).dtTransOffset = 102;
	
	  ;% g0szqdi43g.bunacxzsnx4.TSamp_WtEt
	  section.data(54).logicalSrcIdx = 144;
	  section.data(54).dtTransOffset = 103;
	
	  ;% g0szqdi43g.bunacxzsnx4.Delay_InitialConditi_ewkmqlmd1b
	  section.data(55).logicalSrcIdx = 145;
	  section.data(55).dtTransOffset = 104;
	
	  ;% g0szqdi43g.bunacxzsnx4.Delay1_InitialCondition
	  section.data(56).logicalSrcIdx = 146;
	  section.data(56).dtTransOffset = 105;
	
	  ;% g0szqdi43g.bunacxzsnx4.AlturadeVuelo_Value
	  section.data(57).logicalSrcIdx = 147;
	  section.data(57).dtTransOffset = 106;
	
	  ;% g0szqdi43g.bunacxzsnx4.Gain_Gain_hrrlamgem3
	  section.data(58).logicalSrcIdx = 148;
	  section.data(58).dtTransOffset = 107;
	
	  ;% g0szqdi43g.bunacxzsnx4.kp2_Gain
	  section.data(59).logicalSrcIdx = 149;
	  section.data(59).dtTransOffset = 108;
	
	  ;% g0szqdi43g.bunacxzsnx4.TSamp_WtEt_fq5py0psxe
	  section.data(60).logicalSrcIdx = 150;
	  section.data(60).dtTransOffset = 109;
	
	  ;% g0szqdi43g.bunacxzsnx4.DiscreteTimeIntegrator_gainval
	  section.data(61).logicalSrcIdx = 151;
	  section.data(61).dtTransOffset = 110;
	
	  ;% g0szqdi43g.bunacxzsnx4.DiscreteTimeIntegrator_IC
	  section.data(62).logicalSrcIdx = 152;
	  section.data(62).dtTransOffset = 111;
	
	  ;% g0szqdi43g.bunacxzsnx4.Switch_Threshold_contxuzfpv
	  section.data(63).logicalSrcIdx = 153;
	  section.data(63).dtTransOffset = 112;
	
	  ;% g0szqdi43g.bunacxzsnx4.D_xy1_Gain
	  section.data(64).logicalSrcIdx = 154;
	  section.data(64).dtTransOffset = 113;
	
	  ;% g0szqdi43g.bunacxzsnx4.Delay_InitialConditi_phxi1zc4xy
	  section.data(65).logicalSrcIdx = 155;
	  section.data(65).dtTransOffset = 115;
	
	  ;% g0szqdi43g.bunacxzsnx4.antiWU_Gain_Gain
	  section.data(66).logicalSrcIdx = 156;
	  section.data(66).dtTransOffset = 116;
	
	  ;% g0szqdi43g.bunacxzsnx4.D_pr_Gain
	  section.data(67).logicalSrcIdx = 157;
	  section.data(67).dtTransOffset = 117;
	
	  ;% g0szqdi43g.bunacxzsnx4.DiscreteTimeIntegrat_ahw5m4p5pf
	  section.data(68).logicalSrcIdx = 158;
	  section.data(68).dtTransOffset = 119;
	
	  ;% g0szqdi43g.bunacxzsnx4.DiscreteTimeIntegrat_ahspmvgpnj
	  section.data(69).logicalSrcIdx = 159;
	  section.data(69).dtTransOffset = 120;
	
	  ;% g0szqdi43g.bunacxzsnx4.DiscreteTimeIntegrator_UpperSat
	  section.data(70).logicalSrcIdx = 160;
	  section.data(70).dtTransOffset = 121;
	
	  ;% g0szqdi43g.bunacxzsnx4.DiscreteTimeIntegrator_LowerSat
	  section.data(71).logicalSrcIdx = 161;
	  section.data(71).dtTransOffset = 122;
	
	  ;% g0szqdi43g.bunacxzsnx4.I_pr_Gain
	  section.data(72).logicalSrcIdx = 162;
	  section.data(72).dtTransOffset = 123;
	
	  ;% g0szqdi43g.bunacxzsnx4.P_pr_Gain
	  section.data(73).logicalSrcIdx = 163;
	  section.data(73).dtTransOffset = 124;
	
	  ;% g0szqdi43g.bunacxzsnx4.TorqueTotalThrustToThrustPerMot
	  section.data(74).logicalSrcIdx = 164;
	  section.data(74).dtTransOffset = 126;
	
	  ;% g0szqdi43g.bunacxzsnx4.w1_Value
	  section.data(75).logicalSrcIdx = 165;
	  section.data(75).dtTransOffset = 142;
	
	  ;% g0szqdi43g.bunacxzsnx4.DiscreteTimeIntegrat_eyojpkiou5
	  section.data(76).logicalSrcIdx = 166;
	  section.data(76).dtTransOffset = 143;
	
	  ;% g0szqdi43g.bunacxzsnx4.DiscreteTimeIntegrat_ncb3hew20x
	  section.data(77).logicalSrcIdx = 167;
	  section.data(77).dtTransOffset = 144;
	
	  ;% g0szqdi43g.bunacxzsnx4.DiscreteTimeIntegrat_a2itoknyz0
	  section.data(78).logicalSrcIdx = 168;
	  section.data(78).dtTransOffset = 145;
	
	  ;% g0szqdi43g.bunacxzsnx4.DiscreteTimeIntegrat_bursmxzmht
	  section.data(79).logicalSrcIdx = 169;
	  section.data(79).dtTransOffset = 146;
	
	  ;% g0szqdi43g.bunacxzsnx4.SaturationThrust1_UpperSat
	  section.data(80).logicalSrcIdx = 170;
	  section.data(80).dtTransOffset = 147;
	
	  ;% g0szqdi43g.bunacxzsnx4.SaturationThrust1_LowerSat
	  section.data(81).logicalSrcIdx = 171;
	  section.data(81).dtTransOffset = 148;
	
	  ;% g0szqdi43g.bunacxzsnx4.P_yaw_Gain
	  section.data(82).logicalSrcIdx = 172;
	  section.data(82).dtTransOffset = 149;
	
	  ;% g0szqdi43g.bunacxzsnx4.D_yaw_Gain
	  section.data(83).logicalSrcIdx = 173;
	  section.data(83).dtTransOffset = 150;
	
	  ;% g0szqdi43g.bunacxzsnx4.I_pr_Gain_jfaopuunxi
	  section.data(84).logicalSrcIdx = 174;
	  section.data(84).dtTransOffset = 151;
	
	  ;% g0szqdi43g.bunacxzsnx4.ThrustToMotorCommand_Gain
	  section.data(85).logicalSrcIdx = 175;
	  section.data(85).dtTransOffset = 152;
	
	  ;% g0szqdi43g.bunacxzsnx4.Saturation5_UpperSat
	  section.data(86).logicalSrcIdx = 176;
	  section.data(86).dtTransOffset = 153;
	
	  ;% g0szqdi43g.bunacxzsnx4.Saturation5_LowerSat
	  section.data(87).logicalSrcIdx = 177;
	  section.data(87).dtTransOffset = 154;
	
	  ;% g0szqdi43g.bunacxzsnx4.MotorDirections_Gain
	  section.data(88).logicalSrcIdx = 178;
	  section.data(88).dtTransOffset = 155;
	
	  ;% g0szqdi43g.bunacxzsnx4.Gain_Gain_gnftowihye
	  section.data(89).logicalSrcIdx = 179;
	  section.data(89).dtTransOffset = 159;
	
	  ;% g0szqdi43g.bunacxzsnx4.Gain1_Gain_agli0elevh
	  section.data(90).logicalSrcIdx = 180;
	  section.data(90).dtTransOffset = 160;
	
	  ;% g0szqdi43g.bunacxzsnx4.Gain1_Gain_cvsyya1dep
	  section.data(91).logicalSrcIdx = 181;
	  section.data(91).dtTransOffset = 161;
	
	  ;% g0szqdi43g.bunacxzsnx4.kp1_Gain
	  section.data(92).logicalSrcIdx = 182;
	  section.data(92).dtTransOffset = 162;
	
	  ;% g0szqdi43g.bunacxzsnx4.A_Value_cpfut3oxhe
	  section.data(93).logicalSrcIdx = 183;
	  section.data(93).dtTransOffset = 163;
	
	  ;% g0szqdi43g.bunacxzsnx4.B_Value_cxaggpr5gi
	  section.data(94).logicalSrcIdx = 184;
	  section.data(94).dtTransOffset = 167;
	
	  ;% g0szqdi43g.bunacxzsnx4.D_Value_ib5x31svyn
	  section.data(95).logicalSrcIdx = 185;
	  section.data(95).dtTransOffset = 171;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(7) = section;
      clear section
      
      section.nData     = 17;
      section.data(17)  = dumData; %prealloc
      
	  ;% g0szqdi43g.bunacxzsnx4.Constant_Value_jcujwaxsqd
	  section.data(1).logicalSrcIdx = 186;
	  section.data(1).dtTransOffset = 0;
	
	  ;% g0szqdi43g.bunacxzsnx4.Constant_Value_byup4ixvgw
	  section.data(2).logicalSrcIdx = 187;
	  section.data(2).dtTransOffset = 1;
	
	  ;% g0szqdi43g.bunacxzsnx4.Constant_Value_pd3xq2wbfg
	  section.data(3).logicalSrcIdx = 188;
	  section.data(3).dtTransOffset = 2;
	
	  ;% g0szqdi43g.bunacxzsnx4.Delay2_DelayLength
	  section.data(4).logicalSrcIdx = 189;
	  section.data(4).dtTransOffset = 3;
	
	  ;% g0szqdi43g.bunacxzsnx4.MemoryX_DelayLength
	  section.data(5).logicalSrcIdx = 190;
	  section.data(5).dtTransOffset = 4;
	
	  ;% g0szqdi43g.bunacxzsnx4.Output_InitialCondition
	  section.data(6).logicalSrcIdx = 191;
	  section.data(6).dtTransOffset = 5;
	
	  ;% g0szqdi43g.bunacxzsnx4.MemoryX_DelayLength_bv2gq234w5
	  section.data(7).logicalSrcIdx = 192;
	  section.data(7).dtTransOffset = 6;
	
	  ;% g0szqdi43g.bunacxzsnx4.Delay_DelayLength
	  section.data(8).logicalSrcIdx = 193;
	  section.data(8).dtTransOffset = 7;
	
	  ;% g0szqdi43g.bunacxzsnx4.Delay1_DelayLength
	  section.data(9).logicalSrcIdx = 194;
	  section.data(9).dtTransOffset = 8;
	
	  ;% g0szqdi43g.bunacxzsnx4.Delay_DelayLength_pi0d0epucc
	  section.data(10).logicalSrcIdx = 195;
	  section.data(10).dtTransOffset = 9;
	
	  ;% g0szqdi43g.bunacxzsnx4.Delay_DelayLength_hiexz20tay
	  section.data(11).logicalSrcIdx = 196;
	  section.data(11).dtTransOffset = 10;
	
	  ;% g0szqdi43g.bunacxzsnx4.Output_InitialCondit_idgxhbc3hg
	  section.data(12).logicalSrcIdx = 197;
	  section.data(12).dtTransOffset = 11;
	
	  ;% g0szqdi43g.bunacxzsnx4.FixPtConstant_Value
	  section.data(13).logicalSrcIdx = 198;
	  section.data(13).dtTransOffset = 12;
	
	  ;% g0szqdi43g.bunacxzsnx4.Output_InitialCondit_leqisbc2n4
	  section.data(14).logicalSrcIdx = 199;
	  section.data(14).dtTransOffset = 13;
	
	  ;% g0szqdi43g.bunacxzsnx4.DelayOneStep_DelayLength
	  section.data(15).logicalSrcIdx = 200;
	  section.data(15).dtTransOffset = 14;
	
	  ;% g0szqdi43g.bunacxzsnx4.FixPtConstant_Value_fmccqlbcyz
	  section.data(16).logicalSrcIdx = 201;
	  section.data(16).dtTransOffset = 15;
	
	  ;% g0szqdi43g.bunacxzsnx4.FixPtConstant_Value_ihob2ia4pa
	  section.data(17).logicalSrcIdx = 202;
	  section.data(17).dtTransOffset = 16;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(8) = section;
      clear section
      
      section.nData     = 2;
      section.data(2)  = dumData; %prealloc
      
	  ;% g0szqdi43g.bunacxzsnx4.controlModePosVsOrient_Value
	  section.data(1).logicalSrcIdx = 203;
	  section.data(1).dtTransOffset = 0;
	
	  ;% g0szqdi43g.bunacxzsnx4.Constant1_Value_o23ehrgwno
	  section.data(2).logicalSrcIdx = 204;
	  section.data(2).dtTransOffset = 1;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(9) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% g0szqdi43g.bunacxzsnx4.Merge_InitialOutput_byhzlb24ix
	  section.data(1).logicalSrcIdx = 205;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(10) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% g0szqdi43g.bunacxzsnx4.krd3iq0has.Constant_Value
	  section.data(1).logicalSrcIdx = 206;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(11) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% g0szqdi43g.bunacxzsnx4.ngabwultfc.Constant_Value
	  section.data(1).logicalSrcIdx = 207;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(12) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% g0szqdi43g.bunacxzsnx4.gwbl3rhyvj.Constant_Value
	  section.data(1).logicalSrcIdx = 208;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(13) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% g0szqdi43g.bunacxzsnx4.es3eej2ltm.Constant_Value
	  section.data(1).logicalSrcIdx = 209;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(14) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% g0szqdi43g.bunacxzsnx4.eltziyqckkg.Constant_Value
	  section.data(1).logicalSrcIdx = 210;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(15) = section;
      clear section
      
    
      ;%
      ;% Non-auto Data (parameter)
      ;%
    

    ;%
    ;% Add final counts to struct.
    ;%
    paramMap.nTotData = nTotData;
    


  ;%**************************
  ;% Create Block Output Map *
  ;%**************************
      
    nTotData      = 0; %add to this count as we go
    nTotSects     = 9;
    sectIdxOffset = 0;
    
    ;%
    ;% Define dummy sections & preallocate arrays
    ;%
    dumSection.nData = -1;  
    dumSection.data  = [];
    
    dumData.logicalSrcIdx = -1;
    dumData.dtTransOffset = -1;
    
    ;%
    ;% Init/prealloc sigMap
    ;%
    sigMap.nSections           = nTotSects;
    sigMap.sectIdxOffset       = sectIdxOffset;
      sigMap.sections(nTotSects) = dumSection; %prealloc
    sigMap.nTotData            = -1;
    
    ;%
    ;% Auto data (fsg2leehhck)
    ;%
      section.nData     = 6;
      section.data(6)  = dumData; %prealloc
      
	  ;% fsg2leehhck.j5hshxzpii
	  section.data(1).logicalSrcIdx = 2;
	  section.data(1).dtTransOffset = 0;
	
	  ;% fsg2leehhck.d3vqpg4m0e
	  section.data(2).logicalSrcIdx = 3;
	  section.data(2).dtTransOffset = 4800;
	
	  ;% fsg2leehhck.olojj5ysli
	  section.data(3).logicalSrcIdx = 4;
	  section.data(3).dtTransOffset = 9760;
	
	  ;% fsg2leehhck.fqccbjiifr
	  section.data(4).logicalSrcIdx = 5;
	  section.data(4).dtTransOffset = 19360;
	
	  ;% fsg2leehhck.azp4dj5avr
	  section.data(5).logicalSrcIdx = 6;
	  section.data(5).dtTransOffset = 24320;
	
	  ;% fsg2leehhck.dollcbz2fx
	  section.data(6).logicalSrcIdx = 7;
	  section.data(6).dtTransOffset = 43520;
	
      nTotData = nTotData + section.nData;
      sigMap.sections(1) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% fsg2leehhck.fhvmdtlnp1
	  section.data(1).logicalSrcIdx = 0;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      sigMap.sections(2) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% fsg2leehhck.kiq2kawr31
	  section.data(1).logicalSrcIdx = 1;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      sigMap.sections(3) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% fsg2leehhck.bunacxzsnx4.nbwsaanjtf
	  section.data(1).logicalSrcIdx = 8;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      sigMap.sections(4) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% fsg2leehhck.bunacxzsnx4.ia3k0b1ofd
	  section.data(1).logicalSrcIdx = 16;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      sigMap.sections(5) = section;
      clear section
      
      section.nData     = 7;
      section.data(7)  = dumData; %prealloc
      
	  ;% fsg2leehhck.bunacxzsnx4.lw1slkaroh
	  section.data(1).logicalSrcIdx = 9;
	  section.data(1).dtTransOffset = 0;
	
	  ;% fsg2leehhck.bunacxzsnx4.b5r0etbbxf
	  section.data(2).logicalSrcIdx = 10;
	  section.data(2).dtTransOffset = 1;
	
	  ;% fsg2leehhck.bunacxzsnx4.iec35zi55p
	  section.data(3).logicalSrcIdx = 11;
	  section.data(3).dtTransOffset = 2;
	
	  ;% fsg2leehhck.bunacxzsnx4.pfxjechfyy
	  section.data(4).logicalSrcIdx = 12;
	  section.data(4).dtTransOffset = 3;
	
	  ;% fsg2leehhck.bunacxzsnx4.beg00p1sku
	  section.data(5).logicalSrcIdx = 13;
	  section.data(5).dtTransOffset = 4;
	
	  ;% fsg2leehhck.bunacxzsnx4.k2fgmjfrl5
	  section.data(6).logicalSrcIdx = 14;
	  section.data(6).dtTransOffset = 5;
	
	  ;% fsg2leehhck.bunacxzsnx4.nd1nygfr1d
	  section.data(7).logicalSrcIdx = 15;
	  section.data(7).dtTransOffset = 6;
	
      nTotData = nTotData + section.nData;
      sigMap.sections(6) = section;
      clear section
      
      section.nData     = 4;
      section.data(4)  = dumData; %prealloc
      
	  ;% fsg2leehhck.bunacxzsnx4.c4laplt1c5
	  section.data(1).logicalSrcIdx = 17;
	  section.data(1).dtTransOffset = 0;
	
	  ;% fsg2leehhck.bunacxzsnx4.nnfvmst4oh
	  section.data(2).logicalSrcIdx = 20;
	  section.data(2).dtTransOffset = 2;
	
	  ;% fsg2leehhck.bunacxzsnx4.fisnk5dzka
	  section.data(3).logicalSrcIdx = 21;
	  section.data(3).dtTransOffset = 4;
	
	  ;% fsg2leehhck.bunacxzsnx4.j1xdo1m4ee
	  section.data(4).logicalSrcIdx = 22;
	  section.data(4).dtTransOffset = 7;
	
      nTotData = nTotData + section.nData;
      sigMap.sections(7) = section;
      clear section
      
      section.nData     = 19;
      section.data(19)  = dumData; %prealloc
      
	  ;% fsg2leehhck.bunacxzsnx4.oyc0lmver3
	  section.data(1).logicalSrcIdx = 18;
	  section.data(1).dtTransOffset = 0;
	
	  ;% fsg2leehhck.bunacxzsnx4.bsfdvpdmq4
	  section.data(2).logicalSrcIdx = 19;
	  section.data(2).dtTransOffset = 1;
	
	  ;% fsg2leehhck.bunacxzsnx4.gyvy3wtucm
	  section.data(3).logicalSrcIdx = 23;
	  section.data(3).dtTransOffset = 7;
	
	  ;% fsg2leehhck.bunacxzsnx4.j25eqe11p5
	  section.data(4).logicalSrcIdx = 24;
	  section.data(4).dtTransOffset = 8;
	
	  ;% fsg2leehhck.bunacxzsnx4.cryv3icmuw
	  section.data(5).logicalSrcIdx = 25;
	  section.data(5).dtTransOffset = 11;
	
	  ;% fsg2leehhck.bunacxzsnx4.ay3lcilsbz
	  section.data(6).logicalSrcIdx = 26;
	  section.data(6).dtTransOffset = 23;
	
	  ;% fsg2leehhck.bunacxzsnx4.iopscedcni
	  section.data(7).logicalSrcIdx = 27;
	  section.data(7).dtTransOffset = 24;
	
	  ;% fsg2leehhck.bunacxzsnx4.igfyqdwehz
	  section.data(8).logicalSrcIdx = 28;
	  section.data(8).dtTransOffset = 28;
	
	  ;% fsg2leehhck.bunacxzsnx4.hxjdxdn0nx
	  section.data(9).logicalSrcIdx = 29;
	  section.data(9).dtTransOffset = 30;
	
	  ;% fsg2leehhck.bunacxzsnx4.iy3hfcny33
	  section.data(10).logicalSrcIdx = 30;
	  section.data(10).dtTransOffset = 32;
	
	  ;% fsg2leehhck.bunacxzsnx4.detrsyvn4y
	  section.data(11).logicalSrcIdx = 31;
	  section.data(11).dtTransOffset = 34;
	
	  ;% fsg2leehhck.bunacxzsnx4.ksux2v0mod
	  section.data(12).logicalSrcIdx = 32;
	  section.data(12).dtTransOffset = 35;
	
	  ;% fsg2leehhck.bunacxzsnx4.pd1pv5xcnn
	  section.data(13).logicalSrcIdx = 33;
	  section.data(13).dtTransOffset = 36;
	
	  ;% fsg2leehhck.bunacxzsnx4.bboi0whg3m
	  section.data(14).logicalSrcIdx = 34;
	  section.data(14).dtTransOffset = 37;
	
	  ;% fsg2leehhck.bunacxzsnx4.fas5f4h2q3
	  section.data(15).logicalSrcIdx = 35;
	  section.data(15).dtTransOffset = 38;
	
	  ;% fsg2leehhck.bunacxzsnx4.blfx3vfkgf
	  section.data(16).logicalSrcIdx = 36;
	  section.data(16).dtTransOffset = 39;
	
	  ;% fsg2leehhck.bunacxzsnx4.gv000kjxwj
	  section.data(17).logicalSrcIdx = 37;
	  section.data(17).dtTransOffset = 40;
	
	  ;% fsg2leehhck.bunacxzsnx4.cwumsyuuzg
	  section.data(18).logicalSrcIdx = 38;
	  section.data(18).dtTransOffset = 41;
	
	  ;% fsg2leehhck.bunacxzsnx4.bfqiyxas2r
	  section.data(19).logicalSrcIdx = 39;
	  section.data(19).dtTransOffset = 42;
	
      nTotData = nTotData + section.nData;
      sigMap.sections(8) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% fsg2leehhck.bunacxzsnx4.poga3lpfls
	  section.data(1).logicalSrcIdx = 40;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      sigMap.sections(9) = section;
      clear section
      
    
      ;%
      ;% Non-auto Data (signal)
      ;%
    

    ;%
    ;% Add final counts to struct.
    ;%
    sigMap.nTotData = nTotData;
    


  ;%*******************
  ;% Create DWork Map *
  ;%*******************
      
    nTotData      = 0; %add to this count as we go
    nTotSects     = 21;
    sectIdxOffset = 9;
    
    ;%
    ;% Define dummy sections & preallocate arrays
    ;%
    dumSection.nData = -1;  
    dumSection.data  = [];
    
    dumData.logicalSrcIdx = -1;
    dumData.dtTransOffset = -1;
    
    ;%
    ;% Init/prealloc dworkMap
    ;%
    dworkMap.nSections           = nTotSects;
    dworkMap.sectIdxOffset       = sectIdxOffset;
      dworkMap.sections(nTotSects) = dumSection; %prealloc
    dworkMap.nTotData            = -1;
    
    ;%
    ;% Auto data (kywlqvr0ypq)
    ;%
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% kywlqvr0ypq.ocojtfmwbb
	  section.data(1).logicalSrcIdx = 0;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(1) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% kywlqvr0ypq.b5uxy2qsbo
	  section.data(1).logicalSrcIdx = 1;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(2) = section;
      clear section
      
      section.nData     = 4;
      section.data(4)  = dumData; %prealloc
      
	  ;% kywlqvr0ypq.ls22dadsnk
	  section.data(1).logicalSrcIdx = 2;
	  section.data(1).dtTransOffset = 0;
	
	  ;% kywlqvr0ypq.juwo1swzaf
	  section.data(2).logicalSrcIdx = 3;
	  section.data(2).dtTransOffset = 1;
	
	  ;% kywlqvr0ypq.hl3bpqzvvv
	  section.data(3).logicalSrcIdx = 4;
	  section.data(3).dtTransOffset = 2;
	
	  ;% kywlqvr0ypq.a4axt2fs0m
	  section.data(4).logicalSrcIdx = 5;
	  section.data(4).dtTransOffset = 4;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(3) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% kywlqvr0ypq.clywk5wpb5
	  section.data(1).logicalSrcIdx = 6;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(4) = section;
      clear section
      
      section.nData     = 2;
      section.data(2)  = dumData; %prealloc
      
	  ;% kywlqvr0ypq.achhms2nqc
	  section.data(1).logicalSrcIdx = 7;
	  section.data(1).dtTransOffset = 0;
	
	  ;% kywlqvr0ypq.ddi4fb0usd
	  section.data(2).logicalSrcIdx = 8;
	  section.data(2).dtTransOffset = 1;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(5) = section;
      clear section
      
      section.nData     = 10;
      section.data(10)  = dumData; %prealloc
      
	  ;% kywlqvr0ypq.bunacxzsnx4.brrd2yrshl
	  section.data(1).logicalSrcIdx = 9;
	  section.data(1).dtTransOffset = 0;
	
	  ;% kywlqvr0ypq.bunacxzsnx4.mmxmskrorv
	  section.data(2).logicalSrcIdx = 10;
	  section.data(2).dtTransOffset = 1;
	
	  ;% kywlqvr0ypq.bunacxzsnx4.epe1eu5z10
	  section.data(3).logicalSrcIdx = 11;
	  section.data(3).dtTransOffset = 3;
	
	  ;% kywlqvr0ypq.bunacxzsnx4.csywgycepf
	  section.data(4).logicalSrcIdx = 12;
	  section.data(4).dtTransOffset = 6;
	
	  ;% kywlqvr0ypq.bunacxzsnx4.p31vgqajvy
	  section.data(5).logicalSrcIdx = 13;
	  section.data(5).dtTransOffset = 7;
	
	  ;% kywlqvr0ypq.bunacxzsnx4.bmuelnxv5d
	  section.data(6).logicalSrcIdx = 15;
	  section.data(6).dtTransOffset = 8;
	
	  ;% kywlqvr0ypq.bunacxzsnx4.n00xpcnzjg
	  section.data(7).logicalSrcIdx = 16;
	  section.data(7).dtTransOffset = 9;
	
	  ;% kywlqvr0ypq.bunacxzsnx4.p0xr0zhgwz
	  section.data(8).logicalSrcIdx = 17;
	  section.data(8).dtTransOffset = 10;
	
	  ;% kywlqvr0ypq.bunacxzsnx4.nkd4lquw2b
	  section.data(9).logicalSrcIdx = 18;
	  section.data(9).dtTransOffset = 11;
	
	  ;% kywlqvr0ypq.bunacxzsnx4.aylrvnjg24
	  section.data(10).logicalSrcIdx = 19;
	  section.data(10).dtTransOffset = 12;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(6) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% kywlqvr0ypq.bunacxzsnx4.g05fvq0zk4.LoggedData
	  section.data(1).logicalSrcIdx = 20;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(7) = section;
      clear section
      
      section.nData     = 13;
      section.data(13)  = dumData; %prealloc
      
	  ;% kywlqvr0ypq.bunacxzsnx4.hlsmvpyw3k
	  section.data(1).logicalSrcIdx = 21;
	  section.data(1).dtTransOffset = 0;
	
	  ;% kywlqvr0ypq.bunacxzsnx4.oqntnkktic
	  section.data(2).logicalSrcIdx = 22;
	  section.data(2).dtTransOffset = 2;
	
	  ;% kywlqvr0ypq.bunacxzsnx4.nbbhr2nrgd
	  section.data(3).logicalSrcIdx = 23;
	  section.data(3).dtTransOffset = 5;
	
	  ;% kywlqvr0ypq.bunacxzsnx4.lknd0mtgvz
	  section.data(4).logicalSrcIdx = 24;
	  section.data(4).dtTransOffset = 10;
	
	  ;% kywlqvr0ypq.bunacxzsnx4.nytye0yu1m
	  section.data(5).logicalSrcIdx = 25;
	  section.data(5).dtTransOffset = 25;
	
	  ;% kywlqvr0ypq.bunacxzsnx4.ng3qama02i
	  section.data(6).logicalSrcIdx = 26;
	  section.data(6).dtTransOffset = 27;
	
	  ;% kywlqvr0ypq.bunacxzsnx4.owshvpbxim
	  section.data(7).logicalSrcIdx = 27;
	  section.data(7).dtTransOffset = 37;
	
	  ;% kywlqvr0ypq.bunacxzsnx4.ojyymstkd0
	  section.data(8).logicalSrcIdx = 29;
	  section.data(8).dtTransOffset = 39;
	
	  ;% kywlqvr0ypq.bunacxzsnx4.i3qweqpa5u
	  section.data(9).logicalSrcIdx = 30;
	  section.data(9).dtTransOffset = 41;
	
	  ;% kywlqvr0ypq.bunacxzsnx4.a5azncyq1h
	  section.data(10).logicalSrcIdx = 31;
	  section.data(10).dtTransOffset = 42;
	
	  ;% kywlqvr0ypq.bunacxzsnx4.gwkn4pg2uo
	  section.data(11).logicalSrcIdx = 32;
	  section.data(11).dtTransOffset = 43;
	
	  ;% kywlqvr0ypq.bunacxzsnx4.nadbsxaag5
	  section.data(12).logicalSrcIdx = 33;
	  section.data(12).dtTransOffset = 45;
	
	  ;% kywlqvr0ypq.bunacxzsnx4.gej4o1dclq
	  section.data(13).logicalSrcIdx = 34;
	  section.data(13).dtTransOffset = 47;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(8) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% kywlqvr0ypq.bunacxzsnx4.lero34awpw
	  section.data(1).logicalSrcIdx = 35;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(9) = section;
      clear section
      
      section.nData     = 3;
      section.data(3)  = dumData; %prealloc
      
	  ;% kywlqvr0ypq.bunacxzsnx4.deirpzy3m3
	  section.data(1).logicalSrcIdx = 36;
	  section.data(1).dtTransOffset = 0;
	
	  ;% kywlqvr0ypq.bunacxzsnx4.jusrbvsdpc
	  section.data(2).logicalSrcIdx = 37;
	  section.data(2).dtTransOffset = 1;
	
	  ;% kywlqvr0ypq.bunacxzsnx4.og1ruahet5
	  section.data(3).logicalSrcIdx = 38;
	  section.data(3).dtTransOffset = 2;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(10) = section;
      clear section
      
      section.nData     = 13;
      section.data(13)  = dumData; %prealloc
      
	  ;% kywlqvr0ypq.bunacxzsnx4.m0stneswv0
	  section.data(1).logicalSrcIdx = 40;
	  section.data(1).dtTransOffset = 0;
	
	  ;% kywlqvr0ypq.bunacxzsnx4.kap35wjyev
	  section.data(2).logicalSrcIdx = 42;
	  section.data(2).dtTransOffset = 3;
	
	  ;% kywlqvr0ypq.bunacxzsnx4.aqpaqr0hc1
	  section.data(3).logicalSrcIdx = 43;
	  section.data(3).dtTransOffset = 5;
	
	  ;% kywlqvr0ypq.bunacxzsnx4.ogmelwzflx
	  section.data(4).logicalSrcIdx = 44;
	  section.data(4).dtTransOffset = 6;
	
	  ;% kywlqvr0ypq.bunacxzsnx4.j0zbbzdnp3
	  section.data(5).logicalSrcIdx = 45;
	  section.data(5).dtTransOffset = 7;
	
	  ;% kywlqvr0ypq.bunacxzsnx4.oiq4a3kiae
	  section.data(6).logicalSrcIdx = 46;
	  section.data(6).dtTransOffset = 8;
	
	  ;% kywlqvr0ypq.bunacxzsnx4.k0epmpzzvm
	  section.data(7).logicalSrcIdx = 47;
	  section.data(7).dtTransOffset = 9;
	
	  ;% kywlqvr0ypq.bunacxzsnx4.ppz4hfw55y
	  section.data(8).logicalSrcIdx = 48;
	  section.data(8).dtTransOffset = 10;
	
	  ;% kywlqvr0ypq.bunacxzsnx4.e0h0c5wcso
	  section.data(9).logicalSrcIdx = 49;
	  section.data(9).dtTransOffset = 11;
	
	  ;% kywlqvr0ypq.bunacxzsnx4.h355atdxnr
	  section.data(10).logicalSrcIdx = 50;
	  section.data(10).dtTransOffset = 12;
	
	  ;% kywlqvr0ypq.bunacxzsnx4.gwkcryev2c
	  section.data(11).logicalSrcIdx = 51;
	  section.data(11).dtTransOffset = 13;
	
	  ;% kywlqvr0ypq.bunacxzsnx4.mflgthnxrb
	  section.data(12).logicalSrcIdx = 52;
	  section.data(12).dtTransOffset = 14;
	
	  ;% kywlqvr0ypq.bunacxzsnx4.ic2sbd2fxu
	  section.data(13).logicalSrcIdx = 53;
	  section.data(13).dtTransOffset = 15;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(11) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% kywlqvr0ypq.bunacxzsnx4.gomchoebsz
	  section.data(1).logicalSrcIdx = 54;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(12) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% kywlqvr0ypq.bunacxzsnx4.eh1r3z42xe
	  section.data(1).logicalSrcIdx = 55;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(13) = section;
      clear section
      
      section.nData     = 10;
      section.data(10)  = dumData; %prealloc
      
	  ;% kywlqvr0ypq.bunacxzsnx4.ka5rhw0zvf
	  section.data(1).logicalSrcIdx = 56;
	  section.data(1).dtTransOffset = 0;
	
	  ;% kywlqvr0ypq.bunacxzsnx4.eeyyft0b3y
	  section.data(2).logicalSrcIdx = 57;
	  section.data(2).dtTransOffset = 1;
	
	  ;% kywlqvr0ypq.bunacxzsnx4.orlnfgeosa
	  section.data(3).logicalSrcIdx = 58;
	  section.data(3).dtTransOffset = 2;
	
	  ;% kywlqvr0ypq.bunacxzsnx4.afdxhypgsa
	  section.data(4).logicalSrcIdx = 59;
	  section.data(4).dtTransOffset = 3;
	
	  ;% kywlqvr0ypq.bunacxzsnx4.clddvaw4fq
	  section.data(5).logicalSrcIdx = 60;
	  section.data(5).dtTransOffset = 4;
	
	  ;% kywlqvr0ypq.bunacxzsnx4.gusjluz1oo
	  section.data(6).logicalSrcIdx = 61;
	  section.data(6).dtTransOffset = 5;
	
	  ;% kywlqvr0ypq.bunacxzsnx4.ks2ozqckuc
	  section.data(7).logicalSrcIdx = 62;
	  section.data(7).dtTransOffset = 6;
	
	  ;% kywlqvr0ypq.bunacxzsnx4.bzpxfqxsh1
	  section.data(8).logicalSrcIdx = 63;
	  section.data(8).dtTransOffset = 7;
	
	  ;% kywlqvr0ypq.bunacxzsnx4.mvbcxtqa1w
	  section.data(9).logicalSrcIdx = 64;
	  section.data(9).dtTransOffset = 8;
	
	  ;% kywlqvr0ypq.bunacxzsnx4.ox3svkzqrf
	  section.data(10).logicalSrcIdx = 65;
	  section.data(10).dtTransOffset = 9;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(14) = section;
      clear section
      
      section.nData     = 4;
      section.data(4)  = dumData; %prealloc
      
	  ;% kywlqvr0ypq.bunacxzsnx4.dhbfi5pwjf
	  section.data(1).logicalSrcIdx = 66;
	  section.data(1).dtTransOffset = 0;
	
	  ;% kywlqvr0ypq.bunacxzsnx4.cxrjhx040e
	  section.data(2).logicalSrcIdx = 67;
	  section.data(2).dtTransOffset = 1;
	
	  ;% kywlqvr0ypq.bunacxzsnx4.gwt3umqrwz
	  section.data(3).logicalSrcIdx = 68;
	  section.data(3).dtTransOffset = 2;
	
	  ;% kywlqvr0ypq.bunacxzsnx4.kygpv2arkx
	  section.data(4).logicalSrcIdx = 69;
	  section.data(4).dtTransOffset = 3;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(15) = section;
      clear section
      
      section.nData     = 4;
      section.data(4)  = dumData; %prealloc
      
	  ;% kywlqvr0ypq.bunacxzsnx4.mogu3ug4c3
	  section.data(1).logicalSrcIdx = 70;
	  section.data(1).dtTransOffset = 0;
	
	  ;% kywlqvr0ypq.bunacxzsnx4.hhkoz143nj
	  section.data(2).logicalSrcIdx = 71;
	  section.data(2).dtTransOffset = 1;
	
	  ;% kywlqvr0ypq.bunacxzsnx4.m4a4jbe0aw
	  section.data(3).logicalSrcIdx = 72;
	  section.data(3).dtTransOffset = 2;
	
	  ;% kywlqvr0ypq.bunacxzsnx4.hvfcxd3ue4
	  section.data(4).logicalSrcIdx = 73;
	  section.data(4).dtTransOffset = 3;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(16) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% kywlqvr0ypq.bunacxzsnx4.krd3iq0has.cxab3r4qiu
	  section.data(1).logicalSrcIdx = 74;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(17) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% kywlqvr0ypq.bunacxzsnx4.ngabwultfc.cxab3r4qiu
	  section.data(1).logicalSrcIdx = 75;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(18) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% kywlqvr0ypq.bunacxzsnx4.gwbl3rhyvj.cxab3r4qiu
	  section.data(1).logicalSrcIdx = 76;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(19) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% kywlqvr0ypq.bunacxzsnx4.es3eej2ltm.cxab3r4qiu
	  section.data(1).logicalSrcIdx = 77;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(20) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% kywlqvr0ypq.bunacxzsnx4.eltziyqckkg.cxab3r4qiu
	  section.data(1).logicalSrcIdx = 78;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(21) = section;
      clear section
      
    
      ;%
      ;% Non-auto Data (dwork)
      ;%
    

    ;%
    ;% Add final counts to struct.
    ;%
    dworkMap.nTotData = nTotData;
    


  ;%
  ;% Add individual maps to base struct.
  ;%

  targMap.paramMap  = paramMap;    
  targMap.signalMap = sigMap;
  targMap.dworkMap  = dworkMap;
  
  ;%
  ;% Add checksums to base struct.
  ;%


  targMap.checksum0 = 3708991620;
  targMap.checksum1 = 2558677593;
  targMap.checksum2 = 3235676819;
  targMap.checksum3 = 2190466665;

