/*
 * flightControlSystem_private.h
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "flightControlSystem".
 *
 * Model version              : 1.638
 * Simulink Coder version : 9.1 (R2019a) 23-Nov-2018
 * C source code generated on : Tue Nov 24 19:35:22 2020
 *
 * Target selection: ert.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: ARM Compatible->ARM 9
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_flightControlSystem_private_h_
#define RTW_HEADER_flightControlSystem_private_h_
#include "rtwtypes.h"
#include "builtin_typeid_types.h"
#include "multiword_types.h"
#include "flightControlSystem.h"

/* Private macros used by the generated code to access rtModel */
#ifndef rtmSetTFinal
# define rtmSetTFinal(rtm, val)        ((rtm)->Timing.tFinal = (val))
#endif

/* Imported (extern) pointer block signals */
extern uint8_T *imRGB;                 /* '<Root>/Image Data' */
extern real32_T rt_powf_snf(real32_T u0, real32_T u1);
extern real_T rt_roundd_snf(real_T u);
extern void flightControlSystem_Geofencingerror(uint8_T *rty_Out1,
  P_Geofencingerror_flightControlSystem_T *localP);
extern void flightControlSystem_ControlSystem1_SetupRTR
  (RT_MODEL_flightControlSystem_T * const flightControlSystem_M,
   DW_ControlSystem1_flightControlSystem_T *localDW);
extern void flightControlSystem_ControlSystem1_Init
  (B_ControlSystem1_flightControlSystem_T *localB,
   DW_ControlSystem1_flightControlSystem_T *localDW,
   P_ControlSystem1_flightControlSystem_T *localP);
extern void flightControlSystem_ControlSystem1_Start
  (DW_ControlSystem1_flightControlSystem_T *localDW);
extern void flightControlSystem_ControlSystem1(RT_MODEL_flightControlSystem_T *
  const flightControlSystem_M, const CommandBus *rtu_ReferenceValueServerCmds,
  const SensorsBus *rtu_Sensors, const real_T rtu_VisionbasedData[7],
  B_ControlSystem1_flightControlSystem_T *localB,
  DW_ControlSystem1_flightControlSystem_T *localDW,
  P_ControlSystem1_flightControlSystem_T *localP);

#endif                           /* RTW_HEADER_flightControlSystem_private_h_ */
