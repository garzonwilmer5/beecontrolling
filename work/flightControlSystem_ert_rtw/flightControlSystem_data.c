/*
 * flightControlSystem_data.c
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "flightControlSystem".
 *
 * Model version              : 1.638
 * Simulink Coder version : 9.1 (R2019a) 23-Nov-2018
 * C source code generated on : Tue Nov 24 19:35:22 2020
 *
 * Target selection: ert.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: ARM Compatible->ARM 9
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "flightControlSystem.h"
#include "flightControlSystem_private.h"

/* Block parameters (default storage) */
P_flightControlSystem_T flightControlSystem_P = {
  /* Variable: Sensors
   * Referenced by:
   *   '<S52>/SaturationSonar'
   *   '<S99>/Constant'
   */
  {
    { 1.00596, 1.00383, 0.99454 },

    { 0.99861, 1.00644, 0.99997 },

    {
      { 0.0, 0.0, 0.0 },

      { 0.0, 0.0, 0.0 },
      190.0,
      0.707,

      { 1.00596, 0.0, 0.0, 0.0, 1.00383, 0.0, 0.0, 0.0, 0.99454 },

      { 0.09, -0.06, 0.33699999999999974 },

      { -50.0, -50.0, -50.0, 50.0, 50.0, 50.0 },
      190.0,
      0.707,

      { 0.99861, 0.0, 0.0, 0.0, 1.00644, 0.0, 0.0, 0.0, 0.99997 },

      { -0.0095, -0.0075, 0.0015 },

      { 0.0, 0.0, 0.0 },

      { -10.0, -10.0, -10.0, 10.0, 10.0, 10.0 },

      { 41.0, 41.0, 41.0, 41.0, 41.0, 41.0 },

      { 0.8, 0.8, 0.8, 0.025, 0.025, 0.025 },

      { 0.00021831529882618725, 0.00018641345254680647, 0.00037251068300213613,
        1.0651514622688397e-8, 1.3021327403798377e-8, 1.1929474437781302e-8 }
    },

    {
      1.0,
      41.0
    },
    -99.0,
    -9.0,

    {
      { -99.0, 0.0, 0.0, -9.0 },
      0.0,

      { 3.5, 70.0 }
    },
    1.0,
    1.225,
    12.01725,
    101270.95,

    { 0.99407531114557246, 0.99618461293246863, 1.0054899752649467,
      1.0013919347893572, 0.99360120821906917, 1.0000300009000269 },
    0.44,
    1.0
  },

  /* Expression: 1
   * Referenced by: '<S185>/Constant'
   */
  1.0,

  /* Expression: 1
   * Referenced by: '<S186>/Constant'
   */
  1.0,

  /* Expression: 0
   * Referenced by: '<S186>/Switch'
   */
  0.0,

  /* Expression: 1:160
   * Referenced by: '<S182>/Gain'
   */
  { 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0, 13.0, 14.0,
    15.0, 16.0, 17.0, 18.0, 19.0, 20.0, 21.0, 22.0, 23.0, 24.0, 25.0, 26.0, 27.0,
    28.0, 29.0, 30.0, 31.0, 32.0, 33.0, 34.0, 35.0, 36.0, 37.0, 38.0, 39.0, 40.0,
    41.0, 42.0, 43.0, 44.0, 45.0, 46.0, 47.0, 48.0, 49.0, 50.0, 51.0, 52.0, 53.0,
    54.0, 55.0, 56.0, 57.0, 58.0, 59.0, 60.0, 61.0, 62.0, 63.0, 64.0, 65.0, 66.0,
    67.0, 68.0, 69.0, 70.0, 71.0, 72.0, 73.0, 74.0, 75.0, 76.0, 77.0, 78.0, 79.0,
    80.0, 81.0, 82.0, 83.0, 84.0, 85.0, 86.0, 87.0, 88.0, 89.0, 90.0, 91.0, 92.0,
    93.0, 94.0, 95.0, 96.0, 97.0, 98.0, 99.0, 100.0, 101.0, 102.0, 103.0, 104.0,
    105.0, 106.0, 107.0, 108.0, 109.0, 110.0, 111.0, 112.0, 113.0, 114.0, 115.0,
    116.0, 117.0, 118.0, 119.0, 120.0, 121.0, 122.0, 123.0, 124.0, 125.0, 126.0,
    127.0, 128.0, 129.0, 130.0, 131.0, 132.0, 133.0, 134.0, 135.0, 136.0, 137.0,
    138.0, 139.0, 140.0, 141.0, 142.0, 143.0, 144.0, 145.0, 146.0, 147.0, 148.0,
    149.0, 150.0, 151.0, 152.0, 153.0, 154.0, 155.0, 156.0, 157.0, 158.0, 159.0,
    160.0 },

  /* Expression: 0
   * Referenced by: '<S185>/Switch'
   */
  0.0,

  /* Expression: 1:160
   * Referenced by: '<S181>/Gain'
   */
  { 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0, 13.0, 14.0,
    15.0, 16.0, 17.0, 18.0, 19.0, 20.0, 21.0, 22.0, 23.0, 24.0, 25.0, 26.0, 27.0,
    28.0, 29.0, 30.0, 31.0, 32.0, 33.0, 34.0, 35.0, 36.0, 37.0, 38.0, 39.0, 40.0,
    41.0, 42.0, 43.0, 44.0, 45.0, 46.0, 47.0, 48.0, 49.0, 50.0, 51.0, 52.0, 53.0,
    54.0, 55.0, 56.0, 57.0, 58.0, 59.0, 60.0, 61.0, 62.0, 63.0, 64.0, 65.0, 66.0,
    67.0, 68.0, 69.0, 70.0, 71.0, 72.0, 73.0, 74.0, 75.0, 76.0, 77.0, 78.0, 79.0,
    80.0, 81.0, 82.0, 83.0, 84.0, 85.0, 86.0, 87.0, 88.0, 89.0, 90.0, 91.0, 92.0,
    93.0, 94.0, 95.0, 96.0, 97.0, 98.0, 99.0, 100.0, 101.0, 102.0, 103.0, 104.0,
    105.0, 106.0, 107.0, 108.0, 109.0, 110.0, 111.0, 112.0, 113.0, 114.0, 115.0,
    116.0, 117.0, 118.0, 119.0, 120.0, 121.0, 122.0, 123.0, 124.0, 125.0, 126.0,
    127.0, 128.0, 129.0, 130.0, 131.0, 132.0, 133.0, 134.0, 135.0, 136.0, 137.0,
    138.0, 139.0, 140.0, 141.0, 142.0, 143.0, 144.0, 145.0, 146.0, 147.0, 148.0,
    149.0, 150.0, 151.0, 152.0, 153.0, 154.0, 155.0, 156.0, 157.0, 158.0, 159.0,
    160.0 },

  /* Expression: 1:160
   * Referenced by: '<S184>/Gain'
   */
  { 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0, 13.0, 14.0,
    15.0, 16.0, 17.0, 18.0, 19.0, 20.0, 21.0, 22.0, 23.0, 24.0, 25.0, 26.0, 27.0,
    28.0, 29.0, 30.0, 31.0, 32.0, 33.0, 34.0, 35.0, 36.0, 37.0, 38.0, 39.0, 40.0,
    41.0, 42.0, 43.0, 44.0, 45.0, 46.0, 47.0, 48.0, 49.0, 50.0, 51.0, 52.0, 53.0,
    54.0, 55.0, 56.0, 57.0, 58.0, 59.0, 60.0, 61.0, 62.0, 63.0, 64.0, 65.0, 66.0,
    67.0, 68.0, 69.0, 70.0, 71.0, 72.0, 73.0, 74.0, 75.0, 76.0, 77.0, 78.0, 79.0,
    80.0, 81.0, 82.0, 83.0, 84.0, 85.0, 86.0, 87.0, 88.0, 89.0, 90.0, 91.0, 92.0,
    93.0, 94.0, 95.0, 96.0, 97.0, 98.0, 99.0, 100.0, 101.0, 102.0, 103.0, 104.0,
    105.0, 106.0, 107.0, 108.0, 109.0, 110.0, 111.0, 112.0, 113.0, 114.0, 115.0,
    116.0, 117.0, 118.0, 119.0, 120.0, 121.0, 122.0, 123.0, 124.0, 125.0, 126.0,
    127.0, 128.0, 129.0, 130.0, 131.0, 132.0, 133.0, 134.0, 135.0, 136.0, 137.0,
    138.0, 139.0, 140.0, 141.0, 142.0, 143.0, 144.0, 145.0, 146.0, 147.0, 148.0,
    149.0, 150.0, 151.0, 152.0, 153.0, 154.0, 155.0, 156.0, 157.0, 158.0, 159.0,
    160.0 },

  /* Expression: 0.45
   * Referenced by: '<S2>/Umbral 0.7AR>7'
   */
  0.45,

  /* Expression: 1.5
   * Referenced by: '<S2>/Tolerancia superior'
   */
  1.5,

  /* Expression: 0.2
   * Referenced by: '<S2>/Tolerancia Inferior'
   */
  0.2,

  /* Expression: 1:120
   * Referenced by: '<S183>/Gain'
   */
  { 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0, 11.0, 12.0, 13.0, 14.0,
    15.0, 16.0, 17.0, 18.0, 19.0, 20.0, 21.0, 22.0, 23.0, 24.0, 25.0, 26.0, 27.0,
    28.0, 29.0, 30.0, 31.0, 32.0, 33.0, 34.0, 35.0, 36.0, 37.0, 38.0, 39.0, 40.0,
    41.0, 42.0, 43.0, 44.0, 45.0, 46.0, 47.0, 48.0, 49.0, 50.0, 51.0, 52.0, 53.0,
    54.0, 55.0, 56.0, 57.0, 58.0, 59.0, 60.0, 61.0, 62.0, 63.0, 64.0, 65.0, 66.0,
    67.0, 68.0, 69.0, 70.0, 71.0, 72.0, 73.0, 74.0, 75.0, 76.0, 77.0, 78.0, 79.0,
    80.0, 81.0, 82.0, 83.0, 84.0, 85.0, 86.0, 87.0, 88.0, 89.0, 90.0, 91.0, 92.0,
    93.0, 94.0, 95.0, 96.0, 97.0, 98.0, 99.0, 100.0, 101.0, 102.0, 103.0, 104.0,
    105.0, 106.0, 107.0, 108.0, 109.0, 110.0, 111.0, 112.0, 113.0, 114.0, 115.0,
    116.0, 117.0, 118.0, 119.0, 120.0 },

  /* Expression: 7
   * Referenced by: '<S2>/Ruido'
   */
  7.0,

  /* Expression: 0
   * Referenced by: '<Root>/Rate Transition'
   */
  0.0,

  /* Start of '<Root>/Control System1' */
  {
    /* Mask Parameter: outlierBelowFloor_const
     * Referenced by: '<S54>/Constant'
     */
    0.0,

    /* Mask Parameter: outlierJump_const
     * Referenced by: '<S102>/Constant'
     */
    0.1,

    /* Mask Parameter: currentEstimateVeryOffFromPressure_const
     * Referenced by: '<S100>/Constant'
     */
    0.8,

    /* Mask Parameter: currentStateVeryOffsonarflt_const
     * Referenced by: '<S101>/Constant'
     */
    0.4,

    /* Mask Parameter: Checkerrorcondition_const
     * Referenced by: '<S18>/Constant'
     */
    -1.0,

    /* Mask Parameter: u0continuousOFerrors_const
     * Referenced by: '<S17>/Constant'
     */
    50.0,

    /* Mask Parameter: DiscreteDerivative_ICPrevScaledInput
     * Referenced by: '<S118>/UD'
     */
    0.0F,

    /* Mask Parameter: DiscreteDerivative_ICPrevScaledInput_h
     * Referenced by: '<S39>/UD'
     */
    0.0F,

    /* Mask Parameter: CompareToConstant_const
     * Referenced by: '<S45>/Constant'
     */
    9.79038F,

    /* Mask Parameter: CompareToConstant1_const
     * Referenced by: '<S46>/Constant'
     */
    9.82962F,

    /* Mask Parameter: maxp_const
     * Referenced by: '<S121>/Constant'
     */
    0.6F,

    /* Mask Parameter: maxq_const
     * Referenced by: '<S123>/Constant'
     */
    0.6F,

    /* Mask Parameter: maxw1_const
     * Referenced by: '<S125>/Constant'
     */
    7.0F,

    /* Mask Parameter: maxw2_const
     * Referenced by: '<S126>/Constant'
     */
    7.0F,

    /* Mask Parameter: maxdw1_const
     * Referenced by: '<S119>/Constant'
     */
    80.0F,

    /* Mask Parameter: maxdw2_const
     * Referenced by: '<S120>/Constant'
     */
    80.0F,

    /* Mask Parameter: maxp2_const
     * Referenced by: '<S122>/Constant'
     */
    0.5F,

    /* Mask Parameter: maxq2_const
     * Referenced by: '<S124>/Constant'
     */
    0.5F,

    /* Mask Parameter: maxw3_const
     * Referenced by: '<S127>/Constant'
     */
    5.0F,

    /* Mask Parameter: maxw4_const
     * Referenced by: '<S128>/Constant'
     */
    5.0F,

    /* Mask Parameter: minHeightforOF_const
     * Referenced by: '<S129>/Constant'
     */
    -0.4F,

    /* Mask Parameter: DeactivateAccelerationIfOFisnotusedduetolowaltitude_const
     * Referenced by: '<S111>/Constant'
     */
    -0.4F,

    /* Mask Parameter: donotuseaccifopticalflowneveravailableNoteOF60HzbutZOHto200_con
     * Referenced by: '<S113>/Constant'
     */
    0.0F,

    /* Mask Parameter: donotuseaccifopticalflowneveravailableNoteOF60HzbutZOHto2001_co
     * Referenced by: '<S114>/Constant'
     */
    0.0F,

    /* Mask Parameter: CompareToConstant2_const
     * Referenced by: '<S21>/Constant'
     */
    6.0F,

    /* Mask Parameter: CompareToConstant4_const
     * Referenced by: '<S23>/Constant'
     */
    0.01F,

    /* Mask Parameter: CompareToConstant3_const
     * Referenced by: '<S22>/Constant'
     */
    6.0F,

    /* Mask Parameter: CompareToConstant5_const
     * Referenced by: '<S24>/Constant'
     */
    0.01F,

    /* Mask Parameter: CompareToConstant_const_p
     * Referenced by: '<S19>/Constant'
     */
    10.0F,

    /* Mask Parameter: CompareToConstant1_const_m
     * Referenced by: '<S20>/Constant'
     */
    10.0F,

    /* Mask Parameter: WrapToZero_Threshold
     * Referenced by: '<S16>/FixPt Switch'
     */
    4294967295U,

    /* Mask Parameter: WrapToZero_Threshold_f
     * Referenced by: '<S32>/FixPt Switch'
     */
    4294967295U,

    /* Mask Parameter: WrapToZero_Threshold_d
     * Referenced by: '<S117>/FixPt Switch'
     */
    4294967295U,

    /* Mask Parameter: CompareToConstant_const_o
     * Referenced by: '<S107>/Constant'
     */
    800U,

    /* Mask Parameter: CompareToConstant_const_a
     * Referenced by: '<S13>/Constant'
     */
    100U,

    /* Expression: 0
     * Referenced by: '<S4>/ '
     */
    0.0,

    /* Expression: 1
     * Referenced by: '<S37>/Constant6'
     */
    1.0,

    /* Expression: 0
     * Referenced by: '<S37>/Constant5'
     */
    0.0,

    /* Expression: 0
     * Referenced by: '<S37>/Constant2'
     */
    0.0,

    /* Expression: 50
     * Referenced by: '<S37>/Constant'
     */
    50.0,

    /* Expression: 0
     * Referenced by: '<S37>/Switch'
     */
    0.0,

    /* Expression: 0
     * Referenced by: '<S37>/Constant3'
     */
    0.0,

    /* Expression: 0
     * Referenced by: '<S97>/L*(y[k]-yhat[k|k-1])'
     */
    0.0,

    /* Expression: 0
     * Referenced by: '<S98>/deltax'
     */
    0.0,

    /* Expression: 0
     * Referenced by: '<S42>/Delay2'
     */
    0.0,

    /* Expression: pInitialization.X0
     * Referenced by: '<S51>/X0'
     */
    { -0.046, 0.0 },

    /* Expression: -inf
     * Referenced by: '<S52>/SaturationSonar'
     */
    0.0,

    /* Expression: Estimator.alt.filterSonarNum
     * Referenced by: '<S52>/soonarFilter_IIR'
     */
    { 3.7568380197861018E-6, 1.1270514059358305E-5, 1.1270514059358305E-5,
      3.7568380197861018E-6 },

    /* Expression: Estimator.alt.filterSonarDen
     * Referenced by: '<S52>/soonarFilter_IIR'
     */
    { 1.0, -2.9371707284498907, 2.8762997234793319, -0.939098940325283 },

    /* Expression: 0
     * Referenced by: '<S52>/soonarFilter_IIR'
     */
    0.0,

    /* Expression: pInitialization.M
     * Referenced by: '<S55>/KalmanGainM'
     */
    { 0.02624142064187163, 0.0697767360714917 },

    /* Expression: pInitialization.C
     * Referenced by: '<S51>/C'
     */
    { 1.0, 0.0 },

    /* Expression: pInitialization.M
     * Referenced by: '<S130>/KalmanGainM'
     */
    { 0.12546560898608972, 0.0, 0.0, 0.12546560898608972 },

    /* Expression: 2
     * Referenced by: '<S37>/Constant4'
     */
    2.0,

    /* Expression: 0.0
     * Referenced by: '<S37>/Delay'
     */
    0.0,

    /* Expression: 0.01
     * Referenced by: '<S37>/Gain'
     */
    0.01,

    /* Expression: 1.1
     * Referenced by: '<S37>/Gain1'
     */
    1.1,

    /* Expression: 20
     * Referenced by: '<S37>/Switch2'
     */
    20.0,

    /* Expression: 1
     * Referenced by: '<S5>/Pitch Aleteo'
     */
    1.0,

    /* Expression: 1
     * Referenced by: '<S5>/Pitch Exploracion'
     */
    1.0,

    /* Expression: 100
     * Referenced by: '<S5>/Tiempo de Frenado'
     */
    100.0,

    /* Expression: 1500
     * Referenced by: '<S5>/Tiempo de Calibracion'
     */
    1500.0,

    /* Expression: 80
     * Referenced by: '<S38>/Constant1'
     */
    80.0,

    /* Expression: 0.04*pi/180
     * Referenced by: '<S38>/Gain'
     */
    0.00069813170079773186,

    /* Expression: pInitialization.A
     * Referenced by: '<S51>/A'
     */
    { 1.0, 0.0, 0.005, 1.0 },

    /* Expression: pInitialization.L
     * Referenced by: '<S130>/KalmanGainL'
     */
    { 0.1254656089860898, 0.0, -0.0, 0.1254656089860898 },

    /* Expression: [0 0 -g]
     * Referenced by: '<S106>/gravity'
     */
    { 0.0, 0.0, -9.81 },

    /* Expression: [0 0 g]
     * Referenced by: '<S42>/gravity'
     */
    { 0.0, 0.0, 9.81 },

    /* Expression: Estimator.pos.accelerationInputGain
     * Referenced by: '<S106>/gainaccinput'
     */
    0.2,

    /* Expression: pInitialization.B
     * Referenced by: '<S51>/B'
     */
    { 0.0, 0.005 },

    /* Expression: pInitialization.D
     * Referenced by: '<S51>/D'
     */
    0.0,

    /* Expression: pInitialization.L
     * Referenced by: '<S55>/KalmanGainL'
     */
    { 0.026590304322229058, 0.06977673607149236 },

    /* Expression: 50
     * Referenced by: '<S37>/Constant1'
     */
    50.0,

    /* Expression: 0
     * Referenced by: '<S37>/Switch1'
     */
    0.0,

    /* Expression: 200*3
     * Referenced by: '<S4>/Wait  3 Seconds'
     */
    600.0,

    /* Expression: 0.0
     * Referenced by: '<S4>/Delay One Step'
     */
    0.0,

    /* Expression: 0.5
     * Referenced by: '<S4>/0.5 meters'
     */
    0.5,

    /* Expression: pInitialization.Z
     * Referenced by: '<S55>/CovarianceZ'
     */
    { 0.0026241420641871633, 0.0069776736071491688, 0.0069776736071491688,
      0.037607692935055337 },

    /* Expression: pInitialization.P0
     * Referenced by: '<S51>/P0'
     */
    { 0.0026948589925820388, 0.0071657120718244521, 0.0071657120718244521,
      0.03810769293505533 },

    /* Expression: pInitialization.Z
     * Referenced by: '<S130>/CovarianceZ'
     */
    { 0.62732804493044858, 0.0, 0.0, 0.62732804493044858 },

    /* Expression: pInitialization.G
     * Referenced by: '<S51>/G'
     */
    { 0.0, 1.0 },

    /* Expression: 0
     * Referenced by: '<S55>/ConstantP'
     */
    0.0,

    /* Expression: pInitialization.H
     * Referenced by: '<S51>/H'
     */
    0.0,

    /* Expression: pInitialization.N
     * Referenced by: '<S51>/N'
     */
    0.0,

    /* Expression: pInitialization.Q
     * Referenced by: '<S51>/Q'
     */
    0.0005,

    /* Expression: pInitialization.R
     * Referenced by: '<S51>/R'
     */
    0.1,

    /* Expression: 0
     * Referenced by: '<S130>/ConstantP'
     */
    0.0,

    /* Computed Parameter: D_xy_Gain
     * Referenced by: '<S9>/D_xy'
     */
    { 0.1F, -0.1F },

    /* Computed Parameter: Gain_Gain_e
     * Referenced by: '<S9>/Gain'
     */
    -1.0F,

    /* Computed Parameter: Saturation_UpperSat
     * Referenced by: '<S9>/Saturation'
     */
    3.0F,

    /* Computed Parameter: Saturation_LowerSat
     * Referenced by: '<S9>/Saturation'
     */
    -3.0F,

    /* Computed Parameter: P_xy_Gain
     * Referenced by: '<S9>/P_xy'
     */
    { -0.24F, 0.24F },

    /* Computed Parameter: D_z1_Gain
     * Referenced by: '<S11>/D_z1'
     */
    0.5F,

    /* Computed Parameter: P_z1_Gain
     * Referenced by: '<S11>/P_z1'
     */
    0.8F,

    /* Expression: Controller.takeoffGain
     * Referenced by: '<S11>/takeoff_gain1'
     */
    0.45F,

    /* Computed Parameter: _Value_m
     * Referenced by: '<S4>/    '
     */
    0.0F,

    /* Computed Parameter: kp_Gain
     * Referenced by: '<S34>/kp'
     */
    0.01F,

    /* Computed Parameter: degToRad_Gain
     * Referenced by: '<S34>/degToRad'
     */
    -0.0174532924F,

    /* Computed Parameter: Switch1_Threshold_l
     * Referenced by: '<S34>/Switch1'
     */
    0.0F,

    /* Computed Parameter: Out1_Y0
     * Referenced by: '<S40>/Out1'
     */
    0.0F,

    /* Computed Parameter: Gain_Gain_f
     * Referenced by: '<S40>/Gain'
     */
    -1.0F,

    /* Computed Parameter: Saturation_UpperSat_k
     * Referenced by: '<S40>/Saturation'
     */
    3.0F,

    /* Computed Parameter: Saturation_LowerSat_k
     * Referenced by: '<S40>/Saturation'
     */
    -3.0F,

    /* Computed Parameter: P_xy_Gain_o
     * Referenced by: '<S40>/P_xy'
     */
    { -0.24F, 0.24F },

    /* Computed Parameter: Switch1_Threshold_d
     * Referenced by: '<S38>/Switch1'
     */
    0.0F,

    /* Computed Parameter: Gain1_Gain_p
     * Referenced by: '<S47>/Gain1'
     */
    0.001F,

    /* Computed Parameter: Gain_Gain_n
     * Referenced by: '<S47>/Gain'
     */
    0.999F,

    /* Computed Parameter: Gain2_Gain
     * Referenced by: '<S47>/Gain2'
     */
    0.101936802F,

    /* Computed Parameter: Gain3_Gain
     * Referenced by: '<S47>/Gain3'
     */
    0.001F,

    /* Computed Parameter: Gain4_Gain
     * Referenced by: '<S47>/Gain4'
     */
    0.999F,

    /* Computed Parameter: Gain_Gain_c
     * Referenced by: '<S104>/Gain'
     */
    -1.0F,

    /* Computed Parameter: opticalFlowErrorCorrect_Gain
     * Referenced by: '<S104>/opticalFlowErrorCorrect'
     */
    1.15F,

    /* Computed Parameter: Lykyhatkk1_Y0_e
     * Referenced by: '<S172>/L*(y[k]-yhat[k|k-1])'
     */
    0.0F,

    /* Computed Parameter: deltax_Y0_m
     * Referenced by: '<S173>/deltax'
     */
    0.0F,

    /* Expression: Controller.Q2Ts
     * Referenced by: '<S8>/TorqueTotalThrustToThrustPerMotor'
     */
    { 0.25F, 0.25F, 0.25F, 0.25F, 103.573624F, -103.573624F, 103.573624F,
      -103.573624F, -5.66592F, -5.66592F, 5.66592F, 5.66592F, -5.66592F,
      5.66592F, 5.66592F, -5.66592F },

    /* Computed Parameter: SimplyIntegrateVelocity_gainval
     * Referenced by: '<S105>/SimplyIntegrateVelocity'
     */
    0.005F,

    /* Computed Parameter: SimplyIntegrateVelocity_IC
     * Referenced by: '<S105>/SimplyIntegrateVelocity'
     */
    0.0F,

    /* Computed Parameter: invertzaxisGain_Gain
     * Referenced by: '<S42>/invertzaxisGain'
     */
    -1.0F,

    /* Computed Parameter: prsToAltGain_Gain
     * Referenced by: '<S42>/prsToAltGain'
     */
    0.0832137167F,

    /* Computed Parameter: pressureFilter_IIR_NumCoef
     * Referenced by: '<S52>/pressureFilter_IIR'
     */
    { 3.75683794E-6F, 1.12705138E-5F, 1.12705138E-5F, 3.75683794E-6F },

    /* Computed Parameter: pressureFilter_IIR_DenCoef
     * Referenced by: '<S52>/pressureFilter_IIR'
     */
    { 1.0F, -2.93717074F, 2.87629962F, -0.939098954F },

    /* Computed Parameter: pressureFilter_IIR_InitialStates
     * Referenced by: '<S52>/pressureFilter_IIR'
     */
    0.0F,

    /* Expression: single(Estimator.complementaryFilterInit)
     * Referenced by: '<S41>/Memory'
     */
    { 0.0F, 0.0F, 0.0F },

    /* Expression: single(0)
     * Referenced by: '<S49>/Constant'
     */
    0.0F,

    /* Computed Parameter: Gain_Gain_b
     * Referenced by: '<S49>/Gain'
     */
    -1.0F,

    /* Computed Parameter: Assumingthatcalibwasdonelevel_Bias
     * Referenced by: '<S44>/Assuming that calib was done level!'
     */
    { 0.0F, 0.0F, 9.81F, 0.0F, 0.0F, 0.0F },

    /* Computed Parameter: inverseIMU_gain_Gain
     * Referenced by: '<S44>/inverseIMU_gain'
     */
    { 0.994075298F, 0.996184587F, 1.00549F, 1.00139189F, 0.993601203F, 1.00003F
    },

    /* Computed Parameter: IIR_IMUgyro_r_NumCoef
     * Referenced by: '<S44>/IIR_IMUgyro_r'
     */
    { 0.282124132F, 1.27253926F, 2.42084408F, 2.42084408F, 1.27253926F,
      0.282124132F },

    /* Computed Parameter: IIR_IMUgyro_r_DenCoef
     * Referenced by: '<S44>/IIR_IMUgyro_r'
     */
    { 1.0F, 2.22871494F, 2.52446198F, 1.57725322F, 0.54102242F, 0.0795623958F },

    /* Computed Parameter: IIR_IMUgyro_r_InitialStates
     * Referenced by: '<S44>/IIR_IMUgyro_r'
     */
    0.0F,

    /* Computed Parameter: Gain_Gain_m
     * Referenced by: '<S41>/Gain'
     */
    0.005F,

    /* Computed Parameter: FIR_IMUaccel_InitialStates
     * Referenced by: '<S44>/FIR_IMUaccel'
     */
    0.0F,

    /* Computed Parameter: FIR_IMUaccel_Coefficients
     * Referenced by: '<S44>/FIR_IMUaccel'
     */
    { 0.0264077242F, 0.140531361F, 0.33306092F, 0.33306092F, 0.140531361F,
      0.0264077242F },

    /* Computed Parameter: Constant_Value_o
     * Referenced by: '<S41>/Constant'
     */
    2.0F,

    /* Computed Parameter: Merge_InitialOutput
     * Referenced by: '<S41>/Merge'
     */
    0.0F,

    /* Computed Parameter: X0_Value_a
     * Referenced by: '<S110>/X0'
     */
    { 0.0F, 0.0F },

    /* Computed Parameter: C_Value_h
     * Referenced by: '<S110>/C'
     */
    { 1.0F, 0.0F, 0.0F, 1.0F },

    /* Computed Parameter: IIRgyroz_NumCoef
     * Referenced by: '<S109>/IIRgyroz'
     */
    { 0.282124132F, 1.27253926F, 2.42084408F, 2.42084408F, 1.27253926F,
      0.282124132F },

    /* Computed Parameter: IIRgyroz_DenCoef
     * Referenced by: '<S109>/IIRgyroz'
     */
    { 1.0F, 2.22871494F, 2.52446198F, 1.57725322F, 0.54102242F, 0.0795623958F },

    /* Computed Parameter: IIRgyroz_InitialStates
     * Referenced by: '<S109>/IIRgyroz'
     */
    0.0F,

    /* Computed Parameter: TSamp_WtEt
     * Referenced by: '<S118>/TSamp'
     */
    200.0F,

    /* Computed Parameter: Delay_InitialCondition_f
     * Referenced by: '<S104>/Delay'
     */
    0.0F,

    /* Computed Parameter: Delay1_InitialCondition
     * Referenced by: '<S6>/Delay1'
     */
    0.0F,

    /* Computed Parameter: D_pr_Gain
     * Referenced by: '<S7>/D_pr'
     */
    { 0.002F, 0.003F },

    /* Computed Parameter: DiscreteTimeIntegrator_gainval
     * Referenced by: '<S7>/Discrete-Time Integrator'
     */
    0.005F,

    /* Computed Parameter: DiscreteTimeIntegrator_IC
     * Referenced by: '<S7>/Discrete-Time Integrator'
     */
    0.0F,

    /* Computed Parameter: DiscreteTimeIntegrator_UpperSat
     * Referenced by: '<S7>/Discrete-Time Integrator'
     */
    2.0F,

    /* Computed Parameter: DiscreteTimeIntegrator_LowerSat
     * Referenced by: '<S7>/Discrete-Time Integrator'
     */
    -2.0F,

    /* Computed Parameter: I_pr_Gain
     * Referenced by: '<S7>/I_pr'
     */
    0.01F,

    /* Computed Parameter: AlturadeVuelo_Value
     * Referenced by: '<S5>/Altura de Vuelo'
     */
    -1.0F,

    /* Computed Parameter: Gain_Gain_d
     * Referenced by: '<S34>/Gain'
     */
    -1.0F,

    /* Computed Parameter: kp2_Gain
     * Referenced by: '<S34>/kp2'
     */
    0.01F,

    /* Computed Parameter: TSamp_WtEt_n
     * Referenced by: '<S39>/TSamp'
     */
    200.0F,

    /* Computed Parameter: DiscreteTimeIntegrator_gainval_e
     * Referenced by: '<S34>/Discrete-Time Integrator'
     */
    0.005F,

    /* Computed Parameter: DiscreteTimeIntegrator_IC_c
     * Referenced by: '<S34>/Discrete-Time Integrator'
     */
    0.0F,

    /* Computed Parameter: Switch_Threshold_n
     * Referenced by: '<S38>/Switch'
     */
    0.0F,

    /* Computed Parameter: Switch_Threshold_j
     * Referenced by: '<S36>/Switch'
     */
    0.0F,

    /* Computed Parameter: D_xy1_Gain
     * Referenced by: '<S36>/D_xy1'
     */
    { 0.1F, -0.1F },

    /* Computed Parameter: P_pr_Gain
     * Referenced by: '<S7>/P_pr'
     */
    { 0.013F, 0.011F },

    /* Computed Parameter: w1_Value
     * Referenced by: '<S11>/w1'
     */
    -0.61803F,

    /* Computed Parameter: DiscreteTimeIntegrator_gainval_o
     * Referenced by: '<S11>/Discrete-Time Integrator'
     */
    0.005F,

    /* Computed Parameter: DiscreteTimeIntegrator_IC_g
     * Referenced by: '<S11>/Discrete-Time Integrator'
     */
    0.0F,

    /* Computed Parameter: DiscreteTimeIntegrator_UpperSat_m
     * Referenced by: '<S11>/Discrete-Time Integrator'
     */
    2.0F,

    /* Computed Parameter: DiscreteTimeIntegrator_LowerSat_m
     * Referenced by: '<S11>/Discrete-Time Integrator'
     */
    -2.0F,

    /* Computed Parameter: SaturationThrust1_UpperSat
     * Referenced by: '<S11>/SaturationThrust1'
     */
    1.20204329F,

    /* Computed Parameter: SaturationThrust1_LowerSat
     * Referenced by: '<S11>/SaturationThrust1'
     */
    -1.20204329F,

    /* Computed Parameter: P_yaw_Gain
     * Referenced by: '<S10>/P_yaw'
     */
    0.004F,

    /* Computed Parameter: D_yaw_Gain
     * Referenced by: '<S10>/D_yaw'
     */
    0.0012F,

    /* Computed Parameter: ThrustToMotorCommand_Gain
     * Referenced by: '<S12>/ThrustToMotorCommand'
     */
    -1530.72681F,

    /* Expression: Vehicle.Motor.maxLimit
     * Referenced by: '<S12>/Saturation5'
     */
    500.0F,

    /* Expression: Vehicle.Motor.minLimit
     * Referenced by: '<S12>/Saturation5'
     */
    10.0F,

    /* Computed Parameter: MotorDirections_Gain
     * Referenced by: '<S12>/MotorDirections'
     */
    { 1.0F, -1.0F, 1.0F, -1.0F },

    /* Computed Parameter: A_Value_g
     * Referenced by: '<S110>/A'
     */
    { 1.0F, 0.0F, 0.0F, 1.0F },

    /* Computed Parameter: B_Value_j
     * Referenced by: '<S110>/B'
     */
    { 0.005F, 0.0F, 0.0F, 0.005F },

    /* Computed Parameter: D_Value_c
     * Referenced by: '<S110>/D'
     */
    { 0.0F, 0.0F, 0.0F, 0.0F },

    /* Computed Parameter: Delay_InitialCondition_c
     * Referenced by: '<S7>/Delay'
     */
    0.0F,

    /* Computed Parameter: antiWU_Gain_Gain
     * Referenced by: '<S7>/antiWU_Gain'
     */
    0.001F,

    /* Computed Parameter: I_pr_Gain_l
     * Referenced by: '<S11>/I_pr'
     */
    0.24F,

    /* Computed Parameter: kp1_Gain
     * Referenced by: '<S34>/kp1'
     */
    0.1F,

    /* Computed Parameter: Gain_Gain_ml
     * Referenced by: '<S4>/Gain'
     */
    1.0F,

    /* Computed Parameter: Gain1_Gain_g
     * Referenced by: '<S4>/Gain1'
     */
    1.0F,

    /* Computed Parameter: P0_Value_n
     * Referenced by: '<S110>/P0'
     */
    { 0.717328072F, 0.0F, 0.0F, 0.717328072F },

    /* Computed Parameter: G_Value_i
     * Referenced by: '<S110>/G'
     */
    { 1.0F, 0.0F, 0.0F, 1.0F },

    /* Computed Parameter: H_Value_n
     * Referenced by: '<S110>/H'
     */
    { 0.0F, 0.0F, 0.0F, 0.0F },

    /* Computed Parameter: N_Value_d
     * Referenced by: '<S110>/N'
     */
    { 0.0F, 0.0F, 0.0F, 0.0F },

    /* Computed Parameter: Q_Value_l
     * Referenced by: '<S110>/Q'
     */
    { 0.09F, 0.0F, 0.0F, 0.09F },

    /* Computed Parameter: R_Value_p
     * Referenced by: '<S110>/R'
     */
    { 5.0F, 0.0F, 0.0F, 5.0F },

    /* Computed Parameter: Constant_Value_m
     * Referenced by: '<S73>/Constant'
     */
    0.0F,

    /* Computed Parameter: Constant_Value_a
     * Referenced by: '<S148>/Constant'
     */
    0.0F,

    /* Computed Parameter: Delay2_DelayLength
     * Referenced by: '<S42>/Delay2'
     */
    1U,

    /* Computed Parameter: MemoryX_DelayLength
     * Referenced by: '<S51>/MemoryX'
     */
    1U,

    /* Computed Parameter: MemoryX_DelayLength_j
     * Referenced by: '<S110>/MemoryX'
     */
    1U,

    /* Computed Parameter: Output_InitialCondition
     * Referenced by: '<S108>/Output'
     */
    0U,

    /* Computed Parameter: Delay_DelayLength
     * Referenced by: '<S104>/Delay'
     */
    1U,

    /* Computed Parameter: Delay1_DelayLength
     * Referenced by: '<S6>/Delay1'
     */
    1U,

    /* Computed Parameter: Delay_DelayLength_k
     * Referenced by: '<S37>/Delay'
     */
    1U,

    /* Computed Parameter: Output_InitialCondition_g
     * Referenced by: '<S14>/Output'
     */
    0U,

    /* Computed Parameter: Delay_DelayLength_c
     * Referenced by: '<S7>/Delay'
     */
    1U,

    /* Computed Parameter: FixPtConstant_Value
     * Referenced by: '<S15>/FixPt Constant'
     */
    1U,

    /* Computed Parameter: Constant_Value_f
     * Referenced by: '<S16>/Constant'
     */
    0U,

    /* Computed Parameter: Output_InitialCondition_gu
     * Referenced by: '<S25>/Output'
     */
    0U,

    /* Computed Parameter: DelayOneStep_DelayLength
     * Referenced by: '<S4>/Delay One Step'
     */
    1U,

    /* Computed Parameter: FixPtConstant_Value_d
     * Referenced by: '<S31>/FixPt Constant'
     */
    1U,

    /* Computed Parameter: Constant_Value_m0
     * Referenced by: '<S32>/Constant'
     */
    0U,

    /* Computed Parameter: FixPtConstant_Value_p
     * Referenced by: '<S116>/FixPt Constant'
     */
    1U,

    /* Computed Parameter: Constant_Value_i
     * Referenced by: '<S117>/Constant'
     */
    0U,

    /* Computed Parameter: controlModePosVsOrient_Value
     * Referenced by: '<S1>/controlModePosVsOrient'
     */
    0,

    /* Computed Parameter: Constant1_Value_d
     * Referenced by: '<S5>/Constant1'
     */
    0,

    /* Computed Parameter: Constant_Value_b
     * Referenced by: '<S56>/Constant'
     */
    0,

    /* Computed Parameter: Constant_Value_op
     * Referenced by: '<S131>/Constant'
     */
    0,

    /* Expression: false()
     * Referenced by: '<S110>/Reset'
     */
    0,

    /* Computed Parameter: Merge_InitialOutput_i
     * Referenced by: '<S4>/Merge'
     */
    0U,

    /* Computed Parameter: ManualSwitchPZ_CurrentSetting
     * Referenced by: '<S51>/ManualSwitchPZ'
     */
    1U,

    /* Computed Parameter: ManualSwitchPZ_CurrentSetting_f
     * Referenced by: '<S110>/ManualSwitchPZ'
     */
    1U,

    /* Start of '<S4>/Normal condition' */
    {
      /* Computed Parameter: Constant_Value
       * Referenced by: '<S28>/Constant'
       */
      0U
    }
    ,

    /* End of '<S4>/Normal condition' */

    /* Start of '<S4>/Ultrasound improper' */
    {
      /* Computed Parameter: Constant_Value
       * Referenced by: '<S29>/Constant'
       */
      88U
    }
    ,

    /* End of '<S4>/Ultrasound improper' */

    /* Start of '<S4>/No optical flow ' */
    {
      /* Computed Parameter: Constant_Value
       * Referenced by: '<S27>/Constant'
       */
      69U
    }
    ,

    /* End of '<S4>/No optical flow ' */

    /* Start of '<S4>/estimator//Optical flow error' */
    {
      /* Computed Parameter: Constant_Value
       * Referenced by: '<S30>/Constant'
       */
      99U
    }
    ,

    /* End of '<S4>/estimator//Optical flow error' */

    /* Start of '<S4>/Geofencing error' */
    {
      /* Computed Parameter: Constant_Value
       * Referenced by: '<S26>/Constant'
       */
      1U
    }
    /* End of '<S4>/Geofencing error' */
  }
  /* End of '<Root>/Control System1' */
};
