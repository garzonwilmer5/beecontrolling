/*
 * flightControlSystem.h
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "flightControlSystem".
 *
 * Model version              : 1.638
 * Simulink Coder version : 9.1 (R2019a) 23-Nov-2018
 * C source code generated on : Tue Nov 24 19:35:22 2020
 *
 * Target selection: ert.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: ARM Compatible->ARM 9
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#ifndef RTW_HEADER_flightControlSystem_h_
#define RTW_HEADER_flightControlSystem_h_
#include <stddef.h>
#include <math.h>
#include <string.h>
#include <float.h>
#ifndef flightControlSystem_COMMON_INCLUDES_
# define flightControlSystem_COMMON_INCLUDES_
#include <stdlib.h>
#include "rtwtypes.h"
#include "rtw_continuous.h"
#include "rtw_solver.h"
#include "rt_logging.h"
#include "rsedu_image.h"
#endif                                /* flightControlSystem_COMMON_INCLUDES_ */

#include "flightControlSystem_types.h"

/* Shared type includes */
#include "multiword_types.h"
#include "MW_target_hardware_resources.h"
#include "rt_nonfinite.h"
#include "rtGetInf.h"

/* Macros for accessing real-time model data structure */
#ifndef rtmGetFinalTime
# define rtmGetFinalTime(rtm)          ((rtm)->Timing.tFinal)
#endif

#ifndef rtmGetRTWLogInfo
# define rtmGetRTWLogInfo(rtm)         ((rtm)->rtwLogInfo)
#endif

#ifndef rtmGetErrorStatus
# define rtmGetErrorStatus(rtm)        ((rtm)->errorStatus)
#endif

#ifndef rtmSetErrorStatus
# define rtmSetErrorStatus(rtm, val)   ((rtm)->errorStatus = (val))
#endif

#ifndef rtmStepTask
# define rtmStepTask(rtm, idx)         ((rtm)->Timing.TaskCounters.TID[(idx)] == 0)
#endif

#ifndef rtmGetStopRequested
# define rtmGetStopRequested(rtm)      ((rtm)->Timing.stopRequestedFlag)
#endif

#ifndef rtmSetStopRequested
# define rtmSetStopRequested(rtm, val) ((rtm)->Timing.stopRequestedFlag = (val))
#endif

#ifndef rtmGetStopRequestedPtr
# define rtmGetStopRequestedPtr(rtm)   (&((rtm)->Timing.stopRequestedFlag))
#endif

#ifndef rtmGetT
# define rtmGetT(rtm)                  ((rtm)->Timing.taskTime0)
#endif

#ifndef rtmGetTFinal
# define rtmGetTFinal(rtm)             ((rtm)->Timing.tFinal)
#endif

#ifndef rtmGetTPtr
# define rtmGetTPtr(rtm)               (&(rtm)->Timing.taskTime0)
#endif

#ifndef rtmTaskCounter
# define rtmTaskCounter(rtm, idx)      ((rtm)->Timing.TaskCounters.TID[(idx)])
#endif

/* Block signals for system '<Root>/Control System1' */
typedef struct {
  real32_T TmpSignalConversionAtToWorkspaceInport1[12];/* '<S1>/State Estimator' */
  CommandBus ControlModeUpdate;        /* '<S1>/Control Mode Update' */
  real32_T MathFunction_f[9];          /* '<S42>/Math Function' */
  real32_T Reshape9to3x3columnmajor[9];
                                /* '<S115>/Reshape (9) to [3x3] column-major' */
  real32_T MathFunction[9];            /* '<S105>/Math Function' */
  real32_T rtb_MathFunction_f_m[9];
  real_T ManualSwitchPZ[4];            /* '<S51>/ManualSwitchPZ' */
  real_T Product1[3];                  /* '<S106>/Product1' */
  real_T rtb_MathFunction_f_c[3];
  real32_T fv0[4];
  real32_T ManualSwitchPZ_n[4];        /* '<S110>/ManualSwitchPZ' */
  real32_T Switch[3];                  /* '<S104>/Switch' */
  real32_T MotorDirections[4];         /* '<S12>/MotorDirections' */
  real32_T FIR_IMUaccel[3];            /* '<S44>/FIR_IMUaccel' */
  real32_T sincos_o2[3];               /* '<S174>/sincos' */
  real_T invertzaxisGain;              /* '<S42>/invertzaxisGain' */
  real_T error;                        /* '<S33>/Subtract' */
  real_T Switch2;                      /* '<S37>/Switch2' */
  real_T Sum[3];                       /* '<S42>/Sum' */
  real_T Product2[2];                  /* '<S98>/Product2' */
  real_T Product3[2];                  /* '<S97>/Product3' */
  real32_T inverseIMU_gain[6];         /* '<S44>/inverseIMU_gain' */
  real_T denAccum;
  real_T u_a;                          /* '<S4>/   ' */
  real_T rtb_Add_j_idx_1;
  real_T unnamed_idx_1;
  real_T unnamed_idx_0;
  real_T unnamed_idx_1_k;
  real_T rtb_Product1_tmp;
  real_T rtb_Product1_tmp_c;
  real_T d0;
  real32_T Merge[2];                   /* '<S41>/Merge' */
  real32_T Product[2];                 /* '<S106>/Product' */
  real32_T Product2_o[2];              /* '<S173>/Product2' */
  real32_T Product3_m[2];              /* '<S172>/Product3' */
  real32_T P_xy[2];                    /* '<S40>/P_xy' */
  real32_T pitchrollerror_a[2];        /* '<S7>/Sum19' */
  real32_T Akxhatkk1_f[2];             /* '<S147>/A[k]*xhat[k|k-1]' */
  real32_T TSamp[2];                   /* '<S118>/TSamp' */
  real32_T fv1[2];
  real32_T fv2[2];
  real32_T yaw;                        /* '<S33>/State logic XYZ' */
  real32_T pitch;                      /* '<S33>/State logic XYZ' */
  real32_T fin;                        /* '<S33>/State logic XYZ' */
  real32_T roll;                       /* '<S33>/State logic XYZ' */
  real32_T poliAlpha;                  /* '<S33>/State logic XYZ' */
  real32_T poliBeta;                   /* '<S33>/State logic XYZ' */
  real32_T RefX;                       /* '<S33>/State logic XYZ' */
  real32_T RefY;                       /* '<S33>/State logic XYZ' */
  real32_T RefZ;                       /* '<S33>/State logic XYZ' */
  real32_T denAccum_b;
  real32_T rtb_Product_i_idx_2;
  real32_T rtb_sincos_o1_idx_0;
  int32_T denIdx;
  int32_T cff;
  int32_T memOffset;
  int32_T i;
  uint8_T Merge_b;                     /* '<S4>/Merge' */
  boolean_T Compare;                   /* '<S54>/Compare' */
  boolean_T nicemeasurementornewupdateneeded;
                                 /* '<S52>/nicemeasurementor newupdateneeded' */
  boolean_T LogicalOperator3;          /* '<S109>/Logical Operator3' */
} B_ControlSystem1_flightControlSystem_T;

/* Block states (default storage) for system '<Root>/Control System1' */
typedef struct {
  real_T Delay2_DSTATE;                /* '<S42>/Delay2' */
  real_T MemoryX_DSTATE[2];            /* '<S51>/MemoryX' */
  real_T soonarFilter_IIR_states[3];   /* '<S52>/soonarFilter_IIR' */
  real_T Delay_DSTATE;                 /* '<S37>/Delay' */
  real_T DelayOneStep_DSTATE;          /* '<S4>/Delay One Step' */
  real_T Etapa2;                       /* '<S33>/State logic XYZ' */
  real_T landSpot;                     /* '<S33>/State logic XYZ' */
  struct {
    void *LoggedData;
  } ToWorkspace_PWORK;                 /* '<S1>/To Workspace' */

  struct {
    void *LoggedData[6];
  } Scope_PWORK;                       /* '<S33>/Scope' */

  real32_T SimplyIntegrateVelocity_DSTATE[2];/* '<S105>/SimplyIntegrateVelocity' */
  real32_T pressureFilter_IIR_states[3];/* '<S52>/pressureFilter_IIR' */
  real32_T IIR_IMUgyro_r_states[5];    /* '<S44>/IIR_IMUgyro_r' */
  real32_T FIR_IMUaccel_states[15];    /* '<S44>/FIR_IMUaccel' */
  real32_T MemoryX_DSTATE_m[2];        /* '<S110>/MemoryX' */
  real32_T IIRgyroz_states[10];        /* '<S109>/IIRgyroz' */
  real32_T UD_DSTATE[2];               /* '<S118>/UD' */
  real32_T Delay_DSTATE_a[2];          /* '<S104>/Delay' */
  real32_T Delay1_DSTATE[2];           /* '<S6>/Delay1' */
  real32_T DiscreteTimeIntegrator_DSTATE[2];/* '<S7>/Discrete-Time Integrator' */
  real32_T UD_DSTATE_d;                /* '<S39>/UD' */
  real32_T DiscreteTimeIntegrator_DSTATE_j;/* '<S34>/Discrete-Time Integrator' */
  real32_T DiscreteTimeIntegrator_DSTATE_o;/* '<S11>/Discrete-Time Integrator' */
  real32_T Delay_DSTATE_b[2];          /* '<S7>/Delay' */
  int32_T FIR_IMUaccel_circBuf;        /* '<S44>/FIR_IMUaccel' */
  uint32_T Output_DSTATE;              /* '<S108>/Output' */
  uint32_T Output_DSTATE_c;            /* '<S14>/Output' */
  uint32_T Output_DSTATE_c1;           /* '<S25>/Output' */
  real32_T Memory_PreviousInput[3];    /* '<S41>/Memory' */
  real32_T IIRgyroz_tmp[2];            /* '<S109>/IIRgyroz' */
  real32_T PastPitch;                  /* '<S33>/State logic XYZ' */
  real32_T PastYaw;                    /* '<S33>/State logic XYZ' */
  real32_T zp;                         /* '<S33>/State logic XYZ' */
  uint32_T PT;                         /* '<S33>/State logic XYZ' */
  int8_T SimplyIntegrateVelocity_PrevResetState;/* '<S105>/SimplyIntegrateVelocity' */
  int8_T DiscreteTimeIntegrator_PrevResetState;/* '<S34>/Discrete-Time Integrator' */
  int8_T DiscreteTimeIntegrator_PrevResetState_j;/* '<S11>/Discrete-Time Integrator' */
  uint8_T icLoad;                      /* '<S51>/MemoryX' */
  uint8_T icLoad_c;                    /* '<S110>/MemoryX' */
  uint8_T is_active_c1_flightControlSystem;/* '<S33>/State logic XYZ' */
  uint8_T is_c1_flightControlSystem;   /* '<S33>/State logic XYZ' */
  boolean_T EnabledSubsystem_MODE;     /* '<S152>/Enabled Subsystem' */
  boolean_T MeasurementUpdate_MODE;    /* '<S147>/MeasurementUpdate' */
  boolean_T EnabledSubsystem_MODE_d;   /* '<S77>/Enabled Subsystem' */
  boolean_T MeasurementUpdate_MODE_a;  /* '<S72>/MeasurementUpdate' */
} DW_ControlSystem1_flightControlSystem_T;

/* Block signals (default storage) */
typedef struct {
  uint8_T imageBuff_1[19200];
  uint8_T imageBuff_2[19200];
  uint8_T imageBuff_3[19200];
  boolean_T UPDown[4960];              /* '<S178>/Submatrix1' */
  boolean_T Down[9600];                /* '<S178>/Submatrix3' */
  boolean_T Down_c[4960];              /* '<S178>/Submatrix4' */
  boolean_T BW[19200];                 /* '<S2>/MATLAB Function' */
  real_T Gain[160];                    /* '<S182>/Gain' */
  real_T histh[160];                   /* '<S178>/MATLABHisto2' */
  real_T SumofElements2_k[160];        /* '<S185>/Sum of Elements2' */
  real_T histv[120];                   /* '<S178>/MATLABHisto2' */
  real_T RateTransition[7];            /* '<Root>/Rate Transition' */
  boolean_T UP[9760];                  /* '<S178>/Submatrix' */
  boolean_T UpUP[4800];                /* '<S178>/Submatrix2' */
  real_T totalp;
  real_T Switch;                       /* '<S185>/Switch' */
  real_T d0;
  int32_T i0;
  int32_T i1;
  B_ControlSystem1_flightControlSystem_T ControlSystem1;/* '<Root>/Control System1' */
} B_flightControlSystem_T;

/* Block states (default storage) for system '<Root>' */
typedef struct {
  parrot_ImageProcess_flightControlSystem_T obj;/* '<S2>/PARROT Image Conversion' */
  volatile real_T RateTransition_Buffer[14];/* '<Root>/Rate Transition' */
  volatile int8_T RateTransition_ActiveBufIdx;/* '<Root>/Rate Transition' */
  boolean_T objisempty;                /* '<S2>/PARROT Image Conversion' */
  DW_ControlSystem1_flightControlSystem_T ControlSystem1;/* '<Root>/Control System1' */
} DW_flightControlSystem_T;

/* External inputs (root inport signals with default storage) */
typedef struct {
  CommandBus ACcmd;                    /* '<Root>/AC cmd' */
  SensorsBus Sensor;                   /* '<Root>/Sensor' */
} ExtU_flightControlSystem_T;

/* External outputs (root outports fed by signals with default storage) */
typedef struct {
  real32_T Actuators[4];               /* '<Root>/Actuators' */
  uint8_T Flag;                        /* '<Root>/Flag' */
} ExtY_flightControlSystem_T;

/* Parameters for system: '<S4>/Geofencing error' */
struct P_Geofencingerror_flightControlSystem_T_ {
  uint8_T Constant_Value;              /* Computed Parameter: Constant_Value
                                        * Referenced by: '<S26>/Constant'
                                        */
};

/* Parameters for system: '<Root>/Control System1' */
struct P_ControlSystem1_flightControlSystem_T_ {
  real_T outlierBelowFloor_const;     /* Mask Parameter: outlierBelowFloor_const
                                       * Referenced by: '<S54>/Constant'
                                       */
  real_T outlierJump_const;            /* Mask Parameter: outlierJump_const
                                        * Referenced by: '<S102>/Constant'
                                        */
  real_T currentEstimateVeryOffFromPressure_const;
                     /* Mask Parameter: currentEstimateVeryOffFromPressure_const
                      * Referenced by: '<S100>/Constant'
                      */
  real_T currentStateVeryOffsonarflt_const;
                            /* Mask Parameter: currentStateVeryOffsonarflt_const
                             * Referenced by: '<S101>/Constant'
                             */
  real_T Checkerrorcondition_const; /* Mask Parameter: Checkerrorcondition_const
                                     * Referenced by: '<S18>/Constant'
                                     */
  real_T u0continuousOFerrors_const;
                                   /* Mask Parameter: u0continuousOFerrors_const
                                    * Referenced by: '<S17>/Constant'
                                    */
  real32_T DiscreteDerivative_ICPrevScaledInput;
                         /* Mask Parameter: DiscreteDerivative_ICPrevScaledInput
                          * Referenced by: '<S118>/UD'
                          */
  real32_T DiscreteDerivative_ICPrevScaledInput_h;
                       /* Mask Parameter: DiscreteDerivative_ICPrevScaledInput_h
                        * Referenced by: '<S39>/UD'
                        */
  real32_T CompareToConstant_const;   /* Mask Parameter: CompareToConstant_const
                                       * Referenced by: '<S45>/Constant'
                                       */
  real32_T CompareToConstant1_const; /* Mask Parameter: CompareToConstant1_const
                                      * Referenced by: '<S46>/Constant'
                                      */
  real32_T maxp_const;                 /* Mask Parameter: maxp_const
                                        * Referenced by: '<S121>/Constant'
                                        */
  real32_T maxq_const;                 /* Mask Parameter: maxq_const
                                        * Referenced by: '<S123>/Constant'
                                        */
  real32_T maxw1_const;                /* Mask Parameter: maxw1_const
                                        * Referenced by: '<S125>/Constant'
                                        */
  real32_T maxw2_const;                /* Mask Parameter: maxw2_const
                                        * Referenced by: '<S126>/Constant'
                                        */
  real32_T maxdw1_const;               /* Mask Parameter: maxdw1_const
                                        * Referenced by: '<S119>/Constant'
                                        */
  real32_T maxdw2_const;               /* Mask Parameter: maxdw2_const
                                        * Referenced by: '<S120>/Constant'
                                        */
  real32_T maxp2_const;                /* Mask Parameter: maxp2_const
                                        * Referenced by: '<S122>/Constant'
                                        */
  real32_T maxq2_const;                /* Mask Parameter: maxq2_const
                                        * Referenced by: '<S124>/Constant'
                                        */
  real32_T maxw3_const;                /* Mask Parameter: maxw3_const
                                        * Referenced by: '<S127>/Constant'
                                        */
  real32_T maxw4_const;                /* Mask Parameter: maxw4_const
                                        * Referenced by: '<S128>/Constant'
                                        */
  real32_T minHeightforOF_const;       /* Mask Parameter: minHeightforOF_const
                                        * Referenced by: '<S129>/Constant'
                                        */
  real32_T DeactivateAccelerationIfOFisnotusedduetolowaltitude_const;
    /* Mask Parameter: DeactivateAccelerationIfOFisnotusedduetolowaltitude_const
     * Referenced by: '<S111>/Constant'
     */
  real32_T donotuseaccifopticalflowneveravailableNoteOF60HzbutZOHto200_con;
  /* Mask Parameter: donotuseaccifopticalflowneveravailableNoteOF60HzbutZOHto200_con
   * Referenced by: '<S113>/Constant'
   */
  real32_T donotuseaccifopticalflowneveravailableNoteOF60HzbutZOHto2001_co;
  /* Mask Parameter: donotuseaccifopticalflowneveravailableNoteOF60HzbutZOHto2001_co
   * Referenced by: '<S114>/Constant'
   */
  real32_T CompareToConstant2_const; /* Mask Parameter: CompareToConstant2_const
                                      * Referenced by: '<S21>/Constant'
                                      */
  real32_T CompareToConstant4_const; /* Mask Parameter: CompareToConstant4_const
                                      * Referenced by: '<S23>/Constant'
                                      */
  real32_T CompareToConstant3_const; /* Mask Parameter: CompareToConstant3_const
                                      * Referenced by: '<S22>/Constant'
                                      */
  real32_T CompareToConstant5_const; /* Mask Parameter: CompareToConstant5_const
                                      * Referenced by: '<S24>/Constant'
                                      */
  real32_T CompareToConstant_const_p;
                                    /* Mask Parameter: CompareToConstant_const_p
                                     * Referenced by: '<S19>/Constant'
                                     */
  real32_T CompareToConstant1_const_m;
                                   /* Mask Parameter: CompareToConstant1_const_m
                                    * Referenced by: '<S20>/Constant'
                                    */
  uint32_T WrapToZero_Threshold;       /* Mask Parameter: WrapToZero_Threshold
                                        * Referenced by: '<S16>/FixPt Switch'
                                        */
  uint32_T WrapToZero_Threshold_f;     /* Mask Parameter: WrapToZero_Threshold_f
                                        * Referenced by: '<S32>/FixPt Switch'
                                        */
  uint32_T WrapToZero_Threshold_d;     /* Mask Parameter: WrapToZero_Threshold_d
                                        * Referenced by: '<S117>/FixPt Switch'
                                        */
  uint32_T CompareToConstant_const_o;
                                    /* Mask Parameter: CompareToConstant_const_o
                                     * Referenced by: '<S107>/Constant'
                                     */
  uint32_T CompareToConstant_const_a;
                                    /* Mask Parameter: CompareToConstant_const_a
                                     * Referenced by: '<S13>/Constant'
                                     */
  real_T _Value;                       /* Expression: 0
                                        * Referenced by: '<S4>/ '
                                        */
  real_T Constant6_Value;              /* Expression: 1
                                        * Referenced by: '<S37>/Constant6'
                                        */
  real_T Constant5_Value;              /* Expression: 0
                                        * Referenced by: '<S37>/Constant5'
                                        */
  real_T Constant2_Value;              /* Expression: 0
                                        * Referenced by: '<S37>/Constant2'
                                        */
  real_T Constant_Value;               /* Expression: 50
                                        * Referenced by: '<S37>/Constant'
                                        */
  real_T Switch_Threshold;             /* Expression: 0
                                        * Referenced by: '<S37>/Switch'
                                        */
  real_T Constant3_Value;              /* Expression: 0
                                        * Referenced by: '<S37>/Constant3'
                                        */
  real_T Lykyhatkk1_Y0;                /* Expression: 0
                                        * Referenced by: '<S97>/L*(y[k]-yhat[k|k-1])'
                                        */
  real_T deltax_Y0;                    /* Expression: 0
                                        * Referenced by: '<S98>/deltax'
                                        */
  real_T Delay2_InitialCondition;      /* Expression: 0
                                        * Referenced by: '<S42>/Delay2'
                                        */
  real_T X0_Value[2];                  /* Expression: pInitialization.X0
                                        * Referenced by: '<S51>/X0'
                                        */
  real_T SaturationSonar_LowerSat;     /* Expression: -inf
                                        * Referenced by: '<S52>/SaturationSonar'
                                        */
  real_T soonarFilter_IIR_NumCoef[4];/* Expression: Estimator.alt.filterSonarNum
                                      * Referenced by: '<S52>/soonarFilter_IIR'
                                      */
  real_T soonarFilter_IIR_DenCoef[4];/* Expression: Estimator.alt.filterSonarDen
                                      * Referenced by: '<S52>/soonarFilter_IIR'
                                      */
  real_T soonarFilter_IIR_InitialStates;/* Expression: 0
                                         * Referenced by: '<S52>/soonarFilter_IIR'
                                         */
  real_T KalmanGainM_Value[2];         /* Expression: pInitialization.M
                                        * Referenced by: '<S55>/KalmanGainM'
                                        */
  real_T C_Value[2];                   /* Expression: pInitialization.C
                                        * Referenced by: '<S51>/C'
                                        */
  real_T KalmanGainM_Value_g[4];       /* Expression: pInitialization.M
                                        * Referenced by: '<S130>/KalmanGainM'
                                        */
  real_T Constant4_Value;              /* Expression: 2
                                        * Referenced by: '<S37>/Constant4'
                                        */
  real_T Delay_InitialCondition;       /* Expression: 0.0
                                        * Referenced by: '<S37>/Delay'
                                        */
  real_T Gain_Gain;                    /* Expression: 0.01
                                        * Referenced by: '<S37>/Gain'
                                        */
  real_T Gain1_Gain;                   /* Expression: 1.1
                                        * Referenced by: '<S37>/Gain1'
                                        */
  real_T Switch2_Threshold;            /* Expression: 20
                                        * Referenced by: '<S37>/Switch2'
                                        */
  real_T PitchAleteo_Value;            /* Expression: 1
                                        * Referenced by: '<S5>/Pitch Aleteo'
                                        */
  real_T PitchExploracion_Value;       /* Expression: 1
                                        * Referenced by: '<S5>/Pitch Exploracion'
                                        */
  real_T TiempodeFrenado_Value;        /* Expression: 100
                                        * Referenced by: '<S5>/Tiempo de Frenado'
                                        */
  real_T TiempodeCalibracion_Value;    /* Expression: 1500
                                        * Referenced by: '<S5>/Tiempo de Calibracion'
                                        */
  real_T Constant1_Value;              /* Expression: 80
                                        * Referenced by: '<S38>/Constant1'
                                        */
  real_T Gain_Gain_o;                  /* Expression: 0.04*pi/180
                                        * Referenced by: '<S38>/Gain'
                                        */
  real_T A_Value[4];                   /* Expression: pInitialization.A
                                        * Referenced by: '<S51>/A'
                                        */
  real_T KalmanGainL_Value[4];         /* Expression: pInitialization.L
                                        * Referenced by: '<S130>/KalmanGainL'
                                        */
  real_T gravity_Value[3];             /* Expression: [0 0 -g]
                                        * Referenced by: '<S106>/gravity'
                                        */
  real_T gravity_Value_n[3];           /* Expression: [0 0 g]
                                        * Referenced by: '<S42>/gravity'
                                        */
  real_T gainaccinput_Gain;   /* Expression: Estimator.pos.accelerationInputGain
                               * Referenced by: '<S106>/gainaccinput'
                               */
  real_T B_Value[2];                   /* Expression: pInitialization.B
                                        * Referenced by: '<S51>/B'
                                        */
  real_T D_Value;                      /* Expression: pInitialization.D
                                        * Referenced by: '<S51>/D'
                                        */
  real_T KalmanGainL_Value_i[2];       /* Expression: pInitialization.L
                                        * Referenced by: '<S55>/KalmanGainL'
                                        */
  real_T Constant1_Value_k;            /* Expression: 50
                                        * Referenced by: '<S37>/Constant1'
                                        */
  real_T Switch1_Threshold;            /* Expression: 0
                                        * Referenced by: '<S37>/Switch1'
                                        */
  real_T Wait3Seconds_Value;           /* Expression: 200*3
                                        * Referenced by: '<S4>/Wait  3 Seconds'
                                        */
  real_T DelayOneStep_InitialCondition;/* Expression: 0.0
                                        * Referenced by: '<S4>/Delay One Step'
                                        */
  real_T u5meters_Value;               /* Expression: 0.5
                                        * Referenced by: '<S4>/0.5 meters'
                                        */
  real_T CovarianceZ_Value[4];         /* Expression: pInitialization.Z
                                        * Referenced by: '<S55>/CovarianceZ'
                                        */
  real_T P0_Value[4];                  /* Expression: pInitialization.P0
                                        * Referenced by: '<S51>/P0'
                                        */
  real_T CovarianceZ_Value_j[4];       /* Expression: pInitialization.Z
                                        * Referenced by: '<S130>/CovarianceZ'
                                        */
  real_T G_Value[2];                   /* Expression: pInitialization.G
                                        * Referenced by: '<S51>/G'
                                        */
  real_T ConstantP_Value;              /* Expression: 0
                                        * Referenced by: '<S55>/ConstantP'
                                        */
  real_T H_Value;                      /* Expression: pInitialization.H
                                        * Referenced by: '<S51>/H'
                                        */
  real_T N_Value;                      /* Expression: pInitialization.N
                                        * Referenced by: '<S51>/N'
                                        */
  real_T Q_Value;                      /* Expression: pInitialization.Q
                                        * Referenced by: '<S51>/Q'
                                        */
  real_T R_Value;                      /* Expression: pInitialization.R
                                        * Referenced by: '<S51>/R'
                                        */
  real_T ConstantP_Value_n;            /* Expression: 0
                                        * Referenced by: '<S130>/ConstantP'
                                        */
  real32_T D_xy_Gain[2];               /* Computed Parameter: D_xy_Gain
                                        * Referenced by: '<S9>/D_xy'
                                        */
  real32_T Gain_Gain_e;                /* Computed Parameter: Gain_Gain_e
                                        * Referenced by: '<S9>/Gain'
                                        */
  real32_T Saturation_UpperSat;       /* Computed Parameter: Saturation_UpperSat
                                       * Referenced by: '<S9>/Saturation'
                                       */
  real32_T Saturation_LowerSat;       /* Computed Parameter: Saturation_LowerSat
                                       * Referenced by: '<S9>/Saturation'
                                       */
  real32_T P_xy_Gain[2];               /* Computed Parameter: P_xy_Gain
                                        * Referenced by: '<S9>/P_xy'
                                        */
  real32_T D_z1_Gain;                  /* Computed Parameter: D_z1_Gain
                                        * Referenced by: '<S11>/D_z1'
                                        */
  real32_T P_z1_Gain;                  /* Computed Parameter: P_z1_Gain
                                        * Referenced by: '<S11>/P_z1'
                                        */
  real32_T takeoff_gain1_Gain;         /* Expression: Controller.takeoffGain
                                        * Referenced by: '<S11>/takeoff_gain1'
                                        */
  real32_T _Value_m;                   /* Computed Parameter: _Value_m
                                        * Referenced by: '<S4>/    '
                                        */
  real32_T kp_Gain;                    /* Computed Parameter: kp_Gain
                                        * Referenced by: '<S34>/kp'
                                        */
  real32_T degToRad_Gain;              /* Computed Parameter: degToRad_Gain
                                        * Referenced by: '<S34>/degToRad'
                                        */
  real32_T Switch1_Threshold_l;       /* Computed Parameter: Switch1_Threshold_l
                                       * Referenced by: '<S34>/Switch1'
                                       */
  real32_T Out1_Y0;                    /* Computed Parameter: Out1_Y0
                                        * Referenced by: '<S40>/Out1'
                                        */
  real32_T Gain_Gain_f;                /* Computed Parameter: Gain_Gain_f
                                        * Referenced by: '<S40>/Gain'
                                        */
  real32_T Saturation_UpperSat_k;   /* Computed Parameter: Saturation_UpperSat_k
                                     * Referenced by: '<S40>/Saturation'
                                     */
  real32_T Saturation_LowerSat_k;   /* Computed Parameter: Saturation_LowerSat_k
                                     * Referenced by: '<S40>/Saturation'
                                     */
  real32_T P_xy_Gain_o[2];             /* Computed Parameter: P_xy_Gain_o
                                        * Referenced by: '<S40>/P_xy'
                                        */
  real32_T Switch1_Threshold_d;       /* Computed Parameter: Switch1_Threshold_d
                                       * Referenced by: '<S38>/Switch1'
                                       */
  real32_T Gain1_Gain_p;               /* Computed Parameter: Gain1_Gain_p
                                        * Referenced by: '<S47>/Gain1'
                                        */
  real32_T Gain_Gain_n;                /* Computed Parameter: Gain_Gain_n
                                        * Referenced by: '<S47>/Gain'
                                        */
  real32_T Gain2_Gain;                 /* Computed Parameter: Gain2_Gain
                                        * Referenced by: '<S47>/Gain2'
                                        */
  real32_T Gain3_Gain;                 /* Computed Parameter: Gain3_Gain
                                        * Referenced by: '<S47>/Gain3'
                                        */
  real32_T Gain4_Gain;                 /* Computed Parameter: Gain4_Gain
                                        * Referenced by: '<S47>/Gain4'
                                        */
  real32_T Gain_Gain_c;                /* Computed Parameter: Gain_Gain_c
                                        * Referenced by: '<S104>/Gain'
                                        */
  real32_T opticalFlowErrorCorrect_Gain;
                             /* Computed Parameter: opticalFlowErrorCorrect_Gain
                              * Referenced by: '<S104>/opticalFlowErrorCorrect'
                              */
  real32_T Lykyhatkk1_Y0_e;            /* Computed Parameter: Lykyhatkk1_Y0_e
                                        * Referenced by: '<S172>/L*(y[k]-yhat[k|k-1])'
                                        */
  real32_T deltax_Y0_m;                /* Computed Parameter: deltax_Y0_m
                                        * Referenced by: '<S173>/deltax'
                                        */
  real32_T TorqueTotalThrustToThrustPerMotor_Value[16];/* Expression: Controller.Q2Ts
                                                        * Referenced by: '<S8>/TorqueTotalThrustToThrustPerMotor'
                                                        */
  real32_T SimplyIntegrateVelocity_gainval;
                          /* Computed Parameter: SimplyIntegrateVelocity_gainval
                           * Referenced by: '<S105>/SimplyIntegrateVelocity'
                           */
  real32_T SimplyIntegrateVelocity_IC;
                               /* Computed Parameter: SimplyIntegrateVelocity_IC
                                * Referenced by: '<S105>/SimplyIntegrateVelocity'
                                */
  real32_T invertzaxisGain_Gain;     /* Computed Parameter: invertzaxisGain_Gain
                                      * Referenced by: '<S42>/invertzaxisGain'
                                      */
  real32_T prsToAltGain_Gain;          /* Computed Parameter: prsToAltGain_Gain
                                        * Referenced by: '<S42>/prsToAltGain'
                                        */
  real32_T pressureFilter_IIR_NumCoef[4];
                               /* Computed Parameter: pressureFilter_IIR_NumCoef
                                * Referenced by: '<S52>/pressureFilter_IIR'
                                */
  real32_T pressureFilter_IIR_DenCoef[4];
                               /* Computed Parameter: pressureFilter_IIR_DenCoef
                                * Referenced by: '<S52>/pressureFilter_IIR'
                                */
  real32_T pressureFilter_IIR_InitialStates;
                         /* Computed Parameter: pressureFilter_IIR_InitialStates
                          * Referenced by: '<S52>/pressureFilter_IIR'
                          */
  real32_T Memory_InitialCondition[3];
                        /* Expression: single(Estimator.complementaryFilterInit)
                         * Referenced by: '<S41>/Memory'
                         */
  real32_T Constant_Value_c;           /* Expression: single(0)
                                        * Referenced by: '<S49>/Constant'
                                        */
  real32_T Gain_Gain_b;                /* Computed Parameter: Gain_Gain_b
                                        * Referenced by: '<S49>/Gain'
                                        */
  real32_T Assumingthatcalibwasdonelevel_Bias[6];
                       /* Computed Parameter: Assumingthatcalibwasdonelevel_Bias
                        * Referenced by: '<S44>/Assuming that calib was done level!'
                        */
  real32_T inverseIMU_gain_Gain[6];  /* Computed Parameter: inverseIMU_gain_Gain
                                      * Referenced by: '<S44>/inverseIMU_gain'
                                      */
  real32_T IIR_IMUgyro_r_NumCoef[6];/* Computed Parameter: IIR_IMUgyro_r_NumCoef
                                     * Referenced by: '<S44>/IIR_IMUgyro_r'
                                     */
  real32_T IIR_IMUgyro_r_DenCoef[6];/* Computed Parameter: IIR_IMUgyro_r_DenCoef
                                     * Referenced by: '<S44>/IIR_IMUgyro_r'
                                     */
  real32_T IIR_IMUgyro_r_InitialStates;
                              /* Computed Parameter: IIR_IMUgyro_r_InitialStates
                               * Referenced by: '<S44>/IIR_IMUgyro_r'
                               */
  real32_T Gain_Gain_m;                /* Computed Parameter: Gain_Gain_m
                                        * Referenced by: '<S41>/Gain'
                                        */
  real32_T FIR_IMUaccel_InitialStates;
                               /* Computed Parameter: FIR_IMUaccel_InitialStates
                                * Referenced by: '<S44>/FIR_IMUaccel'
                                */
  real32_T FIR_IMUaccel_Coefficients[6];
                                /* Computed Parameter: FIR_IMUaccel_Coefficients
                                 * Referenced by: '<S44>/FIR_IMUaccel'
                                 */
  real32_T Constant_Value_o;           /* Computed Parameter: Constant_Value_o
                                        * Referenced by: '<S41>/Constant'
                                        */
  real32_T Merge_InitialOutput;       /* Computed Parameter: Merge_InitialOutput
                                       * Referenced by: '<S41>/Merge'
                                       */
  real32_T X0_Value_a[2];              /* Computed Parameter: X0_Value_a
                                        * Referenced by: '<S110>/X0'
                                        */
  real32_T C_Value_h[4];               /* Computed Parameter: C_Value_h
                                        * Referenced by: '<S110>/C'
                                        */
  real32_T IIRgyroz_NumCoef[6];        /* Computed Parameter: IIRgyroz_NumCoef
                                        * Referenced by: '<S109>/IIRgyroz'
                                        */
  real32_T IIRgyroz_DenCoef[6];        /* Computed Parameter: IIRgyroz_DenCoef
                                        * Referenced by: '<S109>/IIRgyroz'
                                        */
  real32_T IIRgyroz_InitialStates; /* Computed Parameter: IIRgyroz_InitialStates
                                    * Referenced by: '<S109>/IIRgyroz'
                                    */
  real32_T TSamp_WtEt;                 /* Computed Parameter: TSamp_WtEt
                                        * Referenced by: '<S118>/TSamp'
                                        */
  real32_T Delay_InitialCondition_f;
                                 /* Computed Parameter: Delay_InitialCondition_f
                                  * Referenced by: '<S104>/Delay'
                                  */
  real32_T Delay1_InitialCondition;
                                  /* Computed Parameter: Delay1_InitialCondition
                                   * Referenced by: '<S6>/Delay1'
                                   */
  real32_T D_pr_Gain[2];               /* Computed Parameter: D_pr_Gain
                                        * Referenced by: '<S7>/D_pr'
                                        */
  real32_T DiscreteTimeIntegrator_gainval;
                           /* Computed Parameter: DiscreteTimeIntegrator_gainval
                            * Referenced by: '<S7>/Discrete-Time Integrator'
                            */
  real32_T DiscreteTimeIntegrator_IC;
                                /* Computed Parameter: DiscreteTimeIntegrator_IC
                                 * Referenced by: '<S7>/Discrete-Time Integrator'
                                 */
  real32_T DiscreteTimeIntegrator_UpperSat;
                          /* Computed Parameter: DiscreteTimeIntegrator_UpperSat
                           * Referenced by: '<S7>/Discrete-Time Integrator'
                           */
  real32_T DiscreteTimeIntegrator_LowerSat;
                          /* Computed Parameter: DiscreteTimeIntegrator_LowerSat
                           * Referenced by: '<S7>/Discrete-Time Integrator'
                           */
  real32_T I_pr_Gain;                  /* Computed Parameter: I_pr_Gain
                                        * Referenced by: '<S7>/I_pr'
                                        */
  real32_T AlturadeVuelo_Value;       /* Computed Parameter: AlturadeVuelo_Value
                                       * Referenced by: '<S5>/Altura de Vuelo'
                                       */
  real32_T Gain_Gain_d;                /* Computed Parameter: Gain_Gain_d
                                        * Referenced by: '<S34>/Gain'
                                        */
  real32_T kp2_Gain;                   /* Computed Parameter: kp2_Gain
                                        * Referenced by: '<S34>/kp2'
                                        */
  real32_T TSamp_WtEt_n;               /* Computed Parameter: TSamp_WtEt_n
                                        * Referenced by: '<S39>/TSamp'
                                        */
  real32_T DiscreteTimeIntegrator_gainval_e;
                         /* Computed Parameter: DiscreteTimeIntegrator_gainval_e
                          * Referenced by: '<S34>/Discrete-Time Integrator'
                          */
  real32_T DiscreteTimeIntegrator_IC_c;
                              /* Computed Parameter: DiscreteTimeIntegrator_IC_c
                               * Referenced by: '<S34>/Discrete-Time Integrator'
                               */
  real32_T Switch_Threshold_n;         /* Computed Parameter: Switch_Threshold_n
                                        * Referenced by: '<S38>/Switch'
                                        */
  real32_T Switch_Threshold_j;         /* Computed Parameter: Switch_Threshold_j
                                        * Referenced by: '<S36>/Switch'
                                        */
  real32_T D_xy1_Gain[2];              /* Computed Parameter: D_xy1_Gain
                                        * Referenced by: '<S36>/D_xy1'
                                        */
  real32_T P_pr_Gain[2];               /* Computed Parameter: P_pr_Gain
                                        * Referenced by: '<S7>/P_pr'
                                        */
  real32_T w1_Value;                   /* Computed Parameter: w1_Value
                                        * Referenced by: '<S11>/w1'
                                        */
  real32_T DiscreteTimeIntegrator_gainval_o;
                         /* Computed Parameter: DiscreteTimeIntegrator_gainval_o
                          * Referenced by: '<S11>/Discrete-Time Integrator'
                          */
  real32_T DiscreteTimeIntegrator_IC_g;
                              /* Computed Parameter: DiscreteTimeIntegrator_IC_g
                               * Referenced by: '<S11>/Discrete-Time Integrator'
                               */
  real32_T DiscreteTimeIntegrator_UpperSat_m;
                        /* Computed Parameter: DiscreteTimeIntegrator_UpperSat_m
                         * Referenced by: '<S11>/Discrete-Time Integrator'
                         */
  real32_T DiscreteTimeIntegrator_LowerSat_m;
                        /* Computed Parameter: DiscreteTimeIntegrator_LowerSat_m
                         * Referenced by: '<S11>/Discrete-Time Integrator'
                         */
  real32_T SaturationThrust1_UpperSat;
                               /* Computed Parameter: SaturationThrust1_UpperSat
                                * Referenced by: '<S11>/SaturationThrust1'
                                */
  real32_T SaturationThrust1_LowerSat;
                               /* Computed Parameter: SaturationThrust1_LowerSat
                                * Referenced by: '<S11>/SaturationThrust1'
                                */
  real32_T P_yaw_Gain;                 /* Computed Parameter: P_yaw_Gain
                                        * Referenced by: '<S10>/P_yaw'
                                        */
  real32_T D_yaw_Gain;                 /* Computed Parameter: D_yaw_Gain
                                        * Referenced by: '<S10>/D_yaw'
                                        */
  real32_T ThrustToMotorCommand_Gain;
                                /* Computed Parameter: ThrustToMotorCommand_Gain
                                 * Referenced by: '<S12>/ThrustToMotorCommand'
                                 */
  real32_T Saturation5_UpperSat;       /* Expression: Vehicle.Motor.maxLimit
                                        * Referenced by: '<S12>/Saturation5'
                                        */
  real32_T Saturation5_LowerSat;       /* Expression: Vehicle.Motor.minLimit
                                        * Referenced by: '<S12>/Saturation5'
                                        */
  real32_T MotorDirections_Gain[4];  /* Computed Parameter: MotorDirections_Gain
                                      * Referenced by: '<S12>/MotorDirections'
                                      */
  real32_T A_Value_g[4];               /* Computed Parameter: A_Value_g
                                        * Referenced by: '<S110>/A'
                                        */
  real32_T B_Value_j[4];               /* Computed Parameter: B_Value_j
                                        * Referenced by: '<S110>/B'
                                        */
  real32_T D_Value_c[4];               /* Computed Parameter: D_Value_c
                                        * Referenced by: '<S110>/D'
                                        */
  real32_T Delay_InitialCondition_c;
                                 /* Computed Parameter: Delay_InitialCondition_c
                                  * Referenced by: '<S7>/Delay'
                                  */
  real32_T antiWU_Gain_Gain;           /* Computed Parameter: antiWU_Gain_Gain
                                        * Referenced by: '<S7>/antiWU_Gain'
                                        */
  real32_T I_pr_Gain_l;                /* Computed Parameter: I_pr_Gain_l
                                        * Referenced by: '<S11>/I_pr'
                                        */
  real32_T kp1_Gain;                   /* Computed Parameter: kp1_Gain
                                        * Referenced by: '<S34>/kp1'
                                        */
  real32_T Gain_Gain_ml;               /* Computed Parameter: Gain_Gain_ml
                                        * Referenced by: '<S4>/Gain'
                                        */
  real32_T Gain1_Gain_g;               /* Computed Parameter: Gain1_Gain_g
                                        * Referenced by: '<S4>/Gain1'
                                        */
  real32_T P0_Value_n[4];              /* Computed Parameter: P0_Value_n
                                        * Referenced by: '<S110>/P0'
                                        */
  real32_T G_Value_i[4];               /* Computed Parameter: G_Value_i
                                        * Referenced by: '<S110>/G'
                                        */
  real32_T H_Value_n[4];               /* Computed Parameter: H_Value_n
                                        * Referenced by: '<S110>/H'
                                        */
  real32_T N_Value_d[4];               /* Computed Parameter: N_Value_d
                                        * Referenced by: '<S110>/N'
                                        */
  real32_T Q_Value_l[4];               /* Computed Parameter: Q_Value_l
                                        * Referenced by: '<S110>/Q'
                                        */
  real32_T R_Value_p[4];               /* Computed Parameter: R_Value_p
                                        * Referenced by: '<S110>/R'
                                        */
  real32_T Constant_Value_m;           /* Computed Parameter: Constant_Value_m
                                        * Referenced by: '<S73>/Constant'
                                        */
  real32_T Constant_Value_a;           /* Computed Parameter: Constant_Value_a
                                        * Referenced by: '<S148>/Constant'
                                        */
  uint32_T Delay2_DelayLength;         /* Computed Parameter: Delay2_DelayLength
                                        * Referenced by: '<S42>/Delay2'
                                        */
  uint32_T MemoryX_DelayLength;       /* Computed Parameter: MemoryX_DelayLength
                                       * Referenced by: '<S51>/MemoryX'
                                       */
  uint32_T MemoryX_DelayLength_j;   /* Computed Parameter: MemoryX_DelayLength_j
                                     * Referenced by: '<S110>/MemoryX'
                                     */
  uint32_T Output_InitialCondition;
                                  /* Computed Parameter: Output_InitialCondition
                                   * Referenced by: '<S108>/Output'
                                   */
  uint32_T Delay_DelayLength;          /* Computed Parameter: Delay_DelayLength
                                        * Referenced by: '<S104>/Delay'
                                        */
  uint32_T Delay1_DelayLength;         /* Computed Parameter: Delay1_DelayLength
                                        * Referenced by: '<S6>/Delay1'
                                        */
  uint32_T Delay_DelayLength_k;       /* Computed Parameter: Delay_DelayLength_k
                                       * Referenced by: '<S37>/Delay'
                                       */
  uint32_T Output_InitialCondition_g;
                                /* Computed Parameter: Output_InitialCondition_g
                                 * Referenced by: '<S14>/Output'
                                 */
  uint32_T Delay_DelayLength_c;       /* Computed Parameter: Delay_DelayLength_c
                                       * Referenced by: '<S7>/Delay'
                                       */
  uint32_T FixPtConstant_Value;       /* Computed Parameter: FixPtConstant_Value
                                       * Referenced by: '<S15>/FixPt Constant'
                                       */
  uint32_T Constant_Value_f;           /* Computed Parameter: Constant_Value_f
                                        * Referenced by: '<S16>/Constant'
                                        */
  uint32_T Output_InitialCondition_gu;
                               /* Computed Parameter: Output_InitialCondition_gu
                                * Referenced by: '<S25>/Output'
                                */
  uint32_T DelayOneStep_DelayLength;
                                 /* Computed Parameter: DelayOneStep_DelayLength
                                  * Referenced by: '<S4>/Delay One Step'
                                  */
  uint32_T FixPtConstant_Value_d;   /* Computed Parameter: FixPtConstant_Value_d
                                     * Referenced by: '<S31>/FixPt Constant'
                                     */
  uint32_T Constant_Value_m0;          /* Computed Parameter: Constant_Value_m0
                                        * Referenced by: '<S32>/Constant'
                                        */
  uint32_T FixPtConstant_Value_p;   /* Computed Parameter: FixPtConstant_Value_p
                                     * Referenced by: '<S116>/FixPt Constant'
                                     */
  uint32_T Constant_Value_i;           /* Computed Parameter: Constant_Value_i
                                        * Referenced by: '<S117>/Constant'
                                        */
  boolean_T controlModePosVsOrient_Value;
                             /* Computed Parameter: controlModePosVsOrient_Value
                              * Referenced by: '<S1>/controlModePosVsOrient'
                              */
  boolean_T Constant1_Value_d;         /* Computed Parameter: Constant1_Value_d
                                        * Referenced by: '<S5>/Constant1'
                                        */
  boolean_T Constant_Value_b;          /* Computed Parameter: Constant_Value_b
                                        * Referenced by: '<S56>/Constant'
                                        */
  boolean_T Constant_Value_op;         /* Computed Parameter: Constant_Value_op
                                        * Referenced by: '<S131>/Constant'
                                        */
  boolean_T Reset_Value;               /* Expression: false()
                                        * Referenced by: '<S110>/Reset'
                                        */
  uint8_T Merge_InitialOutput_i;    /* Computed Parameter: Merge_InitialOutput_i
                                     * Referenced by: '<S4>/Merge'
                                     */
  uint8_T ManualSwitchPZ_CurrentSetting;
                            /* Computed Parameter: ManualSwitchPZ_CurrentSetting
                             * Referenced by: '<S51>/ManualSwitchPZ'
                             */
  uint8_T ManualSwitchPZ_CurrentSetting_f;
                          /* Computed Parameter: ManualSwitchPZ_CurrentSetting_f
                           * Referenced by: '<S110>/ManualSwitchPZ'
                           */
  P_Geofencingerror_flightControlSystem_T Normalcondition;/* '<S4>/Normal condition' */
  P_Geofencingerror_flightControlSystem_T Ultrasoundimproper;/* '<S4>/Ultrasound improper' */
  P_Geofencingerror_flightControlSystem_T Noopticalflow;/* '<S4>/No optical flow ' */
  P_Geofencingerror_flightControlSystem_T estimatorOpticalflowerror;
                                      /* '<S4>/estimator//Optical flow error' */
  P_Geofencingerror_flightControlSystem_T Geofencingerror;/* '<S4>/Geofencing error' */
};

/* Parameters (default storage) */
struct P_flightControlSystem_T_ {
  struct_8SSZ93PxvPkADZcA4gG8MD Sensors;/* Variable: Sensors
                                         * Referenced by:
                                         *   '<S52>/SaturationSonar'
                                         *   '<S99>/Constant'
                                         */
  real_T Constant_Value;               /* Expression: 1
                                        * Referenced by: '<S185>/Constant'
                                        */
  real_T Constant_Value_d;             /* Expression: 1
                                        * Referenced by: '<S186>/Constant'
                                        */
  real_T Switch_Threshold;             /* Expression: 0
                                        * Referenced by: '<S186>/Switch'
                                        */
  real_T Gain_Gain[160];               /* Expression: 1:160
                                        * Referenced by: '<S182>/Gain'
                                        */
  real_T Switch_Threshold_e;           /* Expression: 0
                                        * Referenced by: '<S185>/Switch'
                                        */
  real_T Gain_Gain_m[160];             /* Expression: 1:160
                                        * Referenced by: '<S181>/Gain'
                                        */
  real_T Gain_Gain_c[160];             /* Expression: 1:160
                                        * Referenced by: '<S184>/Gain'
                                        */
  real_T Umbral07AR7_Value;            /* Expression: 0.45
                                        * Referenced by: '<S2>/Umbral 0.7AR>7'
                                        */
  real_T Toleranciasuperior_Value;     /* Expression: 1.5
                                        * Referenced by: '<S2>/Tolerancia superior'
                                        */
  real_T ToleranciaInferior_Value;     /* Expression: 0.2
                                        * Referenced by: '<S2>/Tolerancia Inferior'
                                        */
  real_T Gain_Gain_mb[120];            /* Expression: 1:120
                                        * Referenced by: '<S183>/Gain'
                                        */
  real_T Ruido_Value;                  /* Expression: 7
                                        * Referenced by: '<S2>/Ruido'
                                        */
  real_T RateTransition_InitialCondition;/* Expression: 0
                                          * Referenced by: '<Root>/Rate Transition'
                                          */
  P_ControlSystem1_flightControlSystem_T ControlSystem1;/* '<Root>/Control System1' */
};

/* Real-time Model Data Structure */
struct tag_RTM_flightControlSystem_T {
  const char_T *errorStatus;
  RTWLogInfo *rtwLogInfo;

  /*
   * Timing:
   * The following substructure contains information regarding
   * the timing information for the model.
   */
  struct {
    time_T taskTime0;
    uint32_T clockTick0;
    uint32_T clockTickH0;
    time_T stepSize0;
    struct {
      uint8_T TID[2];
    } TaskCounters;

    time_T tFinal;
    boolean_T stopRequestedFlag;
  } Timing;
};

/* Block parameters (default storage) */
extern P_flightControlSystem_T flightControlSystem_P;

/* Block signals (default storage) */
extern B_flightControlSystem_T flightControlSystem_B;

/* Block states (default storage) */
extern DW_flightControlSystem_T flightControlSystem_DW;

/* External inputs (root inport signals with default storage) */
extern ExtU_flightControlSystem_T flightControlSystem_U;

/* External outputs (root outports fed by signals with default storage) */
extern ExtY_flightControlSystem_T flightControlSystem_Y;

/* External function called from main */
extern void flightControlSystem_SetEventsForThisBaseStep(boolean_T *eventFlags);

/* Model entry point functions */
extern void flightControlSystem_SetEventsForThisBaseStep(boolean_T *eventFlags);
extern void flightControlSystem_initialize(void);
extern void flightControlSystem_step0(void);
extern void flightControlSystem_step1(void);
extern void flightControlSystem_terminate(void);

/* Real-time Model object */
extern RT_MODEL_flightControlSystem_T *const flightControlSystem_M;

/*-
 * The generated code includes comments that allow you to trace directly
 * back to the appropriate location in the model.  The basic format
 * is <system>/block_name, where system is the system number (uniquely
 * assigned by Simulink) and block_name is the name of the block.
 *
 * Use the MATLAB hilite_system command to trace the generated code back
 * to the model.  For example,
 *
 * hilite_system('<S3>')    - opens system 3
 * hilite_system('<S3>/Kp') - opens and selects block Kp which resides in S3
 *
 * Here is the system hierarchy for this model
 *
 * '<Root>' : 'flightControlSystem'
 * '<S1>'   : 'flightControlSystem/Control System1'
 * '<S2>'   : 'flightControlSystem/Image Processing System'
 * '<S3>'   : 'flightControlSystem/Control System1/Controller'
 * '<S4>'   : 'flightControlSystem/Control System1/Crash Predictor Flags'
 * '<S5>'   : 'flightControlSystem/Control System1/Path Planning'
 * '<S6>'   : 'flightControlSystem/Control System1/State Estimator'
 * '<S7>'   : 'flightControlSystem/Control System1/Controller/Attitude'
 * '<S8>'   : 'flightControlSystem/Control System1/Controller/ControlMixer'
 * '<S9>'   : 'flightControlSystem/Control System1/Controller/XY-to-reference-orientation'
 * '<S10>'  : 'flightControlSystem/Control System1/Controller/Yaw'
 * '<S11>'  : 'flightControlSystem/Control System1/Controller/gravity feedforward//equilibrium thrust'
 * '<S12>'  : 'flightControlSystem/Control System1/Controller/thrustsToMotorCommands'
 * '<S13>'  : 'flightControlSystem/Control System1/Controller/gravity feedforward//equilibrium thrust/Compare To Constant'
 * '<S14>'  : 'flightControlSystem/Control System1/Controller/gravity feedforward//equilibrium thrust/Counter Free-Running'
 * '<S15>'  : 'flightControlSystem/Control System1/Controller/gravity feedforward//equilibrium thrust/Counter Free-Running/Increment Real World'
 * '<S16>'  : 'flightControlSystem/Control System1/Controller/gravity feedforward//equilibrium thrust/Counter Free-Running/Wrap To Zero'
 * '<S17>'  : 'flightControlSystem/Control System1/Crash Predictor Flags/50 continuous  OF errors '
 * '<S18>'  : 'flightControlSystem/Control System1/Crash Predictor Flags/Check error condition'
 * '<S19>'  : 'flightControlSystem/Control System1/Crash Predictor Flags/Compare To Constant'
 * '<S20>'  : 'flightControlSystem/Control System1/Crash Predictor Flags/Compare To Constant1'
 * '<S21>'  : 'flightControlSystem/Control System1/Crash Predictor Flags/Compare To Constant2'
 * '<S22>'  : 'flightControlSystem/Control System1/Crash Predictor Flags/Compare To Constant3'
 * '<S23>'  : 'flightControlSystem/Control System1/Crash Predictor Flags/Compare To Constant4'
 * '<S24>'  : 'flightControlSystem/Control System1/Crash Predictor Flags/Compare To Constant5'
 * '<S25>'  : 'flightControlSystem/Control System1/Crash Predictor Flags/Counter Free-Running'
 * '<S26>'  : 'flightControlSystem/Control System1/Crash Predictor Flags/Geofencing error'
 * '<S27>'  : 'flightControlSystem/Control System1/Crash Predictor Flags/No optical flow '
 * '<S28>'  : 'flightControlSystem/Control System1/Crash Predictor Flags/Normal condition'
 * '<S29>'  : 'flightControlSystem/Control System1/Crash Predictor Flags/Ultrasound improper'
 * '<S30>'  : 'flightControlSystem/Control System1/Crash Predictor Flags/estimator//Optical flow error'
 * '<S31>'  : 'flightControlSystem/Control System1/Crash Predictor Flags/Counter Free-Running/Increment Real World'
 * '<S32>'  : 'flightControlSystem/Control System1/Crash Predictor Flags/Counter Free-Running/Wrap To Zero'
 * '<S33>'  : 'flightControlSystem/Control System1/Path Planning/System Logic6'
 * '<S34>'  : 'flightControlSystem/Control System1/Path Planning/System Logic6/Frenar'
 * '<S35>'  : 'flightControlSystem/Control System1/Path Planning/System Logic6/State logic XYZ'
 * '<S36>'  : 'flightControlSystem/Control System1/Path Planning/System Logic6/Subsystem'
 * '<S37>'  : 'flightControlSystem/Control System1/Path Planning/System Logic6/Subsystem1'
 * '<S38>'  : 'flightControlSystem/Control System1/Path Planning/System Logic6/centrarPista'
 * '<S39>'  : 'flightControlSystem/Control System1/Path Planning/System Logic6/Frenar/Discrete Derivative'
 * '<S40>'  : 'flightControlSystem/Control System1/Path Planning/System Logic6/Subsystem/Subsystem'
 * '<S41>'  : 'flightControlSystem/Control System1/State Estimator/Complementary Filter'
 * '<S42>'  : 'flightControlSystem/Control System1/State Estimator/EstimatorAltitude'
 * '<S43>'  : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition'
 * '<S44>'  : 'flightControlSystem/Control System1/State Estimator/SensorPreprocessing'
 * '<S45>'  : 'flightControlSystem/Control System1/State Estimator/Complementary Filter/Compare To Constant'
 * '<S46>'  : 'flightControlSystem/Control System1/State Estimator/Complementary Filter/Compare To Constant1'
 * '<S47>'  : 'flightControlSystem/Control System1/State Estimator/Complementary Filter/If Action Subsystem'
 * '<S48>'  : 'flightControlSystem/Control System1/State Estimator/Complementary Filter/If Action Subsystem2'
 * '<S49>'  : 'flightControlSystem/Control System1/State Estimator/Complementary Filter/Wbe'
 * '<S50>'  : 'flightControlSystem/Control System1/State Estimator/Complementary Filter/Wbe/Create 3x3 Matrix'
 * '<S51>'  : 'flightControlSystem/Control System1/State Estimator/EstimatorAltitude/KalmanFilter_altitude'
 * '<S52>'  : 'flightControlSystem/Control System1/State Estimator/EstimatorAltitude/OutlierHandling'
 * '<S53>'  : 'flightControlSystem/Control System1/State Estimator/EstimatorAltitude/Rotation Angles to Direction Cosine Matrix'
 * '<S54>'  : 'flightControlSystem/Control System1/State Estimator/EstimatorAltitude/outlierBelowFloor'
 * '<S55>'  : 'flightControlSystem/Control System1/State Estimator/EstimatorAltitude/KalmanFilter_altitude/CalculatePL'
 * '<S56>'  : 'flightControlSystem/Control System1/State Estimator/EstimatorAltitude/KalmanFilter_altitude/CalculateYhat'
 * '<S57>'  : 'flightControlSystem/Control System1/State Estimator/EstimatorAltitude/KalmanFilter_altitude/DataTypeConversionA'
 * '<S58>'  : 'flightControlSystem/Control System1/State Estimator/EstimatorAltitude/KalmanFilter_altitude/DataTypeConversionB'
 * '<S59>'  : 'flightControlSystem/Control System1/State Estimator/EstimatorAltitude/KalmanFilter_altitude/DataTypeConversionC'
 * '<S60>'  : 'flightControlSystem/Control System1/State Estimator/EstimatorAltitude/KalmanFilter_altitude/DataTypeConversionD'
 * '<S61>'  : 'flightControlSystem/Control System1/State Estimator/EstimatorAltitude/KalmanFilter_altitude/DataTypeConversionG'
 * '<S62>'  : 'flightControlSystem/Control System1/State Estimator/EstimatorAltitude/KalmanFilter_altitude/DataTypeConversionH'
 * '<S63>'  : 'flightControlSystem/Control System1/State Estimator/EstimatorAltitude/KalmanFilter_altitude/DataTypeConversionN'
 * '<S64>'  : 'flightControlSystem/Control System1/State Estimator/EstimatorAltitude/KalmanFilter_altitude/DataTypeConversionP'
 * '<S65>'  : 'flightControlSystem/Control System1/State Estimator/EstimatorAltitude/KalmanFilter_altitude/DataTypeConversionP0'
 * '<S66>'  : 'flightControlSystem/Control System1/State Estimator/EstimatorAltitude/KalmanFilter_altitude/DataTypeConversionQ'
 * '<S67>'  : 'flightControlSystem/Control System1/State Estimator/EstimatorAltitude/KalmanFilter_altitude/DataTypeConversionR'
 * '<S68>'  : 'flightControlSystem/Control System1/State Estimator/EstimatorAltitude/KalmanFilter_altitude/DataTypeConversionX'
 * '<S69>'  : 'flightControlSystem/Control System1/State Estimator/EstimatorAltitude/KalmanFilter_altitude/DataTypeConversionX0'
 * '<S70>'  : 'flightControlSystem/Control System1/State Estimator/EstimatorAltitude/KalmanFilter_altitude/DataTypeConversionu'
 * '<S71>'  : 'flightControlSystem/Control System1/State Estimator/EstimatorAltitude/KalmanFilter_altitude/MemoryP'
 * '<S72>'  : 'flightControlSystem/Control System1/State Estimator/EstimatorAltitude/KalmanFilter_altitude/Observer'
 * '<S73>'  : 'flightControlSystem/Control System1/State Estimator/EstimatorAltitude/KalmanFilter_altitude/ReducedQRN'
 * '<S74>'  : 'flightControlSystem/Control System1/State Estimator/EstimatorAltitude/KalmanFilter_altitude/ScalarExpansionP0'
 * '<S75>'  : 'flightControlSystem/Control System1/State Estimator/EstimatorAltitude/KalmanFilter_altitude/ScalarExpansionQ'
 * '<S76>'  : 'flightControlSystem/Control System1/State Estimator/EstimatorAltitude/KalmanFilter_altitude/ScalarExpansionR'
 * '<S77>'  : 'flightControlSystem/Control System1/State Estimator/EstimatorAltitude/KalmanFilter_altitude/UseCurrentEstimator'
 * '<S78>'  : 'flightControlSystem/Control System1/State Estimator/EstimatorAltitude/KalmanFilter_altitude/checkA'
 * '<S79>'  : 'flightControlSystem/Control System1/State Estimator/EstimatorAltitude/KalmanFilter_altitude/checkB'
 * '<S80>'  : 'flightControlSystem/Control System1/State Estimator/EstimatorAltitude/KalmanFilter_altitude/checkC'
 * '<S81>'  : 'flightControlSystem/Control System1/State Estimator/EstimatorAltitude/KalmanFilter_altitude/checkD'
 * '<S82>'  : 'flightControlSystem/Control System1/State Estimator/EstimatorAltitude/KalmanFilter_altitude/checkEnable'
 * '<S83>'  : 'flightControlSystem/Control System1/State Estimator/EstimatorAltitude/KalmanFilter_altitude/checkG'
 * '<S84>'  : 'flightControlSystem/Control System1/State Estimator/EstimatorAltitude/KalmanFilter_altitude/checkH'
 * '<S85>'  : 'flightControlSystem/Control System1/State Estimator/EstimatorAltitude/KalmanFilter_altitude/checkN'
 * '<S86>'  : 'flightControlSystem/Control System1/State Estimator/EstimatorAltitude/KalmanFilter_altitude/checkP0'
 * '<S87>'  : 'flightControlSystem/Control System1/State Estimator/EstimatorAltitude/KalmanFilter_altitude/checkQ'
 * '<S88>'  : 'flightControlSystem/Control System1/State Estimator/EstimatorAltitude/KalmanFilter_altitude/checkR'
 * '<S89>'  : 'flightControlSystem/Control System1/State Estimator/EstimatorAltitude/KalmanFilter_altitude/checkReset'
 * '<S90>'  : 'flightControlSystem/Control System1/State Estimator/EstimatorAltitude/KalmanFilter_altitude/checkX0'
 * '<S91>'  : 'flightControlSystem/Control System1/State Estimator/EstimatorAltitude/KalmanFilter_altitude/checku'
 * '<S92>'  : 'flightControlSystem/Control System1/State Estimator/EstimatorAltitude/KalmanFilter_altitude/checky'
 * '<S93>'  : 'flightControlSystem/Control System1/State Estimator/EstimatorAltitude/KalmanFilter_altitude/CalculatePL/DataTypeConversionL'
 * '<S94>'  : 'flightControlSystem/Control System1/State Estimator/EstimatorAltitude/KalmanFilter_altitude/CalculatePL/DataTypeConversionM'
 * '<S95>'  : 'flightControlSystem/Control System1/State Estimator/EstimatorAltitude/KalmanFilter_altitude/CalculatePL/DataTypeConversionP'
 * '<S96>'  : 'flightControlSystem/Control System1/State Estimator/EstimatorAltitude/KalmanFilter_altitude/CalculatePL/DataTypeConversionZ'
 * '<S97>'  : 'flightControlSystem/Control System1/State Estimator/EstimatorAltitude/KalmanFilter_altitude/Observer/MeasurementUpdate'
 * '<S98>'  : 'flightControlSystem/Control System1/State Estimator/EstimatorAltitude/KalmanFilter_altitude/UseCurrentEstimator/Enabled Subsystem'
 * '<S99>'  : 'flightControlSystem/Control System1/State Estimator/EstimatorAltitude/OutlierHandling/check for min altitude'
 * '<S100>' : 'flightControlSystem/Control System1/State Estimator/EstimatorAltitude/OutlierHandling/currentEstimateVeryOffFromPressure'
 * '<S101>' : 'flightControlSystem/Control System1/State Estimator/EstimatorAltitude/OutlierHandling/currentStateVeryOffsonarflt'
 * '<S102>' : 'flightControlSystem/Control System1/State Estimator/EstimatorAltitude/OutlierHandling/outlierJump'
 * '<S103>' : 'flightControlSystem/Control System1/State Estimator/EstimatorAltitude/Rotation Angles to Direction Cosine Matrix/Create 3x3 Matrix'
 * '<S104>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity'
 * '<S105>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorXYPosition'
 * '<S106>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/AccelerationHandling'
 * '<S107>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/Compare To Constant'
 * '<S108>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/Counter Free-Running'
 * '<S109>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/DataHandling'
 * '<S110>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/KalmanFilter_dxdy'
 * '<S111>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/AccelerationHandling/Deactivate Acceleration If OF is not used due to low altitude'
 * '<S112>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/AccelerationHandling/Rotation Angles to Direction Cosine Matrix'
 * '<S113>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/AccelerationHandling/do not use acc if optical flow never available (Note OF@60Hz but ZOH to 200!)'
 * '<S114>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/AccelerationHandling/do not use acc if optical flow never available (Note OF@60Hz but ZOH to 200!)1'
 * '<S115>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/AccelerationHandling/Rotation Angles to Direction Cosine Matrix/Create 3x3 Matrix'
 * '<S116>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/Counter Free-Running/Increment Real World'
 * '<S117>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/Counter Free-Running/Wrap To Zero'
 * '<S118>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/DataHandling/DiscreteDerivative'
 * '<S119>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/DataHandling/maxdw1'
 * '<S120>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/DataHandling/maxdw2'
 * '<S121>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/DataHandling/maxp'
 * '<S122>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/DataHandling/maxp2'
 * '<S123>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/DataHandling/maxq'
 * '<S124>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/DataHandling/maxq2'
 * '<S125>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/DataHandling/maxw1'
 * '<S126>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/DataHandling/maxw2'
 * '<S127>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/DataHandling/maxw3'
 * '<S128>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/DataHandling/maxw4'
 * '<S129>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/DataHandling/minHeightforOF'
 * '<S130>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/KalmanFilter_dxdy/CalculatePL'
 * '<S131>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/KalmanFilter_dxdy/CalculateYhat'
 * '<S132>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/KalmanFilter_dxdy/DataTypeConversionA'
 * '<S133>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/KalmanFilter_dxdy/DataTypeConversionB'
 * '<S134>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/KalmanFilter_dxdy/DataTypeConversionC'
 * '<S135>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/KalmanFilter_dxdy/DataTypeConversionD'
 * '<S136>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/KalmanFilter_dxdy/DataTypeConversionG'
 * '<S137>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/KalmanFilter_dxdy/DataTypeConversionH'
 * '<S138>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/KalmanFilter_dxdy/DataTypeConversionN'
 * '<S139>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/KalmanFilter_dxdy/DataTypeConversionP'
 * '<S140>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/KalmanFilter_dxdy/DataTypeConversionP0'
 * '<S141>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/KalmanFilter_dxdy/DataTypeConversionQ'
 * '<S142>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/KalmanFilter_dxdy/DataTypeConversionR'
 * '<S143>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/KalmanFilter_dxdy/DataTypeConversionX'
 * '<S144>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/KalmanFilter_dxdy/DataTypeConversionX0'
 * '<S145>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/KalmanFilter_dxdy/DataTypeConversionu'
 * '<S146>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/KalmanFilter_dxdy/MemoryP'
 * '<S147>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/KalmanFilter_dxdy/Observer'
 * '<S148>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/KalmanFilter_dxdy/ReducedQRN'
 * '<S149>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/KalmanFilter_dxdy/ScalarExpansionP0'
 * '<S150>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/KalmanFilter_dxdy/ScalarExpansionQ'
 * '<S151>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/KalmanFilter_dxdy/ScalarExpansionR'
 * '<S152>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/KalmanFilter_dxdy/UseCurrentEstimator'
 * '<S153>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/KalmanFilter_dxdy/checkA'
 * '<S154>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/KalmanFilter_dxdy/checkB'
 * '<S155>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/KalmanFilter_dxdy/checkC'
 * '<S156>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/KalmanFilter_dxdy/checkD'
 * '<S157>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/KalmanFilter_dxdy/checkEnable'
 * '<S158>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/KalmanFilter_dxdy/checkG'
 * '<S159>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/KalmanFilter_dxdy/checkH'
 * '<S160>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/KalmanFilter_dxdy/checkN'
 * '<S161>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/KalmanFilter_dxdy/checkP0'
 * '<S162>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/KalmanFilter_dxdy/checkQ'
 * '<S163>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/KalmanFilter_dxdy/checkR'
 * '<S164>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/KalmanFilter_dxdy/checkReset'
 * '<S165>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/KalmanFilter_dxdy/checkX0'
 * '<S166>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/KalmanFilter_dxdy/checku'
 * '<S167>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/KalmanFilter_dxdy/checky'
 * '<S168>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/KalmanFilter_dxdy/CalculatePL/DataTypeConversionL'
 * '<S169>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/KalmanFilter_dxdy/CalculatePL/DataTypeConversionM'
 * '<S170>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/KalmanFilter_dxdy/CalculatePL/DataTypeConversionP'
 * '<S171>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/KalmanFilter_dxdy/CalculatePL/DataTypeConversionZ'
 * '<S172>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/KalmanFilter_dxdy/Observer/MeasurementUpdate'
 * '<S173>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorVelocity/KalmanFilter_dxdy/UseCurrentEstimator/Enabled Subsystem'
 * '<S174>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorXYPosition/Rotation Angles to Direction Cosine Matrix'
 * '<S175>' : 'flightControlSystem/Control System1/State Estimator/EstimatorXYPosition/EstimatorXYPosition/Rotation Angles to Direction Cosine Matrix/Create 3x3 Matrix'
 * '<S176>' : 'flightControlSystem/Control System1/State Estimator/SensorPreprocessing/sensordata_group'
 * '<S177>' : 'flightControlSystem/Image Processing System/MATLAB Function'
 * '<S178>' : 'flightControlSystem/Image Processing System/Subsystem'
 * '<S179>' : 'flightControlSystem/Image Processing System/Subsystem/MATLABCentro '
 * '<S180>' : 'flightControlSystem/Image Processing System/Subsystem/MATLABHisto2'
 * '<S181>' : 'flightControlSystem/Image Processing System/Subsystem/Mean'
 * '<S182>' : 'flightControlSystem/Image Processing System/Subsystem/Mean1'
 * '<S183>' : 'flightControlSystem/Image Processing System/Subsystem/Mean2'
 * '<S184>' : 'flightControlSystem/Image Processing System/Subsystem/Mean3'
 * '<S185>' : 'flightControlSystem/Image Processing System/Subsystem/histh'
 * '<S186>' : 'flightControlSystem/Image Processing System/Subsystem/histh1'
 */
#endif                                 /* RTW_HEADER_flightControlSystem_h_ */
