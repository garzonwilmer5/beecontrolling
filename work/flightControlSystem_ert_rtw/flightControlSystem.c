/*
 * flightControlSystem.c
 *
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 *
 * Code generation for model "flightControlSystem".
 *
 * Model version              : 1.638
 * Simulink Coder version : 9.1 (R2019a) 23-Nov-2018
 * C source code generated on : Tue Nov 24 19:35:22 2020
 *
 * Target selection: ert.tlc
 * Note: GRT includes extra infrastructure and instrumentation for prototyping
 * Embedded hardware selection: ARM Compatible->ARM 9
 * Code generation objectives: Unspecified
 * Validation result: Not run
 */

#include "flightControlSystem.h"
#include "flightControlSystem_private.h"

/* Named constants for Chart: '<S33>/State logic XYZ' */
#define flightControlSystem_IN_Alinearse ((uint8_T)1U)
#define flightControlSystem_IN_Beginning1 ((uint8_T)2U)
#define flightControlSystem_IN_DetectarCirculo ((uint8_T)3U)
#define flightControlSystem_IN_Frenar  ((uint8_T)4U)
#define flightControlSystem_IN_NO_ACTIVE_CHILD ((uint8_T)0U)
#define flightControlSystem_IN_Polinizar ((uint8_T)5U)
#define flightControlSystem_IN_Polinizar1 ((uint8_T)6U)
#define flightControlSystem_IN_vayaRapido ((uint8_T)7U)

/* Block signals (default storage) */
B_flightControlSystem_T flightControlSystem_B;

/* Block states (default storage) */
DW_flightControlSystem_T flightControlSystem_DW;

/* External inputs (root inport signals with default storage) */
ExtU_flightControlSystem_T flightControlSystem_U;

/* External outputs (root outports fed by signals with default storage) */
ExtY_flightControlSystem_T flightControlSystem_Y;

/* Real-time model */
RT_MODEL_flightControlSystem_T flightControlSystem_M_;
RT_MODEL_flightControlSystem_T *const flightControlSystem_M =
  &flightControlSystem_M_;

/* Forward declaration for local functions */
static void flightControlSystem_vayaRapido(const real_T rtu_VisionbasedData[7],
  B_ControlSystem1_flightControlSystem_T *localB,
  DW_ControlSystem1_flightControlSystem_T *localDW,
  P_ControlSystem1_flightControlSystem_T *localP);
static void rate_monotonic_scheduler(void);

/*
 * Set which subrates need to run this base step (base rate always runs).
 * This function must be called prior to calling the model step function
 * in order to "remember" which rates need to run this base step.  The
 * buffering of events allows for overlapping preemption.
 */
void flightControlSystem_SetEventsForThisBaseStep(boolean_T *eventFlags)
{
  /* Task runs when its counter is zero, computed via rtmStepTask macro */
  eventFlags[1] = ((boolean_T)rtmStepTask(flightControlSystem_M, 1));
}

/*
 *   This function updates active task flag for each subrate
 * and rate transition flags for tasks that exchange data.
 * The function assumes rate-monotonic multitasking scheduler.
 * The function must be called at model base rate so that
 * the generated code self-manages all its subrates and rate
 * transition flags.
 */
static void rate_monotonic_scheduler(void)
{
  /* Compute which subrates run during the next base time step.  Subrates
   * are an integer multiple of the base rate counter.  Therefore, the subtask
   * counter is reset when it reaches its limit (zero means run).
   */
  (flightControlSystem_M->Timing.TaskCounters.TID[1])++;
  if ((flightControlSystem_M->Timing.TaskCounters.TID[1]) > 39) {/* Sample time: [0.2s, 0.0s] */
    flightControlSystem_M->Timing.TaskCounters.TID[1] = 0;
  }
}

/*
 * Output and update for action system:
 *    '<S4>/Geofencing error'
 *    '<S4>/estimator//Optical flow error'
 *    '<S4>/No optical flow '
 *    '<S4>/Ultrasound improper'
 *    '<S4>/Normal condition'
 */
void flightControlSystem_Geofencingerror(uint8_T *rty_Out1,
  P_Geofencingerror_flightControlSystem_T *localP)
{
  /* SignalConversion: '<S26>/OutportBufferForOut1' incorporates:
   *  Constant: '<S26>/Constant'
   */
  *rty_Out1 = localP->Constant_Value;
}

real32_T rt_powf_snf(real32_T u0, real32_T u1)
{
  real32_T y;
  real32_T tmp;
  real32_T tmp_0;
  if (rtIsNaNF(u0) || rtIsNaNF(u1)) {
    y = (rtNaNF);
  } else {
    tmp = (real32_T)fabs(u0);
    tmp_0 = (real32_T)fabs(u1);
    if (rtIsInfF(u1)) {
      if (tmp == 1.0F) {
        y = 1.0F;
      } else if (tmp > 1.0F) {
        if (u1 > 0.0F) {
          y = (rtInfF);
        } else {
          y = 0.0F;
        }
      } else if (u1 > 0.0F) {
        y = 0.0F;
      } else {
        y = (rtInfF);
      }
    } else if (tmp_0 == 0.0F) {
      y = 1.0F;
    } else if (tmp_0 == 1.0F) {
      if (u1 > 0.0F) {
        y = u0;
      } else {
        y = 1.0F / u0;
      }
    } else if (u1 == 2.0F) {
      y = u0 * u0;
    } else if ((u1 == 0.5F) && (u0 >= 0.0F)) {
      y = (real32_T)sqrt(u0);
    } else if ((u0 < 0.0F) && (u1 > (real32_T)floor(u1))) {
      y = (rtNaNF);
    } else {
      y = (real32_T)pow(u0, u1);
    }
  }

  return y;
}

/* Function for Chart: '<S33>/State logic XYZ' */
static void flightControlSystem_vayaRapido(const real_T rtu_VisionbasedData[7],
  B_ControlSystem1_flightControlSystem_T *localB,
  DW_ControlSystem1_flightControlSystem_T *localDW,
  P_ControlSystem1_flightControlSystem_T *localP)
{
  /* Chart: '<S33>/State logic XYZ' incorporates:
   *  Constant: '<S5>/Pitch Aleteo'
   *  Constant: '<S5>/Pitch Exploracion'
   */
  if ((localB->Switch2 != 0.0) && (rtu_VisionbasedData[6] != 0.0) &&
      (rtu_VisionbasedData[4] - 60.0 > -20.0)) {
    localDW->PT = localB->ControlModeUpdate.live_time_ticks;
    localB->fin = 1.0F;
    localB->poliAlpha = 1.0F;
    localDW->is_c1_flightControlSystem = flightControlSystem_IN_Frenar;
    localB->pitch = localDW->PastPitch;
  } else {
    localB->d0 = fabs(localB->error);
    if ((localB->d0 < 6.0) && (fabs(rtu_VisionbasedData[1] - 80.0) < 8.0)) {
      localDW->PastPitch = -(real32_T)(0.017453292519943295 *
        localP->PitchAleteo_Value);
      localDW->is_c1_flightControlSystem = flightControlSystem_IN_vayaRapido;
      localB->pitch = localDW->PastPitch;
      localB->yaw = localDW->PastYaw;
    } else if ((localB->d0 > 6.0) && (!(localB->Switch2 != 0.0))) {
      localDW->PastPitch = -(real32_T)(0.017453292519943295 *
        localP->PitchExploracion_Value);
      localDW->is_c1_flightControlSystem = flightControlSystem_IN_Alinearse;
      localB->yaw = localDW->PastYaw;
      localB->pitch = localDW->PastPitch;
    } else {
      localB->pitch = localDW->PastPitch;
      localB->yaw = localDW->PastYaw;
    }
  }

  /* End of Chart: '<S33>/State logic XYZ' */
}

real_T rt_roundd_snf(real_T u)
{
  real_T y;
  if (fabs(u) < 4.503599627370496E+15) {
    if (u >= 0.5) {
      y = floor(u + 0.5);
    } else if (u > -0.5) {
      y = u * 0.0;
    } else {
      y = ceil(u - 0.5);
    }
  } else {
    y = u;
  }

  return y;
}

/* SetupRuntimeResources for atomic system: '<Root>/Control System1' */
void flightControlSystem_ControlSystem1_SetupRTR(RT_MODEL_flightControlSystem_T *
  const flightControlSystem_M, DW_ControlSystem1_flightControlSystem_T *localDW)
{
  /* SetupRuntimeResources for ToWorkspace: '<S1>/To Workspace' */
  {
    static int_T rt_ToWksWidths[] = { 12 };

    static int_T rt_ToWksNumDimensions[] = { 1 };

    static int_T rt_ToWksDimensions[] = { 12 };

    static boolean_T rt_ToWksIsVarDims[] = { 0 };

    static void *rt_ToWksCurrSigDims[] = { (NULL) };

    static int_T rt_ToWksCurrSigDimsSize[] = { 4 };

    static BuiltInDTypeId rt_ToWksDataTypeIds[] = { SS_SINGLE };

    static int_T rt_ToWksComplexSignals[] = { 0 };

    static int_T rt_ToWksFrameData[] = { 0 };

    static RTWPreprocessingFcnPtr rt_ToWksLoggingPreprocessingFcnPtrs[] = {
      (NULL)
    };

    static const char_T *rt_ToWksLabels[] = { "" };

    static RTWLogSignalInfo rt_ToWksSignalInfo = {
      1,
      rt_ToWksWidths,
      rt_ToWksNumDimensions,
      rt_ToWksDimensions,
      rt_ToWksIsVarDims,
      rt_ToWksCurrSigDims,
      rt_ToWksCurrSigDimsSize,
      rt_ToWksDataTypeIds,
      rt_ToWksComplexSignals,
      rt_ToWksFrameData,
      rt_ToWksLoggingPreprocessingFcnPtrs,

      { rt_ToWksLabels },
      (NULL),
      (NULL),
      (NULL),

      { (NULL) },

      { (NULL) },
      (NULL),
      (NULL)
    };

    static const char_T rt_ToWksBlockName[] =
      "flightControlSystem/Control System1/To Workspace";
    localDW->ToWorkspace_PWORK.LoggedData = rt_CreateStructLogVar(
      flightControlSystem_M->rtwLogInfo,
      0.0,
      rtmGetTFinal(flightControlSystem_M),
      flightControlSystem_M->Timing.stepSize0,
      (&rtmGetErrorStatus(flightControlSystem_M)),
      "estimatedStates",
      1,
      0,
      1,
      0.005,
      &rt_ToWksSignalInfo,
      rt_ToWksBlockName);
    if (localDW->ToWorkspace_PWORK.LoggedData == (NULL))
      return;
  }
}

/* System initialize for atomic system: '<Root>/Control System1' */
void flightControlSystem_ControlSystem1_Init
  (B_ControlSystem1_flightControlSystem_T *localB,
   DW_ControlSystem1_flightControlSystem_T *localDW,
   P_ControlSystem1_flightControlSystem_T *localP)
{
  int32_T i;

  /* InitializeConditions for DiscreteIntegrator: '<S105>/SimplyIntegrateVelocity' */
  localDW->SimplyIntegrateVelocity_DSTATE[0] =
    localP->SimplyIntegrateVelocity_IC;
  localDW->SimplyIntegrateVelocity_DSTATE[1] =
    localP->SimplyIntegrateVelocity_IC;
  localDW->SimplyIntegrateVelocity_PrevResetState = 2;

  /* InitializeConditions for Reshape: '<S51>/Reshapexhat' incorporates:
   *  Delay: '<S42>/Delay2'
   */
  localDW->Delay2_DSTATE = localP->Delay2_InitialCondition;

  /* InitializeConditions for Delay: '<S51>/MemoryX' */
  localDW->icLoad = 1U;

  /* InitializeConditions for DiscreteFilter: '<S52>/pressureFilter_IIR' */
  localDW->pressureFilter_IIR_states[0] =
    localP->pressureFilter_IIR_InitialStates;

  /* InitializeConditions for DiscreteFilter: '<S52>/soonarFilter_IIR' */
  localDW->soonarFilter_IIR_states[0] = localP->soonarFilter_IIR_InitialStates;

  /* InitializeConditions for Memory: '<S41>/Memory' */
  localDW->Memory_PreviousInput[0] = localP->Memory_InitialCondition[0];

  /* InitializeConditions for DiscreteFilter: '<S52>/pressureFilter_IIR' */
  localDW->pressureFilter_IIR_states[1] =
    localP->pressureFilter_IIR_InitialStates;

  /* InitializeConditions for DiscreteFilter: '<S52>/soonarFilter_IIR' */
  localDW->soonarFilter_IIR_states[1] = localP->soonarFilter_IIR_InitialStates;

  /* InitializeConditions for Memory: '<S41>/Memory' */
  localDW->Memory_PreviousInput[1] = localP->Memory_InitialCondition[1];

  /* InitializeConditions for DiscreteFilter: '<S52>/pressureFilter_IIR' */
  localDW->pressureFilter_IIR_states[2] =
    localP->pressureFilter_IIR_InitialStates;

  /* InitializeConditions for DiscreteFilter: '<S52>/soonarFilter_IIR' */
  localDW->soonarFilter_IIR_states[2] = localP->soonarFilter_IIR_InitialStates;

  /* InitializeConditions for Memory: '<S41>/Memory' */
  localDW->Memory_PreviousInput[2] = localP->Memory_InitialCondition[2];

  /* InitializeConditions for DiscreteFilter: '<S44>/IIR_IMUgyro_r' */
  for (i = 0; i < 5; i++) {
    localDW->IIR_IMUgyro_r_states[i] = localP->IIR_IMUgyro_r_InitialStates;
  }

  /* End of InitializeConditions for DiscreteFilter: '<S44>/IIR_IMUgyro_r' */

  /* InitializeConditions for DiscreteFir: '<S44>/FIR_IMUaccel' */
  localDW->FIR_IMUaccel_circBuf = 0;
  for (i = 0; i < 15; i++) {
    localDW->FIR_IMUaccel_states[i] = localP->FIR_IMUaccel_InitialStates;
  }

  /* End of InitializeConditions for DiscreteFir: '<S44>/FIR_IMUaccel' */

  /* InitializeConditions for Delay: '<S110>/MemoryX' */
  localDW->icLoad_c = 1U;

  /* InitializeConditions for DiscreteFilter: '<S109>/IIRgyroz' */
  for (i = 0; i < 10; i++) {
    localDW->IIRgyroz_states[i] = localP->IIRgyroz_InitialStates;
  }

  /* End of InitializeConditions for DiscreteFilter: '<S109>/IIRgyroz' */

  /* InitializeConditions for UnitDelay: '<S108>/Output' */
  localDW->Output_DSTATE = localP->Output_InitialCondition;

  /* InitializeConditions for Delay: '<S37>/Delay' */
  localDW->Delay_DSTATE = localP->Delay_InitialCondition;

  /* InitializeConditions for UnitDelay: '<S39>/UD' */
  localDW->UD_DSTATE_d = localP->DiscreteDerivative_ICPrevScaledInput_h;

  /* InitializeConditions for DiscreteIntegrator: '<S34>/Discrete-Time Integrator' */
  localDW->DiscreteTimeIntegrator_DSTATE_j = localP->DiscreteTimeIntegrator_IC_c;
  localDW->DiscreteTimeIntegrator_PrevResetState = 2;

  /* InitializeConditions for UnitDelay: '<S14>/Output' */
  localDW->Output_DSTATE_c = localP->Output_InitialCondition_g;

  /* InitializeConditions for DiscreteIntegrator: '<S11>/Discrete-Time Integrator' */
  localDW->DiscreteTimeIntegrator_DSTATE_o = localP->DiscreteTimeIntegrator_IC_g;
  localDW->DiscreteTimeIntegrator_PrevResetState_j = 2;

  /* InitializeConditions for UnitDelay: '<S25>/Output' */
  localDW->Output_DSTATE_c1 = localP->Output_InitialCondition_gu;

  /* InitializeConditions for Delay: '<S4>/Delay One Step' */
  localDW->DelayOneStep_DSTATE = localP->DelayOneStep_InitialCondition;

  /* SystemInitialize for Chart: '<S33>/State logic XYZ' */
  localDW->is_active_c1_flightControlSystem = 0U;
  localDW->is_c1_flightControlSystem = flightControlSystem_IN_NO_ACTIVE_CHILD;
  localDW->PastPitch = 0.0F;
  localB->yaw = 0.0F;
  localB->pitch = 0.0F;
  localB->roll = 0.0F;
  localB->RefZ = 0.0F;

  /* InitializeConditions for UnitDelay: '<S118>/UD' */
  localDW->UD_DSTATE[0] = localP->DiscreteDerivative_ICPrevScaledInput;

  /* InitializeConditions for Delay: '<S104>/Delay' */
  localDW->Delay_DSTATE_a[0] = localP->Delay_InitialCondition_f;

  /* InitializeConditions for Delay: '<S6>/Delay1' */
  localDW->Delay1_DSTATE[0] = localP->Delay1_InitialCondition;

  /* InitializeConditions for DiscreteIntegrator: '<S7>/Discrete-Time Integrator' */
  localDW->DiscreteTimeIntegrator_DSTATE[0] = localP->DiscreteTimeIntegrator_IC;

  /* InitializeConditions for Delay: '<S7>/Delay' */
  localDW->Delay_DSTATE_b[0] = localP->Delay_InitialCondition_c;

  /* SystemInitialize for Enabled SubSystem: '<S77>/Enabled Subsystem' */
  /* SystemInitialize for Outport: '<S98>/deltax' */
  localB->Product2[0] = localP->deltax_Y0;

  /* End of SystemInitialize for SubSystem: '<S77>/Enabled Subsystem' */

  /* SystemInitialize for Merge: '<S41>/Merge' */
  localB->Merge[0] = localP->Merge_InitialOutput;

  /* SystemInitialize for Enabled SubSystem: '<S152>/Enabled Subsystem' */
  /* SystemInitialize for Outport: '<S173>/deltax' */
  localB->Product2_o[0] = localP->deltax_Y0_m;

  /* End of SystemInitialize for SubSystem: '<S152>/Enabled Subsystem' */

  /* SystemInitialize for Enabled SubSystem: '<S36>/Subsystem' */
  /* SystemInitialize for Outport: '<S40>/Out1' */
  localB->P_xy[0] = localP->Out1_Y0;

  /* End of SystemInitialize for SubSystem: '<S36>/Subsystem' */

  /* SystemInitialize for Enabled SubSystem: '<S147>/MeasurementUpdate' */
  /* SystemInitialize for Outport: '<S172>/L*(y[k]-yhat[k|k-1])' */
  localB->Product3_m[0] = localP->Lykyhatkk1_Y0_e;

  /* End of SystemInitialize for SubSystem: '<S147>/MeasurementUpdate' */

  /* SystemInitialize for Enabled SubSystem: '<S72>/MeasurementUpdate' */
  /* SystemInitialize for Outport: '<S97>/L*(y[k]-yhat[k|k-1])' */
  localB->Product3[0] = localP->Lykyhatkk1_Y0;

  /* End of SystemInitialize for SubSystem: '<S72>/MeasurementUpdate' */

  /* InitializeConditions for UnitDelay: '<S118>/UD' */
  localDW->UD_DSTATE[1] = localP->DiscreteDerivative_ICPrevScaledInput;

  /* InitializeConditions for Delay: '<S104>/Delay' */
  localDW->Delay_DSTATE_a[1] = localP->Delay_InitialCondition_f;

  /* InitializeConditions for Delay: '<S6>/Delay1' */
  localDW->Delay1_DSTATE[1] = localP->Delay1_InitialCondition;

  /* InitializeConditions for DiscreteIntegrator: '<S7>/Discrete-Time Integrator' */
  localDW->DiscreteTimeIntegrator_DSTATE[1] = localP->DiscreteTimeIntegrator_IC;

  /* InitializeConditions for Delay: '<S7>/Delay' */
  localDW->Delay_DSTATE_b[1] = localP->Delay_InitialCondition_c;

  /* SystemInitialize for Enabled SubSystem: '<S77>/Enabled Subsystem' */
  /* SystemInitialize for Outport: '<S98>/deltax' */
  localB->Product2[1] = localP->deltax_Y0;

  /* End of SystemInitialize for SubSystem: '<S77>/Enabled Subsystem' */

  /* SystemInitialize for Merge: '<S41>/Merge' */
  localB->Merge[1] = localP->Merge_InitialOutput;

  /* SystemInitialize for Enabled SubSystem: '<S152>/Enabled Subsystem' */
  /* SystemInitialize for Outport: '<S173>/deltax' */
  localB->Product2_o[1] = localP->deltax_Y0_m;

  /* End of SystemInitialize for SubSystem: '<S152>/Enabled Subsystem' */

  /* SystemInitialize for Enabled SubSystem: '<S36>/Subsystem' */
  /* SystemInitialize for Outport: '<S40>/Out1' */
  localB->P_xy[1] = localP->Out1_Y0;

  /* End of SystemInitialize for SubSystem: '<S36>/Subsystem' */

  /* SystemInitialize for Enabled SubSystem: '<S147>/MeasurementUpdate' */
  /* SystemInitialize for Outport: '<S172>/L*(y[k]-yhat[k|k-1])' */
  localB->Product3_m[1] = localP->Lykyhatkk1_Y0_e;

  /* End of SystemInitialize for SubSystem: '<S147>/MeasurementUpdate' */

  /* SystemInitialize for Enabled SubSystem: '<S72>/MeasurementUpdate' */
  /* SystemInitialize for Outport: '<S97>/L*(y[k]-yhat[k|k-1])' */
  localB->Product3[1] = localP->Lykyhatkk1_Y0;

  /* End of SystemInitialize for SubSystem: '<S72>/MeasurementUpdate' */

  /* SystemInitialize for Merge: '<S4>/Merge' */
  localB->Merge_b = localP->Merge_InitialOutput_i;
}

/* Start for atomic system: '<Root>/Control System1' */
void flightControlSystem_ControlSystem1_Start
  (DW_ControlSystem1_flightControlSystem_T *localDW)
{
  /* Start for Enabled SubSystem: '<S77>/Enabled Subsystem' */
  localDW->EnabledSubsystem_MODE_d = false;

  /* End of Start for SubSystem: '<S77>/Enabled Subsystem' */

  /* Start for Enabled SubSystem: '<S152>/Enabled Subsystem' */
  localDW->EnabledSubsystem_MODE = false;

  /* End of Start for SubSystem: '<S152>/Enabled Subsystem' */

  /* Start for Enabled SubSystem: '<S147>/MeasurementUpdate' */
  localDW->MeasurementUpdate_MODE = false;

  /* End of Start for SubSystem: '<S147>/MeasurementUpdate' */

  /* Start for Enabled SubSystem: '<S72>/MeasurementUpdate' */
  localDW->MeasurementUpdate_MODE_a = false;

  /* End of Start for SubSystem: '<S72>/MeasurementUpdate' */
}

/* Output and update for atomic system: '<Root>/Control System1' */
void flightControlSystem_ControlSystem1(RT_MODEL_flightControlSystem_T * const
  flightControlSystem_M, const CommandBus *rtu_ReferenceValueServerCmds, const
  SensorsBus *rtu_Sensors, const real_T rtu_VisionbasedData[7],
  B_ControlSystem1_flightControlSystem_T *localB,
  DW_ControlSystem1_flightControlSystem_T *localDW,
  P_ControlSystem1_flightControlSystem_T *localP)
{
  /* local block i/o variables */
  real32_T rtb_DataTypeConversion_c;
  boolean_T rtb_Compare_j;
  boolean_T rtb_Reshapeyhat;
  boolean_T rtb_Reshapeyhat_o;
  boolean_T rtb_DataTypeConversionReset_i;
  boolean_T rtb_Compare_mu;
  boolean_T rtb_Compare_ln;
  boolean_T rtb_Compare_ae;
  boolean_T rtb_Compare_p;
  boolean_T rtb_Compare_o;
  real32_T rtb_sincos_o1_idx_1;
  real32_T rtb_sincos_o1_idx_2;
  real32_T rtb_Product_i_idx_1;
  real32_T rtb_Product_i_idx_0;
  real32_T rtb_Sum_e_idx_0;
  real32_T rtb_Sum_e_idx_1;
  uint32_T qY;
  real32_T acc1;
  real32_T rtb_MathFunction_f_tmp;
  real32_T rtb_MathFunction_f_tmp_0;
  boolean_T guard1 = false;
  boolean_T guard2 = false;
  boolean_T guard3 = false;

  /* DiscreteIntegrator: '<S105>/SimplyIntegrateVelocity' incorporates:
   *  Constant: '<S1>/controlModePosVsOrient'
   */
  if (localP->controlModePosVsOrient_Value &&
      (localDW->SimplyIntegrateVelocity_PrevResetState <= 0)) {
    localDW->SimplyIntegrateVelocity_DSTATE[0] =
      localP->SimplyIntegrateVelocity_IC;
    localDW->SimplyIntegrateVelocity_DSTATE[1] =
      localP->SimplyIntegrateVelocity_IC;
  }

  /* RelationalOperator: '<S54>/Compare' incorporates:
   *  Constant: '<S54>/Constant'
   *  Delay: '<S42>/Delay2'
   */
  localB->Compare = (localDW->Delay2_DSTATE > localP->outlierBelowFloor_const);

  /* Delay: '<S51>/MemoryX' incorporates:
   *  Constant: '<S51>/X0'
   *  DataTypeConversion: '<S51>/DataTypeConversionReset'
   *  Reshape: '<S51>/ReshapeX0'
   */
  if (localB->Compare) {
    localDW->icLoad = 1U;
  }

  if (localDW->icLoad != 0) {
    localDW->MemoryX_DSTATE[0] = localP->X0_Value[0];
    localDW->MemoryX_DSTATE[1] = localP->X0_Value[1];
  }

  /* Gain: '<S42>/invertzaxisGain' */
  localB->invertzaxisGain = (real_T)localP->invertzaxisGain_Gain *
    rtu_Sensors->HALSensors.HAL_ultrasound_SI.altitude;

  /* Sum: '<S44>/Sum2' incorporates:
   *  DataTypeConversion: '<S44>/Data Type Conversion'
   */
  rtb_DataTypeConversion_c = rtu_Sensors->HALSensors.HAL_pressure_SI.pressure -
    rtu_Sensors->SensorCalibration[6];

  /* DiscreteFilter: '<S52>/pressureFilter_IIR' incorporates:
   *  Gain: '<S42>/prsToAltGain'
   */
  localB->denAccum_b = (((localP->prsToAltGain_Gain * rtb_DataTypeConversion_c -
    localP->pressureFilter_IIR_DenCoef[1] * localDW->pressureFilter_IIR_states[0])
    - localP->pressureFilter_IIR_DenCoef[2] * localDW->
    pressureFilter_IIR_states[1]) - localP->pressureFilter_IIR_DenCoef[3] *
                        localDW->pressureFilter_IIR_states[2]) /
    localP->pressureFilter_IIR_DenCoef[0];

  /* DiscreteFilter: '<S52>/soonarFilter_IIR' */
  localB->denAccum = (((localB->invertzaxisGain -
                        localP->soonarFilter_IIR_DenCoef[1] *
                        localDW->soonarFilter_IIR_states[0]) -
                       localP->soonarFilter_IIR_DenCoef[2] *
                       localDW->soonarFilter_IIR_states[1]) -
                      localP->soonarFilter_IIR_DenCoef[3] *
                      localDW->soonarFilter_IIR_states[2]) /
    localP->soonarFilter_IIR_DenCoef[0];

  /* Saturate: '<S52>/SaturationSonar' */
  if (localB->invertzaxisGain > -flightControlSystem_P.Sensors.altSensorMin) {
    localB->rtb_Add_j_idx_1 = -flightControlSystem_P.Sensors.altSensorMin;
  } else if (localB->invertzaxisGain < localP->SaturationSonar_LowerSat) {
    localB->rtb_Add_j_idx_1 = localP->SaturationSonar_LowerSat;
  } else {
    localB->rtb_Add_j_idx_1 = localB->invertzaxisGain;
  }

  /* End of Saturate: '<S52>/SaturationSonar' */

  /* Logic: '<S52>/nicemeasurementor newupdateneeded' incorporates:
   *  Abs: '<S52>/Absestdiff'
   *  Abs: '<S52>/Absestdiff1'
   *  Abs: '<S52>/Absestdiff2'
   *  Constant: '<S100>/Constant'
   *  Constant: '<S101>/Constant'
   *  Constant: '<S102>/Constant'
   *  Constant: '<S99>/Constant'
   *  Delay: '<S42>/Delay2'
   *  DiscreteFilter: '<S52>/pressureFilter_IIR'
   *  DiscreteFilter: '<S52>/soonarFilter_IIR'
   *  Logic: '<S52>/NOT'
   *  Logic: '<S52>/findingoutliers'
   *  Logic: '<S52>/newupdateneeded'
   *  RelationalOperator: '<S100>/Compare'
   *  RelationalOperator: '<S101>/Compare'
   *  RelationalOperator: '<S102>/Compare'
   *  RelationalOperator: '<S99>/Compare'
   *  Sum: '<S52>/Add'
   *  Sum: '<S52>/Add1'
   *  Sum: '<S52>/Add2'
   */
  localB->nicemeasurementornewupdateneeded = ((fabs(localDW->Delay2_DSTATE -
    localB->rtb_Add_j_idx_1) <= localP->outlierJump_const) &&
    (localB->invertzaxisGain < -flightControlSystem_P.Sensors.altSensorMin) &&
    ((!(fabs((((localP->pressureFilter_IIR_NumCoef[0] * localB->denAccum_b +
                localP->pressureFilter_IIR_NumCoef[1] *
                localDW->pressureFilter_IIR_states[0]) +
               localP->pressureFilter_IIR_NumCoef[2] *
               localDW->pressureFilter_IIR_states[1]) +
              localP->pressureFilter_IIR_NumCoef[3] *
              localDW->pressureFilter_IIR_states[2]) - localDW->Delay2_DSTATE) >=
        localP->currentEstimateVeryOffFromPressure_const)) || (!(fabs
    ((((localP->soonarFilter_IIR_NumCoef[0] * localB->denAccum +
        localP->soonarFilter_IIR_NumCoef[1] * localDW->soonarFilter_IIR_states[0])
       + localP->soonarFilter_IIR_NumCoef[2] * localDW->soonarFilter_IIR_states
       [1]) + localP->soonarFilter_IIR_NumCoef[3] *
      localDW->soonarFilter_IIR_states[2]) - localDW->Delay2_DSTATE) >=
    localP->currentStateVeryOffsonarflt_const))));

  /* Outputs for Enabled SubSystem: '<S77>/Enabled Subsystem' incorporates:
   *  EnablePort: '<S98>/Enable'
   */
  /* DataTypeConversion: '<S51>/DataTypeConversionEnable' */
  if (localB->nicemeasurementornewupdateneeded) {
    if (!localDW->EnabledSubsystem_MODE_d) {
      localDW->EnabledSubsystem_MODE_d = true;
    }

    /* Sum: '<S98>/Add1' incorporates:
     *  Constant: '<S51>/C'
     *  Delay: '<S51>/MemoryX'
     *  Product: '<S98>/Product'
     *  Reshape: '<S51>/Reshapey'
     */
    localB->u_a = localB->invertzaxisGain - (localP->C_Value[0] *
      localDW->MemoryX_DSTATE[0] + localP->C_Value[1] * localDW->MemoryX_DSTATE
      [1]);

    /* Product: '<S98>/Product2' incorporates:
     *  Constant: '<S55>/KalmanGainM'
     *  DataTypeConversion: '<S94>/Conversion'
     */
    localB->Product2[0] = localP->KalmanGainM_Value[0] * localB->u_a;
    localB->Product2[1] = localP->KalmanGainM_Value[1] * localB->u_a;
  } else {
    if (localDW->EnabledSubsystem_MODE_d) {
      /* Disable for Outport: '<S98>/deltax' */
      localB->Product2[0] = localP->deltax_Y0;
      localB->Product2[1] = localP->deltax_Y0;
      localDW->EnabledSubsystem_MODE_d = false;
    }
  }

  /* End of Outputs for SubSystem: '<S77>/Enabled Subsystem' */

  /* Reshape: '<S51>/Reshapexhat' incorporates:
   *  Delay: '<S51>/MemoryX'
   *  Sum: '<S77>/Add'
   */
  localDW->Delay2_DSTATE = localB->Product2[0] + localDW->MemoryX_DSTATE[0];

  /* Sum: '<S77>/Add' incorporates:
   *  Delay: '<S51>/MemoryX'
   */
  localB->rtb_Add_j_idx_1 = localB->Product2[1] + localDW->MemoryX_DSTATE[1];

  /* SignalConversion: '<S49>/TmpSignal ConversionAtTrigonometric FunctionInport1' incorporates:
   *  Memory: '<S41>/Memory'
   */
  localB->rtb_sincos_o1_idx_0 = localDW->Memory_PreviousInput[2];
  rtb_sincos_o1_idx_1 = localDW->Memory_PreviousInput[1];
  rtb_sincos_o1_idx_2 = localDW->Memory_PreviousInput[0];

  /* Trigonometry: '<S49>/Trigonometric Function' incorporates:
   *  Memory: '<S41>/Memory'
   *  SignalConversion: '<S49>/TmpSignal ConversionAtTrigonometric FunctionInport1'
   */
  localDW->Memory_PreviousInput[0] = (real32_T)sin(localDW->
    Memory_PreviousInput[2]);
  rtb_Product_i_idx_0 = (real32_T)cos(localDW->Memory_PreviousInput[2]);
  localDW->Memory_PreviousInput[1] = (real32_T)sin(localDW->
    Memory_PreviousInput[1]);
  rtb_Product_i_idx_1 = (real32_T)cos(rtb_sincos_o1_idx_1);
  localDW->Memory_PreviousInput[2] = (real32_T)sin(rtb_sincos_o1_idx_2);

  /* SignalConversion: '<S50>/ConcatBufferAtVector ConcatenateIn1' incorporates:
   *  Constant: '<S49>/Constant'
   */
  localB->MathFunction_f[0] = localP->Constant_Value_c;

  /* SignalConversion: '<S50>/ConcatBufferAtVector ConcatenateIn2' incorporates:
   *  Constant: '<S49>/Constant'
   */
  localB->MathFunction_f[1] = localP->Constant_Value_c;

  /* SignalConversion: '<S50>/ConcatBufferAtVector ConcatenateIn3' */
  localB->MathFunction_f[2] = rtb_Product_i_idx_1;

  /* SignalConversion: '<S50>/ConcatBufferAtVector ConcatenateIn4' */
  localB->MathFunction_f[3] = localDW->Memory_PreviousInput[0];

  /* Product: '<S49>/Product1' */
  localB->MathFunction_f[4] = rtb_Product_i_idx_0 * rtb_Product_i_idx_1;

  /* Product: '<S49>/Product3' */
  localB->MathFunction_f[5] = localDW->Memory_PreviousInput[0] *
    localDW->Memory_PreviousInput[1];

  /* SignalConversion: '<S50>/ConcatBufferAtVector ConcatenateIn7' */
  localB->MathFunction_f[6] = rtb_Product_i_idx_0;

  /* Gain: '<S49>/Gain' */
  rtb_DataTypeConversion_c = localP->Gain_Gain_b * localDW->
    Memory_PreviousInput[0];

  /* Product: '<S49>/Product2' */
  localB->MathFunction_f[7] = rtb_Product_i_idx_1 * rtb_DataTypeConversion_c;

  /* Product: '<S49>/Product4' */
  localB->MathFunction_f[8] = rtb_Product_i_idx_0 *
    localDW->Memory_PreviousInput[1];

  /* Product: '<S49>/Divide' incorporates:
   *  Reshape: '<S50>/Reshape (9) to [3x3] column-major'
   */
  for (localB->i = 0; localB->i < 9; localB->i++) {
    localB->Reshape9to3x3columnmajor[localB->i] = localB->MathFunction_f
      [localB->i] / rtb_Product_i_idx_1;
  }

  /* End of Product: '<S49>/Divide' */

  /* Gain: '<S44>/inverseIMU_gain' incorporates:
   *  Bias: '<S44>/Assuming that calib was done level!'
   *  DataTypeConversion: '<S44>/Data Type Conversion'
   *  Sum: '<S44>/Sum1'
   */
  localB->inverseIMU_gain[0] = (rtu_Sensors->HALSensors.HAL_acc_SI.x -
    (rtu_Sensors->SensorCalibration[0] +
     localP->Assumingthatcalibwasdonelevel_Bias[0])) *
    localP->inverseIMU_gain_Gain[0];
  localB->inverseIMU_gain[1] = (rtu_Sensors->HALSensors.HAL_acc_SI.y -
    (rtu_Sensors->SensorCalibration[1] +
     localP->Assumingthatcalibwasdonelevel_Bias[1])) *
    localP->inverseIMU_gain_Gain[1];
  localB->inverseIMU_gain[2] = (rtu_Sensors->HALSensors.HAL_acc_SI.z -
    (rtu_Sensors->SensorCalibration[2] +
     localP->Assumingthatcalibwasdonelevel_Bias[2])) *
    localP->inverseIMU_gain_Gain[2];
  localB->inverseIMU_gain[3] = (rtu_Sensors->HALSensors.HAL_gyro_SI.x -
    (rtu_Sensors->SensorCalibration[3] +
     localP->Assumingthatcalibwasdonelevel_Bias[3])) *
    localP->inverseIMU_gain_Gain[3];
  localB->inverseIMU_gain[4] = (rtu_Sensors->HALSensors.HAL_gyro_SI.y -
    (rtu_Sensors->SensorCalibration[4] +
     localP->Assumingthatcalibwasdonelevel_Bias[4])) *
    localP->inverseIMU_gain_Gain[4];
  localB->inverseIMU_gain[5] = (rtu_Sensors->HALSensors.HAL_gyro_SI.z -
    (rtu_Sensors->SensorCalibration[5] +
     localP->Assumingthatcalibwasdonelevel_Bias[5])) *
    localP->inverseIMU_gain_Gain[5];

  /* DiscreteFilter: '<S44>/IIR_IMUgyro_r' */
  rtb_Product_i_idx_0 = localB->inverseIMU_gain[5];
  localB->denIdx = 1;
  for (localB->i = 0; localB->i < 5; localB->i++) {
    rtb_Product_i_idx_0 -= localP->IIR_IMUgyro_r_DenCoef[localB->denIdx] *
      localDW->IIR_IMUgyro_r_states[localB->i];
    localB->denIdx++;
  }

  rtb_Product_i_idx_0 /= localP->IIR_IMUgyro_r_DenCoef[0];
  rtb_Sum_e_idx_1 = localP->IIR_IMUgyro_r_NumCoef[0] * rtb_Product_i_idx_0;
  localB->denIdx = 1;
  for (localB->i = 0; localB->i < 5; localB->i++) {
    rtb_Sum_e_idx_1 += localP->IIR_IMUgyro_r_NumCoef[localB->denIdx] *
      localDW->IIR_IMUgyro_r_states[localB->i];
    localB->denIdx++;
  }

  rtb_DataTypeConversion_c = rtb_Sum_e_idx_1;

  /* SignalConversion: '<S41>/TmpSignal ConversionAtProductInport2' */
  localB->rtb_Product_i_idx_2 = rtb_DataTypeConversion_c;

  /* Product: '<S41>/Product' incorporates:
   *  SignalConversion: '<S41>/TmpSignal ConversionAtProductInport2'
   */
  for (localB->i = 0; localB->i < 3; localB->i++) {
    localB->sincos_o2[localB->i] = localB->Reshape9to3x3columnmajor[localB->i +
      6] * rtb_DataTypeConversion_c + (localB->Reshape9to3x3columnmajor
      [localB->i + 3] * localB->inverseIMU_gain[4] +
      localB->Reshape9to3x3columnmajor[localB->i] * localB->inverseIMU_gain[3]);
  }

  /* End of Product: '<S41>/Product' */

  /* Sum: '<S41>/Sum' incorporates:
   *  Gain: '<S41>/Gain'
   */
  rtb_Sum_e_idx_0 = localP->Gain_Gain_m * localB->sincos_o2[2] +
    localB->rtb_sincos_o1_idx_0;
  rtb_Sum_e_idx_1 = localP->Gain_Gain_m * localB->sincos_o2[1] +
    rtb_sincos_o1_idx_1;

  /* DiscreteFir: '<S44>/FIR_IMUaccel' */
  rtb_Product_i_idx_1 = localB->inverseIMU_gain[0] *
    localP->FIR_IMUaccel_Coefficients[0];
  localB->cff = 1;
  localB->i = localDW->FIR_IMUaccel_circBuf;
  while (localB->i < 5) {
    rtb_Product_i_idx_1 += localDW->FIR_IMUaccel_states[localB->i] *
      localP->FIR_IMUaccel_Coefficients[localB->cff];
    localB->cff++;
    localB->i++;
  }

  localB->i = 0;
  while (localB->i < localDW->FIR_IMUaccel_circBuf) {
    rtb_Product_i_idx_1 += localDW->FIR_IMUaccel_states[localB->i] *
      localP->FIR_IMUaccel_Coefficients[localB->cff];
    localB->cff++;
    localB->i++;
  }

  localB->FIR_IMUaccel[0] = rtb_Product_i_idx_1;

  /* Math: '<S41>/Math Function' incorporates:
   *  Constant: '<S41>/Constant'
   *  DiscreteFir: '<S44>/FIR_IMUaccel'
   */
  acc1 = (real32_T)floor(localP->Constant_Value_o);
  if ((rtb_Product_i_idx_1 < 0.0F) && (localP->Constant_Value_o > acc1)) {
    localB->rtb_sincos_o1_idx_0 = -rt_powf_snf(-rtb_Product_i_idx_1,
      localP->Constant_Value_o);
  } else {
    localB->rtb_sincos_o1_idx_0 = rt_powf_snf(rtb_Product_i_idx_1,
      localP->Constant_Value_o);
  }

  /* DiscreteFir: '<S44>/FIR_IMUaccel' */
  rtb_Product_i_idx_1 = localB->inverseIMU_gain[1] *
    localP->FIR_IMUaccel_Coefficients[0];
  localB->cff = 1;
  localB->i = localDW->FIR_IMUaccel_circBuf;
  while (localB->i < 5) {
    rtb_Product_i_idx_1 += localDW->FIR_IMUaccel_states[5 + localB->i] *
      localP->FIR_IMUaccel_Coefficients[localB->cff];
    localB->cff++;
    localB->i++;
  }

  localB->i = 0;
  while (localB->i < localDW->FIR_IMUaccel_circBuf) {
    rtb_Product_i_idx_1 += localDW->FIR_IMUaccel_states[5 + localB->i] *
      localP->FIR_IMUaccel_Coefficients[localB->cff];
    localB->cff++;
    localB->i++;
  }

  localB->FIR_IMUaccel[1] = rtb_Product_i_idx_1;

  /* Math: '<S41>/Math Function' incorporates:
   *  Constant: '<S41>/Constant'
   *  DiscreteFir: '<S44>/FIR_IMUaccel'
   */
  if ((rtb_Product_i_idx_1 < 0.0F) && (localP->Constant_Value_o > acc1)) {
    rtb_sincos_o1_idx_1 = -rt_powf_snf(-rtb_Product_i_idx_1,
      localP->Constant_Value_o);
  } else {
    rtb_sincos_o1_idx_1 = rt_powf_snf(rtb_Product_i_idx_1,
      localP->Constant_Value_o);
  }

  /* DiscreteFir: '<S44>/FIR_IMUaccel' */
  rtb_Product_i_idx_1 = localB->inverseIMU_gain[2] *
    localP->FIR_IMUaccel_Coefficients[0];
  localB->cff = 1;
  localB->i = localDW->FIR_IMUaccel_circBuf;
  while (localB->i < 5) {
    rtb_Product_i_idx_1 += localDW->FIR_IMUaccel_states[10 + localB->i] *
      localP->FIR_IMUaccel_Coefficients[localB->cff];
    localB->cff++;
    localB->i++;
  }

  localB->i = 0;
  while (localB->i < localDW->FIR_IMUaccel_circBuf) {
    rtb_Product_i_idx_1 += localDW->FIR_IMUaccel_states[10 + localB->i] *
      localP->FIR_IMUaccel_Coefficients[localB->cff];
    localB->cff++;
    localB->i++;
  }

  localB->FIR_IMUaccel[2] = rtb_Product_i_idx_1;

  /* Math: '<S41>/Math Function' incorporates:
   *  Constant: '<S41>/Constant'
   *  DiscreteFir: '<S44>/FIR_IMUaccel'
   */
  if ((rtb_Product_i_idx_1 < 0.0F) && (localP->Constant_Value_o > acc1)) {
    acc1 = -rt_powf_snf(-rtb_Product_i_idx_1, localP->Constant_Value_o);
  } else {
    acc1 = rt_powf_snf(rtb_Product_i_idx_1, localP->Constant_Value_o);
  }

  /* Sum: '<S41>/Sum of Elements' */
  rtb_DataTypeConversion_c = (localB->rtb_sincos_o1_idx_0 + rtb_sincos_o1_idx_1)
    + acc1;

  /* Sqrt: '<S41>/Sqrt' */
  rtb_DataTypeConversion_c = (real32_T)sqrt(rtb_DataTypeConversion_c);

  /* If: '<S41>/If' incorporates:
   *  Constant: '<S45>/Constant'
   *  Constant: '<S46>/Constant'
   *  Inport: '<S48>/prin'
   *  Logic: '<S41>/Logical Operator'
   *  RelationalOperator: '<S45>/Compare'
   *  RelationalOperator: '<S46>/Compare'
   */
  if ((rtb_DataTypeConversion_c > localP->CompareToConstant_const) &&
      (rtb_DataTypeConversion_c < localP->CompareToConstant1_const)) {
    /* Outputs for IfAction SubSystem: '<S41>/If Action Subsystem' incorporates:
     *  ActionPort: '<S47>/Action Port'
     */
    /* Sum: '<S47>/Sum' incorporates:
     *  DiscreteFir: '<S44>/FIR_IMUaccel'
     *  Gain: '<S47>/Gain'
     *  Gain: '<S47>/Gain1'
     *  Product: '<S47>/Divide'
     *  Trigonometry: '<S47>/Trigonometric Function'
     */
    localB->Merge[1] = (real32_T)atan(localB->FIR_IMUaccel[1] /
      rtb_Product_i_idx_1) * localP->Gain1_Gain_p + localP->Gain_Gain_n *
      rtb_Sum_e_idx_0;

    /* Gain: '<S47>/Gain2' */
    acc1 = localP->Gain2_Gain * localB->FIR_IMUaccel[0];

    /* Trigonometry: '<S47>/Trigonometric Function1' */
    if (acc1 > 1.0F) {
      acc1 = 1.0F;
    } else {
      if (acc1 < -1.0F) {
        acc1 = -1.0F;
      }
    }

    /* Sum: '<S47>/Sum1' incorporates:
     *  Gain: '<S47>/Gain3'
     *  Gain: '<S47>/Gain4'
     *  Trigonometry: '<S47>/Trigonometric Function1'
     */
    localB->Merge[0] = localP->Gain4_Gain * rtb_Sum_e_idx_1 + localP->Gain3_Gain
      * (real32_T)asin(acc1);

    /* End of Outputs for SubSystem: '<S41>/If Action Subsystem' */
  } else {
    /* Outputs for IfAction SubSystem: '<S41>/If Action Subsystem2' incorporates:
     *  ActionPort: '<S48>/Action Port'
     */
    localB->Merge[0] = rtb_Sum_e_idx_1;
    localB->Merge[1] = rtb_Sum_e_idx_0;

    /* End of Outputs for SubSystem: '<S41>/If Action Subsystem2' */
  }

  /* End of If: '<S41>/If' */

  /* DataTypeConversion: '<S41>/Data Type Conversion3' incorporates:
   *  Gain: '<S41>/Gain'
   *  Sum: '<S41>/Sum'
   */
  localDW->Memory_PreviousInput[0] = localP->Gain_Gain_m * localB->sincos_o2[0]
    + rtb_sincos_o1_idx_2;
  localDW->Memory_PreviousInput[1] = localB->Merge[0];
  localDW->Memory_PreviousInput[2] = localB->Merge[1];

  /* Delay: '<S110>/MemoryX' incorporates:
   *  Constant: '<S110>/X0'
   *  Reshape: '<S110>/ReshapeX0'
   */
  if (localDW->icLoad_c != 0) {
    localDW->MemoryX_DSTATE_m[0] = localP->X0_Value_a[0];
    localDW->MemoryX_DSTATE_m[1] = localP->X0_Value_a[1];
  }

  /* Abs: '<S109>/Abs2' */
  rtb_DataTypeConversion_c = (real32_T)fabs(localDW->Memory_PreviousInput[1]);

  /* RelationalOperator: '<S121>/Compare' incorporates:
   *  Constant: '<S121>/Constant'
   */
  rtb_Compare_o = (rtb_DataTypeConversion_c <= localP->maxp_const);

  /* Abs: '<S109>/Abs3' */
  rtb_DataTypeConversion_c = (real32_T)fabs(localDW->Memory_PreviousInput[2]);

  /* RelationalOperator: '<S123>/Compare' incorporates:
   *  Constant: '<S123>/Constant'
   */
  rtb_Compare_p = (rtb_DataTypeConversion_c <= localP->maxq_const);

  /* Abs: '<S109>/Abs' incorporates:
   *  Abs: '<S109>/Abs4'
   */
  rtb_sincos_o1_idx_2 = (real32_T)fabs(localB->inverseIMU_gain[3]);
  rtb_DataTypeConversion_c = rtb_sincos_o1_idx_2;

  /* RelationalOperator: '<S125>/Compare' incorporates:
   *  Constant: '<S125>/Constant'
   */
  rtb_Compare_ae = (rtb_DataTypeConversion_c <= localP->maxw1_const);

  /* Abs: '<S109>/Abs1' incorporates:
   *  Abs: '<S109>/Abs5'
   */
  acc1 = (real32_T)fabs(localB->inverseIMU_gain[4]);
  rtb_DataTypeConversion_c = acc1;

  /* RelationalOperator: '<S126>/Compare' incorporates:
   *  Constant: '<S126>/Constant'
   */
  rtb_Compare_ln = (rtb_DataTypeConversion_c <= localP->maxw2_const);
  for (localB->cff = 0; localB->cff < 2; localB->cff++) {
    /* DiscreteFilter: '<S109>/IIRgyroz' */
    localB->memOffset = localB->cff * 5;
    rtb_Sum_e_idx_1 = localB->inverseIMU_gain[localB->cff + 3];
    localB->denIdx = 1;
    for (localB->i = 0; localB->i < 5; localB->i++) {
      rtb_Sum_e_idx_1 -= localDW->IIRgyroz_states[localB->memOffset + localB->i]
        * localP->IIRgyroz_DenCoef[localB->denIdx];
      localB->denIdx++;
    }

    localDW->IIRgyroz_tmp[localB->cff] = rtb_Sum_e_idx_1 /
      localP->IIRgyroz_DenCoef[0];
    rtb_Sum_e_idx_1 = localP->IIRgyroz_NumCoef[0] * localDW->IIRgyroz_tmp
      [localB->cff];
    localB->denIdx = 1;
    for (localB->i = 0; localB->i < 5; localB->i++) {
      rtb_Sum_e_idx_1 += localDW->IIRgyroz_states[localB->memOffset + localB->i]
        * localP->IIRgyroz_NumCoef[localB->denIdx];
      localB->denIdx++;
    }

    /* SampleTimeMath: '<S118>/TSamp' incorporates:
     *  DiscreteFilter: '<S109>/IIRgyroz'
     *
     * About '<S118>/TSamp':
     *  y = u * K where K = 1 / ( w * Ts )
     */
    rtb_Sum_e_idx_1 *= localP->TSamp_WtEt;

    /* Sum: '<S118>/Diff' incorporates:
     *  UnitDelay: '<S118>/UD'
     */
    localDW->UD_DSTATE[localB->cff] = rtb_Sum_e_idx_1 - localDW->
      UD_DSTATE[localB->cff];

    /* SampleTimeMath: '<S118>/TSamp'
     *
     * About '<S118>/TSamp':
     *  y = u * K where K = 1 / ( w * Ts )
     */
    localB->TSamp[localB->cff] = rtb_Sum_e_idx_1;
  }

  /* Abs: '<S109>/Abs6' */
  rtb_DataTypeConversion_c = (real32_T)fabs(localDW->UD_DSTATE[0]);

  /* RelationalOperator: '<S119>/Compare' incorporates:
   *  Constant: '<S119>/Constant'
   */
  rtb_Compare_mu = (rtb_DataTypeConversion_c <= localP->maxdw1_const);

  /* Abs: '<S109>/Abs7' */
  rtb_DataTypeConversion_c = (real32_T)fabs(localDW->UD_DSTATE[1]);

  /* Logic: '<S109>/Logical Operator' incorporates:
   *  Constant: '<S120>/Constant'
   *  RelationalOperator: '<S120>/Compare'
   */
  rtb_Compare_o = (rtb_Compare_o && rtb_Compare_p && rtb_Compare_ae &&
                   rtb_Compare_ln && rtb_Compare_mu && (rtb_DataTypeConversion_c
    <= localP->maxdw2_const));

  /* Abs: '<S109>/Abs4' */
  rtb_DataTypeConversion_c = rtb_sincos_o1_idx_2;

  /* RelationalOperator: '<S122>/Compare' incorporates:
   *  Constant: '<S122>/Constant'
   */
  rtb_Compare_p = (rtb_DataTypeConversion_c <= localP->maxp2_const);

  /* Abs: '<S109>/Abs5' */
  rtb_DataTypeConversion_c = acc1;

  /* Logic: '<S109>/Logical Operator1' incorporates:
   *  Constant: '<S124>/Constant'
   *  RelationalOperator: '<S124>/Compare'
   */
  rtb_Compare_p = (rtb_Compare_p && (rtb_DataTypeConversion_c <=
    localP->maxq2_const));

  /* Switch: '<S104>/Switch' incorporates:
   *  Constant: '<S107>/Constant'
   *  Gain: '<S104>/opticalFlowErrorCorrect'
   *  Product: '<S104>/ '
   *  RelationalOperator: '<S107>/Compare'
   *  UnitDelay: '<S108>/Output'
   */
  if (localDW->Output_DSTATE < localP->CompareToConstant_const_o) {
    localB->Switch[0] = rtu_Sensors->VisionSensors.opticalFlow_data[0];
    localB->Switch[1] = rtu_Sensors->VisionSensors.opticalFlow_data[1];
    localB->Switch[2] = rtu_Sensors->VisionSensors.opticalFlow_data[2];
  } else {
    /* Gain: '<S104>/Gain' incorporates:
     *  DataTypeConversion: '<S42>/Data Type Conversion'
     *  Reshape: '<S51>/Reshapexhat'
     */
    rtb_sincos_o1_idx_2 = localP->Gain_Gain_c * (real32_T)localDW->Delay2_DSTATE;
    localB->Switch[0] = rtu_Sensors->VisionSensors.opticalFlow_data[0] *
      rtb_sincos_o1_idx_2 * localP->opticalFlowErrorCorrect_Gain;
    localB->Switch[1] = rtu_Sensors->VisionSensors.opticalFlow_data[1] *
      rtb_sincos_o1_idx_2 * localP->opticalFlowErrorCorrect_Gain;
    localB->Switch[2] = rtu_Sensors->VisionSensors.opticalFlow_data[2] *
      rtb_sincos_o1_idx_2 * localP->opticalFlowErrorCorrect_Gain;
  }

  /* End of Switch: '<S104>/Switch' */

  /* Abs: '<S109>/Abs8' incorporates:
   *  Delay: '<S104>/Delay'
   *  Sum: '<S109>/Add'
   */
  rtb_DataTypeConversion_c = (real32_T)fabs(localB->Switch[0] -
    localDW->Delay_DSTATE_a[0]);

  /* RelationalOperator: '<S127>/Compare' incorporates:
   *  Constant: '<S127>/Constant'
   */
  rtb_Compare_ae = (rtb_DataTypeConversion_c <= localP->maxw3_const);

  /* Abs: '<S109>/Abs9' incorporates:
   *  Delay: '<S104>/Delay'
   *  Sum: '<S109>/Add'
   */
  rtb_DataTypeConversion_c = (real32_T)fabs(localB->Switch[1] -
    localDW->Delay_DSTATE_a[1]);

  /* Logic: '<S109>/Logical Operator3' incorporates:
   *  Constant: '<S128>/Constant'
   *  Constant: '<S129>/Constant'
   *  DataTypeConversion: '<S42>/Data Type Conversion'
   *  Logic: '<S109>/Logical Operator2'
   *  RelationalOperator: '<S128>/Compare'
   *  RelationalOperator: '<S129>/Compare'
   *  Reshape: '<S51>/Reshapexhat'
   */
  localB->LogicalOperator3 = ((rtb_Compare_o || rtb_Compare_p) && rtb_Compare_ae
    && (rtb_DataTypeConversion_c <= localP->maxw4_const) && ((real32_T)
    localDW->Delay2_DSTATE <= localP->minHeightforOF_const));

  /* Outputs for Enabled SubSystem: '<S152>/Enabled Subsystem' incorporates:
   *  EnablePort: '<S173>/Enable'
   */
  /* DataTypeConversion: '<S110>/DataTypeConversionEnable' */
  if (localB->LogicalOperator3) {
    if (!localDW->EnabledSubsystem_MODE) {
      localDW->EnabledSubsystem_MODE = true;
    }

    /* Sum: '<S173>/Add1' incorporates:
     *  Constant: '<S110>/C'
     *  Delay: '<S110>/MemoryX'
     *  Product: '<S173>/Product'
     *  Reshape: '<S110>/Reshapey'
     */
    acc1 = localB->Switch[0] - (localP->C_Value_h[0] * localDW->
      MemoryX_DSTATE_m[0] + localP->C_Value_h[2] * localDW->MemoryX_DSTATE_m[1]);
    localB->rtb_sincos_o1_idx_0 = localB->Switch[1] - (localP->C_Value_h[1] *
      localDW->MemoryX_DSTATE_m[0] + localP->C_Value_h[3] *
      localDW->MemoryX_DSTATE_m[1]);

    /* Product: '<S173>/Product2' incorporates:
     *  Constant: '<S130>/KalmanGainM'
     *  DataTypeConversion: '<S169>/Conversion'
     */
    localB->Product2_o[0] = 0.0F;
    localB->Product2_o[0] += (real32_T)localP->KalmanGainM_Value_g[0] * acc1;
    localB->Product2_o[0] += (real32_T)localP->KalmanGainM_Value_g[2] *
      localB->rtb_sincos_o1_idx_0;
    localB->Product2_o[1] = 0.0F;
    localB->Product2_o[1] += (real32_T)localP->KalmanGainM_Value_g[1] * acc1;
    localB->Product2_o[1] += (real32_T)localP->KalmanGainM_Value_g[3] *
      localB->rtb_sincos_o1_idx_0;
  } else {
    if (localDW->EnabledSubsystem_MODE) {
      /* Disable for Outport: '<S173>/deltax' */
      localB->Product2_o[0] = localP->deltax_Y0_m;
      localB->Product2_o[1] = localP->deltax_Y0_m;
      localDW->EnabledSubsystem_MODE = false;
    }
  }

  /* End of Outputs for SubSystem: '<S152>/Enabled Subsystem' */

  /* Reshape: '<S110>/Reshapexhat' incorporates:
   *  Delay: '<S110>/MemoryX'
   *  Sum: '<S152>/Add'
   */
  localDW->Delay_DSTATE_a[0] = localB->Product2_o[0] + localDW->
    MemoryX_DSTATE_m[0];
  localDW->Delay_DSTATE_a[1] = localB->Product2_o[1] + localDW->
    MemoryX_DSTATE_m[1];

  /* Outputs for Enabled SubSystem: '<S36>/Subsystem' incorporates:
   *  EnablePort: '<S40>/Enable'
   */
  /* Trigonometry: '<S53>/sincos' incorporates:
   *  Switch: '<S3>/Switch_refAtt'
   *  Trigonometry: '<S112>/sincos'
   *  Trigonometry: '<S174>/sincos'
   *  Trigonometry: '<S40>/Trigonometric Function'
   *  Trigonometry: '<S9>/Trigonometric Function'
   */
  acc1 = (real32_T)sin(localDW->Memory_PreviousInput[0]);
  rtb_Sum_e_idx_0 = (real32_T)cos(localDW->Memory_PreviousInput[0]);

  /* End of Outputs for SubSystem: '<S36>/Subsystem' */
  rtb_sincos_o1_idx_2 = (real32_T)sin(localDW->Memory_PreviousInput[1]);
  rtb_Sum_e_idx_1 = (real32_T)cos(localDW->Memory_PreviousInput[1]);
  localB->rtb_sincos_o1_idx_0 = (real32_T)sin(localDW->Memory_PreviousInput[2]);
  rtb_sincos_o1_idx_1 = (real32_T)cos(localDW->Memory_PreviousInput[2]);

  /* Fcn: '<S53>/Fcn11' incorporates:
   *  Trigonometry: '<S53>/sincos'
   */
  localB->MathFunction_f[0] = rtb_Sum_e_idx_1 * rtb_Sum_e_idx_0;

  /* Fcn: '<S53>/Fcn21' incorporates:
   *  Fcn: '<S53>/Fcn22'
   *  Trigonometry: '<S53>/sincos'
   */
  rtb_MathFunction_f_tmp = localB->rtb_sincos_o1_idx_0 * rtb_sincos_o1_idx_2;
  localB->MathFunction_f[1] = rtb_MathFunction_f_tmp * rtb_Sum_e_idx_0 -
    rtb_sincos_o1_idx_1 * acc1;

  /* Fcn: '<S53>/Fcn31' incorporates:
   *  Fcn: '<S53>/Fcn32'
   *  Trigonometry: '<S53>/sincos'
   */
  rtb_MathFunction_f_tmp_0 = rtb_sincos_o1_idx_1 * rtb_sincos_o1_idx_2;
  localB->MathFunction_f[2] = rtb_MathFunction_f_tmp_0 * rtb_Sum_e_idx_0 +
    localB->rtb_sincos_o1_idx_0 * acc1;

  /* Fcn: '<S53>/Fcn12' incorporates:
   *  Trigonometry: '<S53>/sincos'
   */
  localB->MathFunction_f[3] = rtb_Sum_e_idx_1 * acc1;

  /* Fcn: '<S53>/Fcn22' incorporates:
   *  Trigonometry: '<S53>/sincos'
   */
  localB->MathFunction_f[4] = rtb_MathFunction_f_tmp * acc1 +
    rtb_sincos_o1_idx_1 * rtb_Sum_e_idx_0;

  /* Fcn: '<S53>/Fcn32' incorporates:
   *  Trigonometry: '<S53>/sincos'
   */
  localB->MathFunction_f[5] = rtb_MathFunction_f_tmp_0 * acc1 -
    localB->rtb_sincos_o1_idx_0 * rtb_Sum_e_idx_0;

  /* Fcn: '<S53>/Fcn13' incorporates:
   *  Trigonometry: '<S53>/sincos'
   */
  localB->MathFunction_f[6] = -rtb_sincos_o1_idx_2;

  /* Fcn: '<S53>/Fcn23' incorporates:
   *  Trigonometry: '<S53>/sincos'
   */
  localB->MathFunction_f[7] = localB->rtb_sincos_o1_idx_0 * rtb_Sum_e_idx_1;

  /* Fcn: '<S53>/Fcn33' incorporates:
   *  Trigonometry: '<S53>/sincos'
   */
  localB->MathFunction_f[8] = rtb_sincos_o1_idx_1 * rtb_Sum_e_idx_1;

  /* SignalConversion: '<S42>/TmpSignal ConversionAtProduct1Inport2' incorporates:
   *  Delay: '<S6>/Delay1'
   */
  localB->u_a = localDW->Delay1_DSTATE[0];
  localB->unnamed_idx_1 = localDW->Delay1_DSTATE[1];
  localB->unnamed_idx_0 = localDW->Delay1_DSTATE[0];
  localB->unnamed_idx_1_k = localDW->Delay1_DSTATE[1];

  /* Product: '<S42>/Product1' incorporates:
   *  Reshape: '<S51>/Reshapexhat'
   *  SignalConversion: '<S42>/TmpSignal ConversionAtProduct1Inport2'
   */
  for (localB->i = 0; localB->i < 3; localB->i++) {
    localB->rtb_Product1_tmp = localB->MathFunction_f[localB->i + 3];
    localB->rtb_Product1_tmp_c = localB->MathFunction_f[localB->i + 6] *
      localB->rtb_Add_j_idx_1;
    localB->Product1[localB->i] = localB->rtb_Product1_tmp_c +
      (localB->rtb_Product1_tmp * localB->unnamed_idx_1 + localB->
       MathFunction_f[localB->i] * localB->u_a);
    localB->rtb_MathFunction_f_c[localB->i] = localB->rtb_Product1_tmp_c +
      (localB->rtb_Product1_tmp * localB->unnamed_idx_1_k +
       localB->MathFunction_f[localB->i] * localB->unnamed_idx_0);
  }

  /* End of Product: '<S42>/Product1' */

  /* SignalConversion: '<S1>/TmpSignal ConversionAtTo WorkspaceInport1' incorporates:
   *  DataTypeConversion: '<S42>/Data Type Conversion'
   *  DataTypeConversion: '<S42>/Data Type Conversion2'
   *  DiscreteIntegrator: '<S105>/SimplyIntegrateVelocity'
   *  Reshape: '<S51>/Reshapexhat'
   *  SignalConversion: '<S41>/TmpSignal ConversionAtProductInport2'
   */
  localB->TmpSignalConversionAtToWorkspaceInport1[0] =
    localDW->SimplyIntegrateVelocity_DSTATE[0];
  localB->TmpSignalConversionAtToWorkspaceInport1[1] =
    localDW->SimplyIntegrateVelocity_DSTATE[1];
  localB->TmpSignalConversionAtToWorkspaceInport1[2] = (real32_T)
    localDW->Delay2_DSTATE;
  localB->TmpSignalConversionAtToWorkspaceInport1[3] =
    localDW->Memory_PreviousInput[0];
  localB->TmpSignalConversionAtToWorkspaceInport1[4] =
    localDW->Memory_PreviousInput[1];
  localB->TmpSignalConversionAtToWorkspaceInport1[5] =
    localDW->Memory_PreviousInput[2];
  localB->TmpSignalConversionAtToWorkspaceInport1[6] = localDW->Delay_DSTATE_a[0];
  localB->TmpSignalConversionAtToWorkspaceInport1[7] = localDW->Delay_DSTATE_a[1];
  localB->TmpSignalConversionAtToWorkspaceInport1[8] = (real32_T)
    localB->Product1[2];
  localB->TmpSignalConversionAtToWorkspaceInport1[9] = localB->inverseIMU_gain[3];
  localB->TmpSignalConversionAtToWorkspaceInport1[10] = localB->inverseIMU_gain
    [4];
  localB->TmpSignalConversionAtToWorkspaceInport1[11] =
    localB->rtb_Product_i_idx_2;

  /* ToWorkspace: '<S1>/To Workspace' */
  {
    double locTime = flightControlSystem_M->Timing.taskTime0;
    ;
    rt_UpdateStructLogVar((StructLogVar *)localDW->ToWorkspace_PWORK.LoggedData,
                          &locTime,
                          &localB->TmpSignalConversionAtToWorkspaceInport1[0]);
  }

  /* Math: '<S42>/Math Function' */
  for (localB->i = 0; localB->i < 3; localB->i++) {
    localB->rtb_MathFunction_f_m[3 * localB->i] = localB->MathFunction_f
      [localB->i];
    localB->rtb_MathFunction_f_m[1 + 3 * localB->i] = localB->
      MathFunction_f[localB->i + 3];
    localB->rtb_MathFunction_f_m[2 + 3 * localB->i] = localB->
      MathFunction_f[localB->i + 6];
  }

  for (localB->i = 0; localB->i < 9; localB->i++) {
    localB->MathFunction_f[localB->i] = localB->rtb_MathFunction_f_m[localB->i];
  }

  /* End of Math: '<S42>/Math Function' */

  /* Fcn: '<S112>/Fcn11' */
  localB->Reshape9to3x3columnmajor[0] = rtb_Sum_e_idx_1 * rtb_Sum_e_idx_0;

  /* Fcn: '<S112>/Fcn21' incorporates:
   *  Fcn: '<S112>/Fcn22'
   */
  rtb_MathFunction_f_tmp = localB->rtb_sincos_o1_idx_0 * rtb_sincos_o1_idx_2;
  localB->Reshape9to3x3columnmajor[1] = rtb_MathFunction_f_tmp * rtb_Sum_e_idx_0
    - rtb_sincos_o1_idx_1 * acc1;

  /* Fcn: '<S112>/Fcn31' incorporates:
   *  Fcn: '<S112>/Fcn32'
   */
  rtb_MathFunction_f_tmp_0 = rtb_sincos_o1_idx_1 * rtb_sincos_o1_idx_2;
  localB->Reshape9to3x3columnmajor[2] = rtb_MathFunction_f_tmp_0 *
    rtb_Sum_e_idx_0 + localB->rtb_sincos_o1_idx_0 * acc1;

  /* Fcn: '<S112>/Fcn12' */
  localB->Reshape9to3x3columnmajor[3] = rtb_Sum_e_idx_1 * acc1;

  /* Fcn: '<S112>/Fcn22' */
  localB->Reshape9to3x3columnmajor[4] = rtb_MathFunction_f_tmp * acc1 +
    rtb_sincos_o1_idx_1 * rtb_Sum_e_idx_0;

  /* Fcn: '<S112>/Fcn32' */
  localB->Reshape9to3x3columnmajor[5] = rtb_MathFunction_f_tmp_0 * acc1 -
    localB->rtb_sincos_o1_idx_0 * rtb_Sum_e_idx_0;

  /* Fcn: '<S112>/Fcn13' */
  localB->Reshape9to3x3columnmajor[6] = -rtb_sincos_o1_idx_2;

  /* Fcn: '<S112>/Fcn23' */
  localB->Reshape9to3x3columnmajor[7] = localB->rtb_sincos_o1_idx_0 *
    rtb_Sum_e_idx_1;

  /* Fcn: '<S112>/Fcn33' */
  localB->Reshape9to3x3columnmajor[8] = rtb_sincos_o1_idx_1 * rtb_Sum_e_idx_1;

  /* Fcn: '<S174>/Fcn11' */
  localB->MathFunction[0] = rtb_Sum_e_idx_1 * rtb_Sum_e_idx_0;

  /* Fcn: '<S174>/Fcn21' */
  localB->MathFunction[1] = localB->rtb_sincos_o1_idx_0 * rtb_sincos_o1_idx_2 *
    rtb_Sum_e_idx_0 - rtb_sincos_o1_idx_1 * acc1;

  /* Fcn: '<S174>/Fcn31' incorporates:
   *  Fcn: '<S174>/Fcn32'
   */
  rtb_MathFunction_f_tmp = rtb_sincos_o1_idx_1 * rtb_sincos_o1_idx_2;
  localB->MathFunction[2] = rtb_MathFunction_f_tmp * rtb_Sum_e_idx_0 +
    localB->rtb_sincos_o1_idx_0 * acc1;

  /* Fcn: '<S174>/Fcn12' */
  localB->MathFunction[3] = rtb_Sum_e_idx_1 * acc1;

  /* Fcn: '<S174>/Fcn22' */
  localB->MathFunction[4] = localB->rtb_sincos_o1_idx_0 * rtb_sincos_o1_idx_2 *
    acc1 + rtb_sincos_o1_idx_1 * rtb_Sum_e_idx_0;

  /* Fcn: '<S174>/Fcn32' */
  localB->MathFunction[5] = rtb_MathFunction_f_tmp * acc1 -
    localB->rtb_sincos_o1_idx_0 * rtb_Sum_e_idx_0;

  /* Fcn: '<S174>/Fcn13' */
  localB->MathFunction[6] = -rtb_sincos_o1_idx_2;

  /* Fcn: '<S174>/Fcn23' */
  localB->MathFunction[7] = localB->rtb_sincos_o1_idx_0 * rtb_Sum_e_idx_1;

  /* Fcn: '<S174>/Fcn33' */
  localB->MathFunction[8] = rtb_sincos_o1_idx_1 * rtb_Sum_e_idx_1;

  /* Math: '<S105>/Math Function' */
  for (localB->i = 0; localB->i < 3; localB->i++) {
    localB->rtb_MathFunction_f_m[3 * localB->i] = localB->MathFunction[localB->i];
    localB->rtb_MathFunction_f_m[1 + 3 * localB->i] = localB->
      MathFunction[localB->i + 3];
    localB->rtb_MathFunction_f_m[2 + 3 * localB->i] = localB->
      MathFunction[localB->i + 6];
  }

  for (localB->i = 0; localB->i < 9; localB->i++) {
    localB->MathFunction[localB->i] = localB->rtb_MathFunction_f_m[localB->i];
  }

  /* End of Math: '<S105>/Math Function' */

  /* DiscreteIntegrator: '<S7>/Discrete-Time Integrator' */
  localB->Akxhatkk1_f[0] = localDW->DiscreteTimeIntegrator_DSTATE[0];
  localB->Akxhatkk1_f[1] = localDW->DiscreteTimeIntegrator_DSTATE[1];

  /* BusAssignment: '<S1>/Control Mode Update' incorporates:
   *  Constant: '<S1>/controlModePosVsOrient'
   */
  localB->ControlModeUpdate = *rtu_ReferenceValueServerCmds;
  localB->ControlModeUpdate.controlModePosVSOrient =
    localP->controlModePosVsOrient_Value;

  /* Sum: '<S33>/Subtract' */
  localB->error = rtu_VisionbasedData[0] - rtu_VisionbasedData[1];

  /* Switch: '<S37>/Switch2' incorporates:
   *  Constant: '<S37>/Constant4'
   *  Constant: '<S37>/Constant5'
   *  Constant: '<S37>/Constant6'
   *  Delay: '<S37>/Delay'
   *  Gain: '<S37>/Gain'
   *  Gain: '<S37>/Gain1'
   *  Sum: '<S37>/Sum3'
   *  Sum: '<S37>/Sum4'
   */
  if (rtu_VisionbasedData[2] - (localP->Gain_Gain * localDW->Delay_DSTATE +
       localP->Constant4_Value) * localP->Gain1_Gain > localP->Switch2_Threshold)
  {
    localB->Switch2 = localP->Constant5_Value;
  } else {
    localB->Switch2 = localP->Constant6_Value;
  }

  /* End of Switch: '<S37>/Switch2' */

  /* Chart: '<S33>/State logic XYZ' incorporates:
   *  Constant: '<S5>/Altura de Vuelo'
   *  Constant: '<S5>/Tiempo de Calibracion'
   *  Constant: '<S5>/Tiempo de Frenado'
   *  DataTypeConversion: '<S42>/Data Type Conversion'
   *  DiscreteIntegrator: '<S105>/SimplyIntegrateVelocity'
   *  Reshape: '<S51>/Reshapexhat'
   */
  if (localDW->is_active_c1_flightControlSystem == 0U) {
    localDW->is_active_c1_flightControlSystem = 1U;
    localDW->is_c1_flightControlSystem = flightControlSystem_IN_Beginning1;
    localB->RefX = 0.0F;
    localB->RefY = 0.0F;
    localB->RefZ = localP->AlturadeVuelo_Value;
    localDW->zp = 0.0F;
    localDW->PastYaw = 0.0F;
    localDW->PastPitch = 0.0F;
    localB->fin = 0.0F;
    localB->poliAlpha = 0.0F;
    localB->poliBeta = 0.0F;
    localDW->Etapa2 = 0.0;
    localDW->landSpot = 2.0;
  } else {
    guard1 = false;
    guard2 = false;
    guard3 = false;
    switch (localDW->is_c1_flightControlSystem) {
     case flightControlSystem_IN_Alinearse:
      localB->rtb_Add_j_idx_1 = fabs(localB->error);
      if (localB->rtb_Add_j_idx_1 < 1.0) {
        guard1 = true;
      } else if (localB->Switch2 != 0.0) {
        localDW->PT = localB->ControlModeUpdate.live_time_ticks;
        localB->fin = 0.0F;
        guard1 = true;
      } else if ((localB->rtb_Add_j_idx_1 >= 1.0) && (!(localB->Switch2 != 0.0)))
      {
        localDW->PastYaw += (real32_T)(0.0001 * localB->error);
        localDW->is_c1_flightControlSystem = flightControlSystem_IN_Alinearse;
        localB->yaw = localDW->PastYaw;
        localB->pitch = localDW->PastPitch;
      } else {
        localB->yaw = localDW->PastYaw;
        localB->pitch = localDW->PastPitch;
      }
      break;

     case flightControlSystem_IN_Beginning1:
      if (localB->ControlModeUpdate.live_time_ticks >=
          localP->TiempodeCalibracion_Value) {
        localDW->PastPitch = -0.0174532924F;
        localB->roll = 1.0F;
        localDW->PT = localB->ControlModeUpdate.live_time_ticks;
        localDW->is_c1_flightControlSystem = flightControlSystem_IN_vayaRapido;
        localB->pitch = localDW->PastPitch;
        localB->yaw = localDW->PastYaw;
      } else {
        localB->RefX = 0.0F;
        localB->RefY = 0.0F;
        localB->RefZ = localP->AlturadeVuelo_Value;
        localDW->zp = 0.0F;
        localDW->PastYaw = 0.0F;
        localDW->PastPitch = 0.0F;
        localB->fin = 0.0F;
        localB->poliAlpha = 0.0F;
        localB->poliBeta = 0.0F;
        localDW->Etapa2 = 0.0;
        localDW->landSpot = 2.0;
      }
      break;

     case flightControlSystem_IN_DetectarCirculo:
      localB->rtb_Add_j_idx_1 = fabs(rtu_VisionbasedData[4] - 60.0);
      if (localB->rtb_Add_j_idx_1 > localDW->landSpot) {
        qY = localDW->PT + 10U;
        if (qY < localDW->PT) {
          qY = MAX_uint32_T;
        }

        if ((localB->ControlModeUpdate.live_time_ticks > qY) && (localDW->Etapa2
             == 0.0)) {
          localDW->PastPitch = (real32_T)((rtu_VisionbasedData[4] - 60.0) * 0.05
            * 0.017453292519943295);
          localDW->is_c1_flightControlSystem =
            flightControlSystem_IN_DetectarCirculo;
          localB->RefZ = localDW->zp;
          localB->pitch = localDW->PastPitch;
        } else {
          guard2 = true;
        }
      } else {
        guard2 = true;
      }
      break;

     case flightControlSystem_IN_Frenar:
      if (!(localB->Switch2 != 0.0)) {
        localDW->PT = localB->ControlModeUpdate.live_time_ticks;
        localB->fin = 0.0F;
        localB->poliAlpha = 0.0F;
        localDW->is_c1_flightControlSystem = flightControlSystem_IN_vayaRapido;
        localB->pitch = localDW->PastPitch;
        localB->yaw = localDW->PastYaw;
      } else {
        localB->rtb_Add_j_idx_1 = (real32_T)fabs(localDW->Delay_DSTATE_a[0]);
        if (localB->rtb_Add_j_idx_1 < 0.1) {
          localB->unnamed_idx_1 = rt_roundd_snf((real_T)localDW->PT +
            localP->TiempodeFrenado_Value);
          if (localB->unnamed_idx_1 < 4.294967296E+9) {
            if (localB->unnamed_idx_1 >= 0.0) {
              qY = (uint32_T)localB->unnamed_idx_1;
            } else {
              qY = 0U;
            }
          } else {
            qY = MAX_uint32_T;
          }

          if (localB->ControlModeUpdate.live_time_ticks > qY) {
            localDW->zp = localB->RefZ;
            localB->fin = 0.0F;
            localDW->PT = localB->ControlModeUpdate.live_time_ticks;
            localDW->PastPitch = 0.0F;
            localDW->is_c1_flightControlSystem = flightControlSystem_IN_Frenar;
            localB->pitch = localDW->PastPitch;
          } else {
            guard3 = true;
          }
        } else {
          guard3 = true;
        }
      }
      break;

     case flightControlSystem_IN_Polinizar:
      qY = localDW->PT + 150U;
      if (qY < localDW->PT) {
        qY = MAX_uint32_T;
      }

      if ((localB->ControlModeUpdate.live_time_ticks > qY) && ((real32_T)
           localDW->Delay2_DSTATE <= -0.6)) {
        localB->roll = 1.0F;
        localB->fin = 0.0F;
        localDW->PT = localB->ControlModeUpdate.live_time_ticks;
        localDW->is_c1_flightControlSystem =
          flightControlSystem_IN_DetectarCirculo;
        localB->RefZ = localDW->zp;
        localB->pitch = localDW->PastPitch;
      } else {
        qY = localDW->PT + 150U;
        if (qY < localDW->PT) {
          qY = MAX_uint32_T;
        }

        if ((localB->ControlModeUpdate.live_time_ticks > qY) && ((real32_T)
             localDW->Delay2_DSTATE > -0.5F)) {
          localB->poliBeta = 1.0F;
          localDW->zp = localB->RefZ + 0.23F;
          localDW->PT = localB->ControlModeUpdate.live_time_ticks;
          localDW->is_c1_flightControlSystem = flightControlSystem_IN_Polinizar;
          localB->RefZ = localDW->zp;
          localB->pitch = 0.0F;
          localB->yaw = localDW->PastYaw;
        } else if ((real32_T)localDW->Delay2_DSTATE > -0.3) {
          localB->poliBeta = 1.0F;
          localB->RefX = localDW->SimplyIntegrateVelocity_DSTATE[0];
          localB->RefY = localDW->SimplyIntegrateVelocity_DSTATE[1];
          localDW->zp = 0.0F;
          localDW->is_c1_flightControlSystem = flightControlSystem_IN_Polinizar1;
          localB->RefZ = localDW->zp;
        } else {
          localB->RefZ = localDW->zp;
          localB->pitch = 0.0F;
          localB->yaw = localDW->PastYaw;
        }
      }
      break;

     case flightControlSystem_IN_Polinizar1:
      localB->RefZ = localDW->zp;
      break;

     default:
      /* case IN_vayaRapido: */
      flightControlSystem_vayaRapido(rtu_VisionbasedData, localB, localDW,
        localP);
      break;
    }

    if (guard3) {
      if ((localB->fin == 0.0F) && (localB->rtb_Add_j_idx_1 < 0.1)) {
        localDW->zp = localB->RefZ;
        localDW->PT = localB->ControlModeUpdate.live_time_ticks;
        localB->poliAlpha = 1.0F;
        localB->roll = 1.0F;
        localDW->is_c1_flightControlSystem =
          flightControlSystem_IN_DetectarCirculo;
        localB->pitch = localDW->PastPitch;
      } else {
        localB->pitch = localDW->PastPitch;
      }
    }

    if (guard2) {
      if ((localB->rtb_Add_j_idx_1 <= localDW->landSpot) && (fabs
           (rtu_VisionbasedData[3] - 80.0) <= localDW->landSpot)) {
        localB->roll = 0.0F;
        localDW->zp = localB->RefZ + 0.23F;
        localB->fin = 0.0F;
        localB->RefX = localDW->SimplyIntegrateVelocity_DSTATE[0];
        localB->RefY = localDW->SimplyIntegrateVelocity_DSTATE[1];
        localDW->landSpot += 5.0;
        localDW->PT = localB->ControlModeUpdate.live_time_ticks;
        localDW->is_c1_flightControlSystem = flightControlSystem_IN_Polinizar;
        localB->RefZ = localDW->zp;
        localB->pitch = 0.0F;
        localB->yaw = localDW->PastYaw;
      } else {
        localB->RefZ = localDW->zp;
        localB->pitch = localDW->PastPitch;
      }
    }

    if (guard1) {
      localDW->is_c1_flightControlSystem = flightControlSystem_IN_vayaRapido;
      localB->pitch = localDW->PastPitch;
      localB->yaw = localDW->PastYaw;
    }
  }

  /* End of Chart: '<S33>/State logic XYZ' */

  /* Gain: '<S34>/Gain' */
  rtb_sincos_o1_idx_2 = localP->Gain_Gain_d * localDW->Delay_DSTATE_a[0];

  /* Gain: '<S34>/kp2' */
  rtb_DataTypeConversion_c = localP->kp2_Gain * rtb_sincos_o1_idx_2;

  /* SampleTimeMath: '<S39>/TSamp'
   *
   * About '<S39>/TSamp':
   *  y = u * K where K = 1 / ( w * Ts )
   */
  rtb_Sum_e_idx_1 = rtb_DataTypeConversion_c * localP->TSamp_WtEt_n;

  /* DiscreteIntegrator: '<S34>/Discrete-Time Integrator' */
  if ((localB->fin > 0.0F) && (localDW->DiscreteTimeIntegrator_PrevResetState <=
       0)) {
    localDW->DiscreteTimeIntegrator_DSTATE_j =
      localP->DiscreteTimeIntegrator_IC_c;
  }

  /* Switch: '<S38>/Switch' incorporates:
   *  Constant: '<S38>/Constant1'
   */
  if (localB->roll > localP->Switch_Threshold_n) {
    /* Switch: '<S38>/Switch1' */
    if (localB->poliAlpha > localP->Switch1_Threshold_d) {
      localB->rtb_Add_j_idx_1 = rtu_VisionbasedData[3];
    } else {
      localB->rtb_Add_j_idx_1 = rtu_VisionbasedData[1];
    }

    /* End of Switch: '<S38>/Switch1' */
  } else {
    localB->rtb_Add_j_idx_1 = localP->Constant1_Value;
  }

  /* End of Switch: '<S38>/Switch' */

  /* DataTypeConversion: '<S38>/Data Type Conversion' incorporates:
   *  Constant: '<S38>/Constant1'
   *  Gain: '<S38>/Gain'
   *  Sum: '<S38>/Subtract1'
   */
  rtb_DataTypeConversion_c = (real32_T)((localB->rtb_Add_j_idx_1 -
    localP->Constant1_Value) * localP->Gain_Gain_o);

  /* Outputs for Enabled SubSystem: '<S36>/Subsystem' incorporates:
   *  EnablePort: '<S40>/Enable'
   */
  if (localB->poliBeta > 0.0F) {
    /* Sum: '<S40>/Sum17' incorporates:
     *  DiscreteIntegrator: '<S105>/SimplyIntegrateVelocity'
     */
    localB->rtb_sincos_o1_idx_0 = localB->RefX -
      localDW->SimplyIntegrateVelocity_DSTATE[0];
    rtb_sincos_o1_idx_1 = localB->RefY - localDW->
      SimplyIntegrateVelocity_DSTATE[1];

    /* Product: '<S40>/Product' incorporates:
     *  Sum: '<S40>/Sum17'
     */
    rtb_MathFunction_f_tmp = rtb_Sum_e_idx_0 * localB->rtb_sincos_o1_idx_0 +
      acc1 * rtb_sincos_o1_idx_1;

    /* Saturate: '<S40>/Saturation' */
    if (rtb_MathFunction_f_tmp > localP->Saturation_UpperSat_k) {
      rtb_MathFunction_f_tmp = localP->Saturation_UpperSat_k;
    } else {
      if (rtb_MathFunction_f_tmp < localP->Saturation_LowerSat_k) {
        rtb_MathFunction_f_tmp = localP->Saturation_LowerSat_k;
      }
    }

    /* Gain: '<S40>/P_xy' */
    localB->P_xy[0] = localP->P_xy_Gain_o[0] * rtb_MathFunction_f_tmp;

    /* Product: '<S40>/Product' incorporates:
     *  Gain: '<S40>/Gain'
     *  Sum: '<S40>/Sum17'
     */
    rtb_MathFunction_f_tmp = localP->Gain_Gain_f * acc1 *
      localB->rtb_sincos_o1_idx_0 + rtb_Sum_e_idx_0 * rtb_sincos_o1_idx_1;

    /* Saturate: '<S40>/Saturation' */
    if (rtb_MathFunction_f_tmp > localP->Saturation_UpperSat_k) {
      rtb_MathFunction_f_tmp = localP->Saturation_UpperSat_k;
    } else {
      if (rtb_MathFunction_f_tmp < localP->Saturation_LowerSat_k) {
        rtb_MathFunction_f_tmp = localP->Saturation_LowerSat_k;
      }
    }

    /* Gain: '<S40>/P_xy' */
    localB->P_xy[1] = localP->P_xy_Gain_o[1] * rtb_MathFunction_f_tmp;
  }

  /* End of Outputs for SubSystem: '<S36>/Subsystem' */

  /* Switch: '<S36>/Switch' incorporates:
   *  Switch: '<S34>/Switch1'
   */
  if (localB->poliBeta > localP->Switch_Threshold_j) {
    localB->pitchrollerror_a[0] = localB->P_xy[0];
    localB->pitchrollerror_a[1] = localB->P_xy[1];
  } else {
    if (localB->fin > localP->Switch1_Threshold_l) {
      /* Switch: '<S34>/Switch1' incorporates:
       *  DiscreteIntegrator: '<S34>/Discrete-Time Integrator'
       *  Gain: '<S34>/degToRad'
       *  Gain: '<S34>/kp'
       *  Sum: '<S34>/Add'
       *  Sum: '<S39>/Diff'
       *  UnitDelay: '<S39>/UD'
       */
      localB->pitchrollerror_a[0] = ((localP->kp_Gain * rtb_sincos_o1_idx_2 +
        (rtb_Sum_e_idx_1 - localDW->UD_DSTATE_d)) +
        localDW->DiscreteTimeIntegrator_DSTATE_j) * localP->degToRad_Gain;
    } else {
      localB->pitchrollerror_a[0] = localB->pitch;
    }

    localB->pitchrollerror_a[1] = rtb_DataTypeConversion_c;
  }

  /* End of Switch: '<S36>/Switch' */

  /* Switch: '<S3>/Switch_refAtt' incorporates:
   *  Constant: '<S5>/Constant1'
   *  Gain: '<S36>/D_xy1'
   *  Gain: '<S9>/D_xy'
   *  Gain: '<S9>/P_xy'
   *  Sum: '<S36>/Sum1'
   *  Sum: '<S9>/Sum18'
   */
  if (localP->Constant1_Value_d) {
    /* Sum: '<S9>/Sum17' incorporates:
     *  BusAssignment: '<S5>/Bus  Assignment11'
     *  DiscreteIntegrator: '<S105>/SimplyIntegrateVelocity'
     */
    localB->rtb_sincos_o1_idx_0 = localB->RefX -
      localDW->SimplyIntegrateVelocity_DSTATE[0];
    rtb_sincos_o1_idx_1 = localB->RefY - localDW->
      SimplyIntegrateVelocity_DSTATE[1];

    /* Product: '<S9>/Product' incorporates:
     *  Sum: '<S9>/Sum17'
     */
    rtb_MathFunction_f_tmp = rtb_Sum_e_idx_0 * localB->rtb_sincos_o1_idx_0 +
      acc1 * rtb_sincos_o1_idx_1;

    /* Saturate: '<S9>/Saturation' */
    if (rtb_MathFunction_f_tmp > localP->Saturation_UpperSat) {
      rtb_MathFunction_f_tmp = localP->Saturation_UpperSat;
    } else {
      if (rtb_MathFunction_f_tmp < localP->Saturation_LowerSat) {
        rtb_MathFunction_f_tmp = localP->Saturation_LowerSat;
      }
    }

    localB->pitchrollerror_a[0] = localP->P_xy_Gain[0] * rtb_MathFunction_f_tmp
      + localP->D_xy_Gain[0] * localDW->Delay_DSTATE_a[0];

    /* Product: '<S9>/Product' incorporates:
     *  Gain: '<S9>/D_xy'
     *  Gain: '<S9>/Gain'
     *  Gain: '<S9>/P_xy'
     *  Sum: '<S9>/Sum17'
     *  Sum: '<S9>/Sum18'
     */
    rtb_MathFunction_f_tmp = localP->Gain_Gain_e * acc1 *
      localB->rtb_sincos_o1_idx_0 + rtb_Sum_e_idx_0 * rtb_sincos_o1_idx_1;

    /* Saturate: '<S9>/Saturation' */
    if (rtb_MathFunction_f_tmp > localP->Saturation_UpperSat) {
      rtb_MathFunction_f_tmp = localP->Saturation_UpperSat;
    } else {
      if (rtb_MathFunction_f_tmp < localP->Saturation_LowerSat) {
        rtb_MathFunction_f_tmp = localP->Saturation_LowerSat;
      }
    }

    localB->pitchrollerror_a[1] = localP->P_xy_Gain[1] * rtb_MathFunction_f_tmp
      + localP->D_xy_Gain[1] * localDW->Delay_DSTATE_a[1];
  } else {
    localB->pitchrollerror_a[0] += localP->D_xy1_Gain[0] *
      localDW->Delay_DSTATE_a[0];
    localB->pitchrollerror_a[1] += localP->D_xy1_Gain[1] *
      localDW->Delay_DSTATE_a[1];
  }

  /* Sum: '<S7>/Sum19' */
  localB->pitchrollerror_a[0] -= localDW->Memory_PreviousInput[1];
  rtb_sincos_o1_idx_1 = localB->pitchrollerror_a[1] -
    localDW->Memory_PreviousInput[2];
  localB->pitchrollerror_a[1] = rtb_sincos_o1_idx_1;

  /* RelationalOperator: '<S13>/Compare' incorporates:
   *  Constant: '<S13>/Constant'
   *  UnitDelay: '<S14>/Output'
   */
  rtb_Compare_j = (localDW->Output_DSTATE_c < localP->CompareToConstant_const_a);

  /* DiscreteIntegrator: '<S11>/Discrete-Time Integrator' */
  if ((!rtb_Compare_j) && (localDW->DiscreteTimeIntegrator_PrevResetState_j == 1))
  {
    localDW->DiscreteTimeIntegrator_DSTATE_o =
      localP->DiscreteTimeIntegrator_IC_g;
  }

  if (localDW->DiscreteTimeIntegrator_DSTATE_o >=
      localP->DiscreteTimeIntegrator_UpperSat_m) {
    localDW->DiscreteTimeIntegrator_DSTATE_o =
      localP->DiscreteTimeIntegrator_UpperSat_m;
  } else {
    if (localDW->DiscreteTimeIntegrator_DSTATE_o <=
        localP->DiscreteTimeIntegrator_LowerSat_m) {
      localDW->DiscreteTimeIntegrator_DSTATE_o =
        localP->DiscreteTimeIntegrator_LowerSat_m;
    }
  }

  /* Sum: '<S11>/Sum2' incorporates:
   *  BusAssignment: '<S5>/Bus  Assignment11'
   *  DataTypeConversion: '<S42>/Data Type Conversion'
   *  Reshape: '<S51>/Reshapexhat'
   */
  rtb_Sum_e_idx_0 = localB->RefZ - (real32_T)localDW->Delay2_DSTATE;

  /* Switch: '<S11>/TakeoffOrControl_Switch1' incorporates:
   *  Constant: '<S11>/w1'
   *  DataTypeConversion: '<S42>/Data Type Conversion2'
   *  DiscreteIntegrator: '<S11>/Discrete-Time Integrator'
   *  Gain: '<S11>/D_z1'
   *  Gain: '<S11>/P_z1'
   *  Gain: '<S11>/takeoff_gain1'
   *  Sum: '<S11>/Sum1'
   */
  if (rtb_Compare_j) {
    acc1 = localP->takeoff_gain1_Gain * localP->w1_Value;
  } else {
    acc1 = (localP->P_z1_Gain * rtb_Sum_e_idx_0 +
            localDW->DiscreteTimeIntegrator_DSTATE_o) - localP->D_z1_Gain *
      (real32_T)localB->Product1[2];
  }

  /* End of Switch: '<S11>/TakeoffOrControl_Switch1' */

  /* Sum: '<S11>/Sum5' incorporates:
   *  Constant: '<S11>/w1'
   */
  localB->rtb_sincos_o1_idx_0 = localP->w1_Value + acc1;

  /* Saturate: '<S11>/SaturationThrust1' */
  if (localB->rtb_sincos_o1_idx_0 > localP->SaturationThrust1_UpperSat) {
    localB->rtb_sincos_o1_idx_0 = localP->SaturationThrust1_UpperSat;
  } else {
    if (localB->rtb_sincos_o1_idx_0 < localP->SaturationThrust1_LowerSat) {
      localB->rtb_sincos_o1_idx_0 = localP->SaturationThrust1_LowerSat;
    }
  }

  /* End of Saturate: '<S11>/SaturationThrust1' */

  /* SignalConversion: '<S8>/TmpSignal ConversionAtProductInport2' incorporates:
   *  BusAssignment: '<S5>/Bus  Assignment11'
   *  DiscreteIntegrator: '<S7>/Discrete-Time Integrator'
   *  Gain: '<S10>/D_yaw'
   *  Gain: '<S10>/P_yaw'
   *  Gain: '<S7>/D_pr'
   *  Gain: '<S7>/I_pr'
   *  Gain: '<S7>/P_pr'
   *  SignalConversion: '<S41>/TmpSignal ConversionAtProductInport2'
   *  Sum: '<S10>/Sum1'
   *  Sum: '<S10>/Sum2'
   *  Sum: '<S7>/Sum16'
   */
  localB->rtb_Product_i_idx_2 = (localB->yaw - localDW->Memory_PreviousInput[0])
    * localP->P_yaw_Gain - localP->D_yaw_Gain * localB->rtb_Product_i_idx_2;
  rtb_MathFunction_f_tmp = (localP->P_pr_Gain[0] * localB->pitchrollerror_a[0] +
    localP->I_pr_Gain * localDW->DiscreteTimeIntegrator_DSTATE[0]) -
    localP->D_pr_Gain[0] * localB->inverseIMU_gain[4];
  rtb_sincos_o1_idx_1 = (localP->P_pr_Gain[1] * rtb_sincos_o1_idx_1 +
    localP->I_pr_Gain * localDW->DiscreteTimeIntegrator_DSTATE[1]) -
    localP->D_pr_Gain[1] * localB->inverseIMU_gain[3];

  /* Product: '<S8>/Product' incorporates:
   *  Constant: '<S8>/TorqueTotalThrustToThrustPerMotor'
   *  SignalConversion: '<S8>/TmpSignal ConversionAtProductInport2'
   */
  for (localB->i = 0; localB->i < 4; localB->i++) {
    acc1 = localP->TorqueTotalThrustToThrustPerMotor_Value[localB->i + 12] *
      rtb_sincos_o1_idx_1 + (localP->
      TorqueTotalThrustToThrustPerMotor_Value[localB->i + 8] *
      rtb_MathFunction_f_tmp + (localP->
      TorqueTotalThrustToThrustPerMotor_Value[localB->i + 4] *
      localB->rtb_Product_i_idx_2 +
      localP->TorqueTotalThrustToThrustPerMotor_Value[localB->i] *
      localB->rtb_sincos_o1_idx_0));
    localB->fv0[localB->i] = acc1;
  }

  /* End of Product: '<S8>/Product' */

  /* Saturate: '<S12>/Saturation5' incorporates:
   *  Gain: '<S12>/MotorDirections'
   *  Gain: '<S12>/ThrustToMotorCommand'
   */
  acc1 = localP->ThrustToMotorCommand_Gain * localB->fv0[0];
  if (acc1 > localP->Saturation5_UpperSat) {
    acc1 = localP->Saturation5_UpperSat;
  } else {
    if (acc1 < localP->Saturation5_LowerSat) {
      acc1 = localP->Saturation5_LowerSat;
    }
  }

  /* Gain: '<S12>/MotorDirections' */
  localB->MotorDirections[0] = localP->MotorDirections_Gain[0] * acc1;

  /* Saturate: '<S12>/Saturation5' incorporates:
   *  Gain: '<S12>/MotorDirections'
   *  Gain: '<S12>/ThrustToMotorCommand'
   */
  acc1 = localP->ThrustToMotorCommand_Gain * localB->fv0[1];
  if (acc1 > localP->Saturation5_UpperSat) {
    acc1 = localP->Saturation5_UpperSat;
  } else {
    if (acc1 < localP->Saturation5_LowerSat) {
      acc1 = localP->Saturation5_LowerSat;
    }
  }

  /* Gain: '<S12>/MotorDirections' */
  localB->MotorDirections[1] = localP->MotorDirections_Gain[1] * acc1;

  /* Saturate: '<S12>/Saturation5' incorporates:
   *  Gain: '<S12>/MotorDirections'
   *  Gain: '<S12>/ThrustToMotorCommand'
   */
  acc1 = localP->ThrustToMotorCommand_Gain * localB->fv0[2];
  if (acc1 > localP->Saturation5_UpperSat) {
    acc1 = localP->Saturation5_UpperSat;
  } else {
    if (acc1 < localP->Saturation5_LowerSat) {
      acc1 = localP->Saturation5_LowerSat;
    }
  }

  /* Gain: '<S12>/MotorDirections' */
  localB->MotorDirections[2] = localP->MotorDirections_Gain[2] * acc1;

  /* Saturate: '<S12>/Saturation5' incorporates:
   *  Gain: '<S12>/MotorDirections'
   *  Gain: '<S12>/ThrustToMotorCommand'
   */
  acc1 = localP->ThrustToMotorCommand_Gain * localB->fv0[3];
  if (acc1 > localP->Saturation5_UpperSat) {
    acc1 = localP->Saturation5_UpperSat;
  } else {
    if (acc1 < localP->Saturation5_LowerSat) {
      acc1 = localP->Saturation5_LowerSat;
    }
  }

  /* Gain: '<S12>/MotorDirections' */
  localB->MotorDirections[3] = localP->MotorDirections_Gain[3] * acc1;

  /* RelationalOperator: '<S111>/Compare' incorporates:
   *  Constant: '<S111>/Constant'
   *  DataTypeConversion: '<S42>/Data Type Conversion'
   *  Reshape: '<S51>/Reshapexhat'
   */
  rtb_Compare_o = ((real32_T)localDW->Delay2_DSTATE <=
                   localP->DeactivateAccelerationIfOFisnotusedduetolowaltitude_const);

  /* Logic: '<S106>/Logical Operator' incorporates:
   *  Constant: '<S113>/Constant'
   *  Constant: '<S114>/Constant'
   *  RelationalOperator: '<S113>/Compare'
   *  RelationalOperator: '<S114>/Compare'
   */
  rtb_Compare_p = ((localB->Switch[0] !=
                    localP->donotuseaccifopticalflowneveravailableNoteOF60HzbutZOHto200_con)
                   || (localB->Switch[1] !=
                       localP->donotuseaccifopticalflowneveravailableNoteOF60HzbutZOHto2001_co));
  for (localB->i = 0; localB->i < 3; localB->i++) {
    /* Sum: '<S42>/Sum' incorporates:
     *  Constant: '<S42>/gravity'
     *  DiscreteFir: '<S44>/FIR_IMUaccel'
     *  Product: '<S42>/Product'
     */
    localB->Sum[localB->i] = ((localB->MathFunction_f[localB->i + 3] *
      localB->FIR_IMUaccel[1] + localB->MathFunction_f[localB->i] *
      localB->FIR_IMUaccel[0]) + localB->MathFunction_f[localB->i + 6] *
      rtb_Product_i_idx_1) + localP->gravity_Value_n[localB->i];

    /* Sum: '<S106>/Add' incorporates:
     *  Constant: '<S106>/gravity'
     *  Product: '<S106>/Product1'
     */
    localB->Product1[localB->i] = localB->FIR_IMUaccel[localB->i] -
      ((localB->Reshape9to3x3columnmajor[localB->i + 3] * localP->gravity_Value
        [1] + localB->Reshape9to3x3columnmajor[localB->i] *
        localP->gravity_Value[0]) + localB->Reshape9to3x3columnmajor[localB->i +
       6] * localP->gravity_Value[2]);
  }

  /* Product: '<S106>/Product' incorporates:
   *  Gain: '<S106>/gainaccinput'
   */
  localB->Product[0] = (real32_T)(localP->gainaccinput_Gain * localB->Product1[0])
    * (real32_T)rtb_Compare_p * (real32_T)rtb_Compare_o;
  localB->Product[1] = (real32_T)(localP->gainaccinput_Gain * localB->Product1[1])
    * (real32_T)rtb_Compare_p * (real32_T)rtb_Compare_o;

  /* Outputs for Enabled SubSystem: '<S147>/MeasurementUpdate' incorporates:
   *  EnablePort: '<S172>/Enable'
   */
  /* DataTypeConversion: '<S110>/DataTypeConversionEnable' */
  if (localB->LogicalOperator3) {
    if (!localDW->MeasurementUpdate_MODE) {
      localDW->MeasurementUpdate_MODE = true;
    }

    /* Sum: '<S172>/Sum' incorporates:
     *  Constant: '<S110>/C'
     *  Constant: '<S110>/D'
     *  Delay: '<S110>/MemoryX'
     *  Product: '<S172>/C[k]*xhat[k|k-1]'
     *  Product: '<S172>/D[k]*u[k]'
     *  Reshape: '<S110>/Reshapeu'
     *  Reshape: '<S110>/Reshapey'
     *  Sum: '<S172>/Add1'
     */
    acc1 = localB->Switch[0] - ((localP->C_Value_h[0] *
      localDW->MemoryX_DSTATE_m[0] + localP->C_Value_h[2] *
      localDW->MemoryX_DSTATE_m[1]) + (localP->D_Value_c[0] * localB->Product[0]
      + localP->D_Value_c[2] * localB->Product[1]));
    localB->rtb_sincos_o1_idx_0 = localB->Switch[1] - ((localP->C_Value_h[1] *
      localDW->MemoryX_DSTATE_m[0] + localP->C_Value_h[3] *
      localDW->MemoryX_DSTATE_m[1]) + (localP->D_Value_c[1] * localB->Product[0]
      + localP->D_Value_c[3] * localB->Product[1]));

    /* Product: '<S172>/Product3' incorporates:
     *  Constant: '<S130>/KalmanGainL'
     *  DataTypeConversion: '<S168>/Conversion'
     */
    localB->Product3_m[0] = 0.0F;
    localB->Product3_m[0] += (real32_T)localP->KalmanGainL_Value[0] * acc1;
    localB->Product3_m[0] += (real32_T)localP->KalmanGainL_Value[2] *
      localB->rtb_sincos_o1_idx_0;
    localB->Product3_m[1] = 0.0F;
    localB->Product3_m[1] += (real32_T)localP->KalmanGainL_Value[1] * acc1;
    localB->Product3_m[1] += (real32_T)localP->KalmanGainL_Value[3] *
      localB->rtb_sincos_o1_idx_0;
  } else {
    if (localDW->MeasurementUpdate_MODE) {
      /* Disable for Outport: '<S172>/L*(y[k]-yhat[k|k-1])' */
      localB->Product3_m[0] = localP->Lykyhatkk1_Y0_e;
      localB->Product3_m[1] = localP->Lykyhatkk1_Y0_e;
      localDW->MeasurementUpdate_MODE = false;
    }
  }

  /* End of Outputs for SubSystem: '<S147>/MeasurementUpdate' */

  /* Outputs for Enabled SubSystem: '<S72>/MeasurementUpdate' incorporates:
   *  EnablePort: '<S97>/Enable'
   */
  /* DataTypeConversion: '<S51>/DataTypeConversionEnable' */
  if (localB->nicemeasurementornewupdateneeded) {
    if (!localDW->MeasurementUpdate_MODE_a) {
      localDW->MeasurementUpdate_MODE_a = true;
    }

    /* Sum: '<S97>/Sum' incorporates:
     *  Constant: '<S51>/C'
     *  Constant: '<S51>/D'
     *  Delay: '<S51>/MemoryX'
     *  Product: '<S97>/C[k]*xhat[k|k-1]'
     *  Product: '<S97>/D[k]*u[k]'
     *  Reshape: '<S51>/Reshapeu'
     *  Reshape: '<S51>/Reshapey'
     *  Sum: '<S97>/Add1'
     */
    localB->u_a = localB->invertzaxisGain - ((localP->C_Value[0] *
      localDW->MemoryX_DSTATE[0] + localP->C_Value[1] * localDW->MemoryX_DSTATE
      [1]) + localP->D_Value * localB->Sum[2]);

    /* Product: '<S97>/Product3' incorporates:
     *  Constant: '<S55>/KalmanGainL'
     *  DataTypeConversion: '<S93>/Conversion'
     */
    localB->Product3[0] = localP->KalmanGainL_Value_i[0] * localB->u_a;
    localB->Product3[1] = localP->KalmanGainL_Value_i[1] * localB->u_a;
  } else {
    if (localDW->MeasurementUpdate_MODE_a) {
      /* Disable for Outport: '<S97>/L*(y[k]-yhat[k|k-1])' */
      localB->Product3[0] = localP->Lykyhatkk1_Y0;
      localB->Product3[1] = localP->Lykyhatkk1_Y0;
      localDW->MeasurementUpdate_MODE_a = false;
    }
  }

  /* End of Outputs for SubSystem: '<S72>/MeasurementUpdate' */

  /* Sum: '<S15>/FixPt Sum1' incorporates:
   *  Constant: '<S15>/FixPt Constant'
   *  UnitDelay: '<S14>/Output'
   */
  localDW->Output_DSTATE_c += localP->FixPtConstant_Value;

  /* Switch: '<S37>/Switch1' incorporates:
   *  Constant: '<S37>/Constant'
   *  Constant: '<S37>/Constant1'
   *  Constant: '<S37>/Constant2'
   *  Constant: '<S37>/Constant3'
   *  Constant: '<S5>/Tiempo de Calibracion'
   *  Sum: '<S37>/Sum1'
   *  Sum: '<S37>/Sum2'
   *  Sum: '<S37>/Sum5'
   *  Sum: '<S37>/Sum6'
   *  Switch: '<S37>/Switch'
   */
  if ((real_T)localB->ControlModeUpdate.live_time_ticks -
      (localP->Constant1_Value_k + localP->TiempodeCalibracion_Value) >
      localP->Switch1_Threshold) {
    localB->rtb_Add_j_idx_1 = localP->Constant3_Value;
  } else if ((real_T)localB->ControlModeUpdate.live_time_ticks -
             (localP->TiempodeCalibracion_Value - localP->Constant_Value) >
             localP->Switch_Threshold) {
    /* Switch: '<S37>/Switch' */
    localB->rtb_Add_j_idx_1 = rtu_VisionbasedData[5];
  } else {
    localB->rtb_Add_j_idx_1 = localP->Constant2_Value;
  }

  /* End of Switch: '<S37>/Switch1' */

  /* Update for Delay: '<S37>/Delay' incorporates:
   *  Sum: '<S37>/Sum'
   */
  localDW->Delay_DSTATE += localB->rtb_Add_j_idx_1;

  /* Switch: '<S4>/Switch' incorporates:
   *  Constant: '<S4>/ '
   *  Constant: '<S4>/Wait  3 Seconds'
   *  RelationalOperator: '<S4>/GreaterThan'
   *  UnitDelay: '<S25>/Output'
   */
  if (localDW->Output_DSTATE_c1 > localP->Wait3Seconds_Value) {
    localB->rtb_Add_j_idx_1 = rtu_Sensors->VisionSensors.opticalFlow_data[2];
    localB->unnamed_idx_1 = rtu_Sensors->VisionSensors.opticalFlow_data[2];
  } else {
    localB->rtb_Add_j_idx_1 = localP->_Value;
    localB->unnamed_idx_1 = localP->_Value;
  }

  /* End of Switch: '<S4>/Switch' */

  /* If: '<S4>/If' incorporates:
   *  Abs: '<S4>/Abs'
   *  Abs: '<S4>/Abs1'
   *  Abs: '<S4>/Abs2'
   *  Abs: '<S4>/Abs3'
   *  Abs: '<S4>/Abs4'
   *  Abs: '<S4>/Abs5'
   *  Constant: '<S19>/Constant'
   *  Constant: '<S20>/Constant'
   *  Constant: '<S21>/Constant'
   *  Constant: '<S22>/Constant'
   *  Constant: '<S23>/Constant'
   *  Constant: '<S24>/Constant'
   *  DiscreteIntegrator: '<S105>/SimplyIntegrateVelocity'
   *  Gain: '<S4>/Gain'
   *  Gain: '<S4>/Gain1'
   *  Logic: '<S4>/Logical Operator'
   *  Logic: '<S4>/Logical Operator1'
   *  Logic: '<S4>/Logical Operator2'
   *  Logic: '<S4>/Logical Operator3'
   *  RelationalOperator: '<S19>/Compare'
   *  RelationalOperator: '<S20>/Compare'
   *  RelationalOperator: '<S21>/Compare'
   *  RelationalOperator: '<S22>/Compare'
   *  RelationalOperator: '<S23>/Compare'
   *  RelationalOperator: '<S24>/Compare'
   *  Sum: '<S4>/Subtract'
   *  Sum: '<S4>/Subtract1'
   */
  if ((((real32_T)fabs(localDW->SimplyIntegrateVelocity_DSTATE[0]) >
        localP->CompareToConstant_const_p) || ((real32_T)fabs
        (localDW->SimplyIntegrateVelocity_DSTATE[1]) >
        localP->CompareToConstant1_const_m)) > 0) {
    /* Outputs for IfAction SubSystem: '<S4>/Geofencing error' incorporates:
     *  ActionPort: '<S26>/Action Port'
     */
    flightControlSystem_Geofencingerror(&localB->Merge_b,
      &localP->Geofencingerror);

    /* End of Outputs for SubSystem: '<S4>/Geofencing error' */
  } else if (((((real32_T)fabs(rtu_Sensors->VisionSensors.opticalFlow_data[0]) >
                localP->CompareToConstant4_const) && ((real32_T)fabs
                (localP->Gain_Gain_ml *
                 rtu_Sensors->VisionSensors.opticalFlow_data[0] -
                 localDW->Delay_DSTATE_a[0]) > localP->CompareToConstant2_const))
              || (((real32_T)fabs(localP->Gain1_Gain_g *
      rtu_Sensors->VisionSensors.opticalFlow_data[1] - localDW->Delay_DSTATE_a[1])
                   > localP->CompareToConstant3_const) && ((real32_T)fabs
                (rtu_Sensors->VisionSensors.opticalFlow_data[1]) >
                localP->CompareToConstant5_const))) > 0) {
    /* Outputs for IfAction SubSystem: '<S4>/estimator//Optical flow error' incorporates:
     *  ActionPort: '<S30>/Action Port'
     */
    flightControlSystem_Geofencingerror(&localB->Merge_b,
      &localP->estimatorOpticalflowerror);

    /* End of Outputs for SubSystem: '<S4>/estimator//Optical flow error' */
  } else {
    /* Outputs for IfAction SubSystem: '<S4>/Normal condition' incorporates:
     *  ActionPort: '<S28>/Action Port'
     */
    flightControlSystem_Geofencingerror(&localB->Merge_b,
      &localP->Normalcondition);

    /* End of Outputs for SubSystem: '<S4>/Normal condition' */
  }

  /* End of If: '<S4>/If' */

  /* Sum: '<S31>/FixPt Sum1' incorporates:
   *  Constant: '<S31>/FixPt Constant'
   *  UnitDelay: '<S25>/Output'
   */
  localDW->Output_DSTATE_c1 += localP->FixPtConstant_Value_d;

  /* Sum: '<S116>/FixPt Sum1' incorporates:
   *  Constant: '<S116>/FixPt Constant'
   *  UnitDelay: '<S108>/Output'
   */
  localDW->Output_DSTATE += localP->FixPtConstant_Value_p;

  /* ManualSwitch: '<S51>/ManualSwitchPZ' incorporates:
   *  Constant: '<S51>/P0'
   *  Constant: '<S55>/CovarianceZ'
   *  DataTypeConversion: '<S96>/Conversion'
   */
  if (localP->ManualSwitchPZ_CurrentSetting == 1) {
    localB->ManualSwitchPZ[0] = localP->CovarianceZ_Value[0];
    localB->ManualSwitchPZ[1] = localP->CovarianceZ_Value[1];
    localB->ManualSwitchPZ[2] = localP->CovarianceZ_Value[2];
    localB->ManualSwitchPZ[3] = localP->CovarianceZ_Value[3];
  } else {
    localB->ManualSwitchPZ[0] = localP->P0_Value[0];
    localB->ManualSwitchPZ[1] = localP->P0_Value[1];
    localB->ManualSwitchPZ[2] = localP->P0_Value[2];
    localB->ManualSwitchPZ[3] = localP->P0_Value[3];
  }

  /* End of ManualSwitch: '<S51>/ManualSwitchPZ' */

  /* ManualSwitch: '<S110>/ManualSwitchPZ' incorporates:
   *  Constant: '<S110>/P0'
   *  Constant: '<S130>/CovarianceZ'
   *  DataTypeConversion: '<S171>/Conversion'
   */
  if (localP->ManualSwitchPZ_CurrentSetting_f == 1) {
    localB->ManualSwitchPZ_n[0] = (real32_T)localP->CovarianceZ_Value_j[0];
    localB->ManualSwitchPZ_n[1] = (real32_T)localP->CovarianceZ_Value_j[1];
    localB->ManualSwitchPZ_n[2] = (real32_T)localP->CovarianceZ_Value_j[2];
    localB->ManualSwitchPZ_n[3] = (real32_T)localP->CovarianceZ_Value_j[3];
  } else {
    localB->ManualSwitchPZ_n[0] = localP->P0_Value_n[0];
    localB->ManualSwitchPZ_n[1] = localP->P0_Value_n[1];
    localB->ManualSwitchPZ_n[2] = localP->P0_Value_n[2];
    localB->ManualSwitchPZ_n[3] = localP->P0_Value_n[3];
  }

  /* End of ManualSwitch: '<S110>/ManualSwitchPZ' */

  /* Reshape: '<S51>/Reshapeyhat' incorporates:
   *  Constant: '<S56>/Constant'
   */
  rtb_Reshapeyhat = localP->Constant_Value_b;

  /* Reshape: '<S110>/Reshapeyhat' incorporates:
   *  Constant: '<S131>/Constant'
   */
  rtb_Reshapeyhat_o = localP->Constant_Value_op;

  /* DataTypeConversion: '<S110>/DataTypeConversionReset' incorporates:
   *  Constant: '<S110>/Reset'
   */
  rtb_DataTypeConversionReset_i = localP->Reset_Value;

  /* SignalConversion: '<S105>/TmpSignal ConversionAtProductInport2' */
  acc1 = localDW->Delay_DSTATE_a[0];
  localB->rtb_sincos_o1_idx_0 = localDW->Delay_DSTATE_a[1];

  /* Product: '<S105>/Product' incorporates:
   *  DataTypeConversion: '<S42>/Data Type Conversion2'
   *  SignalConversion: '<S105>/TmpSignal ConversionAtProductInport2'
   */
  for (localB->i = 0; localB->i < 3; localB->i++) {
    localB->sincos_o2[localB->i] = localB->MathFunction[localB->i + 6] *
      (real32_T)localB->rtb_MathFunction_f_c[2] + (localB->MathFunction
      [localB->i + 3] * localB->rtb_sincos_o1_idx_0 + localB->
      MathFunction[localB->i] * acc1);
  }

  /* End of Product: '<S105>/Product' */

  /* Update for DiscreteIntegrator: '<S105>/SimplyIntegrateVelocity' incorporates:
   *  Constant: '<S1>/controlModePosVsOrient'
   */
  localDW->SimplyIntegrateVelocity_PrevResetState = (int8_T)
    localP->controlModePosVsOrient_Value;

  /* Update for Delay: '<S51>/MemoryX' */
  localDW->icLoad = 0U;

  /* Update for DiscreteIntegrator: '<S105>/SimplyIntegrateVelocity' */
  localDW->SimplyIntegrateVelocity_DSTATE[0] +=
    localP->SimplyIntegrateVelocity_gainval * localB->sincos_o2[0];
  localDW->SimplyIntegrateVelocity_DSTATE[1] +=
    localP->SimplyIntegrateVelocity_gainval * localB->sincos_o2[1];

  /* Product: '<S72>/A[k]*xhat[k|k-1]' incorporates:
   *  Constant: '<S51>/A'
   *  Delay: '<S51>/MemoryX'
   */
  localB->u_a = localP->A_Value[1] * localDW->MemoryX_DSTATE[0] +
    localP->A_Value[3] * localDW->MemoryX_DSTATE[1];

  /* Update for Delay: '<S51>/MemoryX' incorporates:
   *  Constant: '<S51>/A'
   *  Constant: '<S51>/B'
   *  Product: '<S72>/A[k]*xhat[k|k-1]'
   *  Product: '<S72>/B[k]*u[k]'
   *  Reshape: '<S51>/Reshapeu'
   *  Sum: '<S72>/Add'
   */
  localDW->MemoryX_DSTATE[0] = ((localP->A_Value[0] * localDW->MemoryX_DSTATE[0]
    + localP->A_Value[2] * localDW->MemoryX_DSTATE[1]) + localP->B_Value[0] *
    localB->Sum[2]) + localB->Product3[0];

  /* Update for DiscreteFilter: '<S52>/pressureFilter_IIR' */
  localDW->pressureFilter_IIR_states[2] = localDW->pressureFilter_IIR_states[1];

  /* Update for Delay: '<S51>/MemoryX' incorporates:
   *  Constant: '<S51>/B'
   *  Product: '<S72>/B[k]*u[k]'
   *  Reshape: '<S51>/Reshapeu'
   *  Sum: '<S72>/Add'
   */
  localDW->MemoryX_DSTATE[1] = (localP->B_Value[1] * localB->Sum[2] +
    localB->u_a) + localB->Product3[1];

  /* Update for DiscreteFilter: '<S52>/pressureFilter_IIR' */
  localDW->pressureFilter_IIR_states[1] = localDW->pressureFilter_IIR_states[0];
  localDW->pressureFilter_IIR_states[0] = localB->denAccum_b;

  /* Update for DiscreteFilter: '<S52>/soonarFilter_IIR' */
  localDW->soonarFilter_IIR_states[2] = localDW->soonarFilter_IIR_states[1];
  localDW->soonarFilter_IIR_states[1] = localDW->soonarFilter_IIR_states[0];
  localDW->soonarFilter_IIR_states[0] = localB->denAccum;

  /* Update for DiscreteFilter: '<S44>/IIR_IMUgyro_r' */
  localDW->IIR_IMUgyro_r_states[4] = localDW->IIR_IMUgyro_r_states[3];
  localDW->IIR_IMUgyro_r_states[3] = localDW->IIR_IMUgyro_r_states[2];
  localDW->IIR_IMUgyro_r_states[2] = localDW->IIR_IMUgyro_r_states[1];
  localDW->IIR_IMUgyro_r_states[1] = localDW->IIR_IMUgyro_r_states[0];
  localDW->IIR_IMUgyro_r_states[0] = rtb_Product_i_idx_0;

  /* Update for DiscreteFir: '<S44>/FIR_IMUaccel' */
  /* Update circular buffer index */
  localDW->FIR_IMUaccel_circBuf--;
  if (localDW->FIR_IMUaccel_circBuf < 0) {
    localDW->FIR_IMUaccel_circBuf = 4;
  }

  /* Update circular buffer */
  localDW->FIR_IMUaccel_states[localDW->FIR_IMUaccel_circBuf] =
    localB->inverseIMU_gain[0];
  localDW->FIR_IMUaccel_states[localDW->FIR_IMUaccel_circBuf + 5] =
    localB->inverseIMU_gain[1];
  localDW->FIR_IMUaccel_states[localDW->FIR_IMUaccel_circBuf + 10] =
    localB->inverseIMU_gain[2];

  /* End of Update for DiscreteFir: '<S44>/FIR_IMUaccel' */

  /* Update for Delay: '<S110>/MemoryX' */
  localDW->icLoad_c = 0U;

  /* Product: '<S147>/B[k]*u[k]' incorporates:
   *  Constant: '<S110>/B'
   *  Reshape: '<S110>/Reshapeu'
   */
  localB->fv1[0] = localP->B_Value_j[0] * localB->Product[0] + localP->
    B_Value_j[2] * localB->Product[1];

  /* Product: '<S147>/A[k]*xhat[k|k-1]' incorporates:
   *  Constant: '<S110>/A'
   *  Delay: '<S110>/MemoryX'
   */
  localB->fv2[0] = localP->A_Value_g[0] * localDW->MemoryX_DSTATE_m[0] +
    localP->A_Value_g[2] * localDW->MemoryX_DSTATE_m[1];

  /* Product: '<S147>/B[k]*u[k]' incorporates:
   *  Constant: '<S110>/B'
   *  Reshape: '<S110>/Reshapeu'
   */
  localB->fv1[1] = localP->B_Value_j[1] * localB->Product[0];

  /* Product: '<S147>/A[k]*xhat[k|k-1]' incorporates:
   *  Constant: '<S110>/A'
   *  Delay: '<S110>/MemoryX'
   */
  localB->fv2[1] = localP->A_Value_g[1] * localDW->MemoryX_DSTATE_m[0];

  /* Product: '<S147>/B[k]*u[k]' incorporates:
   *  Constant: '<S110>/B'
   *  Reshape: '<S110>/Reshapeu'
   */
  localB->fv1[1] += localP->B_Value_j[3] * localB->Product[1];

  /* Product: '<S147>/A[k]*xhat[k|k-1]' incorporates:
   *  Constant: '<S110>/A'
   *  Delay: '<S110>/MemoryX'
   */
  localB->fv2[1] += localP->A_Value_g[3] * localDW->MemoryX_DSTATE_m[1];

  /* Switch: '<S117>/FixPt Switch' */
  if (localDW->Output_DSTATE > localP->WrapToZero_Threshold_d) {
    /* Update for UnitDelay: '<S108>/Output' incorporates:
     *  Constant: '<S117>/Constant'
     */
    localDW->Output_DSTATE = localP->Constant_Value_i;
  }

  /* End of Switch: '<S117>/FixPt Switch' */
  for (localB->cff = 0; localB->cff < 2; localB->cff++) {
    /* Update for Delay: '<S110>/MemoryX' incorporates:
     *  Sum: '<S147>/Add'
     */
    localDW->MemoryX_DSTATE_m[localB->cff] = (localB->fv1[localB->cff] +
      localB->fv2[localB->cff]) + localB->Product3_m[localB->cff];

    /* Update for DiscreteFilter: '<S109>/IIRgyroz' */
    localB->memOffset = localB->cff * 5;
    localDW->IIRgyroz_states[localB->memOffset + 4] = localDW->
      IIRgyroz_states[localB->memOffset + 3];
    localDW->IIRgyroz_states[localB->memOffset + 3] = localDW->
      IIRgyroz_states[localB->memOffset + 2];
    localDW->IIRgyroz_states[localB->memOffset + 2] = localDW->
      IIRgyroz_states[localB->memOffset + 1];
    localDW->IIRgyroz_states[localB->memOffset + 1] = localDW->
      IIRgyroz_states[localB->memOffset];
    localDW->IIRgyroz_states[localB->memOffset] = localDW->IIRgyroz_tmp
      [localB->cff];

    /* Update for UnitDelay: '<S118>/UD' */
    localDW->UD_DSTATE[localB->cff] = localB->TSamp[localB->cff];

    /* Update for Delay: '<S6>/Delay1' */
    localDW->Delay1_DSTATE[localB->cff] = localDW->Delay_DSTATE_a[localB->cff];

    /* Update for DiscreteIntegrator: '<S7>/Discrete-Time Integrator' incorporates:
     *  Delay: '<S7>/Delay'
     *  Gain: '<S7>/antiWU_Gain'
     *  Sum: '<S7>/Add'
     */
    localDW->DiscreteTimeIntegrator_DSTATE[localB->cff] +=
      (localB->pitchrollerror_a[localB->cff] - localP->antiWU_Gain_Gain *
       localDW->Delay_DSTATE_b[localB->cff]) *
      localP->DiscreteTimeIntegrator_gainval;
    if (localDW->DiscreteTimeIntegrator_DSTATE[localB->cff] >=
        localP->DiscreteTimeIntegrator_UpperSat) {
      localDW->DiscreteTimeIntegrator_DSTATE[localB->cff] =
        localP->DiscreteTimeIntegrator_UpperSat;
    } else {
      if (localDW->DiscreteTimeIntegrator_DSTATE[localB->cff] <=
          localP->DiscreteTimeIntegrator_LowerSat) {
        localDW->DiscreteTimeIntegrator_DSTATE[localB->cff] =
          localP->DiscreteTimeIntegrator_LowerSat;
      }
    }

    /* End of Update for DiscreteIntegrator: '<S7>/Discrete-Time Integrator' */
  }

  /* Update for UnitDelay: '<S39>/UD' */
  localDW->UD_DSTATE_d = rtb_Sum_e_idx_1;

  /* Update for DiscreteIntegrator: '<S34>/Discrete-Time Integrator' incorporates:
   *  Gain: '<S34>/kp1'
   */
  localDW->DiscreteTimeIntegrator_DSTATE_j += localP->kp1_Gain *
    rtb_sincos_o1_idx_2 * localP->DiscreteTimeIntegrator_gainval_e;
  if (localB->fin > 0.0F) {
    localDW->DiscreteTimeIntegrator_PrevResetState = 1;
  } else if (localB->fin < 0.0F) {
    localDW->DiscreteTimeIntegrator_PrevResetState = -1;
  } else if (localB->fin == 0.0F) {
    localDW->DiscreteTimeIntegrator_PrevResetState = 0;
  } else {
    localDW->DiscreteTimeIntegrator_PrevResetState = 2;
  }

  /* End of Update for DiscreteIntegrator: '<S34>/Discrete-Time Integrator' */

  /* Switch: '<S16>/FixPt Switch' */
  if (localDW->Output_DSTATE_c > localP->WrapToZero_Threshold) {
    /* Update for UnitDelay: '<S14>/Output' incorporates:
     *  Constant: '<S16>/Constant'
     */
    localDW->Output_DSTATE_c = localP->Constant_Value_f;
  }

  /* End of Switch: '<S16>/FixPt Switch' */

  /* Update for DiscreteIntegrator: '<S11>/Discrete-Time Integrator' incorporates:
   *  Gain: '<S11>/I_pr'
   */
  localDW->DiscreteTimeIntegrator_DSTATE_o += localP->I_pr_Gain_l *
    rtb_Sum_e_idx_0 * localP->DiscreteTimeIntegrator_gainval_o;
  if (localDW->DiscreteTimeIntegrator_DSTATE_o >=
      localP->DiscreteTimeIntegrator_UpperSat_m) {
    localDW->DiscreteTimeIntegrator_DSTATE_o =
      localP->DiscreteTimeIntegrator_UpperSat_m;
  } else {
    if (localDW->DiscreteTimeIntegrator_DSTATE_o <=
        localP->DiscreteTimeIntegrator_LowerSat_m) {
      localDW->DiscreteTimeIntegrator_DSTATE_o =
        localP->DiscreteTimeIntegrator_LowerSat_m;
    }
  }

  localDW->DiscreteTimeIntegrator_PrevResetState_j = (int8_T)rtb_Compare_j;

  /* End of Update for DiscreteIntegrator: '<S11>/Discrete-Time Integrator' */

  /* Update for Delay: '<S7>/Delay' */
  localDW->Delay_DSTATE_b[0] = localB->Akxhatkk1_f[0];
  localDW->Delay_DSTATE_b[1] = localB->Akxhatkk1_f[1];

  /* Switch: '<S32>/FixPt Switch' */
  if (localDW->Output_DSTATE_c1 > localP->WrapToZero_Threshold_f) {
    /* Update for UnitDelay: '<S25>/Output' incorporates:
     *  Constant: '<S32>/Constant'
     */
    localDW->Output_DSTATE_c1 = localP->Constant_Value_m0;
  }

  /* End of Switch: '<S32>/FixPt Switch' */

  /* Update for Delay: '<S4>/Delay One Step' incorporates:
   *  Constant: '<S18>/Constant'
   *  DataTypeConversion: '<S4>/   '
   *  Product: '<S4>/  '
   *  RelationalOperator: '<S18>/Compare'
   *  Sum: '<S4>/Add'
   */
  localDW->DelayOneStep_DSTATE = (real_T)(localB->rtb_Add_j_idx_1 ==
    localP->Checkerrorcondition_const) * localDW->DelayOneStep_DSTATE + (real_T)
    (localB->unnamed_idx_1 == localP->Checkerrorcondition_const);
}

/* Model step function for TID0 */
void flightControlSystem_step0(void)   /* Sample time: [0.005s, 0.0s] */
{
  {                                    /* Sample time: [0.005s, 0.0s] */
    rate_monotonic_scheduler();
  }

  /* RateTransition: '<Root>/Rate Transition' */
  flightControlSystem_B.i0 = flightControlSystem_DW.RateTransition_ActiveBufIdx *
    7;
  for (flightControlSystem_B.i1 = 0; flightControlSystem_B.i1 < 7;
       flightControlSystem_B.i1++) {
    flightControlSystem_B.RateTransition[flightControlSystem_B.i1] =
      flightControlSystem_DW.RateTransition_Buffer[flightControlSystem_B.i1 +
      flightControlSystem_B.i0];
  }

  /* End of RateTransition: '<Root>/Rate Transition' */

  /* Outputs for Atomic SubSystem: '<Root>/Control System1' */
  /* Inport: '<Root>/AC cmd' incorporates:
   *  Inport: '<Root>/Sensor'
   */
  flightControlSystem_ControlSystem1(flightControlSystem_M,
    &flightControlSystem_U.ACcmd, &flightControlSystem_U.Sensor,
    flightControlSystem_B.RateTransition, &flightControlSystem_B.ControlSystem1,
    &flightControlSystem_DW.ControlSystem1,
    &flightControlSystem_P.ControlSystem1);

  /* End of Outputs for SubSystem: '<Root>/Control System1' */

  /* Outport: '<Root>/Actuators' */
  flightControlSystem_Y.Actuators[0] =
    flightControlSystem_B.ControlSystem1.MotorDirections[0];
  flightControlSystem_Y.Actuators[1] =
    flightControlSystem_B.ControlSystem1.MotorDirections[1];
  flightControlSystem_Y.Actuators[2] =
    flightControlSystem_B.ControlSystem1.MotorDirections[2];
  flightControlSystem_Y.Actuators[3] =
    flightControlSystem_B.ControlSystem1.MotorDirections[3];

  /* Outport: '<Root>/Flag' */
  flightControlSystem_Y.Flag = flightControlSystem_B.ControlSystem1.Merge_b;

  /* Matfile logging */
  rt_UpdateTXYLogVars(flightControlSystem_M->rtwLogInfo,
                      (&flightControlSystem_M->Timing.taskTime0));

  /* signal main to stop simulation */
  {                                    /* Sample time: [0.005s, 0.0s] */
    if ((rtmGetTFinal(flightControlSystem_M)!=-1) &&
        !((rtmGetTFinal(flightControlSystem_M)-
           flightControlSystem_M->Timing.taskTime0) >
          flightControlSystem_M->Timing.taskTime0 * (DBL_EPSILON))) {
      rtmSetErrorStatus(flightControlSystem_M, "Simulation finished");
    }
  }

  /* Update absolute time */
  /* The "clockTick0" counts the number of times the code of this task has
   * been executed. The absolute time is the multiplication of "clockTick0"
   * and "Timing.stepSize0". Size of "clockTick0" ensures timer will not
   * overflow during the application lifespan selected.
   * Timer of this task consists of two 32 bit unsigned integers.
   * The two integers represent the low bits Timing.clockTick0 and the high bits
   * Timing.clockTickH0. When the low bit overflows to 0, the high bits increment.
   */
  if (!(++flightControlSystem_M->Timing.clockTick0)) {
    ++flightControlSystem_M->Timing.clockTickH0;
  }

  flightControlSystem_M->Timing.taskTime0 =
    flightControlSystem_M->Timing.clockTick0 *
    flightControlSystem_M->Timing.stepSize0 +
    flightControlSystem_M->Timing.clockTickH0 *
    flightControlSystem_M->Timing.stepSize0 * 4294967296.0;
}

/* Model step function for TID1 */
void flightControlSystem_step1(void)   /* Sample time: [0.2s, 0.0s] */
{
  /* local block i/o variables */
  real_T rtb_Ruido;
  real_T tmp;
  real_T tmp_0;
  int32_T yIdx;
  int32_T yIdx_0;
  int32_T colIdx;
  int32_T yIdx_1;
  int32_T loop;
  real_T rtb_histv;
  const uint8_T *u0;
  boolean_T exitg1;

  /* Outputs for Atomic SubSystem: '<Root>/Image Processing System' */
  /* MATLABSystem: '<S2>/PARROT Image Conversion' incorporates:
   *  Inport: '<Root>/Image Data'
   */
  u0 = &imRGB[0];
  MW_Build_RGB(u0, flightControlSystem_B.imageBuff_1,
               flightControlSystem_B.imageBuff_2,
               flightControlSystem_B.imageBuff_3);

  /* MATLAB Function: '<S2>/MATLAB Function' incorporates:
   *  MATLABSystem: '<S2>/PARROT Image Conversion'
   */
  for (loop = 0; loop < 19200; loop++) {
    flightControlSystem_B.BW[loop] = ((flightControlSystem_B.imageBuff_1[loop] >=
      184) && (flightControlSystem_B.imageBuff_2[loop] <= 180) &&
      (flightControlSystem_B.imageBuff_3[loop] <= 169));
  }

  /* End of MATLAB Function: '<S2>/MATLAB Function' */

  /* S-Function (sdspsubmtrx): '<S178>/Submatrix' */
  yIdx_1 = 0;
  for (colIdx = 0; colIdx < 160; colIdx++) {
    for (loop = 0; loop < 61; loop++) {
      flightControlSystem_B.UP[loop + yIdx_1] = flightControlSystem_B.BW[colIdx *
        120 + loop];
    }

    yIdx_1 += 61;
  }

  /* End of S-Function (sdspsubmtrx): '<S178>/Submatrix' */

  /* S-Function (sdspsubmtrx): '<S178>/Submatrix2' */
  yIdx_1 = 0;

  /* S-Function (sdspsubmtrx): '<S178>/Submatrix1' */
  yIdx_0 = 0;

  /* S-Function (sdspsubmtrx): '<S178>/Submatrix3' */
  yIdx = 0;
  for (colIdx = 0; colIdx < 160; colIdx++) {
    /* S-Function (sdspsubmtrx): '<S178>/Submatrix2' */
    for (loop = 0; loop < 30; loop++) {
      flightControlSystem_B.UpUP[loop + yIdx_1] =
        flightControlSystem_B.UP[colIdx * 61 + loop];
    }

    yIdx_1 += 30;

    /* S-Function (sdspsubmtrx): '<S178>/Submatrix1' */
    for (loop = 0; loop < 31; loop++) {
      flightControlSystem_B.UPDown[loop + yIdx_0] = flightControlSystem_B.UP
        [(colIdx * 61 + loop) + 30];
    }

    yIdx_0 += 31;

    /* S-Function (sdspsubmtrx): '<S178>/Submatrix3' */
    for (loop = 0; loop < 60; loop++) {
      flightControlSystem_B.Down[loop + yIdx] = flightControlSystem_B.BW[(colIdx
        * 120 + loop) + 60];
    }

    yIdx += 60;
  }

  /* S-Function (sdspsubmtrx): '<S178>/Submatrix4' */
  yIdx_1 = 0;

  /* Sum: '<S186>/Sum of Elements3' */
  flightControlSystem_B.Switch = -0.0;
  for (colIdx = 0; colIdx < 160; colIdx++) {
    /* S-Function (sdspsubmtrx): '<S178>/Submatrix4' */
    for (loop = 0; loop < 31; loop++) {
      flightControlSystem_B.Down_c[loop + yIdx_1] = flightControlSystem_B.Down
        [(colIdx * 60 + loop) + 29];
    }

    yIdx_1 += 31;

    /* Sum: '<S186>/Sum of Elements2' */
    loop = colIdx * 31;
    flightControlSystem_B.totalp = -0.0;
    for (yIdx_0 = 0; yIdx_0 < 31; yIdx_0++) {
      flightControlSystem_B.totalp += (real_T)flightControlSystem_B.UPDown[loop
        + yIdx_0];
    }

    flightControlSystem_B.SumofElements2_k[colIdx] =
      flightControlSystem_B.totalp;

    /* End of Sum: '<S186>/Sum of Elements2' */

    /* Sum: '<S186>/Sum of Elements3' */
    flightControlSystem_B.Switch +=
      flightControlSystem_B.SumofElements2_k[colIdx];
  }

  /* Switch: '<S186>/Switch' incorporates:
   *  Constant: '<S186>/Constant'
   *  Sum: '<S186>/Sum of Elements3'
   */
  if (!(flightControlSystem_B.Switch > flightControlSystem_P.Switch_Threshold))
  {
    flightControlSystem_B.Switch = flightControlSystem_P.Constant_Value_d;
  }

  /* End of Switch: '<S186>/Switch' */

  /* Sum: '<S185>/Sum of Elements3' */
  flightControlSystem_B.totalp = -0.0;
  for (yIdx = 0; yIdx < 160; yIdx++) {
    /* Gain: '<S182>/Gain' incorporates:
     *  Product: '<S186>/Divide1'
     */
    flightControlSystem_B.Gain[yIdx] =
      flightControlSystem_B.SumofElements2_k[yIdx] /
      flightControlSystem_B.Switch * flightControlSystem_P.Gain_Gain[yIdx];

    /* Sum: '<S185>/Sum of Elements2' */
    loop = yIdx * 30;
    rtb_histv = -0.0;
    for (yIdx_0 = 0; yIdx_0 < 30; yIdx_0++) {
      rtb_histv += (real_T)flightControlSystem_B.UpUP[loop + yIdx_0];
    }

    /* Sum: '<S185>/Sum of Elements3' incorporates:
     *  Sum: '<S185>/Sum of Elements2'
     */
    flightControlSystem_B.totalp += rtb_histv;

    /* Sum: '<S185>/Sum of Elements2' */
    flightControlSystem_B.SumofElements2_k[yIdx] = rtb_histv;
  }

  /* Sum: '<S185>/Sum of Elements3' */
  flightControlSystem_B.Switch = flightControlSystem_B.totalp;

  /* Switch: '<S185>/Switch' incorporates:
   *  Constant: '<S185>/Constant'
   *  Sum: '<S185>/Sum of Elements3'
   */
  if (!(flightControlSystem_B.totalp > flightControlSystem_P.Switch_Threshold_e))
  {
    flightControlSystem_B.Switch = flightControlSystem_P.Constant_Value;
  }

  /* End of Switch: '<S185>/Switch' */
  for (yIdx = 0; yIdx < 160; yIdx++) {
    /* MATLAB Function: '<S178>/MATLABHisto2' */
    flightControlSystem_B.histh[yIdx] = 0.0;

    /* Gain: '<S181>/Gain' incorporates:
     *  Product: '<S185>/Divide1'
     */
    flightControlSystem_B.SumofElements2_k[yIdx] =
      flightControlSystem_B.SumofElements2_k[yIdx] /
      flightControlSystem_B.Switch * flightControlSystem_P.Gain_Gain_m[yIdx];
  }

  /* MATLAB Function: '<S178>/MATLABHisto2' */
  for (loop = 0; loop < 120; loop++) {
    flightControlSystem_B.histv[loop] = 0.0;
    for (yIdx_1 = 0; yIdx_1 < 160; yIdx_1++) {
      flightControlSystem_B.totalp = flightControlSystem_B.histh[yIdx_1];
      rtb_histv = flightControlSystem_B.histv[loop];
      if (flightControlSystem_B.BW[120 * yIdx_1 + loop]) {
        rtb_histv = flightControlSystem_B.histv[loop] + 1.0;
        flightControlSystem_B.totalp = flightControlSystem_B.histh[yIdx_1] + 1.0;
      }

      flightControlSystem_B.histh[yIdx_1] = flightControlSystem_B.totalp;
      flightControlSystem_B.histv[loop] = rtb_histv;
    }
  }

  flightControlSystem_B.totalp = flightControlSystem_B.histv[0];
  for (loop = 0; loop < 119; loop++) {
    flightControlSystem_B.totalp += flightControlSystem_B.histv[loop + 1];
  }

  if (flightControlSystem_B.totalp > 0.0) {
    for (loop = 0; loop < 160; loop++) {
      flightControlSystem_B.histh[loop] /= flightControlSystem_B.totalp;
    }

    for (loop = 0; loop < 120; loop++) {
      flightControlSystem_B.histv[loop] /= flightControlSystem_B.totalp;
    }
  }

  /* Gain: '<S184>/Gain' */
  for (loop = 0; loop < 160; loop++) {
    flightControlSystem_B.histh[loop] *= flightControlSystem_P.Gain_Gain_c[loop];
  }

  /* End of Gain: '<S184>/Gain' */

  /* MATLAB Function: '<S178>/MATLABCentro ' incorporates:
   *  Constant: '<S2>/Tolerancia Inferior'
   *  Constant: '<S2>/Tolerancia superior'
   */
  yIdx_1 = 0;
  colIdx = 0;
  yIdx_0 = 0;
  yIdx = 1;
  loop = 1;
  while (yIdx <= 120) {
    if (((colIdx > 0) && (yIdx_0 > 0)) || (yIdx <= 5)) {
      flightControlSystem_B.histv[yIdx - 1] = 0.0;
      yIdx++;
    } else {
      if ((flightControlSystem_B.histv[yIdx - 1] > 0.0) && (yIdx_1 == 0)) {
        yIdx_1 = yIdx;
        loop = yIdx;
      } else {
        flightControlSystem_B.totalp = flightControlSystem_B.histv[yIdx - 1];
        if ((flightControlSystem_B.totalp <= 0.0) && ((yIdx_1 > 0) && (colIdx ==
              0) && (yIdx >= loop + 20))) {
          colIdx = yIdx;
          loop = yIdx;
        } else {
          if ((flightControlSystem_B.totalp > 0.0) && ((yIdx_1 > 0) && (colIdx >
                0) && (yIdx >= loop + 10))) {
            yIdx_0 = yIdx;
            flightControlSystem_B.histv[yIdx - 1] = 0.0;
          }
        }
      }

      yIdx++;
    }
  }

  if (!rtIsNaN(flightControlSystem_B.histv[0])) {
    yIdx = 1;
  } else {
    yIdx = 0;
    loop = 2;
    exitg1 = false;
    while ((!exitg1) && (loop < 121)) {
      if (!rtIsNaN(flightControlSystem_B.histv[loop - 1])) {
        yIdx = loop;
        exitg1 = true;
      } else {
        loop++;
      }
    }
  }

  if (yIdx == 0) {
    flightControlSystem_B.Switch = flightControlSystem_B.histv[0];
  } else {
    flightControlSystem_B.Switch = flightControlSystem_B.histv[yIdx - 1];
    while (yIdx + 1 < 121) {
      if (flightControlSystem_B.Switch < flightControlSystem_B.histv[yIdx]) {
        flightControlSystem_B.Switch = flightControlSystem_B.histv[yIdx];
      }

      yIdx++;
    }
  }

  flightControlSystem_B.totalp = flightControlSystem_B.histv[0];
  for (loop = 0; loop < 119; loop++) {
    flightControlSystem_B.totalp += flightControlSystem_B.histv[loop + 1];
  }

  if (flightControlSystem_B.totalp == 0.0) {
    flightControlSystem_B.totalp = 1.0;
  }

  for (loop = 0; loop < 120; loop++) {
    flightControlSystem_B.histv[loop] /= flightControlSystem_B.totalp;
  }

  if ((yIdx_1 > 0) && (colIdx > 0) && (yIdx_0 > 0)) {
    flightControlSystem_B.totalp = colIdx - yIdx_1;
    rtb_histv = yIdx_0 - colIdx;
    if ((rtb_histv >= flightControlSystem_B.totalp *
         flightControlSystem_P.ToleranciaInferior_Value) && (rtb_histv <=
         flightControlSystem_B.totalp *
         flightControlSystem_P.Toleranciasuperior_Value)) {
      if (!rtIsNaN(flightControlSystem_B.histv[0])) {
        loop = 1;
      } else {
        loop = 0;
        yIdx_1 = 2;
        exitg1 = false;
        while ((!exitg1) && (yIdx_1 < 121)) {
          if (!rtIsNaN(flightControlSystem_B.histv[yIdx_1 - 1])) {
            loop = yIdx_1;
            exitg1 = true;
          } else {
            yIdx_1++;
          }
        }
      }

      if (loop == 0) {
        flightControlSystem_B.totalp = flightControlSystem_B.histv[0];
      } else {
        flightControlSystem_B.totalp = flightControlSystem_B.histv[loop - 1];
        while (loop + 1 < 121) {
          if (flightControlSystem_B.totalp < flightControlSystem_B.histv[loop])
          {
            flightControlSystem_B.totalp = flightControlSystem_B.histv[loop];
          }

          loop++;
        }
      }

      if (flightControlSystem_B.totalp < 5.0 * flightControlSystem_B.Switch) {
        yIdx_1 = 1;
      } else {
        yIdx_1 = 0;
      }
    } else {
      yIdx_1 = 0;
    }
  } else {
    yIdx_1 = 0;
  }

  /* End of MATLAB Function: '<S178>/MATLABCentro ' */

  /* Sum: '<S183>/Sum of Elements2' */
  flightControlSystem_B.totalp = -0.0;
  for (loop = 0; loop < 120; loop++) {
    /* Gain: '<S183>/Gain' */
    rtb_histv = flightControlSystem_P.Gain_Gain_mb[loop] *
      flightControlSystem_B.histv[loop];

    /* Sum: '<S183>/Sum of Elements2' */
    flightControlSystem_B.totalp += rtb_histv;

    /* Gain: '<S183>/Gain' */
    flightControlSystem_B.histv[loop] = rtb_histv;
  }

  /* Sum: '<S184>/Sum of Elements2' */
  rtb_histv = -0.0;

  /* Sum: '<S181>/Sum of Elements2' */
  flightControlSystem_B.Switch = -0.0;

  /* Sum: '<S182>/Sum of Elements2' */
  tmp_0 = -0.0;
  for (loop = 0; loop < 160; loop++) {
    /* Sum: '<S184>/Sum of Elements2' */
    rtb_histv += flightControlSystem_B.histh[loop];

    /* Sum: '<S181>/Sum of Elements2' */
    flightControlSystem_B.Switch += flightControlSystem_B.SumofElements2_k[loop];

    /* Sum: '<S182>/Sum of Elements2' */
    tmp_0 += flightControlSystem_B.Gain[loop];
  }

  /* Sum: '<S178>/Sum of Elements1' */
  tmp = -0.0;
  for (loop = 0; loop < 4960; loop++) {
    tmp += (real_T)flightControlSystem_B.Down_c[loop];
  }

  /* Sum: '<S178>/Sum of Elements' */
  flightControlSystem_B.d0 = -0.0;
  for (loop = 0; loop < 4800; loop++) {
    flightControlSystem_B.d0 += (real_T)flightControlSystem_B.UpUP[loop];
  }

  /* Constant: '<S2>/Ruido' */
  rtb_Ruido = flightControlSystem_P.Ruido_Value;

  /* Update for RateTransition: '<Root>/Rate Transition' incorporates:
   *  Sum: '<S178>/Sum of Elements'
   *  Sum: '<S178>/Sum of Elements1'
   *  Sum: '<S181>/Sum of Elements2'
   *  Sum: '<S182>/Sum of Elements2'
   *  Sum: '<S183>/Sum of Elements2'
   *  Sum: '<S184>/Sum of Elements2'
   */
  flightControlSystem_DW.RateTransition_Buffer
    [(flightControlSystem_DW.RateTransition_ActiveBufIdx == 0) * 7] =
    flightControlSystem_B.Switch;
  flightControlSystem_DW.RateTransition_Buffer[1 +
    (flightControlSystem_DW.RateTransition_ActiveBufIdx == 0) * 7] = tmp_0;
  flightControlSystem_DW.RateTransition_Buffer[2 +
    (flightControlSystem_DW.RateTransition_ActiveBufIdx == 0) * 7] =
    flightControlSystem_B.d0;
  flightControlSystem_DW.RateTransition_Buffer[3 +
    (flightControlSystem_DW.RateTransition_ActiveBufIdx == 0) * 7] = rtb_histv;
  flightControlSystem_DW.RateTransition_Buffer[4 +
    (flightControlSystem_DW.RateTransition_ActiveBufIdx == 0) * 7] =
    flightControlSystem_B.totalp;
  flightControlSystem_DW.RateTransition_Buffer[5 +
    (flightControlSystem_DW.RateTransition_ActiveBufIdx == 0) * 7] = tmp;

  /* End of Outputs for SubSystem: '<Root>/Image Processing System' */
  flightControlSystem_DW.RateTransition_Buffer[6 +
    (flightControlSystem_DW.RateTransition_ActiveBufIdx == 0) * 7] = yIdx_1;
  flightControlSystem_DW.RateTransition_ActiveBufIdx = (int8_T)
    (flightControlSystem_DW.RateTransition_ActiveBufIdx == 0);
}

/* Model initialize function */
void flightControlSystem_initialize(void)
{
  /* Registration code */

  /* initialize non-finites */
  rt_InitInfAndNaN(sizeof(real_T));

  /* non-finite (run-time) assignments */
  flightControlSystem_P.ControlSystem1.SaturationSonar_LowerSat = rtMinusInf;

  /* initialize real-time model */
  (void) memset((void *)flightControlSystem_M, 0,
                sizeof(RT_MODEL_flightControlSystem_T));
  rtmSetTFinal(flightControlSystem_M, 100.0);
  flightControlSystem_M->Timing.stepSize0 = 0.005;

  /* Setup for data logging */
  {
    static RTWLogInfo rt_DataLoggingInfo;
    rt_DataLoggingInfo.loggingInterval = NULL;
    flightControlSystem_M->rtwLogInfo = &rt_DataLoggingInfo;
  }

  /* Setup for data logging */
  {
    rtliSetLogXSignalInfo(flightControlSystem_M->rtwLogInfo, (NULL));
    rtliSetLogXSignalPtrs(flightControlSystem_M->rtwLogInfo, (NULL));
    rtliSetLogT(flightControlSystem_M->rtwLogInfo, "tout");
    rtliSetLogX(flightControlSystem_M->rtwLogInfo, "");
    rtliSetLogXFinal(flightControlSystem_M->rtwLogInfo, "");
    rtliSetLogVarNameModifier(flightControlSystem_M->rtwLogInfo, "rt_");
    rtliSetLogFormat(flightControlSystem_M->rtwLogInfo, 2);
    rtliSetLogMaxRows(flightControlSystem_M->rtwLogInfo, 1000);
    rtliSetLogDecimation(flightControlSystem_M->rtwLogInfo, 1);

    /*
     * Set pointers to the data and signal info for each output
     */
    {
      static void * rt_LoggedOutputSignalPtrs[] = {
        &flightControlSystem_Y.Actuators[0],
        &flightControlSystem_Y.Flag
      };

      rtliSetLogYSignalPtrs(flightControlSystem_M->rtwLogInfo,
                            ((LogSignalPtrsType)rt_LoggedOutputSignalPtrs));
    }

    {
      static int_T rt_LoggedOutputWidths[] = {
        4,
        1
      };

      static int_T rt_LoggedOutputNumDimensions[] = {
        1,
        1
      };

      static int_T rt_LoggedOutputDimensions[] = {
        4,
        1
      };

      static boolean_T rt_LoggedOutputIsVarDims[] = {
        0,
        0
      };

      static void* rt_LoggedCurrentSignalDimensions[] = {
        (NULL),
        (NULL)
      };

      static int_T rt_LoggedCurrentSignalDimensionsSize[] = {
        4,
        4
      };

      static BuiltInDTypeId rt_LoggedOutputDataTypeIds[] = {
        SS_SINGLE,
        SS_UINT8
      };

      static int_T rt_LoggedOutputComplexSignals[] = {
        0,
        0
      };

      static RTWPreprocessingFcnPtr rt_LoggingPreprocessingFcnPtrs[] = {
        (NULL),
        (NULL)
      };

      static const char_T *rt_LoggedOutputLabels[] = {
        "motors",
        "flag" };

      static const char_T *rt_LoggedOutputBlockNames[] = {
        "flightControlSystem/Actuators",
        "flightControlSystem/Flag" };

      static RTWLogDataTypeConvert rt_RTWLogDataTypeConvert[] = {
        { 0, SS_SINGLE, SS_SINGLE, 0, 0, 0, 1.0, 0, 0.0 },

        { 0, SS_UINT8, SS_UINT8, 0, 0, 0, 1.0, 0, 0.0 }
      };

      static RTWLogSignalInfo rt_LoggedOutputSignalInfo[] = {
        {
          2,
          rt_LoggedOutputWidths,
          rt_LoggedOutputNumDimensions,
          rt_LoggedOutputDimensions,
          rt_LoggedOutputIsVarDims,
          rt_LoggedCurrentSignalDimensions,
          rt_LoggedCurrentSignalDimensionsSize,
          rt_LoggedOutputDataTypeIds,
          rt_LoggedOutputComplexSignals,
          (NULL),
          rt_LoggingPreprocessingFcnPtrs,

          { rt_LoggedOutputLabels },
          (NULL),
          (NULL),
          (NULL),

          { rt_LoggedOutputBlockNames },

          { (NULL) },
          (NULL),
          rt_RTWLogDataTypeConvert
        }
      };

      rtliSetLogYSignalInfo(flightControlSystem_M->rtwLogInfo,
                            rt_LoggedOutputSignalInfo);

      /* set currSigDims field */
      rt_LoggedCurrentSignalDimensions[0] = &rt_LoggedOutputWidths[0];
      rt_LoggedCurrentSignalDimensions[1] = &rt_LoggedOutputWidths[1];
    }

    rtliSetLogY(flightControlSystem_M->rtwLogInfo, "yout");
  }

  /* block I/O */
  (void) memset(((void *) &flightControlSystem_B), 0,
                sizeof(B_flightControlSystem_T));

  /* states (dwork) */
  (void) memset((void *)&flightControlSystem_DW, 0,
                sizeof(DW_flightControlSystem_T));

  /* external inputs */
  (void)memset(&flightControlSystem_U, 0, sizeof(ExtU_flightControlSystem_T));

  /* external outputs */
  (void) memset((void *)&flightControlSystem_Y, 0,
                sizeof(ExtY_flightControlSystem_T));

  /* Matfile logging */
  rt_StartDataLoggingWithStartTime(flightControlSystem_M->rtwLogInfo, 0.0,
    rtmGetTFinal(flightControlSystem_M), flightControlSystem_M->Timing.stepSize0,
    (&rtmGetErrorStatus(flightControlSystem_M)));

  {
    int32_T i;

    /* SetupRuntimeResources for Atomic SubSystem: '<Root>/Control System1' */
    flightControlSystem_ControlSystem1_SetupRTR(flightControlSystem_M,
      &flightControlSystem_DW.ControlSystem1);

    /* End of SetupRuntimeResources for SubSystem: '<Root>/Control System1' */

    /* Start for RateTransition: '<Root>/Rate Transition' */
    for (i = 0; i < 7; i++) {
      flightControlSystem_B.RateTransition[i] =
        flightControlSystem_P.RateTransition_InitialCondition;
    }

    /* End of Start for RateTransition: '<Root>/Rate Transition' */

    /* Start for Atomic SubSystem: '<Root>/Control System1' */
    flightControlSystem_ControlSystem1_Start
      (&flightControlSystem_DW.ControlSystem1);

    /* End of Start for SubSystem: '<Root>/Control System1' */

    /* Start for Atomic SubSystem: '<Root>/Image Processing System' */
    /* Start for MATLABSystem: '<S2>/PARROT Image Conversion' */
    flightControlSystem_DW.obj.isInitialized = 0;
    flightControlSystem_DW.objisempty = true;
    flightControlSystem_DW.obj.isInitialized = 1;

    /* End of Start for SubSystem: '<Root>/Image Processing System' */
  }

  {
    int32_T i;

    /* InitializeConditions for RateTransition: '<Root>/Rate Transition' */
    for (i = 0; i < 7; i++) {
      flightControlSystem_DW.RateTransition_Buffer[i] =
        flightControlSystem_P.RateTransition_InitialCondition;
    }

    /* End of InitializeConditions for RateTransition: '<Root>/Rate Transition' */

    /* SystemInitialize for Atomic SubSystem: '<Root>/Control System1' */
    flightControlSystem_ControlSystem1_Init
      (&flightControlSystem_B.ControlSystem1,
       &flightControlSystem_DW.ControlSystem1,
       &flightControlSystem_P.ControlSystem1);

    /* End of SystemInitialize for SubSystem: '<Root>/Control System1' */
  }
}

/* Model terminate function */
void flightControlSystem_terminate(void)
{
  /* (no terminate code required) */
}
