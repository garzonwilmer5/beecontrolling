  function targMap = targDataMap(),

  ;%***********************
  ;% Create Parameter Map *
  ;%***********************
      
    nTotData      = 0; %add to this count as we go
    nTotSects     = 15;
    sectIdxOffset = 0;
    
    ;%
    ;% Define dummy sections & preallocate arrays
    ;%
    dumSection.nData = -1;  
    dumSection.data  = [];
    
    dumData.logicalSrcIdx = -1;
    dumData.dtTransOffset = -1;
    
    ;%
    ;% Init/prealloc paramMap
    ;%
    paramMap.nSections           = nTotSects;
    paramMap.sectIdxOffset       = sectIdxOffset;
      paramMap.sections(nTotSects) = dumSection; %prealloc
    paramMap.nTotData            = -1;
    
    ;%
    ;% Auto data (flightControlSystem_P)
    ;%
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% flightControlSystem_P.Sensors
	  section.data(1).logicalSrcIdx = 0;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(1) = section;
      clear section
      
      section.nData     = 13;
      section.data(13)  = dumData; %prealloc
      
	  ;% flightControlSystem_P.Constant_Value
	  section.data(1).logicalSrcIdx = 1;
	  section.data(1).dtTransOffset = 0;
	
	  ;% flightControlSystem_P.Constant_Value_d
	  section.data(2).logicalSrcIdx = 2;
	  section.data(2).dtTransOffset = 1;
	
	  ;% flightControlSystem_P.Switch_Threshold
	  section.data(3).logicalSrcIdx = 3;
	  section.data(3).dtTransOffset = 2;
	
	  ;% flightControlSystem_P.Gain_Gain
	  section.data(4).logicalSrcIdx = 4;
	  section.data(4).dtTransOffset = 3;
	
	  ;% flightControlSystem_P.Switch_Threshold_e
	  section.data(5).logicalSrcIdx = 5;
	  section.data(5).dtTransOffset = 163;
	
	  ;% flightControlSystem_P.Gain_Gain_m
	  section.data(6).logicalSrcIdx = 6;
	  section.data(6).dtTransOffset = 164;
	
	  ;% flightControlSystem_P.Gain_Gain_c
	  section.data(7).logicalSrcIdx = 7;
	  section.data(7).dtTransOffset = 324;
	
	  ;% flightControlSystem_P.Umbral07AR7_Value
	  section.data(8).logicalSrcIdx = 8;
	  section.data(8).dtTransOffset = 484;
	
	  ;% flightControlSystem_P.Toleranciasuperior_Value
	  section.data(9).logicalSrcIdx = 9;
	  section.data(9).dtTransOffset = 485;
	
	  ;% flightControlSystem_P.ToleranciaInferior_Value
	  section.data(10).logicalSrcIdx = 10;
	  section.data(10).dtTransOffset = 486;
	
	  ;% flightControlSystem_P.Gain_Gain_mb
	  section.data(11).logicalSrcIdx = 11;
	  section.data(11).dtTransOffset = 487;
	
	  ;% flightControlSystem_P.Ruido_Value
	  section.data(12).logicalSrcIdx = 12;
	  section.data(12).dtTransOffset = 607;
	
	  ;% flightControlSystem_P.RateTransition_InitialCondition
	  section.data(13).logicalSrcIdx = 13;
	  section.data(13).dtTransOffset = 608;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(2) = section;
      clear section
      
      section.nData     = 6;
      section.data(6)  = dumData; %prealloc
      
	  ;% flightControlSystem_P.ControlSystem.outlierBelowFloor_const
	  section.data(1).logicalSrcIdx = 14;
	  section.data(1).dtTransOffset = 0;
	
	  ;% flightControlSystem_P.ControlSystem.outlierJump_const
	  section.data(2).logicalSrcIdx = 15;
	  section.data(2).dtTransOffset = 1;
	
	  ;% flightControlSystem_P.ControlSystem.currentEstimateVeryOffFromPressure_const
	  section.data(3).logicalSrcIdx = 16;
	  section.data(3).dtTransOffset = 2;
	
	  ;% flightControlSystem_P.ControlSystem.currentStateVeryOffsonarflt_const
	  section.data(4).logicalSrcIdx = 17;
	  section.data(4).dtTransOffset = 3;
	
	  ;% flightControlSystem_P.ControlSystem.Checkerrorcondition_const
	  section.data(5).logicalSrcIdx = 18;
	  section.data(5).dtTransOffset = 4;
	
	  ;% flightControlSystem_P.ControlSystem.u0continuousOFerrors_const
	  section.data(6).logicalSrcIdx = 19;
	  section.data(6).dtTransOffset = 5;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(3) = section;
      clear section
      
      section.nData     = 24;
      section.data(24)  = dumData; %prealloc
      
	  ;% flightControlSystem_P.ControlSystem.DiscreteDerivative_ICPrevScaledInput
	  section.data(1).logicalSrcIdx = 20;
	  section.data(1).dtTransOffset = 0;
	
	  ;% flightControlSystem_P.ControlSystem.DiscreteDerivative_ICPrevScaledInput_i
	  section.data(2).logicalSrcIdx = 21;
	  section.data(2).dtTransOffset = 1;
	
	  ;% flightControlSystem_P.ControlSystem.CompareToConstant_const
	  section.data(3).logicalSrcIdx = 22;
	  section.data(3).dtTransOffset = 2;
	
	  ;% flightControlSystem_P.ControlSystem.CompareToConstant1_const
	  section.data(4).logicalSrcIdx = 23;
	  section.data(4).dtTransOffset = 3;
	
	  ;% flightControlSystem_P.ControlSystem.maxp_const
	  section.data(5).logicalSrcIdx = 24;
	  section.data(5).dtTransOffset = 4;
	
	  ;% flightControlSystem_P.ControlSystem.maxq_const
	  section.data(6).logicalSrcIdx = 25;
	  section.data(6).dtTransOffset = 5;
	
	  ;% flightControlSystem_P.ControlSystem.maxw1_const
	  section.data(7).logicalSrcIdx = 26;
	  section.data(7).dtTransOffset = 6;
	
	  ;% flightControlSystem_P.ControlSystem.maxw2_const
	  section.data(8).logicalSrcIdx = 27;
	  section.data(8).dtTransOffset = 7;
	
	  ;% flightControlSystem_P.ControlSystem.maxdw1_const
	  section.data(9).logicalSrcIdx = 28;
	  section.data(9).dtTransOffset = 8;
	
	  ;% flightControlSystem_P.ControlSystem.maxdw2_const
	  section.data(10).logicalSrcIdx = 29;
	  section.data(10).dtTransOffset = 9;
	
	  ;% flightControlSystem_P.ControlSystem.maxp2_const
	  section.data(11).logicalSrcIdx = 30;
	  section.data(11).dtTransOffset = 10;
	
	  ;% flightControlSystem_P.ControlSystem.maxq2_const
	  section.data(12).logicalSrcIdx = 31;
	  section.data(12).dtTransOffset = 11;
	
	  ;% flightControlSystem_P.ControlSystem.maxw3_const
	  section.data(13).logicalSrcIdx = 32;
	  section.data(13).dtTransOffset = 12;
	
	  ;% flightControlSystem_P.ControlSystem.maxw4_const
	  section.data(14).logicalSrcIdx = 33;
	  section.data(14).dtTransOffset = 13;
	
	  ;% flightControlSystem_P.ControlSystem.minHeightforOF_const
	  section.data(15).logicalSrcIdx = 34;
	  section.data(15).dtTransOffset = 14;
	
	  ;% flightControlSystem_P.ControlSystem.DeactivateAccelerationIfOFisnotusedduetolowaltitude_const
	  section.data(16).logicalSrcIdx = 35;
	  section.data(16).dtTransOffset = 15;
	
	  ;% flightControlSystem_P.ControlSystem.donotuseaccifopticalflowneveravailableNoteOF60HzbutZOHto200_con
	  section.data(17).logicalSrcIdx = 36;
	  section.data(17).dtTransOffset = 16;
	
	  ;% flightControlSystem_P.ControlSystem.donotuseaccifopticalflowneveravailableNoteOF60HzbutZOHto2001_co
	  section.data(18).logicalSrcIdx = 37;
	  section.data(18).dtTransOffset = 17;
	
	  ;% flightControlSystem_P.ControlSystem.CompareToConstant2_const
	  section.data(19).logicalSrcIdx = 38;
	  section.data(19).dtTransOffset = 18;
	
	  ;% flightControlSystem_P.ControlSystem.CompareToConstant4_const
	  section.data(20).logicalSrcIdx = 39;
	  section.data(20).dtTransOffset = 19;
	
	  ;% flightControlSystem_P.ControlSystem.CompareToConstant3_const
	  section.data(21).logicalSrcIdx = 40;
	  section.data(21).dtTransOffset = 20;
	
	  ;% flightControlSystem_P.ControlSystem.CompareToConstant5_const
	  section.data(22).logicalSrcIdx = 41;
	  section.data(22).dtTransOffset = 21;
	
	  ;% flightControlSystem_P.ControlSystem.CompareToConstant_const_d
	  section.data(23).logicalSrcIdx = 42;
	  section.data(23).dtTransOffset = 22;
	
	  ;% flightControlSystem_P.ControlSystem.CompareToConstant1_const_g
	  section.data(24).logicalSrcIdx = 43;
	  section.data(24).dtTransOffset = 23;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(4) = section;
      clear section
      
      section.nData     = 5;
      section.data(5)  = dumData; %prealloc
      
	  ;% flightControlSystem_P.ControlSystem.WrapToZero_Threshold
	  section.data(1).logicalSrcIdx = 44;
	  section.data(1).dtTransOffset = 0;
	
	  ;% flightControlSystem_P.ControlSystem.WrapToZero_Threshold_f
	  section.data(2).logicalSrcIdx = 45;
	  section.data(2).dtTransOffset = 1;
	
	  ;% flightControlSystem_P.ControlSystem.WrapToZero_Threshold_o
	  section.data(3).logicalSrcIdx = 46;
	  section.data(3).dtTransOffset = 2;
	
	  ;% flightControlSystem_P.ControlSystem.CompareToConstant_const_b
	  section.data(4).logicalSrcIdx = 47;
	  section.data(4).dtTransOffset = 3;
	
	  ;% flightControlSystem_P.ControlSystem.CompareToConstant_const_do
	  section.data(5).logicalSrcIdx = 48;
	  section.data(5).dtTransOffset = 4;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(5) = section;
      clear section
      
      section.nData     = 52;
      section.data(52)  = dumData; %prealloc
      
	  ;% flightControlSystem_P.ControlSystem._Value
	  section.data(1).logicalSrcIdx = 49;
	  section.data(1).dtTransOffset = 0;
	
	  ;% flightControlSystem_P.ControlSystem.Constant1_Value
	  section.data(2).logicalSrcIdx = 50;
	  section.data(2).dtTransOffset = 1;
	
	  ;% flightControlSystem_P.ControlSystem.Gain_Gain
	  section.data(3).logicalSrcIdx = 51;
	  section.data(3).dtTransOffset = 2;
	
	  ;% flightControlSystem_P.ControlSystem.Constant6_Value
	  section.data(4).logicalSrcIdx = 52;
	  section.data(4).dtTransOffset = 3;
	
	  ;% flightControlSystem_P.ControlSystem.Constant5_Value
	  section.data(5).logicalSrcIdx = 53;
	  section.data(5).dtTransOffset = 4;
	
	  ;% flightControlSystem_P.ControlSystem.Constant2_Value
	  section.data(6).logicalSrcIdx = 54;
	  section.data(6).dtTransOffset = 5;
	
	  ;% flightControlSystem_P.ControlSystem.Constant_Value
	  section.data(7).logicalSrcIdx = 55;
	  section.data(7).dtTransOffset = 6;
	
	  ;% flightControlSystem_P.ControlSystem.Switch_Threshold
	  section.data(8).logicalSrcIdx = 56;
	  section.data(8).dtTransOffset = 7;
	
	  ;% flightControlSystem_P.ControlSystem.Constant3_Value
	  section.data(9).logicalSrcIdx = 57;
	  section.data(9).dtTransOffset = 8;
	
	  ;% flightControlSystem_P.ControlSystem.Lykyhatkk1_Y0
	  section.data(10).logicalSrcIdx = 58;
	  section.data(10).dtTransOffset = 9;
	
	  ;% flightControlSystem_P.ControlSystem.deltax_Y0
	  section.data(11).logicalSrcIdx = 59;
	  section.data(11).dtTransOffset = 10;
	
	  ;% flightControlSystem_P.ControlSystem.Delay2_InitialCondition
	  section.data(12).logicalSrcIdx = 60;
	  section.data(12).dtTransOffset = 11;
	
	  ;% flightControlSystem_P.ControlSystem.X0_Value
	  section.data(13).logicalSrcIdx = 61;
	  section.data(13).dtTransOffset = 12;
	
	  ;% flightControlSystem_P.ControlSystem.SaturationSonar_LowerSat
	  section.data(14).logicalSrcIdx = 62;
	  section.data(14).dtTransOffset = 14;
	
	  ;% flightControlSystem_P.ControlSystem.soonarFilter_IIR_NumCoef
	  section.data(15).logicalSrcIdx = 63;
	  section.data(15).dtTransOffset = 15;
	
	  ;% flightControlSystem_P.ControlSystem.soonarFilter_IIR_DenCoef
	  section.data(16).logicalSrcIdx = 64;
	  section.data(16).dtTransOffset = 19;
	
	  ;% flightControlSystem_P.ControlSystem.soonarFilter_IIR_InitialStates
	  section.data(17).logicalSrcIdx = 65;
	  section.data(17).dtTransOffset = 23;
	
	  ;% flightControlSystem_P.ControlSystem.KalmanGainM_Value
	  section.data(18).logicalSrcIdx = 66;
	  section.data(18).dtTransOffset = 24;
	
	  ;% flightControlSystem_P.ControlSystem.C_Value
	  section.data(19).logicalSrcIdx = 67;
	  section.data(19).dtTransOffset = 26;
	
	  ;% flightControlSystem_P.ControlSystem.KalmanGainM_Value_d
	  section.data(20).logicalSrcIdx = 68;
	  section.data(20).dtTransOffset = 28;
	
	  ;% flightControlSystem_P.ControlSystem.Constant4_Value
	  section.data(21).logicalSrcIdx = 69;
	  section.data(21).dtTransOffset = 32;
	
	  ;% flightControlSystem_P.ControlSystem.Delay_InitialCondition
	  section.data(22).logicalSrcIdx = 70;
	  section.data(22).dtTransOffset = 33;
	
	  ;% flightControlSystem_P.ControlSystem.Gain_Gain_f
	  section.data(23).logicalSrcIdx = 71;
	  section.data(23).dtTransOffset = 34;
	
	  ;% flightControlSystem_P.ControlSystem.Gain1_Gain
	  section.data(24).logicalSrcIdx = 72;
	  section.data(24).dtTransOffset = 35;
	
	  ;% flightControlSystem_P.ControlSystem.Switch2_Threshold
	  section.data(25).logicalSrcIdx = 73;
	  section.data(25).dtTransOffset = 36;
	
	  ;% flightControlSystem_P.ControlSystem.PitchAleteo_Value
	  section.data(26).logicalSrcIdx = 74;
	  section.data(26).dtTransOffset = 37;
	
	  ;% flightControlSystem_P.ControlSystem.PitchExploracion_Value
	  section.data(27).logicalSrcIdx = 75;
	  section.data(27).dtTransOffset = 38;
	
	  ;% flightControlSystem_P.ControlSystem.TiempodeFrenado_Value
	  section.data(28).logicalSrcIdx = 76;
	  section.data(28).dtTransOffset = 39;
	
	  ;% flightControlSystem_P.ControlSystem.TiempodeCalibracion_Value
	  section.data(29).logicalSrcIdx = 77;
	  section.data(29).dtTransOffset = 40;
	
	  ;% flightControlSystem_P.ControlSystem.A_Value
	  section.data(30).logicalSrcIdx = 78;
	  section.data(30).dtTransOffset = 41;
	
	  ;% flightControlSystem_P.ControlSystem.KalmanGainL_Value
	  section.data(31).logicalSrcIdx = 79;
	  section.data(31).dtTransOffset = 45;
	
	  ;% flightControlSystem_P.ControlSystem.gravity_Value
	  section.data(32).logicalSrcIdx = 80;
	  section.data(32).dtTransOffset = 49;
	
	  ;% flightControlSystem_P.ControlSystem.gravity_Value_b
	  section.data(33).logicalSrcIdx = 81;
	  section.data(33).dtTransOffset = 52;
	
	  ;% flightControlSystem_P.ControlSystem.gainaccinput_Gain
	  section.data(34).logicalSrcIdx = 82;
	  section.data(34).dtTransOffset = 55;
	
	  ;% flightControlSystem_P.ControlSystem.B_Value
	  section.data(35).logicalSrcIdx = 83;
	  section.data(35).dtTransOffset = 56;
	
	  ;% flightControlSystem_P.ControlSystem.D_Value
	  section.data(36).logicalSrcIdx = 84;
	  section.data(36).dtTransOffset = 58;
	
	  ;% flightControlSystem_P.ControlSystem.KalmanGainL_Value_p
	  section.data(37).logicalSrcIdx = 85;
	  section.data(37).dtTransOffset = 59;
	
	  ;% flightControlSystem_P.ControlSystem.Constant1_Value_h
	  section.data(38).logicalSrcIdx = 86;
	  section.data(38).dtTransOffset = 61;
	
	  ;% flightControlSystem_P.ControlSystem.Switch1_Threshold
	  section.data(39).logicalSrcIdx = 87;
	  section.data(39).dtTransOffset = 62;
	
	  ;% flightControlSystem_P.ControlSystem.Wait3Seconds_Value
	  section.data(40).logicalSrcIdx = 88;
	  section.data(40).dtTransOffset = 63;
	
	  ;% flightControlSystem_P.ControlSystem.DelayOneStep_InitialCondition
	  section.data(41).logicalSrcIdx = 89;
	  section.data(41).dtTransOffset = 64;
	
	  ;% flightControlSystem_P.ControlSystem.u5meters_Value
	  section.data(42).logicalSrcIdx = 90;
	  section.data(42).dtTransOffset = 65;
	
	  ;% flightControlSystem_P.ControlSystem.CovarianceZ_Value
	  section.data(43).logicalSrcIdx = 91;
	  section.data(43).dtTransOffset = 66;
	
	  ;% flightControlSystem_P.ControlSystem.P0_Value
	  section.data(44).logicalSrcIdx = 92;
	  section.data(44).dtTransOffset = 70;
	
	  ;% flightControlSystem_P.ControlSystem.CovarianceZ_Value_i
	  section.data(45).logicalSrcIdx = 93;
	  section.data(45).dtTransOffset = 74;
	
	  ;% flightControlSystem_P.ControlSystem.G_Value
	  section.data(46).logicalSrcIdx = 94;
	  section.data(46).dtTransOffset = 78;
	
	  ;% flightControlSystem_P.ControlSystem.ConstantP_Value
	  section.data(47).logicalSrcIdx = 95;
	  section.data(47).dtTransOffset = 80;
	
	  ;% flightControlSystem_P.ControlSystem.H_Value
	  section.data(48).logicalSrcIdx = 96;
	  section.data(48).dtTransOffset = 81;
	
	  ;% flightControlSystem_P.ControlSystem.N_Value
	  section.data(49).logicalSrcIdx = 97;
	  section.data(49).dtTransOffset = 82;
	
	  ;% flightControlSystem_P.ControlSystem.Q_Value
	  section.data(50).logicalSrcIdx = 98;
	  section.data(50).dtTransOffset = 83;
	
	  ;% flightControlSystem_P.ControlSystem.R_Value
	  section.data(51).logicalSrcIdx = 99;
	  section.data(51).dtTransOffset = 84;
	
	  ;% flightControlSystem_P.ControlSystem.ConstantP_Value_a
	  section.data(52).logicalSrcIdx = 100;
	  section.data(52).dtTransOffset = 85;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(6) = section;
      clear section
      
      section.nData     = 102;
      section.data(102)  = dumData; %prealloc
      
	  ;% flightControlSystem_P.ControlSystem.D_xy_Gain
	  section.data(1).logicalSrcIdx = 101;
	  section.data(1).dtTransOffset = 0;
	
	  ;% flightControlSystem_P.ControlSystem.Gain_Gain_i
	  section.data(2).logicalSrcIdx = 102;
	  section.data(2).dtTransOffset = 2;
	
	  ;% flightControlSystem_P.ControlSystem.Saturation_UpperSat
	  section.data(3).logicalSrcIdx = 103;
	  section.data(3).dtTransOffset = 3;
	
	  ;% flightControlSystem_P.ControlSystem.Saturation_LowerSat
	  section.data(4).logicalSrcIdx = 104;
	  section.data(4).dtTransOffset = 4;
	
	  ;% flightControlSystem_P.ControlSystem.P_xy_Gain
	  section.data(5).logicalSrcIdx = 105;
	  section.data(5).dtTransOffset = 5;
	
	  ;% flightControlSystem_P.ControlSystem.D_z1_Gain
	  section.data(6).logicalSrcIdx = 106;
	  section.data(6).dtTransOffset = 7;
	
	  ;% flightControlSystem_P.ControlSystem.P_z1_Gain
	  section.data(7).logicalSrcIdx = 107;
	  section.data(7).dtTransOffset = 8;
	
	  ;% flightControlSystem_P.ControlSystem.takeoff_gain1_Gain
	  section.data(8).logicalSrcIdx = 108;
	  section.data(8).dtTransOffset = 9;
	
	  ;% flightControlSystem_P.ControlSystem._Value_n
	  section.data(9).logicalSrcIdx = 109;
	  section.data(9).dtTransOffset = 10;
	
	  ;% flightControlSystem_P.ControlSystem.Switch1_Threshold_p
	  section.data(10).logicalSrcIdx = 110;
	  section.data(10).dtTransOffset = 11;
	
	  ;% flightControlSystem_P.ControlSystem.kp_Gain
	  section.data(11).logicalSrcIdx = 111;
	  section.data(11).dtTransOffset = 12;
	
	  ;% flightControlSystem_P.ControlSystem.degToRad_Gain
	  section.data(12).logicalSrcIdx = 112;
	  section.data(12).dtTransOffset = 13;
	
	  ;% flightControlSystem_P.ControlSystem.Switch1_Threshold_f
	  section.data(13).logicalSrcIdx = 113;
	  section.data(13).dtTransOffset = 14;
	
	  ;% flightControlSystem_P.ControlSystem.Switch_Threshold_n
	  section.data(14).logicalSrcIdx = 114;
	  section.data(14).dtTransOffset = 15;
	
	  ;% flightControlSystem_P.ControlSystem.Out1_Y0
	  section.data(15).logicalSrcIdx = 115;
	  section.data(15).dtTransOffset = 16;
	
	  ;% flightControlSystem_P.ControlSystem.Gain_Gain_h
	  section.data(16).logicalSrcIdx = 116;
	  section.data(16).dtTransOffset = 17;
	
	  ;% flightControlSystem_P.ControlSystem.Saturation_UpperSat_m
	  section.data(17).logicalSrcIdx = 117;
	  section.data(17).dtTransOffset = 18;
	
	  ;% flightControlSystem_P.ControlSystem.Saturation_LowerSat_i
	  section.data(18).logicalSrcIdx = 118;
	  section.data(18).dtTransOffset = 19;
	
	  ;% flightControlSystem_P.ControlSystem.P_xy_Gain_h
	  section.data(19).logicalSrcIdx = 119;
	  section.data(19).dtTransOffset = 20;
	
	  ;% flightControlSystem_P.ControlSystem.Gain1_Gain_p
	  section.data(20).logicalSrcIdx = 120;
	  section.data(20).dtTransOffset = 22;
	
	  ;% flightControlSystem_P.ControlSystem.Gain_Gain_a
	  section.data(21).logicalSrcIdx = 121;
	  section.data(21).dtTransOffset = 23;
	
	  ;% flightControlSystem_P.ControlSystem.Gain2_Gain
	  section.data(22).logicalSrcIdx = 122;
	  section.data(22).dtTransOffset = 24;
	
	  ;% flightControlSystem_P.ControlSystem.Gain3_Gain
	  section.data(23).logicalSrcIdx = 123;
	  section.data(23).dtTransOffset = 25;
	
	  ;% flightControlSystem_P.ControlSystem.Gain4_Gain
	  section.data(24).logicalSrcIdx = 124;
	  section.data(24).dtTransOffset = 26;
	
	  ;% flightControlSystem_P.ControlSystem.Gain_Gain_i5
	  section.data(25).logicalSrcIdx = 125;
	  section.data(25).dtTransOffset = 27;
	
	  ;% flightControlSystem_P.ControlSystem.opticalFlowErrorCorrect_Gain
	  section.data(26).logicalSrcIdx = 126;
	  section.data(26).dtTransOffset = 28;
	
	  ;% flightControlSystem_P.ControlSystem.Lykyhatkk1_Y0_o
	  section.data(27).logicalSrcIdx = 127;
	  section.data(27).dtTransOffset = 29;
	
	  ;% flightControlSystem_P.ControlSystem.deltax_Y0_n
	  section.data(28).logicalSrcIdx = 128;
	  section.data(28).dtTransOffset = 30;
	
	  ;% flightControlSystem_P.ControlSystem.TorqueTotalThrustToThrustPerMotor_Value
	  section.data(29).logicalSrcIdx = 129;
	  section.data(29).dtTransOffset = 31;
	
	  ;% flightControlSystem_P.ControlSystem.SimplyIntegrateVelocity_gainval
	  section.data(30).logicalSrcIdx = 130;
	  section.data(30).dtTransOffset = 47;
	
	  ;% flightControlSystem_P.ControlSystem.SimplyIntegrateVelocity_IC
	  section.data(31).logicalSrcIdx = 131;
	  section.data(31).dtTransOffset = 48;
	
	  ;% flightControlSystem_P.ControlSystem.invertzaxisGain_Gain
	  section.data(32).logicalSrcIdx = 132;
	  section.data(32).dtTransOffset = 49;
	
	  ;% flightControlSystem_P.ControlSystem.prsToAltGain_Gain
	  section.data(33).logicalSrcIdx = 133;
	  section.data(33).dtTransOffset = 50;
	
	  ;% flightControlSystem_P.ControlSystem.pressureFilter_IIR_NumCoef
	  section.data(34).logicalSrcIdx = 134;
	  section.data(34).dtTransOffset = 51;
	
	  ;% flightControlSystem_P.ControlSystem.pressureFilter_IIR_DenCoef
	  section.data(35).logicalSrcIdx = 135;
	  section.data(35).dtTransOffset = 55;
	
	  ;% flightControlSystem_P.ControlSystem.pressureFilter_IIR_InitialStates
	  section.data(36).logicalSrcIdx = 136;
	  section.data(36).dtTransOffset = 59;
	
	  ;% flightControlSystem_P.ControlSystem.Memory_InitialCondition
	  section.data(37).logicalSrcIdx = 137;
	  section.data(37).dtTransOffset = 60;
	
	  ;% flightControlSystem_P.ControlSystem.Constant_Value_p
	  section.data(38).logicalSrcIdx = 138;
	  section.data(38).dtTransOffset = 63;
	
	  ;% flightControlSystem_P.ControlSystem.Gain_Gain_e
	  section.data(39).logicalSrcIdx = 139;
	  section.data(39).dtTransOffset = 64;
	
	  ;% flightControlSystem_P.ControlSystem.Assumingthatcalibwasdonelevel_Bias
	  section.data(40).logicalSrcIdx = 140;
	  section.data(40).dtTransOffset = 65;
	
	  ;% flightControlSystem_P.ControlSystem.inverseIMU_gain_Gain
	  section.data(41).logicalSrcIdx = 141;
	  section.data(41).dtTransOffset = 71;
	
	  ;% flightControlSystem_P.ControlSystem.IIR_IMUgyro_r_NumCoef
	  section.data(42).logicalSrcIdx = 142;
	  section.data(42).dtTransOffset = 77;
	
	  ;% flightControlSystem_P.ControlSystem.IIR_IMUgyro_r_DenCoef
	  section.data(43).logicalSrcIdx = 143;
	  section.data(43).dtTransOffset = 83;
	
	  ;% flightControlSystem_P.ControlSystem.IIR_IMUgyro_r_InitialStates
	  section.data(44).logicalSrcIdx = 144;
	  section.data(44).dtTransOffset = 89;
	
	  ;% flightControlSystem_P.ControlSystem.Gain_Gain_m
	  section.data(45).logicalSrcIdx = 145;
	  section.data(45).dtTransOffset = 90;
	
	  ;% flightControlSystem_P.ControlSystem.FIR_IMUaccel_InitialStates
	  section.data(46).logicalSrcIdx = 146;
	  section.data(46).dtTransOffset = 91;
	
	  ;% flightControlSystem_P.ControlSystem.FIR_IMUaccel_Coefficients
	  section.data(47).logicalSrcIdx = 147;
	  section.data(47).dtTransOffset = 92;
	
	  ;% flightControlSystem_P.ControlSystem.Constant_Value_l
	  section.data(48).logicalSrcIdx = 148;
	  section.data(48).dtTransOffset = 98;
	
	  ;% flightControlSystem_P.ControlSystem.Merge_InitialOutput
	  section.data(49).logicalSrcIdx = 149;
	  section.data(49).dtTransOffset = 99;
	
	  ;% flightControlSystem_P.ControlSystem.X0_Value_j
	  section.data(50).logicalSrcIdx = 150;
	  section.data(50).dtTransOffset = 100;
	
	  ;% flightControlSystem_P.ControlSystem.C_Value_n
	  section.data(51).logicalSrcIdx = 151;
	  section.data(51).dtTransOffset = 102;
	
	  ;% flightControlSystem_P.ControlSystem.IIRgyroz_NumCoef
	  section.data(52).logicalSrcIdx = 152;
	  section.data(52).dtTransOffset = 106;
	
	  ;% flightControlSystem_P.ControlSystem.IIRgyroz_DenCoef
	  section.data(53).logicalSrcIdx = 153;
	  section.data(53).dtTransOffset = 112;
	
	  ;% flightControlSystem_P.ControlSystem.IIRgyroz_InitialStates
	  section.data(54).logicalSrcIdx = 154;
	  section.data(54).dtTransOffset = 118;
	
	  ;% flightControlSystem_P.ControlSystem.TSamp_WtEt
	  section.data(55).logicalSrcIdx = 155;
	  section.data(55).dtTransOffset = 119;
	
	  ;% flightControlSystem_P.ControlSystem.Delay_InitialCondition_g
	  section.data(56).logicalSrcIdx = 156;
	  section.data(56).dtTransOffset = 120;
	
	  ;% flightControlSystem_P.ControlSystem.Delay1_InitialCondition
	  section.data(57).logicalSrcIdx = 157;
	  section.data(57).dtTransOffset = 121;
	
	  ;% flightControlSystem_P.ControlSystem.D_pr_Gain
	  section.data(58).logicalSrcIdx = 158;
	  section.data(58).dtTransOffset = 122;
	
	  ;% flightControlSystem_P.ControlSystem.DiscreteTimeIntegrator_gainval
	  section.data(59).logicalSrcIdx = 159;
	  section.data(59).dtTransOffset = 124;
	
	  ;% flightControlSystem_P.ControlSystem.DiscreteTimeIntegrator_IC
	  section.data(60).logicalSrcIdx = 160;
	  section.data(60).dtTransOffset = 125;
	
	  ;% flightControlSystem_P.ControlSystem.DiscreteTimeIntegrator_UpperSat
	  section.data(61).logicalSrcIdx = 161;
	  section.data(61).dtTransOffset = 126;
	
	  ;% flightControlSystem_P.ControlSystem.DiscreteTimeIntegrator_LowerSat
	  section.data(62).logicalSrcIdx = 162;
	  section.data(62).dtTransOffset = 127;
	
	  ;% flightControlSystem_P.ControlSystem.I_pr_Gain
	  section.data(63).logicalSrcIdx = 163;
	  section.data(63).dtTransOffset = 128;
	
	  ;% flightControlSystem_P.ControlSystem.AlturadeVuelo_Value
	  section.data(64).logicalSrcIdx = 164;
	  section.data(64).dtTransOffset = 129;
	
	  ;% flightControlSystem_P.ControlSystem.Gain_Gain_e2
	  section.data(65).logicalSrcIdx = 165;
	  section.data(65).dtTransOffset = 130;
	
	  ;% flightControlSystem_P.ControlSystem.kp2_Gain
	  section.data(66).logicalSrcIdx = 166;
	  section.data(66).dtTransOffset = 131;
	
	  ;% flightControlSystem_P.ControlSystem.TSamp_WtEt_e
	  section.data(67).logicalSrcIdx = 167;
	  section.data(67).dtTransOffset = 132;
	
	  ;% flightControlSystem_P.ControlSystem.DiscreteTimeIntegrator_gainval_d
	  section.data(68).logicalSrcIdx = 168;
	  section.data(68).dtTransOffset = 133;
	
	  ;% flightControlSystem_P.ControlSystem.DiscreteTimeIntegrator_IC_n
	  section.data(69).logicalSrcIdx = 169;
	  section.data(69).dtTransOffset = 134;
	
	  ;% flightControlSystem_P.ControlSystem.Switch_Threshold_i
	  section.data(70).logicalSrcIdx = 170;
	  section.data(70).dtTransOffset = 135;
	
	  ;% flightControlSystem_P.ControlSystem.D_xy1_Gain
	  section.data(71).logicalSrcIdx = 171;
	  section.data(71).dtTransOffset = 136;
	
	  ;% flightControlSystem_P.ControlSystem.P_pr_Gain
	  section.data(72).logicalSrcIdx = 172;
	  section.data(72).dtTransOffset = 138;
	
	  ;% flightControlSystem_P.ControlSystem.w1_Value
	  section.data(73).logicalSrcIdx = 173;
	  section.data(73).dtTransOffset = 140;
	
	  ;% flightControlSystem_P.ControlSystem.DiscreteTimeIntegrator_gainval_n
	  section.data(74).logicalSrcIdx = 174;
	  section.data(74).dtTransOffset = 141;
	
	  ;% flightControlSystem_P.ControlSystem.DiscreteTimeIntegrator_IC_b
	  section.data(75).logicalSrcIdx = 175;
	  section.data(75).dtTransOffset = 142;
	
	  ;% flightControlSystem_P.ControlSystem.DiscreteTimeIntegrator_UpperSat_i
	  section.data(76).logicalSrcIdx = 176;
	  section.data(76).dtTransOffset = 143;
	
	  ;% flightControlSystem_P.ControlSystem.DiscreteTimeIntegrator_LowerSat_n
	  section.data(77).logicalSrcIdx = 177;
	  section.data(77).dtTransOffset = 144;
	
	  ;% flightControlSystem_P.ControlSystem.SaturationThrust1_UpperSat
	  section.data(78).logicalSrcIdx = 178;
	  section.data(78).dtTransOffset = 145;
	
	  ;% flightControlSystem_P.ControlSystem.SaturationThrust1_LowerSat
	  section.data(79).logicalSrcIdx = 179;
	  section.data(79).dtTransOffset = 146;
	
	  ;% flightControlSystem_P.ControlSystem.P_yaw_Gain
	  section.data(80).logicalSrcIdx = 180;
	  section.data(80).dtTransOffset = 147;
	
	  ;% flightControlSystem_P.ControlSystem.D_yaw_Gain
	  section.data(81).logicalSrcIdx = 181;
	  section.data(81).dtTransOffset = 148;
	
	  ;% flightControlSystem_P.ControlSystem.ThrustToMotorCommand_Gain
	  section.data(82).logicalSrcIdx = 182;
	  section.data(82).dtTransOffset = 149;
	
	  ;% flightControlSystem_P.ControlSystem.Saturation5_UpperSat
	  section.data(83).logicalSrcIdx = 183;
	  section.data(83).dtTransOffset = 150;
	
	  ;% flightControlSystem_P.ControlSystem.Saturation5_LowerSat
	  section.data(84).logicalSrcIdx = 184;
	  section.data(84).dtTransOffset = 151;
	
	  ;% flightControlSystem_P.ControlSystem.MotorDirections_Gain
	  section.data(85).logicalSrcIdx = 185;
	  section.data(85).dtTransOffset = 152;
	
	  ;% flightControlSystem_P.ControlSystem.A_Value_k
	  section.data(86).logicalSrcIdx = 186;
	  section.data(86).dtTransOffset = 156;
	
	  ;% flightControlSystem_P.ControlSystem.B_Value_p
	  section.data(87).logicalSrcIdx = 187;
	  section.data(87).dtTransOffset = 160;
	
	  ;% flightControlSystem_P.ControlSystem.D_Value_g
	  section.data(88).logicalSrcIdx = 188;
	  section.data(88).dtTransOffset = 164;
	
	  ;% flightControlSystem_P.ControlSystem.Delay_InitialCondition_i
	  section.data(89).logicalSrcIdx = 189;
	  section.data(89).dtTransOffset = 168;
	
	  ;% flightControlSystem_P.ControlSystem.antiWU_Gain_Gain
	  section.data(90).logicalSrcIdx = 190;
	  section.data(90).dtTransOffset = 169;
	
	  ;% flightControlSystem_P.ControlSystem.I_pr_Gain_e
	  section.data(91).logicalSrcIdx = 191;
	  section.data(91).dtTransOffset = 170;
	
	  ;% flightControlSystem_P.ControlSystem.kp1_Gain
	  section.data(92).logicalSrcIdx = 192;
	  section.data(92).dtTransOffset = 171;
	
	  ;% flightControlSystem_P.ControlSystem.Gain_Gain_mc
	  section.data(93).logicalSrcIdx = 193;
	  section.data(93).dtTransOffset = 172;
	
	  ;% flightControlSystem_P.ControlSystem.Gain1_Gain_g
	  section.data(94).logicalSrcIdx = 194;
	  section.data(94).dtTransOffset = 173;
	
	  ;% flightControlSystem_P.ControlSystem.P0_Value_k
	  section.data(95).logicalSrcIdx = 195;
	  section.data(95).dtTransOffset = 174;
	
	  ;% flightControlSystem_P.ControlSystem.G_Value_d
	  section.data(96).logicalSrcIdx = 196;
	  section.data(96).dtTransOffset = 178;
	
	  ;% flightControlSystem_P.ControlSystem.H_Value_n
	  section.data(97).logicalSrcIdx = 197;
	  section.data(97).dtTransOffset = 182;
	
	  ;% flightControlSystem_P.ControlSystem.N_Value_e
	  section.data(98).logicalSrcIdx = 198;
	  section.data(98).dtTransOffset = 186;
	
	  ;% flightControlSystem_P.ControlSystem.Q_Value_h
	  section.data(99).logicalSrcIdx = 199;
	  section.data(99).dtTransOffset = 190;
	
	  ;% flightControlSystem_P.ControlSystem.R_Value_p
	  section.data(100).logicalSrcIdx = 200;
	  section.data(100).dtTransOffset = 194;
	
	  ;% flightControlSystem_P.ControlSystem.Constant_Value_j
	  section.data(101).logicalSrcIdx = 201;
	  section.data(101).dtTransOffset = 198;
	
	  ;% flightControlSystem_P.ControlSystem.Constant_Value_n
	  section.data(102).logicalSrcIdx = 202;
	  section.data(102).dtTransOffset = 199;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(7) = section;
      clear section
      
      section.nData     = 17;
      section.data(17)  = dumData; %prealloc
      
	  ;% flightControlSystem_P.ControlSystem.Delay2_DelayLength
	  section.data(1).logicalSrcIdx = 203;
	  section.data(1).dtTransOffset = 0;
	
	  ;% flightControlSystem_P.ControlSystem.MemoryX_DelayLength
	  section.data(2).logicalSrcIdx = 204;
	  section.data(2).dtTransOffset = 1;
	
	  ;% flightControlSystem_P.ControlSystem.MemoryX_DelayLength_k
	  section.data(3).logicalSrcIdx = 205;
	  section.data(3).dtTransOffset = 2;
	
	  ;% flightControlSystem_P.ControlSystem.Output_InitialCondition
	  section.data(4).logicalSrcIdx = 206;
	  section.data(4).dtTransOffset = 3;
	
	  ;% flightControlSystem_P.ControlSystem.Delay_DelayLength
	  section.data(5).logicalSrcIdx = 207;
	  section.data(5).dtTransOffset = 4;
	
	  ;% flightControlSystem_P.ControlSystem.Delay1_DelayLength
	  section.data(6).logicalSrcIdx = 208;
	  section.data(6).dtTransOffset = 5;
	
	  ;% flightControlSystem_P.ControlSystem.Delay_DelayLength_b
	  section.data(7).logicalSrcIdx = 209;
	  section.data(7).dtTransOffset = 6;
	
	  ;% flightControlSystem_P.ControlSystem.Output_InitialCondition_k
	  section.data(8).logicalSrcIdx = 210;
	  section.data(8).dtTransOffset = 7;
	
	  ;% flightControlSystem_P.ControlSystem.Delay_DelayLength_o
	  section.data(9).logicalSrcIdx = 211;
	  section.data(9).dtTransOffset = 8;
	
	  ;% flightControlSystem_P.ControlSystem.FixPtConstant_Value
	  section.data(10).logicalSrcIdx = 212;
	  section.data(10).dtTransOffset = 9;
	
	  ;% flightControlSystem_P.ControlSystem.Constant_Value_lh
	  section.data(11).logicalSrcIdx = 213;
	  section.data(11).dtTransOffset = 10;
	
	  ;% flightControlSystem_P.ControlSystem.Output_InitialCondition_l
	  section.data(12).logicalSrcIdx = 214;
	  section.data(12).dtTransOffset = 11;
	
	  ;% flightControlSystem_P.ControlSystem.DelayOneStep_DelayLength
	  section.data(13).logicalSrcIdx = 215;
	  section.data(13).dtTransOffset = 12;
	
	  ;% flightControlSystem_P.ControlSystem.FixPtConstant_Value_j
	  section.data(14).logicalSrcIdx = 216;
	  section.data(14).dtTransOffset = 13;
	
	  ;% flightControlSystem_P.ControlSystem.Constant_Value_ne
	  section.data(15).logicalSrcIdx = 217;
	  section.data(15).dtTransOffset = 14;
	
	  ;% flightControlSystem_P.ControlSystem.FixPtConstant_Value_p
	  section.data(16).logicalSrcIdx = 218;
	  section.data(16).dtTransOffset = 15;
	
	  ;% flightControlSystem_P.ControlSystem.Constant_Value_d
	  section.data(17).logicalSrcIdx = 219;
	  section.data(17).dtTransOffset = 16;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(8) = section;
      clear section
      
      section.nData     = 5;
      section.data(5)  = dumData; %prealloc
      
	  ;% flightControlSystem_P.ControlSystem.controlModePosVsOrient_Value
	  section.data(1).logicalSrcIdx = 220;
	  section.data(1).dtTransOffset = 0;
	
	  ;% flightControlSystem_P.ControlSystem.Constant1_Value_o
	  section.data(2).logicalSrcIdx = 221;
	  section.data(2).dtTransOffset = 1;
	
	  ;% flightControlSystem_P.ControlSystem.Constant_Value_c
	  section.data(3).logicalSrcIdx = 222;
	  section.data(3).dtTransOffset = 2;
	
	  ;% flightControlSystem_P.ControlSystem.Constant_Value_a
	  section.data(4).logicalSrcIdx = 223;
	  section.data(4).dtTransOffset = 3;
	
	  ;% flightControlSystem_P.ControlSystem.Reset_Value
	  section.data(5).logicalSrcIdx = 224;
	  section.data(5).dtTransOffset = 4;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(9) = section;
      clear section
      
      section.nData     = 3;
      section.data(3)  = dumData; %prealloc
      
	  ;% flightControlSystem_P.ControlSystem.Merge_InitialOutput_i
	  section.data(1).logicalSrcIdx = 225;
	  section.data(1).dtTransOffset = 0;
	
	  ;% flightControlSystem_P.ControlSystem.ManualSwitchPZ_CurrentSetting
	  section.data(2).logicalSrcIdx = 226;
	  section.data(2).dtTransOffset = 1;
	
	  ;% flightControlSystem_P.ControlSystem.ManualSwitchPZ_CurrentSetting_p
	  section.data(3).logicalSrcIdx = 227;
	  section.data(3).dtTransOffset = 2;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(10) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% flightControlSystem_P.ControlSystem.Normalcondition.Constant_Value
	  section.data(1).logicalSrcIdx = 228;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(11) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% flightControlSystem_P.ControlSystem.Ultrasoundimproper.Constant_Value
	  section.data(1).logicalSrcIdx = 229;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(12) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% flightControlSystem_P.ControlSystem.Noopticalflow.Constant_Value
	  section.data(1).logicalSrcIdx = 230;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(13) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% flightControlSystem_P.ControlSystem.estimatorOpticalflowerror.Constant_Value
	  section.data(1).logicalSrcIdx = 231;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(14) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% flightControlSystem_P.ControlSystem.Geofencingerror.Constant_Value
	  section.data(1).logicalSrcIdx = 232;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      paramMap.sections(15) = section;
      clear section
      
    
      ;%
      ;% Non-auto Data (parameter)
      ;%
    

    ;%
    ;% Add final counts to struct.
    ;%
    paramMap.nTotData = nTotData;
    


  ;%**************************
  ;% Create Block Output Map *
  ;%**************************
      
    nTotData      = 0; %add to this count as we go
    nTotSects     = 10;
    sectIdxOffset = 0;
    
    ;%
    ;% Define dummy sections & preallocate arrays
    ;%
    dumSection.nData = -1;  
    dumSection.data  = [];
    
    dumData.logicalSrcIdx = -1;
    dumData.dtTransOffset = -1;
    
    ;%
    ;% Init/prealloc sigMap
    ;%
    sigMap.nSections           = nTotSects;
    sigMap.sectIdxOffset       = sectIdxOffset;
      sigMap.sections(nTotSects) = dumSection; %prealloc
    sigMap.nTotData            = -1;
    
    ;%
    ;% Auto data (flightControlSystem_B)
    ;%
      section.nData     = 4;
      section.data(4)  = dumData; %prealloc
      
	  ;% flightControlSystem_B.UPDown
	  section.data(1).logicalSrcIdx = 3;
	  section.data(1).dtTransOffset = 0;
	
	  ;% flightControlSystem_B.Down
	  section.data(2).logicalSrcIdx = 4;
	  section.data(2).dtTransOffset = 4960;
	
	  ;% flightControlSystem_B.Down_c
	  section.data(3).logicalSrcIdx = 5;
	  section.data(3).dtTransOffset = 14560;
	
	  ;% flightControlSystem_B.BW
	  section.data(4).logicalSrcIdx = 6;
	  section.data(4).dtTransOffset = 19520;
	
      nTotData = nTotData + section.nData;
      sigMap.sections(1) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% flightControlSystem_B.RateTransition
	  section.data(1).logicalSrcIdx = 0;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      sigMap.sections(2) = section;
      clear section
      
      section.nData     = 2;
      section.data(2)  = dumData; %prealloc
      
	  ;% flightControlSystem_B.UP
	  section.data(1).logicalSrcIdx = 1;
	  section.data(1).dtTransOffset = 0;
	
	  ;% flightControlSystem_B.UpUP
	  section.data(2).logicalSrcIdx = 2;
	  section.data(2).dtTransOffset = 9760;
	
      nTotData = nTotData + section.nData;
      sigMap.sections(3) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% flightControlSystem_B.ControlSystem.ControlModeUpdate
	  section.data(1).logicalSrcIdx = 7;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      sigMap.sections(4) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% flightControlSystem_B.ControlSystem.inverseIMU_gain
	  section.data(1).logicalSrcIdx = 20;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      sigMap.sections(5) = section;
      clear section
      
      section.nData     = 10;
      section.data(10)  = dumData; %prealloc
      
	  ;% flightControlSystem_B.ControlSystem.invertzaxisGain
	  section.data(1).logicalSrcIdx = 8;
	  section.data(1).dtTransOffset = 0;
	
	  ;% flightControlSystem_B.ControlSystem.error
	  section.data(2).logicalSrcIdx = 9;
	  section.data(2).dtTransOffset = 1;
	
	  ;% flightControlSystem_B.ControlSystem.Switch2
	  section.data(3).logicalSrcIdx = 10;
	  section.data(3).dtTransOffset = 2;
	
	  ;% flightControlSystem_B.ControlSystem.PitchAleteo
	  section.data(4).logicalSrcIdx = 11;
	  section.data(4).dtTransOffset = 3;
	
	  ;% flightControlSystem_B.ControlSystem.PitchExploracion
	  section.data(5).logicalSrcIdx = 12;
	  section.data(5).dtTransOffset = 4;
	
	  ;% flightControlSystem_B.ControlSystem.TiempodeFrenado
	  section.data(6).logicalSrcIdx = 13;
	  section.data(6).dtTransOffset = 5;
	
	  ;% flightControlSystem_B.ControlSystem.TiempodeCalibracion
	  section.data(7).logicalSrcIdx = 14;
	  section.data(7).dtTransOffset = 6;
	
	  ;% flightControlSystem_B.ControlSystem.Sum
	  section.data(8).logicalSrcIdx = 15;
	  section.data(8).dtTransOffset = 7;
	
	  ;% flightControlSystem_B.ControlSystem.Product2
	  section.data(9).logicalSrcIdx = 16;
	  section.data(9).dtTransOffset = 10;
	
	  ;% flightControlSystem_B.ControlSystem.Product3
	  section.data(10).logicalSrcIdx = 17;
	  section.data(10).dtTransOffset = 12;
	
      nTotData = nTotData + section.nData;
      sigMap.sections(6) = section;
      clear section
      
      section.nData     = 5;
      section.data(5)  = dumData; %prealloc
      
	  ;% flightControlSystem_B.ControlSystem.SimplyIntegrateVelocity
	  section.data(1).logicalSrcIdx = 18;
	  section.data(1).dtTransOffset = 0;
	
	  ;% flightControlSystem_B.ControlSystem.Merge
	  section.data(2).logicalSrcIdx = 21;
	  section.data(2).dtTransOffset = 2;
	
	  ;% flightControlSystem_B.ControlSystem.DataTypeConversion3
	  section.data(3).logicalSrcIdx = 22;
	  section.data(3).dtTransOffset = 4;
	
	  ;% flightControlSystem_B.ControlSystem.Switch
	  section.data(4).logicalSrcIdx = 23;
	  section.data(4).dtTransOffset = 7;
	
	  ;% flightControlSystem_B.ControlSystem.Reshapexhat
	  section.data(5).logicalSrcIdx = 24;
	  section.data(5).dtTransOffset = 10;
	
      nTotData = nTotData + section.nData;
      sigMap.sections(7) = section;
      clear section
      
      section.nData     = 19;
      section.data(19)  = dumData; %prealloc
      
	  ;% flightControlSystem_B.ControlSystem.DataTypeConversion
	  section.data(1).logicalSrcIdx = 19;
	  section.data(1).dtTransOffset = 0;
	
	  ;% flightControlSystem_B.ControlSystem.DataTypeConversion2
	  section.data(2).logicalSrcIdx = 25;
	  section.data(2).dtTransOffset = 1;
	
	  ;% flightControlSystem_B.ControlSystem.DataTypeConversion1
	  section.data(3).logicalSrcIdx = 26;
	  section.data(3).dtTransOffset = 2;
	
	  ;% flightControlSystem_B.ControlSystem.TmpSignalConversionAtToWorkspaceInport1
	  section.data(4).logicalSrcIdx = 27;
	  section.data(4).dtTransOffset = 5;
	
	  ;% flightControlSystem_B.ControlSystem.AlturadeVuelo
	  section.data(5).logicalSrcIdx = 28;
	  section.data(5).dtTransOffset = 17;
	
	  ;% flightControlSystem_B.ControlSystem.MotorDirections
	  section.data(6).logicalSrcIdx = 29;
	  section.data(6).dtTransOffset = 18;
	
	  ;% flightControlSystem_B.ControlSystem.Product
	  section.data(7).logicalSrcIdx = 30;
	  section.data(7).dtTransOffset = 22;
	
	  ;% flightControlSystem_B.ControlSystem.Product2_o
	  section.data(8).logicalSrcIdx = 31;
	  section.data(8).dtTransOffset = 24;
	
	  ;% flightControlSystem_B.ControlSystem.Product3_b
	  section.data(9).logicalSrcIdx = 32;
	  section.data(9).dtTransOffset = 26;
	
	  ;% flightControlSystem_B.ControlSystem.P_xy
	  section.data(10).logicalSrcIdx = 33;
	  section.data(10).dtTransOffset = 28;
	
	  ;% flightControlSystem_B.ControlSystem.yaw
	  section.data(11).logicalSrcIdx = 34;
	  section.data(11).dtTransOffset = 30;
	
	  ;% flightControlSystem_B.ControlSystem.pitch
	  section.data(12).logicalSrcIdx = 35;
	  section.data(12).dtTransOffset = 31;
	
	  ;% flightControlSystem_B.ControlSystem.fin
	  section.data(13).logicalSrcIdx = 36;
	  section.data(13).dtTransOffset = 32;
	
	  ;% flightControlSystem_B.ControlSystem.roll
	  section.data(14).logicalSrcIdx = 37;
	  section.data(14).dtTransOffset = 33;
	
	  ;% flightControlSystem_B.ControlSystem.poliAlpha
	  section.data(15).logicalSrcIdx = 38;
	  section.data(15).dtTransOffset = 34;
	
	  ;% flightControlSystem_B.ControlSystem.poliBeta
	  section.data(16).logicalSrcIdx = 39;
	  section.data(16).dtTransOffset = 35;
	
	  ;% flightControlSystem_B.ControlSystem.RefX
	  section.data(17).logicalSrcIdx = 40;
	  section.data(17).dtTransOffset = 36;
	
	  ;% flightControlSystem_B.ControlSystem.RefY
	  section.data(18).logicalSrcIdx = 41;
	  section.data(18).dtTransOffset = 37;
	
	  ;% flightControlSystem_B.ControlSystem.RefZ
	  section.data(19).logicalSrcIdx = 42;
	  section.data(19).dtTransOffset = 38;
	
      nTotData = nTotData + section.nData;
      sigMap.sections(8) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% flightControlSystem_B.ControlSystem.Merge_m
	  section.data(1).logicalSrcIdx = 43;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      sigMap.sections(9) = section;
      clear section
      
      section.nData     = 3;
      section.data(3)  = dumData; %prealloc
      
	  ;% flightControlSystem_B.ControlSystem.Compare
	  section.data(1).logicalSrcIdx = 44;
	  section.data(1).dtTransOffset = 0;
	
	  ;% flightControlSystem_B.ControlSystem.nicemeasurementornewupdateneeded
	  section.data(2).logicalSrcIdx = 45;
	  section.data(2).dtTransOffset = 1;
	
	  ;% flightControlSystem_B.ControlSystem.LogicalOperator3
	  section.data(3).logicalSrcIdx = 46;
	  section.data(3).dtTransOffset = 2;
	
      nTotData = nTotData + section.nData;
      sigMap.sections(10) = section;
      clear section
      
    
      ;%
      ;% Non-auto Data (signal)
      ;%
    

    ;%
    ;% Add final counts to struct.
    ;%
    sigMap.nTotData = nTotData;
    


  ;%*******************
  ;% Create DWork Map *
  ;%*******************
      
    nTotData      = 0; %add to this count as we go
    nTotSects     = 20;
    sectIdxOffset = 10;
    
    ;%
    ;% Define dummy sections & preallocate arrays
    ;%
    dumSection.nData = -1;  
    dumSection.data  = [];
    
    dumData.logicalSrcIdx = -1;
    dumData.dtTransOffset = -1;
    
    ;%
    ;% Init/prealloc dworkMap
    ;%
    dworkMap.nSections           = nTotSects;
    dworkMap.sectIdxOffset       = sectIdxOffset;
      dworkMap.sections(nTotSects) = dumSection; %prealloc
    dworkMap.nTotData            = -1;
    
    ;%
    ;% Auto data (flightControlSystem_DW)
    ;%
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% flightControlSystem_DW.obj
	  section.data(1).logicalSrcIdx = 0;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(1) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% flightControlSystem_DW.RateTransition_Buffer
	  section.data(1).logicalSrcIdx = 1;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(2) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% flightControlSystem_DW.RateTransition_ActiveBufIdx
	  section.data(1).logicalSrcIdx = 2;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(3) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% flightControlSystem_DW.objisempty
	  section.data(1).logicalSrcIdx = 3;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(4) = section;
      clear section
      
      section.nData     = 10;
      section.data(10)  = dumData; %prealloc
      
	  ;% flightControlSystem_DW.ControlSystem.Delay2_DSTATE
	  section.data(1).logicalSrcIdx = 4;
	  section.data(1).dtTransOffset = 0;
	
	  ;% flightControlSystem_DW.ControlSystem.MemoryX_DSTATE
	  section.data(2).logicalSrcIdx = 5;
	  section.data(2).dtTransOffset = 1;
	
	  ;% flightControlSystem_DW.ControlSystem.soonarFilter_IIR_states
	  section.data(3).logicalSrcIdx = 6;
	  section.data(3).dtTransOffset = 3;
	
	  ;% flightControlSystem_DW.ControlSystem.Delay_DSTATE
	  section.data(4).logicalSrcIdx = 7;
	  section.data(4).dtTransOffset = 6;
	
	  ;% flightControlSystem_DW.ControlSystem.DelayOneStep_DSTATE
	  section.data(5).logicalSrcIdx = 8;
	  section.data(5).dtTransOffset = 7;
	
	  ;% flightControlSystem_DW.ControlSystem.PastY
	  section.data(6).logicalSrcIdx = 10;
	  section.data(6).dtTransOffset = 8;
	
	  ;% flightControlSystem_DW.ControlSystem.PastVarV
	  section.data(7).logicalSrcIdx = 11;
	  section.data(7).dtTransOffset = 9;
	
	  ;% flightControlSystem_DW.ControlSystem.Stop
	  section.data(8).logicalSrcIdx = 12;
	  section.data(8).dtTransOffset = 10;
	
	  ;% flightControlSystem_DW.ControlSystem.Etapa2
	  section.data(9).logicalSrcIdx = 13;
	  section.data(9).dtTransOffset = 11;
	
	  ;% flightControlSystem_DW.ControlSystem.landSpot
	  section.data(10).logicalSrcIdx = 14;
	  section.data(10).dtTransOffset = 12;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(5) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% flightControlSystem_DW.ControlSystem.ToWorkspace_PWORK.LoggedData
	  section.data(1).logicalSrcIdx = 15;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(6) = section;
      clear section
      
      section.nData     = 13;
      section.data(13)  = dumData; %prealloc
      
	  ;% flightControlSystem_DW.ControlSystem.SimplyIntegrateVelocity_DSTATE
	  section.data(1).logicalSrcIdx = 16;
	  section.data(1).dtTransOffset = 0;
	
	  ;% flightControlSystem_DW.ControlSystem.pressureFilter_IIR_states
	  section.data(2).logicalSrcIdx = 17;
	  section.data(2).dtTransOffset = 2;
	
	  ;% flightControlSystem_DW.ControlSystem.IIR_IMUgyro_r_states
	  section.data(3).logicalSrcIdx = 18;
	  section.data(3).dtTransOffset = 5;
	
	  ;% flightControlSystem_DW.ControlSystem.FIR_IMUaccel_states
	  section.data(4).logicalSrcIdx = 19;
	  section.data(4).dtTransOffset = 10;
	
	  ;% flightControlSystem_DW.ControlSystem.MemoryX_DSTATE_h
	  section.data(5).logicalSrcIdx = 20;
	  section.data(5).dtTransOffset = 25;
	
	  ;% flightControlSystem_DW.ControlSystem.IIRgyroz_states
	  section.data(6).logicalSrcIdx = 21;
	  section.data(6).dtTransOffset = 27;
	
	  ;% flightControlSystem_DW.ControlSystem.UD_DSTATE
	  section.data(7).logicalSrcIdx = 22;
	  section.data(7).dtTransOffset = 37;
	
	  ;% flightControlSystem_DW.ControlSystem.Delay1_DSTATE
	  section.data(8).logicalSrcIdx = 24;
	  section.data(8).dtTransOffset = 39;
	
	  ;% flightControlSystem_DW.ControlSystem.DiscreteTimeIntegrator_DSTATE
	  section.data(9).logicalSrcIdx = 25;
	  section.data(9).dtTransOffset = 41;
	
	  ;% flightControlSystem_DW.ControlSystem.UD_DSTATE_d
	  section.data(10).logicalSrcIdx = 26;
	  section.data(10).dtTransOffset = 43;
	
	  ;% flightControlSystem_DW.ControlSystem.DiscreteTimeIntegrator_DSTATE_e
	  section.data(11).logicalSrcIdx = 27;
	  section.data(11).dtTransOffset = 44;
	
	  ;% flightControlSystem_DW.ControlSystem.DiscreteTimeIntegrator_DSTATE_c
	  section.data(12).logicalSrcIdx = 28;
	  section.data(12).dtTransOffset = 45;
	
	  ;% flightControlSystem_DW.ControlSystem.Delay_DSTATE_ee
	  section.data(13).logicalSrcIdx = 29;
	  section.data(13).dtTransOffset = 46;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(7) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% flightControlSystem_DW.ControlSystem.FIR_IMUaccel_circBuf
	  section.data(1).logicalSrcIdx = 30;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(8) = section;
      clear section
      
      section.nData     = 3;
      section.data(3)  = dumData; %prealloc
      
	  ;% flightControlSystem_DW.ControlSystem.Output_DSTATE
	  section.data(1).logicalSrcIdx = 31;
	  section.data(1).dtTransOffset = 0;
	
	  ;% flightControlSystem_DW.ControlSystem.Output_DSTATE_n
	  section.data(2).logicalSrcIdx = 32;
	  section.data(2).dtTransOffset = 1;
	
	  ;% flightControlSystem_DW.ControlSystem.Output_DSTATE_m
	  section.data(3).logicalSrcIdx = 33;
	  section.data(3).dtTransOffset = 2;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(9) = section;
      clear section
      
      section.nData     = 13;
      section.data(13)  = dumData; %prealloc
      
	  ;% flightControlSystem_DW.ControlSystem.Memory_PreviousInput
	  section.data(1).logicalSrcIdx = 35;
	  section.data(1).dtTransOffset = 0;
	
	  ;% flightControlSystem_DW.ControlSystem.IIRgyroz_tmp
	  section.data(2).logicalSrcIdx = 37;
	  section.data(2).dtTransOffset = 3;
	
	  ;% flightControlSystem_DW.ControlSystem.PastX
	  section.data(3).logicalSrcIdx = 38;
	  section.data(3).dtTransOffset = 5;
	
	  ;% flightControlSystem_DW.ControlSystem.PastPitch
	  section.data(4).logicalSrcIdx = 39;
	  section.data(4).dtTransOffset = 6;
	
	  ;% flightControlSystem_DW.ControlSystem.TopL
	  section.data(5).logicalSrcIdx = 40;
	  section.data(5).dtTransOffset = 7;
	
	  ;% flightControlSystem_DW.ControlSystem.TopR
	  section.data(6).logicalSrcIdx = 41;
	  section.data(6).dtTransOffset = 8;
	
	  ;% flightControlSystem_DW.ControlSystem.mTopL
	  section.data(7).logicalSrcIdx = 42;
	  section.data(7).dtTransOffset = 9;
	
	  ;% flightControlSystem_DW.ControlSystem.mTopR
	  section.data(8).logicalSrcIdx = 43;
	  section.data(8).dtTransOffset = 10;
	
	  ;% flightControlSystem_DW.ControlSystem.mStepRoll
	  section.data(9).logicalSrcIdx = 44;
	  section.data(9).dtTransOffset = 11;
	
	  ;% flightControlSystem_DW.ControlSystem.PastYaw
	  section.data(10).logicalSrcIdx = 45;
	  section.data(10).dtTransOffset = 12;
	
	  ;% flightControlSystem_DW.ControlSystem.PastRoll
	  section.data(11).logicalSrcIdx = 46;
	  section.data(11).dtTransOffset = 13;
	
	  ;% flightControlSystem_DW.ControlSystem.flag
	  section.data(12).logicalSrcIdx = 47;
	  section.data(12).dtTransOffset = 14;
	
	  ;% flightControlSystem_DW.ControlSystem.zp
	  section.data(13).logicalSrcIdx = 48;
	  section.data(13).dtTransOffset = 15;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(10) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% flightControlSystem_DW.ControlSystem.sfEvent
	  section.data(1).logicalSrcIdx = 49;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(11) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% flightControlSystem_DW.ControlSystem.PT
	  section.data(1).logicalSrcIdx = 50;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(12) = section;
      clear section
      
      section.nData     = 10;
      section.data(10)  = dumData; %prealloc
      
	  ;% flightControlSystem_DW.ControlSystem.SimplyIntegrateVelocity_PrevResetState
	  section.data(1).logicalSrcIdx = 51;
	  section.data(1).dtTransOffset = 0;
	
	  ;% flightControlSystem_DW.ControlSystem.DiscreteTimeIntegrator_PrevResetState
	  section.data(2).logicalSrcIdx = 52;
	  section.data(2).dtTransOffset = 1;
	
	  ;% flightControlSystem_DW.ControlSystem.DiscreteTimeIntegrator_PrevResetState_m
	  section.data(3).logicalSrcIdx = 53;
	  section.data(3).dtTransOffset = 2;
	
	  ;% flightControlSystem_DW.ControlSystem.EnabledSubsystem_SubsysRanBC
	  section.data(4).logicalSrcIdx = 54;
	  section.data(4).dtTransOffset = 3;
	
	  ;% flightControlSystem_DW.ControlSystem.MeasurementUpdate_SubsysRanBC
	  section.data(5).logicalSrcIdx = 55;
	  section.data(5).dtTransOffset = 4;
	
	  ;% flightControlSystem_DW.ControlSystem.EnabledSubsystem_SubsysRanBC_f
	  section.data(6).logicalSrcIdx = 56;
	  section.data(6).dtTransOffset = 5;
	
	  ;% flightControlSystem_DW.ControlSystem.MeasurementUpdate_SubsysRanBC_o
	  section.data(7).logicalSrcIdx = 57;
	  section.data(7).dtTransOffset = 6;
	
	  ;% flightControlSystem_DW.ControlSystem.IfActionSubsystem2_SubsysRanBC
	  section.data(8).logicalSrcIdx = 58;
	  section.data(8).dtTransOffset = 7;
	
	  ;% flightControlSystem_DW.ControlSystem.IfActionSubsystem_SubsysRanBC
	  section.data(9).logicalSrcIdx = 59;
	  section.data(9).dtTransOffset = 8;
	
	  ;% flightControlSystem_DW.ControlSystem.Subsystem_SubsysRanBC
	  section.data(10).logicalSrcIdx = 60;
	  section.data(10).dtTransOffset = 9;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(13) = section;
      clear section
      
      section.nData     = 4;
      section.data(4)  = dumData; %prealloc
      
	  ;% flightControlSystem_DW.ControlSystem.icLoad
	  section.data(1).logicalSrcIdx = 61;
	  section.data(1).dtTransOffset = 0;
	
	  ;% flightControlSystem_DW.ControlSystem.icLoad_g
	  section.data(2).logicalSrcIdx = 62;
	  section.data(2).dtTransOffset = 1;
	
	  ;% flightControlSystem_DW.ControlSystem.is_active_c208_flightControlSystem
	  section.data(3).logicalSrcIdx = 63;
	  section.data(3).dtTransOffset = 2;
	
	  ;% flightControlSystem_DW.ControlSystem.is_c208_flightControlSystem
	  section.data(4).logicalSrcIdx = 64;
	  section.data(4).dtTransOffset = 3;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(14) = section;
      clear section
      
      section.nData     = 4;
      section.data(4)  = dumData; %prealloc
      
	  ;% flightControlSystem_DW.ControlSystem.EnabledSubsystem_MODE
	  section.data(1).logicalSrcIdx = 65;
	  section.data(1).dtTransOffset = 0;
	
	  ;% flightControlSystem_DW.ControlSystem.MeasurementUpdate_MODE
	  section.data(2).logicalSrcIdx = 66;
	  section.data(2).dtTransOffset = 1;
	
	  ;% flightControlSystem_DW.ControlSystem.EnabledSubsystem_MODE_m
	  section.data(3).logicalSrcIdx = 67;
	  section.data(3).dtTransOffset = 2;
	
	  ;% flightControlSystem_DW.ControlSystem.MeasurementUpdate_MODE_p
	  section.data(4).logicalSrcIdx = 68;
	  section.data(4).dtTransOffset = 3;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(15) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% flightControlSystem_DW.ControlSystem.Normalcondition.Geofencingerror_SubsysRanBC
	  section.data(1).logicalSrcIdx = 69;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(16) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% flightControlSystem_DW.ControlSystem.Ultrasoundimproper.Geofencingerror_SubsysRanBC
	  section.data(1).logicalSrcIdx = 70;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(17) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% flightControlSystem_DW.ControlSystem.Noopticalflow.Geofencingerror_SubsysRanBC
	  section.data(1).logicalSrcIdx = 71;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(18) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% flightControlSystem_DW.ControlSystem.estimatorOpticalflowerror.Geofencingerror_SubsysRanBC
	  section.data(1).logicalSrcIdx = 72;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(19) = section;
      clear section
      
      section.nData     = 1;
      section.data(1)  = dumData; %prealloc
      
	  ;% flightControlSystem_DW.ControlSystem.Geofencingerror.Geofencingerror_SubsysRanBC
	  section.data(1).logicalSrcIdx = 73;
	  section.data(1).dtTransOffset = 0;
	
      nTotData = nTotData + section.nData;
      dworkMap.sections(20) = section;
      clear section
      
    
      ;%
      ;% Non-auto Data (dwork)
      ;%
    

    ;%
    ;% Add final counts to struct.
    ;%
    dworkMap.nTotData = nTotData;
    


  ;%
  ;% Add individual maps to base struct.
  ;%

  targMap.paramMap  = paramMap;    
  targMap.signalMap = sigMap;
  targMap.dworkMap  = dworkMap;
  
  ;%
  ;% Add checksums to base struct.
  ;%


  targMap.checksum0 = 584976538;
  targMap.checksum1 = 3365270652;
  targMap.checksum2 = 655906373;
  targMap.checksum3 = 3098865102;

